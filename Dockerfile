FROM openjdk:17-alpine
ADD target/fergana.jar fergana.jar
ENTRYPOINT ["java","-jar","fergana.jar"]
