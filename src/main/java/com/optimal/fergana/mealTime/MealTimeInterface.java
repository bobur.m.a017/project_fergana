package com.optimal.fergana.mealTime;

public interface MealTimeInterface {
    default MealTimeResDto parse(MealTime mealTime) {
        return new MealTimeResDto(mealTime.getId(), mealTime.getName(), mealTime.isDelete());
    }
}
