package com.optimal.fergana.mealTime;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.stayTime.StayTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private boolean delete = false;
    private Integer sortNumber = 10;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToMany
    private List<AgeGroup> ageGroupList;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private RegionalDepartment regionalDepartment;

    @ManyToMany
    private List<StayTime> stayTime;


    public MealTime(String name, boolean delete, List<AgeGroup> ageGroupList) {
        this.name = name;
        this.delete = delete;
        this.ageGroupList = ageGroupList;
    }

    public MealTime(String name, boolean delete, List<AgeGroup> ageGroupList, RegionalDepartment regionalDepartment) {
        this.name = name;
        this.delete = delete;
        this.ageGroupList = ageGroupList;
        this.regionalDepartment = regionalDepartment;
    }
}
