package com.optimal.fergana.measurementType;



public enum MeasurementType {
    KG(1, "kilogram"),
    PIECE(2, "dona");

    private Integer id;
    private String name;

    MeasurementType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
