package com.optimal.fergana.accountant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersRepo;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Accountant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(mappedBy = "accountant", fetch = FetchType.LAZY)
    private Set<Kindergarten> kindergartensOfAccountant;
    @JsonIgnore
    @OneToOne(mappedBy = "accountant")
    private Users user;


    public Accountant(Set<Kindergarten> kindergartensOfAccountant, Users user) {
        this.kindergartensOfAccountant = kindergartensOfAccountant;
        this.user = user;
    }
}
