package com.optimal.fergana.accountant;

import com.optimal.fergana.kindergarten.Kindergarten;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@AllArgsConstructor
@Setter
public class AccountantResDTO {
    private Integer userId;
    private String name;
    private String fatherName;
    private String surname;
    private String phoneNumber;
    private Set<Kindergarten> kindergartens;
}
