package com.optimal.fergana.accountant;

import com.optimal.fergana.department.Department;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownServiceException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class AccountantService {
    private final UsersRepo usersRepo;
    private final KindergartenRepo kindergartenRepo;
    private final AccountantRepo accountantRepo;
    private final UserService userService;


    public StateMessage add(Integer userId, List<Integer> kindergartenIdList) throws UnknownServiceException {
        Optional<Users> optionalUser = usersRepo.findById(userId);
        List<Kindergarten> kindergartenList = kindergartenRepo.findAllById(kindergartenIdList);

        Set<Kindergarten> kindergartenSet = new HashSet<>();

        if (optionalUser.isPresent()) {
            Users user = optionalUser.get();
            Accountant accountant;
            for (Kindergarten kindergarten : kindergartenList) {
                if (kindergarten.getAccountant() != null) {
                    kindergartenSet.add(kindergarten);
                } else {
                    throw new UsernameNotFoundException("bog'chalar boshqa buxgalterga biriktirlgan");
                }
            }
            if (kindergartenSet.size() == kindergartenList.size()) {
                if (user.getAccountant() != null) {
                    accountant = user.getAccountant();
                    accountant.setKindergartensOfAccountant(kindergartenSet);
                    accountant.setUser(user);
                } else {
                    accountant = new Accountant(kindergartenSet, user);
                }
                accountantRepo.save(accountant);
            }
        }
        return new StateMessage("muvaffaqiyatli biriktirildi", true, 200);
    }

    public List<AccountantResDTO> getAll(HttpServletRequest request) {
        Users user = userService.parseToken(request);
        Department department = user.getDepartment();
        List<Accountant> accountantList = accountantRepo.findAllByUser_Department_Id(department.getId());
        ArrayList<AccountantResDTO> arrayList=new ArrayList<>();
        for (Accountant accountant : accountantList) {
            AccountantResDTO accountantResDTO=new AccountantResDTO(
                    accountant.getId(),
                    accountant.getUser().getName(),
                    accountant.getUser().getName(),
                    accountant.getUser().getSurname(),
                    accountant.getUser().getPhoneNumber(),
                    accountant.getKindergartensOfAccountant()
            );
            arrayList.add(accountantResDTO);
        }
        return arrayList;
    }
    public Set<Kindergarten> getOne(Integer userId) {

        Optional<Accountant> optionalAccountant = accountantRepo.findById(userId);
        if (optionalAccountant.isPresent()) {
            Accountant accountant = optionalAccountant.get();
            return accountant.getKindergartensOfAccountant();
        }
        throw new UsernameNotFoundException("bunday buxgalter mavjud emas");
    }
}
