package com.optimal.fergana.accountant;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AccountantDTO {
    private Integer userId;
    private List<Integer> kindergartenIds;
}
