package com.optimal.fergana.department;

import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartmentRepo extends JpaRepository<Department,Integer> {

    boolean existsAllByRegionalDepartmentAndName(RegionalDepartment regionalDepartment, String name);
    List<Department> findAllByRegionalDepartment(RegionalDepartment regionalDepartment);
}
