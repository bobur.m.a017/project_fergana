package com.optimal.fergana.department;

import com.optimal.fergana.address.district.District;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.users.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    private LocalTime localTime;


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne
    private District district;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private RegionalDepartment regionalDepartment;


    @OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
    private List<Kindergarten> kindergartenList;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
    private List<Users> usersList;


    @ManyToMany(mappedBy = "departments")
    private List<Supplier> suppliers;

    public Department(String name, District district, RegionalDepartment regionalDepartment) {
        this.name = name;
        this.district = district;
        this.regionalDepartment = regionalDepartment;
    }
}
