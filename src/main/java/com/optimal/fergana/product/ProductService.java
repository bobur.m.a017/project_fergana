package com.optimal.fergana.product;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.product.productPack.ProductPack;
import com.optimal.fergana.product.productPack.ProductPackRepo;
import com.optimal.fergana.product.product_category.ProductCategory;
import com.optimal.fergana.product.product_category.ProductCategoryRepository;
import com.optimal.fergana.product.product_price.ProductPrice;
import com.optimal.fergana.product.product_price.ProductPriceRepo;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategory;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategoryRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService implements ProductParseDTO, ProductServiceInter {
    private final ProductRepo productRepo;
    private final SanpinCategoryRepo sanpinCategoryRepo;
    private final ProductCategoryRepository productCategoryRepository;
    private final ProductPriceRepo productPriceRepo;
    private final CustomConverter converter;
    private final ProductPackRepo productPackRepo;


    public StateMessage add(ProductDTO productDTO, Users users) {

        RegionalDepartment regionalDepartment = users.getRegionalDepartment();

        boolean res = productRepo.existsByNameAndRegionalDepartment(productDTO.getName(), regionalDepartment);
        Optional<SanpinCategory> optionalSanpinCategory = sanpinCategoryRepo.findById(productDTO.getSanpinCategoryId());
        Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(productDTO.getProductCategoryId());

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (!res && optionalSanpinCategory.isPresent() && optionalProductCategory.isPresent()) {

            productRepo.save(new Product(
                    productDTO.getName(),
                    productDTO.getMeasurementType(),
                    BigDecimal.valueOf(productDTO.getProtein()),
                    BigDecimal.valueOf(productDTO.getKcal()),
                    BigDecimal.valueOf(productDTO.getOil()),
                    BigDecimal.valueOf(productDTO.getCarbohydrates()),
                    optionalProductCategory.get(),
                    optionalSanpinCategory.get(),
                    regionalDepartment,
                    false

            ));

            message = Message.SUCCESS_UZ;
        } else {
            Optional<Product> optionalProduct = productRepo.findAllByRegionalDepartmentAndNameAndDelete(regionalDepartment, productDTO.getName(), true);
            if (optionalProduct.isPresent()) {
                Product product = optionalProduct.get();
                return edit(productDTO, product.getId());
            }
        }
        return new StateMessage().parse(message);
    }

    public StateMessage edit(ProductDTO productDTO, Integer id) {

        Optional<Product> optionalProduct = productRepo.findById(id);
        Optional<SanpinCategory> optionalSanpinCategory = sanpinCategoryRepo.findById(productDTO.getSanpinCategoryId());
        Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(productDTO.getProductCategoryId());
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        boolean res = false;

        if (optionalProduct.isPresent() && optionalProductCategory.isPresent() && optionalSanpinCategory.isPresent()) {

            Product product = optionalProduct.get();
            if (!product.getName().equals(productDTO.getName())) {
                res = productRepo.existsByName(productDTO.getName());
            }
            if (!res) {
                product.setName(productDTO.getName());
                product.setMeasurementType(productDTO.getMeasurementType());

                product.setCarbohydrates(BigDecimal.valueOf(productDTO.getCarbohydrates()));
                product.setKcal(BigDecimal.valueOf(productDTO.getKcal()));
                product.setProtein(BigDecimal.valueOf(productDTO.getProtein()));
                product.setOil(BigDecimal.valueOf(productDTO.getOil()));
                product.setSanpinCategory(optionalSanpinCategory.get());
                product.setProductCategory(optionalProductCategory.get());
                product.setDelete(false);
                productRepo.save(product);

                message = Message.EDIT_UZ;
            }
        }
        return new StateMessage().parse(message);
    }

    public List<ProductResDTO> getAll(Users users) {

        List<ProductResDTO> list = new ArrayList<>();
        List<Product> productList = productRepo.findAllByRegionalDepartmentAndDeleteIsFalse(users.getRegionalDepartment());

        for (Product product : productList) {

            ProductResDTO parse = parse(product);
            List<ProductPrice> priceCheckList = productPriceRepo.getProductPriceCheck(product.getId(), users.getRegionalDepartment().getId(), LocalDate.now(), false);
            if (priceCheckList.size() > 0) {
                parse.setPrice(converter.productPriceToDTO(priceCheckList.get(0)));
            }

            list.add(parse);
        }

        if (users.getDepartment() != null) {
            Department department = users.getDepartment();
            for (ProductResDTO productResDTO : list) {
                Optional<ProductPack> optional = productPackRepo.findByProduct_IdAndDepartment_Id(productResDTO.getId(), department.getId());
                if (optional.isPresent()) {
                    productResDTO.setPack(optional.get().getPack());
                } else {
                    productResDTO.setPack(BigDecimal.valueOf(0));
                }
            }
        }

        list.sort(Comparator.comparing(ProductResDTO::getName));
        return list;
    }

    public ResponseEntity<?> getOne(Integer id) {
        Optional<Product> optionalProduct = productRepo.findById(id);
        if (optionalProduct.isPresent()) {
            return ResponseEntity.status(200).body(parse(optionalProduct.get()));
        }
        StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
        return ResponseEntity.status(message.getCode()).body(message);
    }

    public ResponseEntity<?> delete(Integer id) {

        Optional<Product> optionalProduct = productRepo.findById(id);
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            product.setDelete(true);
            productRepo.save(product);
            message = Message.DELETE_UZ;
        }
        return ResponseEntity.status(message.getCode()).body(new StateMessage().parse(message));
    }

}
