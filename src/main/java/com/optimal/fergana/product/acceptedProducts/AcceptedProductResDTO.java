package com.optimal.fergana.product.acceptedProducts;

import com.optimal.fergana.supplier.contract.product.ProductContractResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AcceptedProductResDTO {

    private UUID id;
    private BigDecimal weight;
    private boolean delete = false;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String recipient;
    private Date date;
    private BigDecimal packWeight;
    private BigDecimal pack;
    private BigDecimal price;
    private String yetkazibBeruvchi;
    private String shartnomaRaqami;
    private String producyName;
}
