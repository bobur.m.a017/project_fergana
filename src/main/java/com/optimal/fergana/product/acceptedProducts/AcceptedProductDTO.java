package com.optimal.fergana.product.acceptedProducts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AcceptedProductDTO {

    private UUID id;

    @NotNull
    private Double receivedWeight;

    @NotNull
    private Double pack;

    @NotNull
    private Double packWeight;

    @NotNull
    private Date date;
}
