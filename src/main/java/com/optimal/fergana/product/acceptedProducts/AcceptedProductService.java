package com.optimal.fergana.product.acceptedProducts;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.supplier.contract.product.ProductContract;
import com.optimal.fergana.supplier.contract.product.ProductContractRepo;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.WarehouseService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AcceptedProductService {

    private final AcceptedProductRepo acceptedProductRepo;
    private final ProductContractRepo productContractRepo;
    private final WarehouseService warehouseService;
    private final CustomConverter converter;
    private final ProductPackService productPackService;


    public StateMessage add(UUID productContractId, AcceptedProductDTO dto, Users users) {

        Optional<ProductContract> optionalProductContract = productContractRepo.findById(productContractId);
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if ((optionalProductContract.isPresent())) {
            ProductContract productContract = optionalProductContract.get();
            BigDecimal checkPack = productPackService.checkPack(productContract.getProduct().getId(), users.getDepartment().getId());

            BigDecimal weight = BigDecimal.valueOf(dto.getPackWeight()).setScale(3, RoundingMode.HALF_UP);
            BigDecimal weightPack = BigDecimal.valueOf(dto.getPackWeight()).setScale(3, RoundingMode.HALF_UP);
            BigDecimal pack = BigDecimal.ZERO;

            if (checkPack != null) {
                BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.ONE);
                if (bigDecimals[1].compareTo(BigDecimal.ZERO) > 0) {
                    return new StateMessage("Maxsulot miqdorini donada kiriting", false, 417);
                }
                pack = checkPack;
                weight = weightPack.multiply(pack).divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP);
            }


            if (((productContract.getPackWeight().subtract(productContract.getSuccessPackWeight())).compareTo(weightPack) < 0)) {
                message = Message.THE_AMOUNT_ENTERED_IS_TOO_HIGH;
            } else {

                AcceptedProduct save = acceptedProductRepo.save(new AcceptedProduct(weight, weightPack, pack, productContract, users, dto.getDate(), productContract.getProduct(), productContract.getPrice(), users.getKindergarten()));

                productContract.setSuccessWeight(productContract.getSuccessWeight().add(weight));
                productContract.setSuccessPackWeight(productContract.getSuccessPackWeight().add(weightPack));

                warehouseService.add(save.getProductContract().getProduct(), weight, save.getProductContract().getKindergartenContract().getKindergarten(), productContract.getPrice(), productContract.getPack(), weightPack);

//                inputOutputService.addInput(save.getProductContract().getProduct(), save.getProductContract().getKindergartenContract().getKindergarten(), LocalDate.now(), weight,packWeight,productContract.getPack());

                if (productContract.getWeight().compareTo(productContract.getSuccessWeight()) == 0) {
                    productContract.setSuccessState(false);
                }

                productContractRepo.save(productContract);
                message = Message.SUCCESS_UZ;
            }
        }
        return new StateMessage().parse(message);
    }

    public boolean checkPack(ProductContract productContract, AcceptedProductDTO dto) {
        if (productContract.getPack().compareTo(BigDecimal.valueOf(0)) > 0) {
            return !((dto.getPackWeight() % 1) > 0);
        }
        return true;
    }

//    public StateMessage edit(UUID id, AcceptedProductDTO dto, Users users) {
//
//        Optional<AcceptedProduct> optionalProductContract = acceptedProductRepo.findById(id);
//        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
//
//        if (optionalProductContract.isPresent()) {
//            AcceptedProduct acceptedProduct = optionalProductContract.get();
//            if (Objects.equals(acceptedProduct.getCreateDate().toLocalDateTime().toLocalDate(), LocalDate.now())) {
//
//                BigDecimal subtract = acceptedProduct.getWeight().subtract(BigDecimal.valueOf(dto.getReceivedWeight()));
//
//
//
//
//                ProductContract productContract = acceptedProduct.getProductContract();
//
//                if (subtract.compareTo(BigDecimal.valueOf(0)) > 0) {
//                    Optional<Warehouse> warehouseOptional = warehouseRepo.findByKindergarten_IdAndProduct_IdAndDeleteIsFalse(acceptedProduct.getProductContract().getKindergartenContract().getKindergarten().getId(), acceptedProduct.getProductContract().getProduct().getId());
//                    if (warehouseOptional.isPresent()) {
//                        Warehouse warehouse = warehouseOptional.get();
//                        if (warehouse.getTotalWeight().compareTo(subtract) > 0) {
//
//                            inputOutputService.addInput(acceptedProduct.getProductContract().getProduct(), acceptedProduct.getProductContract().getKindergartenContract().getKindergarten(), LocalDate.now(), acceptedProduct.getWeight().multiply(BigDecimal.valueOf(-1)));
//
//                            warehouseService.add(acceptedProduct.getProductContract().getProduct(), subtract, acceptedProduct.getProductContract().getKindergartenContract().getKindergarten(),productContract.getPrice()));
//                            productContract.setSuccessWeight(productContract.getSuccessWeight().add(acceptedProduct.getWeight().subtract(BigDecimal.valueOf(dto.getReceivedWeight()))));
//                            productContractRepo.save(productContract);
//                            acceptedProduct.setWeight(BigDecimal.valueOf(dto.getReceivedWeight()));
//                            acceptedProduct.setRecipient(users);
//                            AcceptedProduct save = acceptedProductRepo.save(acceptedProduct);
//
//                            inputOutputService.addInput(save.getProductContract().getProduct(), save.getProductContract().getKindergartenContract().getKindergarten(), LocalDate.now(), save.getWeight());
//
//                            message = Message.EDIT_UZ;
//
//                        }
//                    }
//                } else {
//
//                    inputOutputService.addInput(acceptedProduct.getProductContract().getProduct(), acceptedProduct.getProductContract().getKindergartenContract().getKindergarten(), LocalDate.now(), acceptedProduct.getWeight().multiply(BigDecimal.valueOf(-1)));
//
//
//                    productContract.setSuccessWeight(productContract.getSuccessWeight().add(acceptedProduct.getWeight().subtract(BigDecimal.valueOf(dto.getReceivedWeight()))));
//                    productContractRepo.save(productContract);
//
//                    acceptedProduct.setWeight(BigDecimal.valueOf(dto.getReceivedWeight()));
//                    acceptedProduct.setRecipient(users);
//                    AcceptedProduct save = acceptedProductRepo.save(acceptedProduct);
//
//                    inputOutputService.addInput(save.getProductContract().getProduct(), save.getProductContract().getKindergartenContract().getKindergarten(), LocalDate.now(), save.getWeight());
//
//                    message = Message.EDIT_UZ;
//
//                    warehouseService.add(acceptedProduct.getProductContract().getProduct(), subtract, acceptedProduct.getProductContract().getKindergartenContract().getKindergarten(),productContract.getPrice());
//                }
//            }
//        }
//        return new StateMessage().parse(message);
//    }

//    public StateMessage delete(UUID id, Users users) {
//
//        Optional<AcceptedProduct> optionalProductContract = acceptedProductRepo.findById(id);
//        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
//
//        if (optionalProductContract.isPresent()) {
//            AcceptedProduct acceptedProduct = optionalProductContract.get();
//            if (Objects.equals(acceptedProduct.getCreateDate().toLocalDateTime().toLocalDate(), LocalDate.now())) {
//
//                ProductContract productContract = acceptedProduct.getProductContract();
//                productContract.setSuccessWeight(productContract.getSuccessWeight().subtract(acceptedProduct.getWeight()));
//                productContractRepo.save(productContract);
//
//                acceptedProduct.setDelete(true);
//                acceptedProduct.setRecipient(users);
//                acceptedProductRepo.save(acceptedProduct);
//
//                inputOutputService.addInput(acceptedProduct.getProductContract().getProduct(), acceptedProduct.getProductContract().getKindergartenContract().getKindergarten(), LocalDate.now(), acceptedProduct.getWeight().multiply(BigDecimal.valueOf(-1)));
//
//                message = Message.DELETE_UZ;
//            }
//        }
//        return new StateMessage().parse(message);
//    }

    public CustomPageable getAll(Integer page, Integer pageSize, Users user) {

        Page<AcceptedProduct> productPage;

        if (pageSize == null)
            pageSize = 10;
        if (page == null)
            page = 0;

        Pageable pageable = PageRequest.of(page, pageSize);

        if (user.getKindergarten() != null)
            productPage = acceptedProductRepo.findAllByDeleteIsFalseAndKindergarten_id(user.getKindergarten().getId(), pageable);
        else if (user.getDepartment() != null)
            productPage = acceptedProductRepo.findAllByDeleteIsFalseAndKindergarten_Department_Id(user.getDepartment().getId(), pageable);
        else
            productPage = acceptedProductRepo.findAllByDeleteIsFalseAndKindergarten_RegionalDepartment_Id(user.getRegionalDepartment().getId(), pageable);

        List<AcceptedProductResDTO> list = converter.acceptedProductToDTO(productPage.stream().toList());

        return new CustomPageable(productPage.getSize(), pageable.getPageNumber(), list, productPage.getTotalElements());
    }
}
