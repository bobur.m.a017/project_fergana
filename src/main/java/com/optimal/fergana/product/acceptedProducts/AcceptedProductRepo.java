package com.optimal.fergana.product.acceptedProducts;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AcceptedProductRepo extends JpaRepository<AcceptedProduct, UUID> {
    Page<AcceptedProduct> findAllByDeleteIsFalseAndKindergarten_id(Integer id, Pageable pageable);
    Page<AcceptedProduct> findAllByDeleteIsFalseAndKindergarten_Department_Id(Integer id, Pageable pageable);
    Page<AcceptedProduct> findAllByDeleteIsFalseAndKindergarten_RegionalDepartment_Id(Integer id, Pageable pageable);
}
