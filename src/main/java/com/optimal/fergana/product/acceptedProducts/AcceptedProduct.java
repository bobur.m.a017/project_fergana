package com.optimal.fergana.product.acceptedProducts;

import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.supplier.contract.product.ProductContract;
import com.optimal.fergana.users.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AcceptedProduct {

    @Id
    private UUID id = UUID.randomUUID();

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal packWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal pack;

    @Column(precision = 19, scale = 2)
    private BigDecimal price;


    private boolean delete = false;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    private Users recipient;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductContract productContract;

    @ManyToOne
    private Product product;


    @ManyToOne
    private Kindergarten kindergarten;



    public AcceptedProduct(BigDecimal weight, Users recipient, ProductContract productContract) {
        this.weight = weight;
        this.recipient = recipient;
        this.productContract = productContract;
    }


    public AcceptedProduct(BigDecimal weight, BigDecimal packWeight, BigDecimal pack, ProductContract productContract, Users recipient, Date date, Product product, BigDecimal price, Kindergarten kindergarten) {
        this.weight = weight;
        this.packWeight = packWeight;
        this.pack = pack;
        this.productContract = productContract;
        this.recipient = recipient;
        this.date = date;
        this.product = product;
        this.price = price;
        this.kindergarten = kindergarten;
    }
}
