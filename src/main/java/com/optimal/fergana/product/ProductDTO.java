package com.optimal.fergana.product;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.math.BigDecimal;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {
    @NotNull
    private String name;
    @NotNull
    private String measurementType;
    @NotNull
    private Double protein;
    @NotNull
    private Double kcal;
    @NotNull
    private Double oil;
    @NotNull
    private Double carbohydrates;
    @NotNull
    private Integer productCategoryId;
    @NotNull
    private Integer sanpinCategoryId;
}
