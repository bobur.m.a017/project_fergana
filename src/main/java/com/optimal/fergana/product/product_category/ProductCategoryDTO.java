package com.optimal.fergana.product.product_category;


import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
public class ProductCategoryDTO {
    @NotNull
    private Integer id;
    @NotNull
    private String name;

    public ProductCategoryDTO() {
    }

    public ProductCategoryDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
