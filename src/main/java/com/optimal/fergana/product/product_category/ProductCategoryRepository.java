package com.optimal.fergana.product.product_category;

import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer> {

    boolean existsByNameAndRegionalDepartment(String name, RegionalDepartment regionalDepartment);

    List<ProductCategory> findAllByRegionalDepartment(RegionalDepartment regionalDepartment);
}
