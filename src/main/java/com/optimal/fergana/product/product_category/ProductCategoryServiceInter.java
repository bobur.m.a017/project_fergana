package com.optimal.fergana.product.product_category;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;

import java.util.List;

public interface ProductCategoryServiceInter {
    StateMessage add(ProductCategoryDTO productCategoryDTO, Users users);
    StateMessage edit(ProductCategoryDTO productCategoryDTO, Integer id);
    StateMessage delete(Integer id);
    List<ProductCategoryDTO> getAll(Users users);
}
