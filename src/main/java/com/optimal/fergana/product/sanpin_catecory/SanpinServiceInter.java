package com.optimal.fergana.product.sanpin_catecory;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;

public interface SanpinServiceInter {
    StateMessage add(SanpinCategoryDTO sanpinCategoryDTO, Users users);
    StateMessage edit(SanpinCategoryDTO sanpinCategoryDTO, Integer id);
    StateMessage delete(Integer id);
    ResponseEntity<?> getAll(Users users);
    ResponseEntity<?> getOne(Integer id);
}
