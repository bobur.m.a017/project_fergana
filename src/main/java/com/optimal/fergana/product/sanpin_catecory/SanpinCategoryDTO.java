package com.optimal.fergana.product.sanpin_catecory;


import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNormDTO;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SanpinCategoryDTO {
    @NotNull
    private String name;
    @NotNull
    private List<DailyNormDTO> dailyNormDTOList;

}
