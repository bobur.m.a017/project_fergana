package com.optimal.fergana.product.sanpin_catecory;

import com.optimal.fergana.product.ProductResDTO;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNormResDTO;

import java.util.List;


public class SanpinCategoryResDTO {
    private Integer id;
    private String name;
    private List<ProductResDTO> product;
    private List<DailyNormResDTO> dailyNormDTOList;

    public SanpinCategoryResDTO(String name) {
        this.name = name;
    }


    public SanpinCategoryResDTO() {
    }

    public SanpinCategoryResDTO(Integer id, String name, List<ProductResDTO> product, List<DailyNormResDTO> dailyNormDTOList) {
        this.id = id;
        this.name = name;
        this.product = product;
        this.dailyNormDTOList = dailyNormDTOList;
    }

    public SanpinCategoryResDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public List<ProductResDTO> getProduct() {
        return product;
    }

    public void setProduct(List<ProductResDTO> product) {
        this.product = product;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DailyNormResDTO> getDailyNormDTOList() {
        return dailyNormDTOList;
    }

    public void setDailyNormDTOList(List<DailyNormResDTO> dailyNormDTOList) {
        this.dailyNormDTOList = dailyNormDTOList;
    }

}
