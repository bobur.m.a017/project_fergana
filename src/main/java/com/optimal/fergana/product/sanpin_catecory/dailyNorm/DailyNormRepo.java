package com.optimal.fergana.product.sanpin_catecory.dailyNorm;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DailyNormRepo extends JpaRepository<DailyNorm,Integer> {

    Optional<DailyNorm> findByAgeGroup_IdAndSanpinCategory_Id(Integer ageGroup_id, Integer sanpinCategory_id);
}
