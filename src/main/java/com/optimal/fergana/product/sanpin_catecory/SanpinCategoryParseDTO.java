package com.optimal.fergana.product.sanpin_catecory;


public interface SanpinCategoryParseDTO {


    default SanpinCategoryResDTO parser(SanpinCategory sanpinCategory) {

        return new SanpinCategoryResDTO(
                sanpinCategory.getId(),
                sanpinCategory.getName()
                );
    }
}
