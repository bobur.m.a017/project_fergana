package com.optimal.fergana.product;

import com.optimal.fergana.product.product_price.ProductPriceResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductResDTO {
    private Integer id;
    private String name;
    private String measurementType;
    private BigDecimal protein;
    private BigDecimal kcal;
    private BigDecimal oil;
    private BigDecimal carbohydrates;
    private String productCategoryName;
    private Integer productCategoryId;
    private String sanpinCategoryName;
    private Integer sanpinCategoryId;
    private String regionalDepartmentName;
    private Integer regionalDepartmentId;
    private ProductPriceResDTO price;
    private BigDecimal pack;

    public ProductResDTO(Integer id, String name, String measurementType, BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates, String productCategoryName, Integer productCategoryId, String sanpinCategoryName, Integer sanpinCategoryId, String regionalDepartmentName, Integer regionalDepartmentId) {
        this.id = id;
        this.name = name;
        this.measurementType = measurementType;
        this.protein = protein;
        this.kcal = kcal;
        this.oil = oil;
        this.carbohydrates = carbohydrates;
        this.productCategoryName = productCategoryName;
        this.productCategoryId = productCategoryId;
        this.sanpinCategoryName = sanpinCategoryName;
        this.sanpinCategoryId = sanpinCategoryId;
        this.regionalDepartmentName = regionalDepartmentName;
        this.regionalDepartmentId = regionalDepartmentId;
    }
}
