package com.optimal.fergana.product.productPack;

import com.optimal.fergana.department.Department;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ProductPackService {

    private final ProductPackRepo productPackRepo;
    private final ProductRepo productRepo;

    public StateMessage add(Users users, ProductPackDTO dto, Integer productId) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (users.getDepartment() != null && dto.getPack() > 1 && dto.getPack() < 5001) {

            Department department = users.getDepartment();
            Optional<ProductPack> optionalProductPack = productPackRepo.findByProduct_IdAndDepartment_Id(productId, department.getId());
            Optional<Product> optionalProduct = productRepo.findById(productId);

            if (optionalProduct.isPresent()) {
                Product product = optionalProduct.get();
                if (optionalProductPack.isEmpty()) {
                    productPackRepo.save(new ProductPack(product, department, BigDecimal.valueOf(dto.getPack())));
                    message = Message.SUCCESS_UZ;
                } else {
                    ProductPack productPack = optionalProductPack.get();
                    if (Objects.equals(product.getId(), productPack.getProduct().getId())) {
                        productPack.setPack(BigDecimal.valueOf(dto.getPack()));
                        productPackRepo.save(productPack);
                        message = Message.EDIT_UZ;
                    }
                }
            }
        }
        return new StateMessage().parse(message);
    }

    public StateMessage delete(UUID id) {

        try {
            productPackRepo.deleteById(id);
            return new StateMessage().parse(Message.DELETE_UZ);
        } catch (Exception e) {
            e.printStackTrace();
            return new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
        }
    }

    public CustomPageable getAll(Users users, Integer page, Integer pageSize) {

        if (page == null)
            page = 0;
        if (pageSize == null)
            pageSize = 10;


        if (users.getDepartment() != null) {
            Department department = users.getDepartment();

            Page<ProductPack> productPackPage = productPackRepo.findAllByDepartment_IdOrderByProduct_Name(department.getId(), PageRequest.of(page, pageSize));

            return new CustomPageable(productPackPage.getSize(), productPackPage.getNumber(), toDTO(productPackPage.stream().toList()), productPackPage.getTotalElements());
        }
        return new CustomPageable();
    }

    public List<ProductPackResDTO> toDTO(List<ProductPack> productPackList) {

        List<ProductPackResDTO> dtoList = new ArrayList<>();

        for (ProductPack productPack : productPackList) {
            dtoList.add(new ProductPackResDTO(productPack.getId(), productPack.getCreateDate(), productPack.getUpdateDate(), productPack.getProduct().getId(), productPack.getProduct().getName(), productPack.getPack()));
        }

        return dtoList;
    }

    public BigDecimal checkPack(Integer productId, Integer departmentId) {
        Optional<ProductPack> optionalProductPack = productPackRepo.findByProduct_IdAndDepartment_Id(productId, departmentId);

        if (optionalProductPack.isPresent()) {
            ProductPack productPack = optionalProductPack.get();
            return productPack.getPack();
        }

        return null;
    }
}
