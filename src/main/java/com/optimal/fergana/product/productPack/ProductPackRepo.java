package com.optimal.fergana.product.productPack;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ProductPackRepo extends JpaRepository<ProductPack, UUID> {

    Optional<ProductPack> findByProduct_IdAndDepartment_Id(Integer product_id, Integer department_id);

    Page<ProductPack> findAllByDepartment_IdOrderByProduct_Name(Integer departmentId, Pageable pageable);
}
