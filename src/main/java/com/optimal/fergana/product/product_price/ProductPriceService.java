package com.optimal.fergana.product.product_price;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.exception.ProductNotFoundException;
import com.optimal.fergana.exception.ProductPriceNotFoundException;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductPriceService {
    private final ProductPriceRepo productPriceRepo;
    private final UserService userService;
    private final ProductRepo productRepo;
    private final CustomConverter converter;

    public StateMessage add(HttpServletRequest request, ProductPriceDTO dto) throws ProductNotFoundException {
        Users user = userService.parseToken(request);
        Optional<Product> optionalProduct = productRepo.findById(dto.getProductId());

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;


        if (dto.getEndDay().compareTo(dto.getStartDay()) >= 0 && dto.getMaxPrice() >= dto.getMinPrice() && dto.getMinPrice() > 0 && dto.getMaxPrice() > 0 && (dto.getEndDay().compareTo(new Date(System.currentTimeMillis())) > 0)) {
            if (optionalProduct.isPresent()) {
                Product product = optionalProduct.get();

                List<ProductPrice> productPriceList = productPriceRepo.getProductPrice(product.getId(), user.getRegionalDepartment().getId(), new Timestamp(dto.getStartDay().getTime()).toLocalDateTime().toLocalDate(), new Timestamp(dto.getEndDay().getTime()).toLocalDateTime().toLocalDate(), false);

                if (productPriceList.size() == 0) {
                    ProductPrice productPrice = new ProductPrice(
                            product,
                            BigDecimal.valueOf(dto.getMinPrice()),
                            BigDecimal.valueOf(dto.getMaxPrice()),
                            user.getRegionalDepartment(),
                            new Timestamp(dto.getStartDay().getTime()).toLocalDateTime().toLocalDate(),
                            new Timestamp(dto.getEndDay().getTime()).toLocalDateTime().toLocalDate()
                    );
                    productPriceRepo.save(productPrice);
                    message = Message.SUCCESS_UZ;

                    checkEdit(product, user.getRegionalDepartment());
                }else {
                    message.setName("Sanalar ketma ketligi noto`g`ri kiritildi.");
                }
            }
        }

        return new StateMessage().parse(message);
    }

    public StateMessage edit(ProductPriceDTO dto, Integer productPriceId) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Optional<ProductPrice> optionalProductPrice = productPriceRepo.findById(productPriceId);

        if (dto.getEndDay().compareTo(dto.getStartDay()) >= 0 && dto.getMaxPrice() >= dto.getMinPrice() && dto.getMinPrice() > 0 && dto.getMaxPrice() > 0 && (dto.getEndDay().compareTo(new Date(System.currentTimeMillis())) > 0)){
            if (optionalProductPrice.isPresent()) {
                ProductPrice productPrice = optionalProductPrice.get();

                List<ProductPrice> productPriceList = productPriceRepo.getProductPrice(productPrice.getProduct().getId(), productPrice.getRegionalDepartment().getId(), new Timestamp(dto.getStartDay().getTime()).toLocalDateTime().toLocalDate(), new Timestamp(dto.getEndDay().getTime()).toLocalDateTime().toLocalDate(), false);

                if ((productPriceList.size() == 0 && productPrice.isEdit()) || (productPriceList.size() == 1 && productPriceList.get(0).getId().equals(productPriceId))) {
                    productPrice.setMinPrice(BigDecimal.valueOf(dto.getMinPrice()));
                    productPrice.setMaxPrice(BigDecimal.valueOf(dto.getMaxPrice()));
                    productPrice.setStartDay(new Timestamp(dto.getStartDay().getTime()).toLocalDateTime().toLocalDate());
                    productPrice.setEndDay(new Timestamp(dto.getEndDay().getTime()).toLocalDateTime().toLocalDate());
                    productPriceRepo.save(productPrice);
                    message = Message.EDIT_UZ;
                }
            }
        }
        return new StateMessage().parse(message);
    }

    public CustomPageable getAllPriceByProduct(HttpServletRequest request, Integer productId, Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);

        Users user = userService.parseToken(request);
        Integer departmentId = user.getRegionalDepartment().getId();

        Page<ProductPrice> page = productPriceRepo.findAllByRegionalDepartment_IdAndProduct_IdAndDeleteIsFalseOrderByEndDay(departmentId, productId, pageable);

        return new CustomPageable(page.getSize(), page.getNumber(), converter.productPriceToDTO(page.stream().toList()), page.getTotalElements());
    }

    public CustomPageable getAll(HttpServletRequest request, Integer pageNumber, Integer pageSize, String search) {

        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);

        Users user = userService.parseToken(request);
        Integer departmentId = user.getRegionalDepartment().getId();

        Page<Product> productPage;
        if (search == null) {
            productPage = productRepo.findAllByDeleteIsFalseAndRegionalDepartment_Id(departmentId, pageable);
        } else {
            productPage = productRepo.findAllByDeleteIsFalseAndNameContainsAndRegionalDepartment_Id(search, departmentId, pageable);
        }
        return new CustomPageable(productPage.getSize(), productPage.getNumber(), converter.productToDTO(productPage.stream().toList()), productPage.getTotalElements());
    }

    public StateMessage delete(Integer id) {
        Optional<ProductPrice> optionalProductPrice = productPriceRepo.findById(id);
        if (optionalProductPrice.isPresent()) {
            ProductPrice productPrice = optionalProductPrice.get();
            productPrice.setDelete(true);
            productPriceRepo.save(productPrice);
            return new StateMessage("muvaffaqiyatli o'chirildi", true, 200);
        } else {
            throw new ProductPriceNotFoundException("bunday narx mavjud emas");
        }
    }

    private void checkEdit(Product product, RegionalDepartment regionalDepartment) {
        List<ProductPrice> list = productPriceRepo.findAllByRegionalDepartment_IdAndProduct_IdAndDeleteIsFalseAndEditIsTrueOrderByCreateDateDesc(regionalDepartment.getId(), product.getId());

        for (int i = 1; i < list.size(); i++) {
            ProductPrice productPrice = list.get(i);
            productPrice.setEdit(false);
        }

        productPriceRepo.saveAll(list);
    }
}