package com.optimal.fergana.product.product_price;

import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductPriceResDTO {

    private Integer id;
    private int productPriceId;
    private ProductResDTO product;
    private BigDecimal minPrice;
    private BigDecimal maxPrice;
    private Timestamp startDay;
    private Timestamp endDay;
    private boolean edit;

}
