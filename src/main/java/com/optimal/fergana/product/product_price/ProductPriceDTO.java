package com.optimal.fergana.product.product_price;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
public class ProductPriceDTO {
    @NotNull
    private int productId;
    @NotNull
    private double minPrice;
    @NotNull
    private double maxPrice;
    @NotNull
    private Date startDay;
    @NotNull
    private Date endDay;
}
