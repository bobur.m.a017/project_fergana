package com.optimal.fergana.product.product_price;


import com.optimal.fergana.product.Product;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ProductPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private Product product;

    private BigDecimal minPrice;

    private BigDecimal maxPrice;

    @ManyToOne
    private RegionalDepartment regionalDepartment;

    private LocalDate startDay;

    private LocalDate endDay;

    private boolean delete = false;

    private boolean edit = true;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    public ProductPrice(Product product, BigDecimal minPrice, BigDecimal maxPrice, RegionalDepartment regionalDepartment, LocalDate startDay, LocalDate endDay) {
        this.product = product;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.regionalDepartment = regionalDepartment;
        this.startDay = startDay;
        this.endDay = endDay;
    }
}
