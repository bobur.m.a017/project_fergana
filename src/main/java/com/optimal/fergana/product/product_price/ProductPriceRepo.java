package com.optimal.fergana.product.product_price;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;


public interface ProductPriceRepo extends JpaRepository<ProductPrice, Integer> {

    Page<ProductPrice> findAllByRegionalDepartment_IdAndProduct_IdAndDeleteIsFalseOrderByEndDay(Integer regionalDepartment_id, Integer product_id, Pageable pageable);

    List<ProductPrice> findAllByRegionalDepartment_IdAndProduct_IdAndDeleteIsFalseAndEditIsTrueOrderByCreateDateDesc(Integer regionalDepartment_id, Integer product_id);

    long countAllByRegionalDepartment_IdAndProduct_IdAndDeleteIsFalseOrderByEndDay(Integer regionalDepartment_id, Integer product_id);

    long countAllByRegionalDepartment_IdAndDeleteIsFalseOrderByEndDayDesc(Integer regionalDepartment_id);

    @Query(nativeQuery = true,
            value = "SELECT e.* FROM product_price e WHERE (e.product_id = :productId AND e.regional_department_id = :regionalDepartmentId) AND  ((e.start_day <= :date1 AND e.end_day >= :date1) OR (e.start_day <= :date2 AND e.end_day >= :date2) OR (e.start_day >= :date1 AND e.start_day <= :date2)) AND e.delete = :res")
    List<ProductPrice> getProductPrice(Integer productId, Integer regionalDepartmentId, LocalDate date1, LocalDate date2, boolean res);


    @Query(nativeQuery = true,
            value = "SELECT e.* FROM product_price e WHERE (e.product_id = :productId AND e.regional_department_id = :regionalDepartmentId) AND  ((e.start_day <= :date) OR (e.end_day >= :date))AND e.delete = :res")
    List<ProductPrice> getProductPriceCheck(Integer productId, Integer regionalDepartmentId, LocalDate date, boolean res);

}
