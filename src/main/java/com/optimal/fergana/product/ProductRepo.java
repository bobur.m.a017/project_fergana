package com.optimal.fergana.product;

import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.parameters.P;

import java.util.List;
import java.util.Optional;


public interface ProductRepo extends JpaRepository<Product, Integer> {
    boolean existsByName(String name);

    boolean existsByNameAndRegionalDepartment(String name, RegionalDepartment regionalDepartment);

    Optional<Product> findAllByRegionalDepartmentAndNameAndDelete(RegionalDepartment regionalDepartment, String name, boolean res);

    List<Product> findAllByRegionalDepartment(RegionalDepartment regionalDepartment);

    List<Product> findAllByRegionalDepartmentAndDeleteIsFalse(RegionalDepartment regionalDepartment);

    Optional<Product> findByName(String name);

    Page<Product> findAllByDeleteIsFalseAndNameContainsAndRegionalDepartment_Id( String name,Integer id, Pageable pageable);

    Page<Product> findAllByDeleteIsFalseAndRegionalDepartment_Id(Integer id, Pageable pageable);
}
