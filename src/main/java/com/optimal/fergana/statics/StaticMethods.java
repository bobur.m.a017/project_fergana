package com.optimal.fergana.statics;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;

public class StaticMethods {
    public static PdfPCell createCell(String content, int rowspan, int size, int colspan) {

        PdfPCell cell = new PdfPCell(new Phrase(content, new Font(Font.FontFamily.TIMES_ROMAN, size)));
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        cell.setBorder(Rectangle.BOX);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        return cell;
    }

    public static void style(XSSFFont font, XSSFCellStyle cellStyleCouple) {
        cellStyleCouple.setFont(font);
        cellStyleCouple.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyleCouple.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellStyleCouple.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyleCouple.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyleCouple.setBorderRight(CellStyle.BORDER_THIN);
        cellStyleCouple.setBorderTop(CellStyle.BORDER_THIN);
    }
}
