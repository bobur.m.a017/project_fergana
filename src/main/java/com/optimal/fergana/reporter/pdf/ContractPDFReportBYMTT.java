package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.supplier.contract.Contract;
import com.optimal.fergana.supplier.contract.ContractRepo;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContract;
import com.optimal.fergana.supplier.contract.product.ProductContract;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.Set;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;

@RequiredArgsConstructor
@Service
public class ContractPDFReportBYMTT {

    private final ContractRepo contractRepo;


    public String createPDFKindergarten(Integer contractId, HttpServletResponse response) throws FileNotFoundException, DocumentException {

        String path = DEST + contractId + ".pdf";


        File file = new File(path);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream(path));
        document.open();
        document.add(new Paragraph("Paragraph" + " \n \n "));
        document.add(createTableKindergarten(contractId));
        document.newPage();
        document.close();
        return path;
    }


    private PdfPTable createTableKindergarten(Integer contractId) {

        Optional<Contract> optionalContract = contractRepo.findById(contractId);

        if (optionalContract.isPresent()) {
            Contract contract = optionalContract.get();
            Set<ProductContract> productContracts = contract.getKindergartenContractList().get(0).getProductContracts();

            int size = productContracts.size();

            float[] aa = new float[size + 3];

            aa[0] = (1.5F);
            aa[1] = (5F);

            for (int i = 2; i < size + 3; i++) {
                aa[i] = (1.5F);
            }
            PdfPTable table = new PdfPTable(aa);
            table.setWidthPercentage(105);
            int fontSize = 7;


            table.addCell(createCell("T/R", 1, 10, 1));

            table.addCell(createCell("MTT", 1, 10, 2));

            for (ProductContract productContract : productContracts) {
                table.addCell(createCell(productContract.getProduct().getName(), 1, 9, 1));
            }

            table.addCell(createCell("Narxi", 1, 10, 3));
            for (ProductContract productContract : productContracts) {
                table.addCell(createCell(productContract.getPrice().setScale(1, RoundingMode.HALF_UP).toString(), 1, 9, 1));
            }


            int tr = 0;
            for (KindergartenContract kindergartenContract : contract.getKindergartenContractList()) {
                table.addCell(createCell(String.valueOf(++tr), 4, 9, 1));
                table.addCell(createCell(kindergartenContract.getKindergarten().getNumber() + kindergartenContract.getKindergarten().getName(), 4, 9, 1));
                table.addCell(createCell("Umumiy miqdor", 1, fontSize, 1));
                Set<ProductContract> productContracts1 = kindergartenContract.getProductContracts();
                for (ProductContract productContract : productContracts1) {
                    BigDecimal weight = productContract.getWeight().setScale(1, RoundingMode.HALF_UP);
                    table.addCell(createCell(weight.toString(), 1, fontSize, 1));
                }
                table.addCell(createCell("Yetkazib berildi", 1, fontSize, 1));

                for (ProductContract productContract : productContracts1) {
                    BigDecimal successWeight = productContract.getSuccessWeight().setScale(1, RoundingMode.HALF_UP);
                    table.addCell(createCell(successWeight.toString(), 1, fontSize, 1));
                }
                table.addCell(createCell("Qoldiq", 1, fontSize, 1));

                for (ProductContract productContract : productContracts1) {
                    BigDecimal subtract = productContract.getWeight().subtract(productContract.getSuccessWeight()).setScale(1, RoundingMode.HALF_UP);
                    table.addCell(createCell(subtract.toString(), 1, fontSize, 1));
                }
                table.addCell(createCell("Qoldiq Summa", 1, fontSize, 1));

                for (ProductContract productContract : productContracts1) {
                    BigDecimal subtract = productContract.getWeight().subtract(productContract.getSuccessWeight());
                    BigDecimal multiply = subtract.multiply(productContract.getPrice()).setScale(1, RoundingMode.HALF_UP);
                    table.addCell(createCell(multiply.toString(), 1, fontSize, 1));
                }
            }

            table.addCell(createCell("Umumiy", 1, 9, 2));
            for (ProductContract productContract : productContracts) {
                table.addCell(createCell(productContract.getTotalSum().setScale(1, RoundingMode.HALF_UP).toString(), 1,9 , 1));
            }
            return table;
        } else {
            throw new RuntimeException();
        }
    }
}
