package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;

@Service
@RequiredArgsConstructor
public class PDFReporterByPrice {
    public String createPDFMenu(String paragraph) throws FileNotFoundException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream(DEST));
        document.open();
        document.add(new Paragraph(paragraph + " \n \n "));




        float[] aa = new float[1 + 4];

        aa[0] = (1.5F);
        aa[1] = (5F);
        aa[2] = (3F);
        aa[3] = (2F);
        for (int i = 4; i < 1 + 4; i++) {
            aa[i] = (1.5F);
        }

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);

        int fontSize = 7;

        PdfPCell ovqatlar_vaqti = createCell("Ovqatlar vaqti", 1, fontSize,1);
        ovqatlar_vaqti.setRotation(90);
        table.addCell(ovqatlar_vaqti);
        table.addCell(createCell("Taomlar nomi", 1, 8,1));
        table.addCell(createCell("Yosh toifasi", 1, 8,1));
        table.addCell(createCell("Bir kishi ovqat xajmi", 1, fontSize,1));








//        document.add(createTableMenu(menu));
        document.newPage();
        document.close();
        return DEST;
    }
}
