package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.multiMenu.product.DateWeight;
import com.optimal.fergana.multiMenu.product.ProductWeight;
import com.optimal.fergana.multiMenu.product.SanPinCategoryWeight;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;

@Service
public class MultiMenuProductPDF {

    public String getFileProduct(List<SanPinCategoryWeight> list, MultiMenu multiMenu) throws FileNotFoundException, DocumentException {

        String path = DEST + multiMenu.getName().replace(' ','_') + "Product" + ".pdf";

        File file = new File(path);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream(path));
        document.open();

        Paragraph paragraph = new Paragraph(multiMenu.getName() + " menyusida foydalanilgan maxsulotlar miqdori to`g`risida \n MA‘LUMOT \n ");
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);


        document.add(createTableMenu(list, multiMenu.getDaily()));

        document.close();
        return path;
    }

    private PdfPTable createTableMenu(List<SanPinCategoryWeight> list, int day) {
        int size = day + 8;
        float[] aa = new float[size];

        aa[0] = (1F);
        aa[1] = (5F);
        aa[2] = (1F);
        aa[3] = (5F);
        for (int i = 4; i < size-4; i++) {
            aa[i] = (2F);
        }
        for (int i = size-4; i < size; i++) {
            aa[i] = (3F);
        }

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(100);

        int fontSize = 8;

        table.addCell(createCell("T/r", 1, fontSize, 1));
        table.addCell(createCell("SanPin kategoriya nomi", 1, fontSize, 1));
        table.addCell(createCell("T/r", 1, fontSize, 1));
        table.addCell(createCell("Maxsulot nomi", 1, fontSize, 1));

        for (int i = 1; i <= day; i++) {
            table.addCell(createCell(i + "-kun", 1, fontSize, 1));
        }

        table.addCell(createCell("Jami", 1, fontSize, 1));
        table.addCell(createCell("Me‘yor", 1, fontSize, 1));
        table.addCell(createCell("Haqiqatda", 1, fontSize, 1));
        table.addCell(createCell("Bajarilishi \n(% da)", 1, fontSize, 1));

        int num = 1;
        int numProduct = 1;
        for (SanPinCategoryWeight sanPinCategoryWeight : list) {

            int rowSpan = sanPinCategoryWeight.getProductWeightList().size();

            table.addCell(createCell(String.valueOf(num++), rowSpan, fontSize, 1));
            PdfPCell cell = createCell(nameCustom(sanPinCategoryWeight.getSanPinCategoryName(),47),rowSpan , fontSize, 1);
//            cell.setRotation(90);
            table.addCell(cell);

            int count = 1;
            for (ProductWeight productWeight : sanPinCategoryWeight.getProductWeightList()) {

                table.addCell(createCell(String.valueOf(numProduct++), 1, fontSize, 1));
                table.addCell(createCell(nameCustom(productWeight.getProductName(),20), 1, fontSize, 1));

                BigDecimal totalWeight = BigDecimal.valueOf(0);
                for (DateWeight dateWeight : productWeight.getDateWeightList()) {
                    if (dateWeight.getWeight() != null) {
                        BigDecimal weight = dateWeight.getWeight().setScale(1, RoundingMode.HALF_UP);
                        totalWeight = totalWeight.add(weight);
                        table.addCell(createCell(weight.toString(), 1, fontSize, 1));
                    } else {
                        table.addCell(createCell("", 1, fontSize, 1));
                    }
                }
                table.addCell(createCell(totalWeight.toString(), 1, fontSize, 1));

                if (count == 1) {
                    table.addCell(createCell(sanPinCategoryWeight.getPlanWeight().setScale(1, RoundingMode.HALF_UP).toString(), rowSpan, fontSize, 1));
                    table.addCell(createCell(sanPinCategoryWeight.getDoneWeight().setScale(1, RoundingMode.HALF_UP).toString(), rowSpan, fontSize, 1));
                    table.addCell(createCell(sanPinCategoryWeight.getDoneWeight().divide(sanPinCategoryWeight.getPlanWeight(), 4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100)).setScale(0,RoundingMode.UP)+" %", rowSpan, fontSize, 1));
                }
                count++;
            }
        }
        return table;
    }

    public String nameCustom(String name,int number) {
        if (name.length() <= number)
            return name;
        return name.substring(0, number-3) + "...";
    }
}
