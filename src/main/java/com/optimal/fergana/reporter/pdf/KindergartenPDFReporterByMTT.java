package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.kids.KidsNumberRepo;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.KidsNumberSubRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;

@RequiredArgsConstructor
@Service
public class KindergartenPDFReporterByMTT {
    private final KidsNumberRepo kidsNumberRepo;
    private final KindergartenRepo kindergartenRepo;
    private final KidsNumberSubRepo kidsNumberSubRepo;
    private final AgeGroupRepo ageGroupRepo;


    public String createPDFKindergarten(String paragraph, Timestamp startDate, Timestamp endDate, Users users) throws FileNotFoundException, DocumentException {


        File file = new File(DEST);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4);
        PdfWriter.getInstance(document, new FileOutputStream(DEST));
        document.open();
        document.add(new Paragraph(paragraph + " \n \n "));


        List<KidsNumberSub> subList;
        List<AgeGroup> ageGroupList = ageGroupRepo.findAllByDeleteIsFalse();


        int size = ageGroupList.size();

        float[] aa = new float[size + 3];

        aa[0] = (1.5F);
        aa[1] = (5F);

        for (int i = 2; i < size + 3; i++) {
            aa[i] = (1.5F);
        }
        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);
        int fontSize = 7;


        PdfPCell TR = createCell("T/R", 1, fontSize, 1);

        table.addCell(TR);
        table.addCell(createCell("MTT nomi", 1, 8, 1));


        int num = 0;
        ageGroupList.sort(Comparator.comparing(AgeGroup::getSortNumber));

        for (AgeGroup ageGroup : ageGroupList) {
            table.addCell(createCell(ageGroup.getName(), 1, 8, 1));
        }
        table.addCell(createCell("Jami", 1, 8, 1));


        if (users.getKindergarten() != null) {
            table.addCell(createCell(String.valueOf(++num), 1, 8, 1));

            table.addCell(createCell(users.getKindergarten().getNumber() + users.getKindergarten().getName(), 1, 8, 1));

            int total = 0;

            for (AgeGroup ageGroup : ageGroupList) {
                subList = kidsNumberSubRepo.findAllByKidsNumber_Report_Kindergarten_IdAndKidsNumber_DeleteIsFalseAndAgeGroup_IdAndKidsNumber_DateBetween(users.getKindergarten().getNumber(), ageGroup.getId(), startDate.toLocalDateTime().toLocalDate(), endDate.toLocalDateTime().toLocalDate());

                Integer totalSum = getTotalSum(subList);

                table.addCell(createCell(totalSum.toString(), 1, 8, 1));

                total += totalSum;
            }
            table.addCell(createCell(Integer.toString(total), 1, 8, 1));

        } else if (users.getDepartment() != null) {

            Department department = users.getDepartment();

            List<Kindergarten> kindergartenList = department.getKindergartenList();
            kindergartenList.sort(Comparator.comparing(Kindergarten::getNumber));

            for (Kindergarten kindergarten : kindergartenList) {
                int total = 0;

                table.addCell(createCell(String.valueOf(++num), 1, 8, 1));
                table.addCell(createCell(kindergarten.getNumber() + kindergarten.getName(), 1, 8, 1));

                for (AgeGroup ageGroup : ageGroupList) {
                    subList = kidsNumberSubRepo.findAllByKidsNumber_Report_Kindergarten_IdAndKidsNumber_DeleteIsFalseAndAgeGroup_IdAndKidsNumber_DateBetween(kindergarten.getId(), ageGroup.getId(), startDate.toLocalDateTime().toLocalDate(), endDate.toLocalDateTime().toLocalDate());
                    Integer totalSum = getTotalSum(subList);
                    table.addCell(createCell(totalSum.toString(), 1, 8, 1));
                    total += totalSum;
                }
                table.addCell(createCell(Integer.toString(total), 1, 8, 1));
            }
        } else {
            RegionalDepartment regionalDepartment = users.getRegionalDepartment();

            for (Department department : regionalDepartment.getDepartmentList()) {
                num = 0;
                table.addCell(createCell(department.getName(), 1, 11, size + 3));



                for (Kindergarten kindergarten : department.getKindergartenList()) {

                    int total = 0;
                    table.addCell(createCell(String.valueOf(++num), 1, 8, 1));

                    table.addCell(createCell(kindergarten.getNumber() + kindergarten.getName(), 1, 8, 1));

                    for (AgeGroup ageGroup : ageGroupList) {
                        subList = kidsNumberSubRepo.findAllByKidsNumber_Report_Kindergarten_IdAndKidsNumber_DeleteIsFalseAndAgeGroup_IdAndKidsNumber_DateBetween(kindergarten.getId(), ageGroup.getId(), startDate.toLocalDateTime().toLocalDate(), endDate.toLocalDateTime().toLocalDate());
                        Integer totalSum = getTotalSum(subList);
                        table.addCell(createCell(totalSum.toString(), 1, 8, 1));
                        total += totalSum;
                    }
                    table.addCell(createCell(Integer.toString(total), 1, 8, 1));
                }
            }
        }


        document.add(table);
        document.newPage();
        document.close();
        return DEST;
    }

    private Integer getTotalSum(List<KidsNumberSub> list) {
        Integer totalSum = 0;

        for (KidsNumberSub kidsNumberSub : list) {
            totalSum += kidsNumberSub.getNumber();
        }

        return totalSum;
    }
}





