package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.stayTime.StayTime;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;

@RequiredArgsConstructor
@Service
public class KidsNumberPDFReporterByDate {
    private final AgeGroupRepo ageGroupRepo;


    public String createPDFKindergarten(List<KidsNumber> kidsNumberList, LocalDate start, LocalDate end, Kindergarten kindergarten) {

        String path = DEST + System.currentTimeMillis() + ".pdf";

        try {

            if (kidsNumberList.size() > 0) {
                File file = new File(path);
                file.getParentFile().mkdirs();
                Document document = new Document();
                document.setPageSize(PageSize.A4.rotate());
                PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
                document.add(new Paragraph(kidsNumberList.get(0).getReport().getKindergarten().getFullName() + " \n" + start + "  " + end + "  vaqt oralig`idagi bolalar qatnov xisoboti \n \n "));
                document.add(createTableKindergarten(kidsNumberList, kindergarten));
                document.newPage();
                document.close();
                return path;
            }
            return "";

        } catch (Exception e) {
            return path;
        }
    }

    private PdfPTable createTableKindergarten(List<KidsNumber> kidsNumberList, Kindergarten kindergarten) {
        List<AgeGroup> ageGroupList = new ArrayList<>();
        for (StayTime stayTime : kindergarten.getStayTimeList()) {
            ageGroupList.addAll(stayTime.getAgeGroupList());
        }

        ageGroupList.sort(Comparator.comparing(AgeGroup::getSortNumber));

        int size = ageGroupList.size();

        float[] aa = new float[size + 3];

        aa[0] = (1.5F);
        aa[1] = (5F);

        for (int i = 2; i < size + 3; i++) {
            aa[i] = (1.5F);
        }
        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);
        int fontSize = 7;


        PdfPCell TR = createCell("T/R", 1, fontSize, 1);

        table.addCell(TR);
        table.addCell(createCell("Sana", 1, 8, 1));

        for (AgeGroup ageGroup : ageGroupList) {
            table.addCell(createCell(ageGroup.getName(), 1, 8, 1));
        }
        table.addCell(createCell("Jami", 1, 8, 1));


//        List<KidsNumber> kidsNumberList = kidsNumberRepo.findAllByReport_Kindergarten_IdAndDateBetweenAndDeleteIsFalse(users.getKindergarten().getId(), startDate.toLocalDateTime().toLocalDate(), endDate.toLocalDateTime().toLocalDate());

        kidsNumberList.sort(Comparator.comparing(KidsNumber::getDate));

        int num = 0;
        for (KidsNumber kidsNumber : kidsNumberList) {
            String date = kidsNumber.getDate().toString();
            table.addCell(createCell(String.valueOf(++num), 1, 8, 1));
            table.addCell(createCell(date, 1, 8, 1));
            int totalHorizontal = 0;
            for (KidsNumberSub kidsNumberSub : kidsNumber.getKidsNumberSubList()) {

                Integer number = kidsNumberSub.getNumber();
                totalHorizontal += number;
                table.addCell(createCell(number.toString(), 1, 8, 1));
            }
            table.addCell(createCell(String.valueOf(totalHorizontal), 1, 8, 1));
        }
        return table;
    }


    public String getAllByDepartmentIdPDF(List<KidsNumber> kidsNumberList, LocalDate date, Department department) {
        String path = DEST + System.currentTimeMillis() + ".pdf";
        try {
            File file = new File(path);
            file.getParentFile().mkdirs();
            Document document = new Document();
            document.setPageSize(PageSize.A4);
            PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();
            document.add(new Paragraph(department.getName() + " maktabgacha ta‘lim bo‘limining \n" + date + " kungi bolalar qatnov xisoboti \n \n "));
            document.add(createTableByDepartmentIdPDF(kidsNumberList));
            document.newPage();
            document.close();
            return path;
        } catch (Exception e) {
            return path;
        }
    }

    private PdfPTable createTableByDepartmentIdPDF(List<KidsNumber> kidsNumberList) {
        List<AgeGroup> ageGroupList = ageGroupRepo.findAllByDeleteIsFalse();

        ageGroupList.sort(Comparator.comparing(AgeGroup::getSortNumber));

        int size = ageGroupList.size();

        float[] aa = new float[size + 3];

        aa[0] = (1.5F);
        aa[1] = (5F);

        for (int i = 2; i < size + 3; i++) {
            aa[i] = (1.5F);
        }
        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);
        int fontSize = 7;


        PdfPCell TR = createCell("T/R", 1, fontSize, 1);

        table.addCell(TR);
        table.addCell(createCell("Sana", 1, 8, 1));

        for (AgeGroup ageGroup : ageGroupList) {
            table.addCell(createCell(ageGroup.getName(), 1, 8, 1));
        }
        table.addCell(createCell("Jami", 1, 8, 1));


        kidsNumberList.sort(Comparator.comparing(KidsNumber::getDate));

        int num = 0;
        for (KidsNumber kidsNumber : kidsNumberList) {

            String name = kidsNumber.getReport().getKindergarten().getNumber() + kidsNumber.getReport().getKindergarten().getName();

            table.addCell(createCell(String.valueOf(++num), 1, 8, 1));
            table.addCell(createCell(name, 1, 8, 1));
            int totalHorizontal = 0;
            for (KidsNumberSub kidsNumberSub : kidsNumber.getKidsNumberSubList()) {

                Integer number = kidsNumberSub.getNumber();
                totalHorizontal += number;
                table.addCell(createCell(number.toString(), 1, 8, 1));
            }
            table.addCell(createCell(String.valueOf(totalHorizontal), 1, 8, 1));
        }
        return table;
    }
}
