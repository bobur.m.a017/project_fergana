package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.inputOutput.InputOutput;
import com.optimal.fergana.inputOutput.InputOutputService;
import com.optimal.fergana.inputOutput.inOut.InOut;
import com.optimal.fergana.inputOutput.inOut.InOutRepo;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.MenuService;
import com.optimal.fergana.menu.ProductMenuDTO;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.multiMenu.MultiMenuRepo;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNormRepo;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.report.menuReport.*;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNorm;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import com.optimal.fergana.stayTime.StayTime;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static com.optimal.fergana.statics.StaticMethods.createCell;

import static com.optimal.fergana.statics.StaticWords.DEST;
import static java.awt.SystemColor.menu;


@Service
@RequiredArgsConstructor
public class MenuPDFReporter {


    private final MenuService menuService;
    private final ReportRepo reportRepo;
    private final InputOutputService inputOutputService;
    private final MultiMenuRepo multiMenuRepo;
    private final InOutRepo inOutRepo;
    private final ProductPackService productPackService;


    public String createPDFMenu(UUID id) throws FileNotFoundException, DocumentException {

        Optional<MultiMenu> optionalMultiMenu = multiMenuRepo.findById(id);

        if (optionalMultiMenu.isPresent()) {


            MultiMenu multiMenu = optionalMultiMenu.get();
            String replace = multiMenu.getName().replace(' ', '_');
            String path = DEST + replace + ".pdf";

            File file = new File(path);
            file.getParentFile().mkdirs();
            Document document = new Document();
            document.setPageSize(PageSize.A4.rotate());
            PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();

            Paragraph paragraph = new Paragraph(multiMenu.getName());
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            List<Menu> menuList = multiMenu.getMenuList();
            menuList.sort(Comparator.comparing(Menu::getNumber));

            int num = 1;
            for (Menu menu : menuList) {
                Paragraph paragraphMenu = new Paragraph(menu.getName() + " \n ");
                paragraphMenu.setAlignment(Element.ALIGN_CENTER);
                document.add(paragraphMenu);
                document.add(createTableMenu(menu));

                if (num != menuList.size()) {
                    document.newPage();
                }

                num++;
            }

            document.setPageSize(PageSize.A4);
            document.newPage();
            Paragraph paragraphMenu = new Paragraph("SANPIN meyyor bajarilishi \n \n");
            paragraphMenu.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraphMenu);
            document.add(createTableSanPin(multiMenu));

            document.close();
            return path;
        }
        return null;
    }

    private PdfPTable createTableMenu(Menu menu) {

        Set<ProductMenuDTO> productList = menuService.getProductList(menu);
        int size = productList.size();
        float[] aa = new float[size + 4];

        aa[0] = (1.5F);
        aa[1] = (5F);
        aa[2] = (3F);
        aa[3] = (2F);
        for (int i = 4; i < size + 4; i++) {
            aa[i] = (1.5F);
        }

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);

        int fontSize = 7;

        PdfPCell ovqatlar_vaqti = createCell("Ovqatlar vaqti", 1, fontSize, 1);
        ovqatlar_vaqti.setRotation(90);
        table.addCell(ovqatlar_vaqti);
        table.addCell(createCell("Taomlar nomi", 1, 8, 1));
        table.addCell(createCell("Yosh toifasi", 1, 8, 1));
        table.addCell(createCell("Bir kishi ovqat xajmi", 1, fontSize, 1));

        for (ProductMenuDTO menuDTO : productList) {
            PdfPCell cell = createCell(nameCustom(menuDTO.getName()), 1, 8, 1);
            cell.setRotation(90);
            table.addCell(cell);
        }

        List<MealTimeStandard> mealTimeStandardList = menu.getMealTimeStandardList();

        mealTimeStandardList.sort(Comparator.comparing(a -> a.getMealTime().getSortNumber()));


        for (MealTimeStandard mealTimeStandard : mealTimeStandardList) {
            List<MealAgeStandard> mealAgeStandardList = mealTimeStandard.getMealAgeStandardList();

            if (mealTimeStandard.getMealAgeStandardList().size() > 0) {

                String name = mealTimeStandard.getMealTime().getName();
                StringBuilder mealTimeName = new StringBuilder();
                int num = 1;
                for (String s : name.split(" ")) {
                    if (num == 2) {
                        mealTimeName.append("\n");
                    }
                    mealTimeName.append(s);
                    num++;
                }


                PdfPCell mealTimeCell = createCell(mealTimeName.toString(), mealAgeStandardList.size() * mealAgeStandardList.get(0).getAgeStandardList().size(), 8, 1);
                mealTimeCell.setRotation(90);
                table.addCell(mealTimeCell);
                for (MealAgeStandard mealAgeStandard : mealAgeStandardList) {
                    List<AgeStandard> ageStandardList = mealAgeStandard.getAgeStandardList();

                    ageStandardList.sort(Comparator.comparing(a -> a.getAgeGroup().getSortNumber()));

                    PdfPCell mealName = createCell(mealAgeStandard.getMeal().getName(), ageStandardList.size(), fontSize, 1);
                    table.addCell(mealName);
                    for (AgeStandard ageStandard : ageStandardList) {
                        PdfPCell ageGroupName = createCell(ageStandard.getAgeGroup().getName(), 1, fontSize, 1);
                        table.addCell(ageGroupName);
                        PdfPCell ageGroupWeight = createCell(ageStandard.getWeight().setScale(0, RoundingMode.HALF_UP).toString(), 1, fontSize, 1);
                        table.addCell(ageGroupWeight);
                        List<ProductMenuDTO> productListWeight = menuService.getProductList(ageStandard, ageStandard.getMealAgeStandard().getMeal(), menu);
                        for (ProductMenuDTO productMenuDTO : productListWeight) {

                            BigDecimal bigDecimal = productMenuDTO.getWeight().setScale(2, RoundingMode.HALF_UP);

                            String value = bigDecimal.compareTo(BigDecimal.ZERO) == 0 ? "" : bigDecimal.toString();
                            PdfPCell weight = createCell(value, 1, fontSize, 1);
                            table.addCell(weight);
                        }
                    }
                }
            }
        }
        return table;
    }

    private PdfPTable createTableSanPin(MultiMenu multiMenu) {

        List<SanpinMenuNorm> sanpinMenuNormList = multiMenu.getSanpinMenuNorm();

        float[] aa = new float[6];

        aa[0] = (1F);
        aa[1] = (5F);
        aa[2] = (3F);
        for (int i = 3; i < aa.length; i++) {
            aa[i] = (2F);
        }

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(70);

        int fontSize = 8;

        PdfPCell ovqatlar_vaqti = createCell("T/r", 1, fontSize, 1);
        table.addCell(ovqatlar_vaqti);
        table.addCell(createCell("Kategoriya nomi", 1, fontSize, 1));
        table.addCell(createCell("Yosh toifasi", 1, fontSize, 1));
        table.addCell(createCell("Rejada", 1, fontSize, 1));
        table.addCell(createCell("Amalda", 1, fontSize, 1));
        table.addCell(createCell("Farqi %", 1, fontSize, 1));

        int num = 1;

        for (SanpinMenuNorm sanpinMenuNorm : sanpinMenuNormList) {


            PdfPCell cellNum = createCell(String.valueOf(num), sanpinMenuNorm.getAgeGroupSanpinNormList().size(), 8, 1);
            table.addCell(cellNum);


            PdfPCell cell = createCell(sanpinMenuNorm.getSanpinCategory().getName(), sanpinMenuNorm.getAgeGroupSanpinNormList().size(), 8, 1);
            table.addCell(cell);
            for (AgeGroupSanpinNorm ageGroupSanpinNorm : sanpinMenuNorm.getAgeGroupSanpinNormList()) {

                PdfPCell cell1 = createCell(ageGroupSanpinNorm.getAgeGroup().getName(), 1, 8, 1);
                table.addCell(cell1);

                PdfPCell cell2 = createCell(ageGroupSanpinNorm.getPlanWeight().setScale(0, RoundingMode.UP).toString(), 1, 8, 1);
                table.addCell(cell2);

                PdfPCell cell3 = createCell(ageGroupSanpinNorm.getDoneWeight().setScale(0, RoundingMode.UP).toString(), 1, 8, 1);
                table.addCell(cell3);

                PdfPCell cell4 = createCell(ageGroupSanpinNorm.getDoneWeight().divide(ageGroupSanpinNorm.getPlanWeight(), 4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100)).setScale(0, RoundingMode.HALF_UP).toString() + " %", 1, 8, 1);
                table.addCell(cell4);
            }
            num++;
        }
        return table;
    }

    private PdfPTable createTableKcal(MultiMenu multiMenu) {

        List<SanpinMenuNorm> sanpinMenuNormList = multiMenu.getSanpinMenuNorm();

        float[] aa = new float[5];
        Arrays.fill(aa, 4F);

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(70);

        int fontSize = 8;

        PdfPCell ovqatlar_vaqti = createCell("Kunlar", 2, fontSize, 1);
        table.addCell(ovqatlar_vaqti);
        table.addCell(createCell("Tarkibiy qismi", 1, fontSize, 3));
        table.addCell(createCell("Energetik qiymati", 2, fontSize, 1));
        table.addCell(createCell("Rejada", 1, fontSize, 1));
        table.addCell(createCell("Amalda", 1, fontSize, 1));
        table.addCell(createCell("Farqi %", 1, fontSize, 1));

        int num = 1;

        for (SanpinMenuNorm sanpinMenuNorm : sanpinMenuNormList) {


            PdfPCell cellNum = createCell(String.valueOf(num), sanpinMenuNorm.getAgeGroupSanpinNormList().size(), 8, 1);
            table.addCell(cellNum);


            PdfPCell cell = createCell(sanpinMenuNorm.getSanpinCategory().getName(), sanpinMenuNorm.getAgeGroupSanpinNormList().size(), 8, 1);
            table.addCell(cell);
            for (AgeGroupSanpinNorm ageGroupSanpinNorm : sanpinMenuNorm.getAgeGroupSanpinNormList()) {

                PdfPCell cell1 = createCell(ageGroupSanpinNorm.getAgeGroup().getName(), 1, 8, 1);
                table.addCell(cell1);

                PdfPCell cell2 = createCell(ageGroupSanpinNorm.getPlanWeight().setScale(0, RoundingMode.UP).toString(), 1, 8, 1);
                table.addCell(cell2);

                PdfPCell cell3 = createCell(ageGroupSanpinNorm.getDoneWeight().setScale(0, RoundingMode.UP).toString(), 1, 8, 1);
                table.addCell(cell3);

                PdfPCell cell4 = createCell(ageGroupSanpinNorm.getDoneWeight().divide(ageGroupSanpinNorm.getPlanWeight(), 4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100)).setScale(0, RoundingMode.HALF_UP).toString() + " %", 1, 8, 1);
                table.addCell(cell4);
            }
            num++;
        }
        return table;
    }

    public String nameCustom(String name) {
        if (name.length() <= 20)
            return name;
        return name.substring(0, 18) + "...";
    }

    private PdfPTable createTableReport(Report report, boolean fact) {
        Menu menu = report.getMenu();
        Set<ProductMenuDTO> productList = menuService.getProductList(menu);
        int size = productList.size();
        float[] aa = new float[size + 4];

        aa[0] = (1.5F);
        aa[1] = (5F);
        aa[2] = (3F);
        aa[3] = (2F);
        for (int i = 4; i < size + 4; i++) {
            aa[i] = (1.5F);
        }

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);

        int fontSize = 7;

        PdfPCell ovqatlar_vaqti = createCell("Ovqatlar vaqti", 1, fontSize, 1);
        ovqatlar_vaqti.setRotation(90);
        table.addCell(ovqatlar_vaqti);
        table.addCell(createCell("Taomlar nomi", 1, 8, 1));
        table.addCell(createCell("Yosh toifasi", 1, 8, 1));
        table.addCell(createCell("Bir kishi ovqat xajmi", 1, fontSize, 1));

        for (ProductMenuDTO menuDTO : productList) {
            PdfPCell cell = createCell(nameCustom(menuDTO.getName()), 1, 8, 1);
            cell.setRotation(90);
            table.addCell(cell);
        }

        List<MealTimeStandard> mealTimeStandardList = menu.getMealTimeStandardList();

        mealTimeStandardList.sort(Comparator.comparing(a -> a.getMealTime().getSortNumber()));


        for (MealTimeStandard mealTimeStandard : mealTimeStandardList) {
            List<MealAgeStandard> mealAgeStandardList = mealTimeStandard.getMealAgeStandardList();

            if (mealAgeStandardList.size() > 0) {

                String name = mealTimeStandard.getMealTime().getName();
                StringBuilder mealTimeName = new StringBuilder();
                int num = 1;
                for (String s : name.split(" ")) {
                    if (num == 2) {
                        mealTimeName.append("\n");
                    }
                    mealTimeName.append(s);
                    num++;
                }

                PdfPCell mealTimeCell = createCell(mealTimeName.toString(), mealAgeStandardList.size() * mealAgeStandardList.get(0).getAgeStandardList().size(), 8, 1);
                mealTimeCell.setRotation(90);
                table.addCell(mealTimeCell);
                for (MealAgeStandard mealAgeStandard : mealAgeStandardList) {

                    List<AgeStandard> ageStandardList = mealAgeStandard.getAgeStandardList();

                    ageStandardList.sort(Comparator.comparing(a -> a.getAgeGroup().getSortNumber()));

                    PdfPCell mealName = createCell(mealAgeStandard.getMeal().getName(), ageStandardList.size(), fontSize, 1);
                    table.addCell(mealName);

                    for (AgeStandard ageStandard : ageStandardList) {

                        PdfPCell ageGroupName = createCell(ageStandard.getAgeGroup().getName(), 1, fontSize, 1);
                        table.addCell(ageGroupName);
                        PdfPCell ageGroupWeight = createCell(ageStandard.getWeight().setScale(0, RoundingMode.HALF_UP).toString(), 1, fontSize, 1);
                        table.addCell(ageGroupWeight);

                        List<ProductMenuDTO> productListWeight = menuService.getProductListByReport(ageStandard, ageStandard.getMealAgeStandard().getMeal(), menu, report);

                        for (ProductMenuDTO productMenuDTO : productListWeight) {

                            BigDecimal divide = productMenuDTO.getWeight().divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP);

                            BigDecimal factWeight = getFactWeight(divide, report, productMenuDTO.getId(), fact);

                            String value = factWeight.compareTo(BigDecimal.ZERO) == 0 ? "" : factWeight.setScale(2, RoundingMode.HALF_UP).toString();

                            PdfPCell weight = createCell(value, 1, fontSize, 1);
                            table.addCell(weight);
                        }
                    }
                }
            }

        }
        return table;
    }

    public String createPDFReport(Report report, boolean fact) {

        String path = DEST + report.getId() + ".pdf";

        File file = new File(path);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4.rotate());

        try {
            PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();

            Paragraph paragraph = new Paragraph(report.getKindergarten().getFullName() + " \n" + report.getDate() + " \n ");
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            Paragraph paragraph1 = new Paragraph(report.getMenu().getMultiMenu().getName() + "\n" + report.getMenu().getName() + " \n ");
            paragraph1.setAlignment(Element.ALIGN_CENTER);

            document.add(paragraph1);


            PdfPTable tableMenu = createTableReport(report, fact);

            Menu menu = report.getMenu();

            Vector<ProductMenuDTO> productList = new Vector<>(menuService.getProductList(menu));

            tableMenu.addCell(createCell("Narxi", 1, 8, 4));

            productList = inputOutputService.getPrice(productList, report.getDate(), report.getKindergarten());

            for (ProductMenuDTO productMenuDTO : productList) {
                tableMenu.addCell(createCell(productMenuDTO.getPrice().setScale(0, RoundingMode.HALF_UP).toString(), 1, 8, 1));
            }

            Map<Integer, BigDecimal> map = new TreeMap<>();

            List<AgeGroup> ageGroupList = new ArrayList<>();

            for (StayTime stayTime : report.getKindergarten().getStayTimeList()) {
                ageGroupList.addAll(stayTime.getAgeGroupList());
            }

            ageGroupList.sort(Comparator.comparing(AgeGroup::getSortNumber));

            for (AgeGroup ageGroup : ageGroupList) {
                BigDecimal weight = BigDecimal.valueOf(0);
                productList = menuService.getAllProductWeightBtMenu(productList, menu, ageGroup, report);
                tableMenu.addCell(createCell(ageGroup.getName() + " bir bolaga (gramm xis)", 1, 8, 4));
                for (ProductMenuDTO productMenuDTO : productList) {
                    tableMenu.addCell(createCell(productMenuDTO.getSanpinMeyyor().toString(), 1, 8, 1));
                }
                tableMenu.addCell(createCell("Jami miqdor (kg)", 1, 8, 4));
                for (ProductMenuDTO productMenuDTO : productList) {

                    BigDecimal totalWeight = productMenuDTO.getTotalWeight();

                    BigDecimal factWeight = getFactWeight(totalWeight, report, productMenuDTO.getId(), fact);
                    if (factWeight.compareTo(BigDecimal.valueOf(0.01)) < 0) {
                        factWeight = factWeight.setScale(3, RoundingMode.HALF_UP);
                    } else {
                        factWeight = factWeight.setScale(2, RoundingMode.HALF_UP);
                    }

                    tableMenu.addCell(createCell(factWeight.setScale(2, RoundingMode.HALF_UP).toString(), 1, 8, 1));
                }
                tableMenu.addCell(createCell("Summa", 1, 8, 4));
                for (ProductMenuDTO productMenuDTO : productList) {
                    BigDecimal totalWeight = productMenuDTO.getTotalWeight();

                    BigDecimal factWeight = getFactWeight(totalWeight, report, productMenuDTO.getId(), fact);

                    BigDecimal totalPrice = factWeight.multiply(productMenuDTO.getPrice()).setScale(0, RoundingMode.HALF_UP);
                    weight = weight.add(totalPrice);
                    tableMenu.addCell(createCell(totalPrice.toString(), 1, 8, 1));
                }
                int rowSpanNum = 4 + (productList.size() / 2);
                PdfPCell cell = createCell(ageGroup.getName() + " umumiy summa", 1, 8, rowSpanNum);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                tableMenu.addCell(cell);

                PdfPCell cell1 = createCell(weight.setScale(0, RoundingMode.HALF_UP).toString(), 1, 8, productList.size() + 4 - rowSpanNum);
                cell1.setBackgroundColor(BaseColor.LIGHT_GRAY);
                tableMenu.addCell(cell1);
                map.put(ageGroup.getId(), weight);

            }

            document.add(tableMenu);
            document.add(createTableReportKidsNumber(report, map));

            document.newPage();
        } catch (Exception r) {
            r.printStackTrace();
            return "";
        }

        document.close();

        report.setFilePathPdf(path);
        reportRepo.save(report);
        return path;
    }

    private PdfPTable createTableReportKidsNumber(Report report, Map<Integer, BigDecimal> map) {
        KidsNumber kidsNumber = report.getKidsNumber();
        float[] aa = new float[4];

        aa[0] = (1.5F);
        aa[1] = (8F);
        for (int i = 2; i < aa.length; i++) {
            aa[i] = (3F);
        }

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);

        int fontSize = 7;

        PdfPCell tr = createCell("T/r", 1, fontSize, 1);
        tr.setBackgroundColor(new BaseColor(218, 219, 221));
        table.addCell(tr);
        PdfPCell yosh_toifasi = createCell("Yosh toifasi", 1, fontSize, 1);
        yosh_toifasi.setBackgroundColor(new BaseColor(218, 219, 221));
        table.addCell(yosh_toifasi);

        PdfPCell kunlik_bola_soni = createCell("Kunlik bola soni", 1, fontSize, 1);
        kunlik_bola_soni.setBackgroundColor(new BaseColor(218, 219, 221));
        table.addCell(kunlik_bola_soni);

        PdfPCell cell3 = createCell("Kunlik o`rtacha ovqatlantirish narxi", 1, fontSize, 1);
        cell3.setBackgroundColor(new BaseColor(218, 219, 221));
        table.addCell(cell3);

        int number = 1;

        Set<KidsNumberSub> kidsNumberSubList = kidsNumber.getKidsNumberSubList();
//        kidsNumberSubList.stream().toList().sort(Comparator.comparing(KidsNumberSub::getId));
        kidsNumberSubList = kidsNumberSubList.stream().sorted(Comparator.comparing(KidsNumberSub::getSortNumber)).collect(Collectors.toCollection(LinkedHashSet::new));

        Kindergarten kindergarten = kidsNumber.getReport().getKindergarten();

        List<AgeGroup> ageGroupList = new ArrayList<>();
        for (StayTime stayTime : kindergarten.getStayTimeList()) {
            ageGroupList.addAll(stayTime.getAgeGroupList());
        }

        ageGroupList.sort(Comparator.comparing(AgeGroup::getSortNumber));

        for (KidsNumberSub kidsNumberSub : kidsNumberSubList) {
            for (AgeGroup ageGroup : ageGroupList) {
                if (kidsNumberSub.getAgeGroup().getId().equals(ageGroup.getId())) {
                    PdfPCell cell0 = createCell(String.valueOf(number), 1, fontSize, 1);
                    cell0.setBackgroundColor(new BaseColor(218, 219, 221));
                    table.addCell(cell0);
                    PdfPCell cell = createCell(kidsNumberSub.getAgeGroup().getName(), 1, fontSize, 1);
                    cell.setBackgroundColor(new BaseColor(218, 219, 221));
                    table.addCell(cell);
                    PdfPCell cell1 = createCell(kidsNumberSub.getNumber().toString(), 1, fontSize, 1);
                    cell1.setBackgroundColor(new BaseColor(218, 219, 221));
                    table.addCell(cell1);

                    if (kidsNumberSub.getNumber() != 0) {
                        PdfPCell cell2 = createCell(map.get(kidsNumberSub.getAgeGroup().getId()).divide(BigDecimal.valueOf(kidsNumberSub.getNumber()), 2, RoundingMode.HALF_UP).toString(), 1, fontSize, 1);
                        cell2.setBackgroundColor(new BaseColor(218, 219, 221));
                        table.addCell(cell2);
                    } else {
                        PdfPCell cell2 = createCell(String.valueOf(0), 1, fontSize, 1);
                        cell2.setBackgroundColor(new BaseColor(218, 219, 221));
                        table.addCell(cell2);
                    }
                    number++;
                }
            }
        }

        return table;
    }

    public BigDecimal getFactWeight(BigDecimal weight, Report report, Integer productId, boolean fact) {

        if (fact) {
            Optional<InOut> optional = inOutRepo.findByInputOutput_LocalDateAndInputOutput_Kindergarten_IdAndProduct_Id(report.getDate(), report.getKindergarten().getId(), productId);
            if (optional.isPresent()) {
                InOut inOut = optional.get();

                BigDecimal pack = productPackService.checkPack(productId, report.getKindergarten().getDepartment().getId());

                if (pack != null) {
                    BigDecimal multiply = weight.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP).multiply(inOut.getPercentageOutput());

                    return multiply.multiply(BigDecimal.valueOf(1000)).divide(pack, 6, RoundingMode.HALF_UP);
                }

                return weight.divide(BigDecimal.valueOf(100), 3, RoundingMode.HALF_UP).multiply(inOut.getPercentageOutput());
            }
            return BigDecimal.ZERO;
        }

        BigDecimal pack = productPackService.checkPack(productId, report.getKindergarten().getDepartment().getId());

        if (pack != null) {
            return weight.multiply(BigDecimal.valueOf(1000)).divide(pack, 6, RoundingMode.HALF_UP);
        }
        return weight;
    }

    public String createPDFReport1(Report report) {

        String path = DEST + report.getId() + ".pdf";
        int fontSize = 7;

        File file = new File(path);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4.rotate());

        try {
            PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();

            Paragraph paragraph = new Paragraph(report.getKindergarten().getRegionalDepartment().getName()+" \n"+report.getKindergarten().getDepartment().getName()+" maktabgacha va maktab ta'limi bo`limi \n "+report.getKindergarten().getName() + " \n" + report.getDate() + " \n ");
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            Paragraph paragraph1 = new Paragraph(report.getMenuReport().getMultiMenuName() + "\n" + report.getMenuReport().getName() + " \n ");
            paragraph1.setAlignment(Element.ALIGN_CENTER);

            document.add(paragraph1);


            PdfPTable tableMenu = createTableReport1(report);

            MenuReport menuReport = report.getMenuReport();

            Set<ProductMenuDTO> productList = getProductList(menuReport);

            tableMenu.addCell(createCell("Narxi", 1, fontSize, 4));

            for (ProductMenuDTO productMenuDTO : productList) {
                tableMenu.addCell(createCell(productMenuDTO.getPrice().setScale(0, RoundingMode.HALF_UP).toString(), 1, fontSize, 1));
            }


            List<AgeGroupReport> ageGroupReportList = report.getAgeGroupReportList();
            Map<Integer, BigDecimal> map = new TreeMap<>();

            ageGroupReportList.sort(Comparator.comparing(AgeGroupReport::getSortNumber));

            for (AgeGroupReport ageGroupReport : ageGroupReportList) {
                BigDecimal weight = BigDecimal.valueOf(0);

                List<AgeGroupTotal> productListAgeGroup = addListProductAgeGroup(productList, ageGroupReport.getAgeGroupTotalList());

                tableMenu.addCell(createCell(ageGroupReport.getAgeGroupName() + " bir bolaga (gramm xis)", 1, fontSize, 4));
                for (AgeGroupTotal ageGroupTotal : productListAgeGroup) {
                    tableMenu.addCell(createCell(ageGroupTotal.getSanpinNorm().setScale(0, RoundingMode.HALF_UP).toString(), 1, fontSize, 1));
                }
                tableMenu.addCell(createCell("Jami miqdor (kg)", 1, fontSize, 4));
                for (AgeGroupTotal ageGroupTotal : productListAgeGroup) {

                    BigDecimal totalWeight = ageGroupTotal.getTotalWeight().divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP);
                    if (totalWeight.compareTo(BigDecimal.valueOf(0.01)) < 0) {
                        totalWeight = totalWeight.setScale(3, RoundingMode.HALF_UP);
                    } else {
                        totalWeight = totalWeight.setScale(3, RoundingMode.HALF_UP);
                    }

                    tableMenu.addCell(createCell(totalWeight.toString(), 1, fontSize, 1));
                }
                tableMenu.addCell(createCell("Summa", 1, fontSize, 4));
                for (AgeGroupTotal ageGroupTotal : productListAgeGroup) {
                    BigDecimal totalWeight = ageGroupTotal.getTotalWeight();

                    BigDecimal totalPrice = totalWeight.multiply(ageGroupTotal.getProductPrice()).setScale(0, RoundingMode.HALF_UP);
                    weight = weight.add(totalPrice);
                    tableMenu.addCell(createCell(totalPrice.toString(), 1, fontSize, 1));
                }
                int rowSpanNum = 4 + (productList.size() / 2);
                PdfPCell cell = createCell(ageGroupReport.getAgeGroupName() + " umumiy summa", 1, fontSize, rowSpanNum);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                tableMenu.addCell(cell);

                PdfPCell cell1 = createCell(weight.setScale(0, RoundingMode.HALF_UP).toString(), 1, fontSize, productList.size() + 4 - rowSpanNum);
                cell1.setBackgroundColor(BaseColor.LIGHT_GRAY);
                tableMenu.addCell(cell1);
                map.put(ageGroupReport.getAgeGroupId(), weight);

            }

            document.add(tableMenu);
            document.add(createTableReportKidsNumber(report, map));

            document.newPage();
        } catch (Exception r) {
            r.printStackTrace();
            return "";
        }

        document.close();

        report.setFilePathPdf(path);
        reportRepo.save(report);
        return path;
    }

    private PdfPTable createTableReport1(Report report) {

        MenuReport menuReport = report.getMenuReport();
        Set<ProductMenuDTO> productList = getProductList(menuReport);
        int size = productList.size();
        float[] aa = new float[size + 4];

        aa[0] = (1F);
        aa[1] = (4F);
        aa[2] = (3F);
        aa[3] = (2.5F);
        for (int i = 4; i < size + 4; i++) {
            aa[i] = (1.5F);
        }

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);

        int fontSize = 7;

        PdfPCell ovqatlar_vaqti = createCell("Ovqatlar vaqti", 1, fontSize, 1);
        ovqatlar_vaqti.setRotation(90);
        table.addCell(ovqatlar_vaqti);
        table.addCell(createCell("Taomlar nomi", 1, fontSize, 1));
        table.addCell(createCell("Yosh toifasi", 1, fontSize, 1));
        table.addCell(createCell("Bir kishi ovqat xajmi", 1, fontSize, 1));

        for (ProductMenuDTO menuDTO : productList) {
            PdfPCell cell = createCell(nameCustom(menuDTO.getName()), 1, fontSize, 1);
            cell.setRotation(90);
            table.addCell(cell);
        }

        List<MealTimeReport> mealTimeReportList = menuReport.getMealTimeReportList();

        mealTimeReportList.sort(Comparator.comparing(MealTimeReport::getSortNumber));


        for (MealTimeReport mealTimeReport : mealTimeReportList) {
            List<MealReport> mealReportList = mealTimeReport.getMealReportList();

            if (mealReportList.size() > 0) {

                String name = mealTimeReport.getMealTimeName();
                StringBuilder mealTimeName = new StringBuilder();
                int num = 1;
                for (String s : name.split(" ")) {
                    if (num == 2) {
                        mealTimeName.append("\n");
                    }
                    mealTimeName.append(s);
                    num++;
                }

                PdfPCell mealTimeCell = createCell(mealTimeName.toString(), mealTimeReport.getMealReportList().size() * mealReportList.get(0).getMealAgeStandardReportList().size(), fontSize, 1);
                mealTimeCell.setRotation(90);
                table.addCell(mealTimeCell);
                for (MealReport mealReport : mealReportList) {

                    List<MealAgeStandardReport> mealAgeStandardReportList = mealReport.getMealAgeStandardReportList();

                    mealAgeStandardReportList.sort(Comparator.comparing(MealAgeStandardReport::getSortNumber));

                    PdfPCell mealName = createCell(mealReport.getMealName(), mealAgeStandardReportList.size(), fontSize, 1);
                    table.addCell(mealName);

                    for (MealAgeStandardReport mealAgeStandardReport : mealAgeStandardReportList) {

                        PdfPCell ageGroupName = createCell(mealAgeStandardReport.getAgeGroupName(), 1, fontSize, 1);
                        table.addCell(ageGroupName);
                        PdfPCell ageGroupWeight = createCell(mealAgeStandardReport.getMealWeight().setScale(0, RoundingMode.HALF_UP).toString(), 1, fontSize, 1);
                        table.addCell(ageGroupWeight);

                        List<MealProductReport> mealProductReportList = addListProduct(productList, mealAgeStandardReport.getMealProductReportList());

                        for (MealProductReport mealProductReport : mealProductReportList) {

                            BigDecimal weight = mealProductReport.getWeight().divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP);
                            String value = weight.compareTo(BigDecimal.ZERO) == 0 ? "" : weight.setScale(weight.compareTo(BigDecimal.valueOf(0.01)) < 0 ? 3 : 2, RoundingMode.HALF_UP).toString();
                            table.addCell(createCell(value, 1, fontSize, 1));
                        }
                    }
                }
            }

        }
        return table;
    }

    public Set<ProductMenuDTO> getProductList(MenuReport menuReport) {
        HashSet<ProductMenuDTO> set = new HashSet<>();
        for (MealTimeReport mealTimeReport : menuReport.getMealTimeReportList()) {
            for (MealReport mealReport : mealTimeReport.getMealReportList()) {
                for (MealAgeStandardReport mealAgeStandardReport : mealReport.getMealAgeStandardReportList()) {
                    for (MealProductReport mealProductReport : mealAgeStandardReport.getMealProductReportList()) {

                        boolean res = true;
                        for (ProductMenuDTO productMenuDTO : set) {
                            if (productMenuDTO.getId().equals(mealProductReport.getIdProduct())) {
                                res = false;
                                break;
                            }
                        }
                        if (res) {
                            set.add(new ProductMenuDTO(mealProductReport.getIdProduct(), mealProductReport.getProductName(), BigDecimal.valueOf(0), null, BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0)));
                        }
                    }
                }
            }
        }
        return set.stream().sorted(Comparator.comparing(ProductMenuDTO::getName)).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public List<MealProductReport> addListProduct(Set<ProductMenuDTO> productList, List<MealProductReport> mealProductReportList) {
        for (ProductMenuDTO productMenuDTO : productList) {
            boolean res = true;
            for (MealProductReport mealProductReport : mealProductReportList) {
                if (productMenuDTO.getId().equals(mealProductReport.getIdProduct())) {
                    res = false;
                    break;
                }
            }
            if (res) {
                mealProductReportList.add(new MealProductReport(productMenuDTO.getName(), productMenuDTO.getId(), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO));
            }
        }

        mealProductReportList.sort(Comparator.comparing(MealProductReport::getProductName));

        return mealProductReportList;
    }

    public List<AgeGroupTotal> addListProductAgeGroup(Set<ProductMenuDTO> productList, List<AgeGroupTotal> productListAgeGroup) {
        for (ProductMenuDTO productMenuDTO : productList) {
            boolean res = true;
            for (AgeGroupTotal ageGroupTotal : productListAgeGroup) {
                if (productMenuDTO.getId().equals(ageGroupTotal.getProductId())) {
                    res = false;
                    break;
                }
            }
            if (res) {
                productListAgeGroup.add(new AgeGroupTotal(productMenuDTO.getId(), productMenuDTO.getName(), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO));
            }
        }

        productListAgeGroup.sort(Comparator.comparing(AgeGroupTotal::getProductName));

        return productListAgeGroup;
    }
}
