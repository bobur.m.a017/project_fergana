package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.kids.KidsNumberRepo;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.KidsNumberSubRepo;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;

@Service
@RequiredArgsConstructor
public class KindergartenPDFReporterByDistrict {
    private final KidsNumberRepo kidsNumberRepo;
    private final KindergartenRepo kindergartenRepo;
    private final KidsNumberSubRepo kidsNumberSubRepo;
    private final AgeGroupRepo ageGroupRepo;

    public String createPDFKindergarten(String paragraph, Timestamp startDate, Timestamp endDate, Users users) throws FileNotFoundException, DocumentException {


        File file = new File(DEST);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4);
        PdfWriter.getInstance(document, new FileOutputStream(DEST));
        document.open();
        document.add(new Paragraph(paragraph + " \n \n "));


        List<KidsNumberSub> subList;
        List<AgeGroup> ageGroupList = ageGroupRepo.findAllByDeleteIsFalse();


        int size = ageGroupList.size();

        float[] aa = new float[size + 3];

        aa[0] = (1.5F);
        aa[1] = (5F);

        for (int i = 2; i < size + 3; i++) {
            aa[i] = (1.5F);
        }
        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);
        int fontSize = 7;


        PdfPCell TR = createCell("T/R", 1, fontSize, 1);

        table.addCell(TR);
        table.addCell(createCell("Tuman nomi", 1, 8, 1));


        int num = 1;
        ageGroupList.sort(Comparator.comparing(AgeGroup::getSortNumber));

        for (AgeGroup ageGroup : ageGroupList) {
            table.addCell(createCell(ageGroup.getName(), 1, 8, 1));
        }
        table.addCell(createCell("Jami", 1, 8, 1));


        RegionalDepartment regionalDepartment = users.getRegionalDepartment();


        for (Department department : regionalDepartment.getDepartmentList()) {
            Integer total = 0;

            table.addCell(createCell(String.valueOf(num), 1, 8, 1));
            table.addCell(createCell(department.getName(), 1, 8, 1));

            for (AgeGroup ageGroup : ageGroupList) {
                List<KidsNumberSub> list = kidsNumberSubRepo.findAllByKidsNumber_DateBetweenAndKidsNumber_DeleteIsFalseAndKidsNumber_Report_Kindergarten_Department_IdAndAgeGroup_Id(startDate.toLocalDateTime().toLocalDate(), endDate.toLocalDateTime().toLocalDate(), department.getId(),ageGroup.getId());
                Integer totalSum = getTotalSum(list);
                total += totalSum;
                table.addCell(createCell(totalSum.toString(), 1, 8, 1));
            }
            table.addCell(createCell(total.toString(), 1, 8, 1));
            num++;
        }

        document.add(table);
        document.newPage();
        document.close();
        return DEST;
    }

    private Integer getTotalSum(List<KidsNumberSub> list) {
        Integer totalSum = 0;

        for (KidsNumberSub kidsNumberSub : list) {
            totalSum += kidsNumberSub.getNumber();
        }

        return totalSum;
    }


}
