package com.optimal.fergana.reporter.excel;

import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.MenuService;
import com.optimal.fergana.menu.ProductMenuDTO;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static com.optimal.fergana.statics.StaticMethods.style;
import static com.optimal.fergana.statics.StaticWords.DEST;

@Service
@RequiredArgsConstructor
public class MenuExcelReporter {
    private final MenuService menuService;
    private final ReportRepo reportRepo;
    // Java program to write data in excel sheet using java code

    // any exceptions need to be caught
    public void createExcelMenu(Menu menu) throws IOException {

        // workbook object
        XSSFWorkbook workbook = new XSSFWorkbook();


        // spreadsheet object
        XSSFSheet spreadsheet = workbook.createSheet(" Menu ");

        spreadsheet.setColumnWidth(0, 1500);
        spreadsheet.setColumnWidth(1, 6000);
        spreadsheet.setColumnWidth(2, 4000);


        //CELL STYLE
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setRotation((short) 90);
        style(font, cellStyle);


        XSSFCellStyle cellStyleWrapText = workbook.createCellStyle();
        cellStyleWrapText.setWrapText(true);
        style(font, cellStyleWrapText);


        XSSFCellStyle cellStyleCouple = workbook.createCellStyle();
        style(font, cellStyleCouple);

        int rowNum = 0;
        int cellNum = 0;


        Set<ProductMenuDTO> productList = menuService.getProductList(menu);

        XSSFRow row1 = spreadsheet.createRow(rowNum);


        XSSFCell cell1 = row1.createCell(cellNum++);
        cell1.setCellValue("Ovqatlar nomi");
        cell1.setCellStyle(cellStyle);


        XSSFCell cell2 = row1.createCell(cellNum++);
        cell2.setCellValue("Taomlar nomi");
        cell2.setCellStyle(workbook.createCellStyle());
        cell2.setCellStyle(cellStyleCouple);

        XSSFCell cell3 = row1.createCell(cellNum++);
        cell3.setCellValue("Yosh toifasi");
        cell3.setCellStyle(cellStyleCouple);


        XSSFCell cell4 = row1.createCell(cellNum++);
        cell4.setCellValue("Bir kishi ovqat xajmi");
        cell4.setCellStyle(cellStyleWrapText);


        for (ProductMenuDTO productMenuDTO : productList) {
            XSSFCell cell = row1.createCell(cellNum++);
            cell.setCellValue(productMenuDTO.getName());

            cell.setCellStyle(cellStyle);
        }


        int firstRowspan = 1;
        int lastRowspan = 0;
        int firstRowspanMeal = 1;
        int lastRowspanMeal = 0;
        int rowNumber = 1;

        List<MealTimeStandard> mealTimeStandardList = menu.getMealTimeStandardList();
        mealTimeStandardList.sort(Comparator.comparing(a -> a.getMealTime().getSortNumber()));

        for (MealTimeStandard mealTimeStandard : mealTimeStandardList) {
            for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandardList()) {
                lastRowspan = lastRowspan + mealAgeStandard.getAgeStandardList().size();
            }

            XSSFRow row = spreadsheet.createRow(firstRowspan);
            XSSFCell mealTimeCell = row.createCell(0);
            mealTimeCell.setCellValue(mealTimeStandard.getMealTime().getName());
            mealTimeCell.setCellStyle(cellStyle);
            spreadsheet.addMergedRegion(new CellRangeAddress(firstRowspan, lastRowspan, 0, 0));


            for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandardList()) {
                lastRowspanMeal = lastRowspanMeal + mealAgeStandard.getAgeStandardList().size();

                XSSFCell mealCell;
                XSSFRow row2 = null;
                if (rowNumber == firstRowspan) {
                    mealCell = row.createCell(1);
                } else {
                    row2 = spreadsheet.createRow(rowNumber);
                    mealCell = row2.createCell(1);
                }
                mealCell.setCellValue(mealAgeStandard.getMeal().getName());
                mealCell.setCellStyle(cellStyleWrapText);
                spreadsheet.addMergedRegion(new CellRangeAddress(firstRowspanMeal, lastRowspanMeal, 1, 1));

                List<AgeStandard> ageStandardList = mealAgeStandard.getAgeStandardList();

                ageStandardList.sort(Comparator.comparing(a -> a.getAgeGroup().getSortNumber()));

                for (AgeStandard ageStandard : ageStandardList) {
                    XSSFCell ageGroupCell;
                    XSSFCell ageGroupWeightCell;

                    XSSFRow row3;

                    if (rowNumber == firstRowspan) {
                        ageGroupCell = row.createCell(2);
                        ageGroupWeightCell = row.createCell(3);
                        row3 = row;

                    } else if (rowNumber == firstRowspanMeal) {
                        assert row2 != null;
                        ageGroupCell = row2.createCell(2);
                        ageGroupWeightCell = row2.createCell(3);
                        row3 = row2;
                    } else {
                        row3 = spreadsheet.createRow(rowNumber);
                        ageGroupCell = row3.createCell(2);
                        ageGroupWeightCell = row3.createCell(3);
                    }

                    ageGroupCell.setCellValue(ageStandard.getAgeGroup().getName());
                    ageGroupCell.setCellStyle(cellStyleWrapText);

                    ageGroupWeightCell.setCellValue(ageStandard.getWeight().multiply(BigDecimal.valueOf(1000)).setScale(0, RoundingMode.HALF_UP).toString());
                    ageGroupWeightCell.setCellStyle(cellStyleWrapText);
                    List<ProductMenuDTO> productListWeight = menuService.getProductList(ageStandard, ageStandard.getMealAgeStandard().getMeal(), menu);

                    int cell = 4;
                    for (ProductMenuDTO productMenuDTO : productListWeight) {
                        XSSFCell weight = row3.createCell(cell++);
                        weight.setCellValue(productMenuDTO.getWeight().toString());
                        weight.setCellStyle(cellStyleWrapText);
                    }
                    rowNumber++;
                }
                firstRowspanMeal = lastRowspanMeal + 1;
            }
            firstRowspan = lastRowspan + 1;
        }
        FileOutputStream out = new FileOutputStream(DEST +menu.getName()+".xlsx");
        workbook.write(out);
        out.close();
    }

    public String createExcelReport(Report report){


        Menu menu = report.getMenu();


        // workbook object
        XSSFWorkbook workbook = new XSSFWorkbook();


        // spreadsheet object
        XSSFSheet spreadsheet = workbook.createSheet(" Menu ");

        spreadsheet.setColumnWidth(0, 1500);
        spreadsheet.setColumnWidth(1, 6000);
        spreadsheet.setColumnWidth(2, 4000);


        //CELL STYLE
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setRotation((short) 90);
        style(font, cellStyle);


        XSSFCellStyle cellStyleWrapText = workbook.createCellStyle();
        cellStyleWrapText.setWrapText(true);
        style(font, cellStyleWrapText);

        XSSFCellStyle cellStyleCouple = workbook.createCellStyle();
        style(font, cellStyleCouple);

        int rowNum = 0;
        int cellNum = 0;


        Set<ProductMenuDTO> productList = menuService.getProductList(menu);

        XSSFRow row1 = spreadsheet.createRow(rowNum++);


        XSSFCell cell1 = row1.createCell(cellNum++);
        cell1.setCellValue("Ovqatlar nomi");
        cell1.setCellStyle(cellStyle);


        XSSFCell cell2 = row1.createCell(cellNum++);
        cell2.setCellValue("Taomlar nomi");
        cell2.setCellStyle(workbook.createCellStyle());
        cell2.setCellStyle(cellStyleCouple);

        XSSFCell cell3 = row1.createCell(cellNum++);
        cell3.setCellValue("Yosh toifasi");
        cell3.setCellStyle(cellStyleCouple);


        XSSFCell cell4 = row1.createCell(cellNum++);
        cell4.setCellValue("Bir kishi ovqat xajmi");
        cell4.setCellStyle(cellStyleWrapText);


        for (ProductMenuDTO productMenuDTO : productList) {
            XSSFCell cell = row1.createCell(cellNum++);
            cell.setCellValue(productMenuDTO.getName());

            cell.setCellStyle(cellStyle);
        }


        int firstRowspan = 1;
        int lastRowspan = 0;
        int firstRowspanMeal = 1;
        int lastRowspanMeal = 0;
        int rowNumber = 1;

        List<MealTimeStandard> mealTimeStandardList = menu.getMealTimeStandardList();
        mealTimeStandardList.sort(Comparator.comparing(a -> a.getMealTime().getSortNumber()));

        for (MealTimeStandard mealTimeStandard : mealTimeStandardList) {
            for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandardList()) {
                lastRowspan = lastRowspan + mealAgeStandard.getAgeStandardList().size();
            }

            XSSFRow row = spreadsheet.createRow(firstRowspan);
            XSSFCell mealTimeCell = row.createCell(0);
            mealTimeCell.setCellValue(mealTimeStandard.getMealTime().getName());
            mealTimeCell.setCellStyle(cellStyle);
            spreadsheet.addMergedRegion(new CellRangeAddress(firstRowspan, lastRowspan, 0, 0));


            for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandardList()) {
                lastRowspanMeal = lastRowspanMeal + mealAgeStandard.getAgeStandardList().size();

                XSSFCell mealCell;
                XSSFRow row2 = null;
                if (rowNumber == firstRowspan) {
                    mealCell = row.createCell(1);
                } else {
                    row2 = spreadsheet.createRow(rowNumber);
                    mealCell = row2.createCell(1);
                }
                mealCell.setCellValue(mealAgeStandard.getMeal().getName());
                mealCell.setCellStyle(cellStyleWrapText);
                spreadsheet.addMergedRegion(new CellRangeAddress(firstRowspanMeal, lastRowspanMeal, 1, 1));

                List<AgeStandard> ageStandardList = mealAgeStandard.getAgeStandardList();

                ageStandardList.sort(Comparator.comparing(a -> a.getAgeGroup().getSortNumber()));

                for (AgeStandard ageStandard : ageStandardList) {
                    XSSFCell ageGroupCell;
                    XSSFCell ageGroupWeightCell;

                    XSSFRow row3;

                    if (rowNumber == firstRowspan) {
                        ageGroupCell = row.createCell(2);
                        ageGroupWeightCell = row.createCell(3);
                        row3 = row;

                    } else if (rowNumber == firstRowspanMeal) {
                        assert row2 != null;
                        ageGroupCell = row2.createCell(2);
                        ageGroupWeightCell = row2.createCell(3);
                        row3 = row2;
                    } else {
                        row3 = spreadsheet.createRow(rowNumber);
                        ageGroupCell = row3.createCell(2);
                        ageGroupWeightCell = row3.createCell(3);
                    }

                    ageGroupCell.setCellValue(ageStandard.getAgeGroup().getName());
                    ageGroupCell.setCellStyle(cellStyleWrapText);

                    ageGroupWeightCell.setCellValue(ageStandard.getWeight().multiply(BigDecimal.valueOf(1000)).setScale(0, RoundingMode.HALF_UP).toString());
                    ageGroupWeightCell.setCellStyle(cellStyleWrapText);
                    List<ProductMenuDTO> productListWeight = menuService.getProductList(ageStandard, ageStandard.getMealAgeStandard().getMeal(), menu);

                    int cell = 4;
                    for (ProductMenuDTO productMenuDTO : productListWeight) {
                        XSSFCell weight = row3.createCell(cell++);
                        weight.setCellValue(productMenuDTO.getWeight().toString());
                        weight.setCellStyle(cellStyleWrapText);
                    }
                    rowNumber++;
                }
                firstRowspanMeal = lastRowspanMeal + 1;
            }
            firstRowspan = lastRowspan + 1;
        }

        String path = DEST + report.getId() + ".xlsx";

        try {
            FileOutputStream out = new FileOutputStream(path);
            workbook.write(out);
            out.close();
        }catch (Exception e){
            return "";
        }

        report.setFilePathExcel(path);
        reportRepo.save(report);

        return path;
    }

}
