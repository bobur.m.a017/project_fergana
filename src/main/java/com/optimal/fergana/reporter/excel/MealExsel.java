package com.optimal.fergana.reporter.excel;

import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.MenuService;
import com.optimal.fergana.menu.ProductMenuDTO;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.multiMenu.MultiMenuRepo;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static com.optimal.fergana.statics.StaticMethods.style;
import static com.optimal.fergana.statics.StaticWords.DEST;

@Service
@RequiredArgsConstructor
public class MealExsel {
    private final MultiMenuRepo multiMenuRepo;
    private final AgeStandardRepo ageStandardRepo;

    public void createExcel(UUID id) throws IOException {

        MultiMenu multiMenu = multiMenuRepo.findById(id).get();


        // workbook object
        XSSFWorkbook workbook = new XSSFWorkbook();


        // spreadsheet object
        XSSFSheet spreadsheet = workbook.createSheet("Meal");

        spreadsheet.setColumnWidth(0, 1500);
        spreadsheet.setColumnWidth(1, 6000);
        spreadsheet.setColumnWidth(2, 4000);


        //CELL STYLE
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setRotation((short) 90);
        style(font, cellStyle);


        XSSFCellStyle cellStyleWrapText = workbook.createCellStyle();
        cellStyleWrapText.setWrapText(true);
        style(font, cellStyleWrapText);


        XSSFCellStyle cellStyleCouple = workbook.createCellStyle();
        style(font, cellStyleCouple);

        int rowNum = 0;
        int cellNum = 0;

        XSSFRow row = spreadsheet.createRow(rowNum++);

        XSSFCell cell = row.createCell(cellNum++);
        cell.setCellValue("T/r");
        cell.setCellStyle(cellStyleCouple);

        XSSFCell cell1 = row.createCell(cellNum++);
        cell1.setCellValue("Taomlar nomi");
        cell1.setCellStyle(cellStyleCouple);

        XSSFCell cell01 = row.createCell(cellNum++);
        cell01.setCellValue("Taomlar vazni");
        cell01.setCellStyle(cellStyleCouple);

        XSSFCell cell2 = row.createCell(cellNum++);
        cell2.setCellValue("Maxsulot nomi");
        cell2.setCellStyle(cellStyleCouple);

        XSSFCell cell3 = row.createCell(cellNum++);
        cell3.setCellValue("Chiqitli miqdor");
        cell3.setCellStyle(cellStyleWrapText);

        XSSFCell cell4 = row.createCell(cellNum++);
        cell4.setCellValue("Chiqitsiz miqdor");
        cell4.setCellStyle(cellStyleWrapText);

        XSSFCell cell5 = row.createCell(cellNum++);
        cell5.setCellValue("Maxsulot toifasi");
        cell5.setCellStyle(cellStyleWrapText);


        List<Meal> mealList = new ArrayList<>();

        for (Menu menu : multiMenu.getMenuList()) {
            for (MealTimeStandard mealTimeStandard : menu.getMealTimeStandardList()) {
                for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandardList()) {
                    boolean res = true;
                    for (Meal meal : mealList) {
                        if (mealAgeStandard.getMeal().getId().equals(meal.getId())) {
                            res = false;
                            break;
                        }
                    }
                    if (res)
                        mealList.add(mealAgeStandard.getMeal());
                }
            }
        }

        mealList.sort(Comparator.comparing(Meal::getName));

        int number = 1;

        for (Meal meal : mealList) {
            List<ProductMeal> productMealList = meal.getProductMealList();

            productMealList.sort(Comparator.comparing(ProductMeal::getProductSort));

            int firstRow = rowNum;
            int lastRow = rowNum + productMealList.size() - 1;

            XSSFRow row1 = spreadsheet.createRow(rowNum);
            spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, 0, 0));
            XSSFCell cell6 = row1.createCell(0);
            cell6.setCellValue(number++);
            cell6.setCellStyle(cellStyleCouple);

            spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, 1, 1));
            XSSFCell cell7 = row1.createCell(1);
            cell7.setCellValue(meal.getName());
            cell7.setCellStyle(cellStyleCouple);

            spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, 2, 2));
            XSSFCell cell0 = row1.createCell(2);
            cell0.setCellValue(meal.getWeight().doubleValue());
            cell0.setCellStyle(cellStyleCouple);


            int index = 0;
            for (ProductMeal productMeal : productMealList) {
                if (index == 0) {
                    spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum++, 0, 0));
                    XSSFCell cell8 = row1.createCell(3);
                    cell8.setCellValue(productMeal.getProduct().getName());
                    cell8.setCellStyle(cellStyleCouple);

                    XSSFCell cell9 = row1.createCell(4);
                    cell9.setCellValue(productMeal.getWeight().setScale(2, RoundingMode.HALF_UP).doubleValue());
                    cell9.setCellStyle(cellStyleCouple);

                    XSSFCell cell10 = row1.createCell(5);
                    cell10.setCellValue(productMeal.getWaste().setScale(2, RoundingMode.HALF_UP).doubleValue());
                    cell10.setCellStyle(cellStyleCouple);

                    XSSFCell cell11 = row1.createCell(6);
                    cell11.setCellValue(productMeal.getProduct().getSanpinCategory().getName());
                    cell11.setCellStyle(cellStyleCouple);
                } else {
                    XSSFRow row2 = spreadsheet.createRow(rowNum);
                    spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum++, 0, 0));
                    XSSFCell cell8 = row2.createCell(3);
                    cell8.setCellValue(productMeal.getProduct().getName());
                    cell8.setCellStyle(cellStyleCouple);

                    XSSFCell cell9 = row2.createCell(4);
                    cell9.setCellValue(productMeal.getWeight().toString().replace('.', ','));
                    cell9.setCellStyle(cellStyleCouple);

                    XSSFCell cell10 = row2.createCell(5);
                    cell10.setCellValue(productMeal.getWaste().toString().replace('.', ','));
                    cell10.setCellStyle(cellStyleCouple);

                    XSSFCell cell11 = row2.createCell(6);
                    cell11.setCellValue(productMeal.getProduct().getSanpinCategory().getName());
                    cell11.setCellStyle(cellStyleCouple);
                }
                index++;
            }
        }


        FileOutputStream out = new FileOutputStream(DEST + System.currentTimeMillis() + "ovqatlar.xlsx");
        workbook.write(out);
        out.close();
    }

    public void createExcel2(UUID id) throws IOException {

        MultiMenu multiMenu = multiMenuRepo.findById(id).get();


        // workbook object
        XSSFWorkbook workbook = new XSSFWorkbook();


        // spreadsheet object
        XSSFSheet spreadsheet = workbook.createSheet("Meal");

        spreadsheet.setColumnWidth(0, 1500);
        spreadsheet.setColumnWidth(1, 6000);
        spreadsheet.setColumnWidth(2, 4000);


        //CELL STYLE
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setRotation((short) 90);
        style(font, cellStyle);


        XSSFCellStyle cellStyleWrapText = workbook.createCellStyle();
        cellStyleWrapText.setWrapText(true);
        style(font, cellStyleWrapText);


        XSSFCellStyle cellStyleCouple = workbook.createCellStyle();
        style(font, cellStyleCouple);

        int rowNum = 0;
        int cellNum = 0;

        XSSFRow row = spreadsheet.createRow(rowNum++);

        XSSFCell cell = row.createCell(cellNum++);
        cell.setCellValue("T/r");
        cell.setCellStyle(cellStyleCouple);

        XSSFCell cell1 = row.createCell(cellNum++);
        cell1.setCellValue("Taomlar nomi");
        cell1.setCellStyle(cellStyleCouple);

        XSSFCell cell01 = row.createCell(cellNum++);
        cell01.setCellValue("Taomlar vazni");
        cell01.setCellStyle(cellStyleCouple);

        XSSFCell cell2 = row.createCell(cellNum++);
        cell2.setCellValue("Maxsulot nomi");
        cell2.setCellStyle(cellStyleCouple);

        XSSFCell cell3 = row.createCell(cellNum++);
        cell3.setCellValue("Chiqitli miqdor");
        cell3.setCellStyle(cellStyleWrapText);

        XSSFCell cell4 = row.createCell(cellNum++);
        cell4.setCellValue("Chiqitsiz miqdor");
        cell4.setCellStyle(cellStyleWrapText);

        XSSFCell cell5 = row.createCell(cellNum++);
        cell5.setCellValue("Maxsulot toifasi");
        cell5.setCellStyle(cellStyleWrapText);


        List<Meal> mealList = new ArrayList<>();
        int number = 1;

        for (Menu menu : multiMenu.getMenuList()) {
            for (MealTimeStandard mealTimeStandard : menu.getMealTimeStandardList()) {
                for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandardList()) {
                    boolean res = true;
                    for (Meal meal : mealList) {
                        if (mealAgeStandard.getMeal().getId().equals(meal.getId())) {
                            res = false;
                            break;
                        }
                    }
                    if (res)
                        mealList.add(mealAgeStandard.getMeal());
                }
            }
        }

        mealList.sort(Comparator.comparing(Meal::getName));

        for (Meal meal : mealList) {
            List<ProductMeal> productMealList = meal.getProductMealList();
            productMealList.sort(Comparator.comparing(ProductMeal::getProductSort));
            for (ProductMeal productMeal : productMealList) {

                XSSFRow row1 = spreadsheet.createRow(rowNum);
                spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum++, 0, 0));
                XSSFCell cell6 = row1.createCell(0);
                cell6.setCellValue(number++);
                cell6.setCellStyle(cellStyleCouple);

                XSSFCell cell7 = row1.createCell(1);
                cell7.setCellValue(meal.getName());
                cell7.setCellStyle(cellStyleCouple);

                XSSFCell cell0 = row1.createCell(2);
                cell0.setCellValue(meal.getWeight().doubleValue());
                cell0.setCellStyle(cellStyleCouple);

                XSSFCell cell8 = row1.createCell(3);
                cell8.setCellValue(productMeal.getProduct().getName());
                cell8.setCellStyle(cellStyleCouple);

                XSSFCell cell9 = row1.createCell(4);
                cell9.setCellValue(productMeal.getWeight().toString().replace('.', ','));
                cell9.setCellStyle(cellStyleCouple);

                XSSFCell cell10 = row1.createCell(5);
                cell10.setCellValue(productMeal.getWaste().toString().replace('.', ','));
                cell10.setCellStyle(cellStyleCouple);

                XSSFCell cell11 = row1.createCell(6);
                cell11.setCellValue(productMeal.getProduct().getSanpinCategory().getName());
                cell11.setCellStyle(cellStyleCouple);

            }
        }


        FileOutputStream out = new FileOutputStream(DEST + System.currentTimeMillis() + "ovqatlar.xlsx");
        workbook.write(out);
        out.close();
    }
}
