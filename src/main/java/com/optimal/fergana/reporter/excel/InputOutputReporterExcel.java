package com.optimal.fergana.reporter.excel;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.inputOutput.inOutAgeGroup.InOutAgeGroup;
import com.optimal.fergana.inputOutput.inOutAgeGroup.InOutAgeGroupRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.users.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static com.optimal.fergana.statics.StaticMethods.style;
import static com.optimal.fergana.statics.StaticWords.DEST;

@Service
@RequiredArgsConstructor
public class InputOutputReporterExcel {

    private final UserService userService;
    private final InOutAgeGroupRepo inOutAgeGroupRepo;
    private final ProductRepo productRepo;
    private final AgeGroupRepo ageGroupRepo;


    public String createExcelInputOutput(LocalDate startDate, LocalDate endDate, Kindergarten kindergarten) throws IOException {

        String path = DEST + System.currentTimeMillis() + ".xlsx";
        // workbook object
        XSSFWorkbook workbook = new XSSFWorkbook();

        //CELL STYLE
        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");


        List<AgeGroup> all = ageGroupRepo.findAll();

        for (AgeGroup ageGroup : all) {
            getTable(workbook, font, startDate, endDate, kindergarten, ageGroup);
        }

        FileOutputStream out = new FileOutputStream(path);
        workbook.write(out);
        out.close();
        return path;
    }

    public String getNumber(BigDecimal num) {

//        return num.toString().replace(".",",");
        return num.toString();
    }

    public void getTable(XSSFWorkbook workbook, XSSFFont font, LocalDate startDate, LocalDate endDate, Kindergarten kindergarten, AgeGroup ageGroup) {


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        style(font, cellStyle);
        cellStyle.setRotation((short) 0);

        XSSFSheet spreadsheet = workbook.createSheet(ageGroup.getName());

        spreadsheet.setColumnWidth(0, 1500);
        spreadsheet.setColumnWidth(1, 6000);
        spreadsheet.setColumnWidth(2, 4000);


        Set<LocalDate> dateList = new TreeSet<>();

        for (int i = startDate.getDayOfMonth(); i <= endDate.getDayOfMonth(); i++) {
            dateList.add(LocalDate.of(startDate.getYear(), startDate.getMonthValue(), i));
        }
        XSSFRow row0 = spreadsheet.createRow(0);

        spreadsheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
        spreadsheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));


        XSSFCell cell0 = row0.createCell(0);
        cell0.setCellValue("T/R");
        cell0.setCellStyle(cellStyle);
        XSSFCell cell1 = row0.createCell(1);
        cell1.setCellStyle(cellStyle);

        cell1.setCellValue("Maxsulot nomi");

        XSSFCell cell2 = row0.createCell(2);
        cell2.setCellValue("Jami oy davomida");
        cell2.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 3));

        int colspan = 4;
        for (LocalDate date : dateList) {
            XSSFCell cell4 = row0.createCell(colspan);
            cell4.setCellValue(date.toString());
            cell4.setCellStyle(cellStyle);

            spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, colspan, ++colspan));
            colspan++;
        }

        XSSFRow row1 = spreadsheet.createRow(1);
        int cellNum = 2;
        for (int i = 0; i < dateList.size() + 1; i++) {
            XSSFCell cellMiqdor = row1.createCell(cellNum++);
            cellMiqdor.setCellValue("Miqdor");
            cellMiqdor.setCellStyle(cellStyle);

            XSSFCell cellSumma = row1.createCell(cellNum++);
            cellSumma.setCellValue("Summa");
            cellSumma.setCellStyle(cellStyle);

        }

        int numTr = 1;
        int rowNum = 2;


        for (Product product : productRepo.findAll()) {
            cellNum = 0;

            BigDecimal totalWeight = BigDecimal.valueOf(0);
            BigDecimal totalPrice = BigDecimal.valueOf(0);

            List<InOutAgeGroup> list = inOutAgeGroupRepo.findAllByInOut_InputOutput_Kindergarten_IdAndProduct_IdAndAgeGroup_IdAndInOut_InputOutput_LocalDateBetween(kindergarten.getId(), product.getId(), ageGroup.getId(), startDate, endDate);

            if (list.size() > 0) {

                XSSFRow row3 = spreadsheet.createRow(rowNum++);

                XSSFCell numTrCel = row3.createCell(cellNum++);
                numTrCel.setCellValue(numTr);
                numTrCel.setCellStyle(cellStyle);

                XSSFCell numTrCel1 = row3.createCell(cellNum++);
                numTrCel1.setCellValue(product.getName());
                numTrCel1.setCellStyle(cellStyle);

                XSSFCell numTrCel2 = row3.createCell(cellNum++);
                numTrCel2.setCellValue(0);
                numTrCel2.setCellStyle(cellStyle);

                XSSFCell numTrCel3 = row3.createCell(cellNum++);
                numTrCel3.setCellValue(0);
                numTrCel3.setCellStyle(cellStyle);

                for (LocalDate date : dateList) {
                    BigDecimal weight = BigDecimal.valueOf(0);
                    BigDecimal sum = BigDecimal.valueOf(0);
                    for (InOutAgeGroup inOutAgeGroup : list) {
                        if (date.equals(inOutAgeGroup.getDate())) {
                            weight = weight.add(inOutAgeGroup.getOutputWeight());
                            sum = sum.add(inOutAgeGroup.getTotalPrice());
                        }
                    }
                    weight = weight.setScale(2, RoundingMode.HALF_UP);
                    sum = sum.setScale(2, RoundingMode.HALF_UP);


                    totalWeight = totalWeight.add(weight);
                    totalPrice = totalPrice.add(sum);


                    XSSFCell cellMiqdor1 = row3.createCell(cellNum++);
                    cellMiqdor1.setCellValue(Double.parseDouble(getNumber(weight)));
                    cellMiqdor1.setCellStyle(cellStyle);

                    XSSFCell cellSum1 = row3.createCell(cellNum++);
                    cellSum1.setCellValue(Double.parseDouble(getNumber(sum)));
                    cellSum1.setCellStyle(cellStyle);
                }

                numTrCel2.setCellValue(Double.parseDouble(getNumber(totalWeight.setScale(2, RoundingMode.HALF_UP))));
                numTrCel3.setCellValue(Double.parseDouble(getNumber(totalPrice.setScale(2, RoundingMode.HALF_UP))));

                numTr++;
            }
        }

    }
}
