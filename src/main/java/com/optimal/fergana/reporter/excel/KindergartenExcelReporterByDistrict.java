package com.optimal.fergana.reporter.excel;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;

public class KindergartenExcelReporterByDistrict {
    public void createExcelKindergarten() throws IOException {

        // workbook object
        XSSFWorkbook workbook = new XSSFWorkbook();


        // spreadsheet object
        XSSFSheet spreadsheet = workbook.createSheet(" Menu ");

        spreadsheet.setColumnWidth(0, 1500);
        spreadsheet.setColumnWidth(1, 6000);

        spreadsheet.setColumnWidth(2, 4000);
    }
}
