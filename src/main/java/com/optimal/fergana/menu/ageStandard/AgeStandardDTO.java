package com.optimal.fergana.menu.ageStandard;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AgeStandardDTO {

    private Double weight;
    private Integer ageGroupId;
}
