package com.optimal.fergana.menu.ageStandard;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public interface AgeStandardRepo extends JpaRepository<AgeStandard, UUID> {

    List<AgeStandard> findAllByMealAgeStandard_MealTimeStandard_Menu_MultiMenu_Id(UUID mealAgeStandard_mealTimeStandard_menu_multiMenu_id);

    Vector<AgeStandard> findAllByMealAgeStandard_MealTimeStandard_Menu_Id(UUID mealAgeStandard_mealTimeStandard_menu_id);

    Vector<AgeStandard> findAllByMealAgeStandard_MealTimeStandard_Menu_IdAndAgeGroup_Id(UUID mealAgeStandard_mealTimeStandard_menu_id, Integer ageGroupId);



}
