package com.optimal.fergana.menu.ageStandard;

import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;

import java.util.List;

public interface AgeStandardServiceInter {
    List<AgeStandard> add(List<AgeStandardDTO> dtoList, MealAgeStandard mealAgeStandard);
}
