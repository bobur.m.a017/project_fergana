package com.optimal.fergana.menu;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@AllArgsConstructor
@NotNull
@Getter
@Setter
public class ProductMenuDTO {

    private Integer id;
    private String name;
    private BigDecimal weight;
    private Integer sanpinCategoryId;
    private BigDecimal price;
    private BigDecimal sanpinMeyyor;
    private BigDecimal totalWeight;

    public ProductMenuDTO(Integer id, BigDecimal weight) {
        this.id = id;
        this.weight = weight;
    }
}
