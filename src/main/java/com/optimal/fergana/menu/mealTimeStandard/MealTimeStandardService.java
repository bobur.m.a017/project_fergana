package com.optimal.fergana.menu.mealTimeStandard;

import com.optimal.fergana.ageGroup.AgeGroupInterface;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandardService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MealTimeStandardService implements MealTimeStandardInterface, AgeGroupInterface,MealTimeStandardServiceInter {

    private final MealAgeStandardService mealAgeStandardService;

    public List<MealTimeStandardResDTO> getAll(Menu menu) {
        List<MealTimeStandardResDTO> list = new ArrayList<>();
        List<MealTimeStandard> mealTimeStandardList = menu.getMealTimeStandardList();

        mealTimeStandardList.sort(Comparator.comparing(MealTimeStandard::getSortNumber));

        for (MealTimeStandard mealTimeStandard : mealTimeStandardList) {
            MealTimeStandardResDTO dto = parser(mealTimeStandard);
            dto.setMealAgeStandardList(mealAgeStandardService.getAll(mealTimeStandard));
            dto.setAgeStandardList(parseList(mealTimeStandard.getMealTime().getAgeGroupList()));
            list.add(dto);
        }
        return list;
    }
}
