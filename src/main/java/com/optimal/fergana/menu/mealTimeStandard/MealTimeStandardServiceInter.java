package com.optimal.fergana.menu.mealTimeStandard;

import com.optimal.fergana.menu.Menu;

import java.util.List;

public interface MealTimeStandardServiceInter {
    List<MealTimeStandardResDTO> getAll(Menu menu);
}
