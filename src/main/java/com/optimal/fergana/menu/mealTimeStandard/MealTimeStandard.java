package com.optimal.fergana.menu.mealTimeStandard;


import com.optimal.fergana.mealTime.MealTime;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealTimeStandard {
    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate;
    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToOne
    private MealTime mealTime;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mealTimeStandard")
    private List<MealAgeStandard> mealAgeStandardList;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private Menu menu;

    public Integer sortNumber;

    public MealTimeStandard(MealTime mealTime, Menu menu) {
        this.mealTime = mealTime;
        this.menu = menu;
        this.sortNumber = mealTime.getSortNumber();
    }
}
