package com.optimal.fergana.menu;

import com.optimal.fergana.kindergarten.dto.KindergartenByAddressDTO;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddMenuDTO {
    @NotNull
    private Timestamp date;
    @NotNull
    private List<KindergartenByAddressDTO> kinList;
}
