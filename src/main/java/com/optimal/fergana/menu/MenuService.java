package com.optimal.fergana.menu;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.KidsNumberSubRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.kindergarten.dto.DepartmentDTOMenu;
import com.optimal.fergana.kindergarten.dto.KindergartenByAddressDTO;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.mealTime.MealTime;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandardService;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.multiMenu.MultiMenuRepo;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNorm;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNormRepo;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.report.menuReport.MenuReport;
import com.optimal.fergana.report.menuReport.MenuReportRepo;
import com.optimal.fergana.report.menuReport.MenuReportService;
import com.optimal.fergana.stayTime.StayTime;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.permission.PermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.*;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class MenuService implements MenuServiceInter {

    private final MealTimeStandardService mealTimeStandardService;
    private final KindergartenRepo kindergartenRepo;
    private final ReportRepo reportRepo;
    private final CustomConverter converter;
    private final DepartmentRepo departmentRepo;
    private final MultiMenuRepo multiMenuRepo;
    private final AgeStandardRepo ageStandardRepo;
    private final KidsNumberSubRepo kidsNumberSubRepo;
    private final DailyNormRepo dailyNormRepo;
    private final MenuRepo menuRepo;
    private final PermissionService permissionService;
    private final MenuReportService menuReportService;
    private final MenuReportRepo menuReportRepo;


    public List<Menu> add(MultiMenu multiMenu, Integer day, List<MealTime> mealTimeList) {
        List<Menu> list = new ArrayList<>();
        for (int i = 1; i <= day; i++) {
            Menu menu = new Menu();
            List<MealTimeStandard> mealTimeStandardList = new ArrayList<>();
            for (MealTime mealTime : mealTimeList) {
                mealTimeStandardList.add(new MealTimeStandard(mealTime, menu));
            }
            menu.setMultiMenu(multiMenu);
            menu.setName(i + "-kunlik taomnoma");
            menu.setMealTimeStandardList(mealTimeStandardList);
            menu.setNumber(i);
            list.add(menu);
        }
        return list;
    }

    public List<MenuResDTO> getAll(MultiMenu multiMenu) {

        List<MenuResDTO> list = new ArrayList<>();
        for (Menu menu : multiMenu.getMenuList()) {
            MenuResDTO dto = converter.menuToDTO(menu);
            dto.setMealTimeStandardList(mealTimeStandardService.getAll(menu));
            list.add(dto);
        }
        return list;
    }

    @Override
    public StateMessage replace(UUID menuId, LocalDate date, Users users) {

        if (users.getKindergarten() != null) {
            Kindergarten kindergarten = users.getKindergarten();
            Optional<Report> optionalReport = reportRepo.findByKindergarten_IdAndYearAndMonthAndDay(kindergarten.getId(), date.getYear(), date.getMonthValue(), date.getDayOfMonth());
            Optional<Menu> optionalMenu = menuRepo.findById(menuId);
            if (optionalReport.isPresent() && optionalMenu.isPresent()) {
                Report report = optionalReport.get();
                Menu menu = optionalMenu.get();

                report.setMenu(menu);
                report.setFilePathPdf(null);
                report.setFilePathExcel(null);

                if (report.getMenuReport() != null) {
                    MenuReport menuReport = report.getMenuReport();
                    report.setMenuReport(null);
                    menuReportRepo.delete(menuReport);
                }

                Thread thread = new Thread(() -> {
                    report.setMenuReport(menuReportService.parseMenu(report.getMenu(), report));
                    Report save = reportRepo.save(report);
                    menuReportService.addAgeGroupReport(save);
                });
                thread.start();


                return new StateMessage(date + " kun uchun taomnoma muvaffaqiyatli o`zgartirildi", true, 200);
            }
        }
        return new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
    }

    public MenuResDTO getOne(UUID menuId) {

        Optional<Menu> optionalMenu = menuRepo.findById(menuId);
        if (optionalMenu.isPresent()) {
            Menu menu = optionalMenu.get();
            MenuResDTO dto = converter.menuToDTO(menu);
            dto.setMealTimeStandardList(mealTimeStandardService.getAll(menu));
            return dto;
        }

        return null;
    }

    public List<ProductMenuDTO> getProductListByReport(AgeStandard ageStandard, Meal meal, Menu menu, Report report) {

        Set<ProductMenuDTO> productSet = getProductList(menu);

        BigDecimal number = BigDecimal.valueOf(0);

        Optional<KidsNumberSub> kidsNumberSubOptional = kidsNumberSubRepo.findByAgeGroup_IdAndKidsNumber_Report_Id(ageStandard.getAgeGroup().getId(), report.getId());
        if (kidsNumberSubOptional.isPresent()) {
            KidsNumberSub kidsNumberSub = kidsNumberSubOptional.get();
            number = BigDecimal.valueOf(kidsNumberSub.getNumber());
        }

        for (ProductMeal productMeal : meal.getProductMealList()) {
            BigDecimal weight = (productMeal.getWeight().divide(meal.getWeight(), 10, RoundingMode.HALF_UP)).multiply(ageStandard.getWeight());
            addList(productSet, productMeal.getProduct(), weight.multiply(number));
        }

        List<ProductMenuDTO> list = new ArrayList<>(productSet.stream().toList());
        list.sort(Comparator.comparing(ProductMenuDTO::getName));
        return list;
    }

    public Vector<ProductMenuDTO> getAllProductWeightBtMenu(Vector<ProductMenuDTO> productList, Menu menu, AgeGroup ageGroup, Report report) {

        Vector<AgeStandard> list = ageStandardRepo.findAllByMealAgeStandard_MealTimeStandard_Menu_IdAndAgeGroup_Id(menu.getId(), ageGroup.getId());
        for (int a = 0; a < productList.size(); a++) {

            Optional<DailyNorm> optionalDailyNorm = dailyNormRepo.findByAgeGroup_IdAndSanpinCategory_Id(ageGroup.getId(), productList.get(a).getSanpinCategoryId());

            BigDecimal sanpinMeyyor = BigDecimal.valueOf(0);

            if (optionalDailyNorm.isPresent()) {
                sanpinMeyyor = optionalDailyNorm.get().getWeight().setScale(0, RoundingMode.HALF_UP);
            }
            productList.get(a).setSanpinMeyyor(sanpinMeyyor);


            BigDecimal weight = BigDecimal.valueOf(0);

            BigDecimal number = BigDecimal.valueOf(1);

            Optional<KidsNumberSub> kidsNumberSubOptional = kidsNumberSubRepo.findByAgeGroup_IdAndKidsNumber_Report_IdAndKidsNumberDeleteIsFalse(ageGroup.getId(), report.getId());
            if (kidsNumberSubOptional.isPresent()) {
                KidsNumberSub kidsNumberSub = kidsNumberSubOptional.get();
                number = BigDecimal.valueOf(kidsNumberSub.getNumber());
            }


            for (AgeStandard ageStandard : list) {
                Meal meal = ageStandard.getMealAgeStandard().getMeal();
                Vector<ProductMeal> productMealList = new Vector<>(meal.getProductMealList());
                for (ProductMeal productMeal : productMealList) {

                    Product product = productMeal.getProduct();
                    if (product != null) {
                        if (productList.get(a).getId().equals(product.getId()))
                            weight = weight.add(((productMeal.getWeight().divide(meal.getWeight(), 6, RoundingMode.HALF_UP)).multiply(ageStandard.getWeight())).multiply(number));
                    }
                }
            }
            productList.get(a).setTotalWeight(weight.divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP));
        }

        return productList;
    }

    public void addList(Set<ProductMenuDTO> set, Product product, BigDecimal weight) {

        for (ProductMenuDTO productMenuDTO : set) {
            if (productMenuDTO.getId().equals(product.getId())) {
                productMenuDTO.setWeight(productMenuDTO.getWeight().add(weight));
                break;
            }
        }
    }

    public Set<ProductMenuDTO> getProductList(Menu menu) {
        HashSet<ProductMenuDTO> set = new HashSet<>();
        for (MealTimeStandard mealTimeStandard : menu.getMealTimeStandardList()) {
            for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandardList()) {
                for (ProductMeal productMeal : mealAgeStandard.getMeal().getProductMealList()) {
                    addListProduct(set, productMeal.getProduct());
                }
            }
        }
        return set.stream().sorted(Comparator.comparing(ProductMenuDTO::getName)).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public void addListProduct(Set<ProductMenuDTO> set, Product product) {
        boolean res = true;
        for (ProductMenuDTO productMenuDTO : set) {
            if (productMenuDTO.getId().equals(product.getId())) {
                res = false;
                break;
            }
        }
        if (res) {
            set.add(new ProductMenuDTO(product.getId(), product.getName(), BigDecimal.valueOf(0), product.getSanpinCategory().getId(), BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0)));
        }
    }

    public List<ProductMenuDTO> getProductList(AgeStandard ageStandard, Meal meal, Menu menu) {  //ishlagan bir kunda

        Set<ProductMenuDTO> productSet = getProductList(menu);

        for (ProductMeal productMeal : meal.getProductMealList()) {
            BigDecimal weight = (productMeal.getWeight().divide(meal.getWeight(), 10, RoundingMode.HALF_UP)).multiply(ageStandard.getWeight());
            addList(productSet, productMeal.getProduct(), weight);
        }

        List<ProductMenuDTO> list = new ArrayList<>(productSet.stream().toList());
        list.sort(Comparator.comparing(ProductMenuDTO::getName));
        return list;
    }


    public List<KindergartenByAddressDTO> getKindergartenByDepartmentIdAddMenu(Integer id, Timestamp date, Users users, boolean res) {

        LocalDate localDate = date.toLocalDateTime().toLocalDate();

        List<KindergartenByAddressDTO> list = new ArrayList<>();
        Optional<Department> optionalDepartment = departmentRepo.findById(id);

        if (optionalDepartment.isPresent() && date.getTime() > System.currentTimeMillis()) {
            Department department = optionalDepartment.get();

            if (users.getDepartment() != null)
                department = users.getDepartment();


            List<Kindergarten> kindergartenList = new ArrayList<>();
            List<Kindergarten> kindergartenListTest = new ArrayList<>();

            if (users.getKindergarten() != null) {
                kindergartenListTest.add(users.getKindergarten());
            } else {
                kindergartenListTest = department.getKindergartenList();
            }

            if (res) {
                for (Kindergarten kindergarten : kindergartenListTest) {
                    if (!permissionService.checkKindergarten(kindergarten)) {
                        kindergartenList.add(kindergarten);
                    }
                }
            } else {
                kindergartenList.addAll(kindergartenListTest);
            }


            kindergartenList.sort(Comparator.comparing(Kindergarten::getNumber));

            for (Kindergarten kindergarten : kindergartenList) {
                if (!kindergarten.getDelete()) {
                    KindergartenByAddressDTO dto = converter.kindergartenToByAddressDTO(kindergarten);
                    Optional<Report> optional = reportRepo.findByKindergarten_IdAndYearAndMonthAndDay(kindergarten.getId(), localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth());
                    if (optional.isPresent()) {

                        Report report = optional.get();

                        if (report.isEdit()) {
                            dto.setReportId(report.getId());
                            dto.setMenuName(report.getMenu().getMultiMenu().getName() + "\n" + report.getMenu().getName());
                            dto.setAttached(report.getMenu() != null);
                            dto.setDate(date);
                            dto.setStayTimeNumber(getStatTimeKindergarten(kindergarten));
                            list.add(dto);
                        }
                    } else {
                        dto.setDate(date);
                        dto.setAttached(false);
                        dto.setStayTimeNumber(getStatTimeKindergarten(kindergarten));
                        list.add(dto);
                    }
                }
            }
        }
        list.sort(Comparator.comparing(KindergartenByAddressDTO::getNumber));
        return list;
    }

    public int getStatTimeKindergarten(Kindergarten kindergarten) {

        int res = 0;
        int num = 0;
        for (StayTime stayTime : kindergarten.getStayTimeList()) {

            int size = stayTime.getMealTimeList().size();

            if (num == 0) {
                res = size;
            } else {
                if (res < size) {
                    res = size;
                }
            }
            num++;
        }

        return res;
    }

    public List<DepartmentDTOMenu> getDepartmentIdAddMenu(Timestamp date, Users users, boolean res) {

        LocalDate localDate = date.toLocalDateTime().toLocalDate();
        List<Department> departmentList = new ArrayList<>();
        if (users.getDepartment() != null) {
            departmentList.add(users.getDepartment());
        } else {
            List<Department> departmentListTest = users.getRegionalDepartment().getDepartmentList();

            if (res) {
                for (Department department : departmentListTest) {
                    if (!permissionService.checkDepartment(department)) {
                        departmentList.add(department);
                    }
                }
            } else {
                departmentList.addAll(departmentListTest);
            }
        }


        List<DepartmentDTOMenu> departmentDTOMenuList = converter.departmentToDTO(departmentList);

        for (DepartmentDTOMenu dto : departmentDTOMenuList) {


            int attached = reportRepo.countAllByKindergarten_Department_IdAndYearAndMonthAndDayAndMenuNotNull(dto.getDepartmentId(), localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth());
            int all = kindergartenRepo.countAllByDepartment_IdAndDeleteIsFalse(dto.getDepartmentId());
            if (users.getKindergarten() != null) {
                all = 1;
                attached = reportRepo.countAllByKindergarten_IdAndYearAndMonthAndDayAndMenuNotNull(dto.getDepartmentId(), localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth());
            }

            dto.setAttached(attached > 0);
            dto.setAttachedNumber(attached);
            dto.setNotAttachedNumber(all - attached);
            dto.setDate(date);
        }
        departmentDTOMenuList.sort(Comparator.comparing(DepartmentDTOMenu::getDepartmentName));

        return departmentDTOMenuList;
    }

    synchronized
    public StateMessage addMenuKindergarten(UUID menuId, List<AddMenuDTO> dtoList) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        dtoList.sort(Comparator.comparing(AddMenuDTO::getDate));

        Vector<Report> reportList = new Vector<>();
        Optional<MultiMenu> optionalMultiMenu = multiMenuRepo.findById(menuId);

        if (optionalMultiMenu.isPresent()) {
            MultiMenu multiMenu = optionalMultiMenu.get();
//            if (dtoList.size() % multiMenu.getMenuList().size() == 0) {

            List<Menu> menuList = getMenuList(multiMenu, dtoList.size());
            int number = 0;

            for (AddMenuDTO addMenuDTO : dtoList) {
                Menu menu = menuList.get(number);

                LocalDate localDate = addMenuDTO.getDate().toLocalDateTime().toLocalDate();
//                LocalDate localDate = addMenuDTO.getDate();

                int year = localDate.getYear();
                int month = localDate.getMonthValue();
                int day = localDate.getDayOfMonth();

                Vector<Kindergarten> kindergartenList = getKindergartenList(addMenuDTO.getKinList());
                createReport(reportList, kindergartenList, day, month, year, menu);
                number++;
            }
            message = Message.SUCCESS_UZ;
//            }
            JDBC(reportList);
        }
        return new StateMessage().parse(message);
    }

    public Vector<Kindergarten> getKindergartenList(List<KindergartenByAddressDTO> dtoList) {

        Vector<Integer> idList = new Vector<>();
        int num = dtoList.size() / 4;

        ThreadGroup threadGroup = new ThreadGroup("myThread");

        Thread thread = new Thread(threadGroup, () -> {
            for (int i = 0; i < num; i++) {
                idList.addElement(dtoList.get(i).getId());
            }
        });
        thread.start();

        Thread thread1 = new Thread(threadGroup, () -> {
            for (int i = num; i < num * 2; i++) {
                idList.addElement(dtoList.get(i).getId());
            }
        });
        thread1.start();


        Thread thread2 = new Thread(threadGroup, () -> {
            for (int i = num * 2; i < num * 3; i++) {
                idList.addElement(dtoList.get(i).getId());
            }
        });
        thread2.start();

        for (int i = num * 3; i < dtoList.size(); i++) {
            idList.addElement(dtoList.get(i).getId());
        }

        while (true) {
            if (threadGroup.activeCount() == 0) {
                break;
            }
        }
        Vector<Kindergarten> list = new Vector<>(kindergartenRepo.findAllById(idList));

        return list;
    }

    public List<Menu> getMenuList(MultiMenu multiMenu, Integer size) {

        List<Menu> menuList1 = multiMenu.getMenuList();

        menuList1.sort(Comparator.comparing(Menu::getId));
        List<Menu> menuList = new ArrayList<>();
        menuList1.sort(Comparator.comparing(Menu::getNumber));

        int num = size / menuList1.size();
        int num1 = size % menuList1.size();

        for (int i = 0; i < num; i++) {
            menuList.addAll(menuList1);
        }
        if (num1 > 0) {
            menuList.addAll(menuList1.subList(0, num1));
        }


        return menuList;
    }

    private void createReport(Vector<Report> reportList, Vector<Kindergarten> kindergartenList, int day, int month, int year, Menu menu) {

        if (menu != null) {

            int number = 400;

            ThreadGroup threadGroup = new ThreadGroup("myThread");

            for (int a = 0; a < kindergartenList.size(); ) {

                int finalI = a;

                Thread thread = new Thread(threadGroup, () -> {
                    for (int i = finalI; i < finalI + number; i++) {

                        if (i == kindergartenList.size())
                            break;
                        Kindergarten kindergarten = kindergartenList.get(i);
                        Optional<Report> optionalReport = reportRepo.findByYearAndMonthAndDayAndKindergarten_Id(year, month, day, kindergarten.getId());
                        if (optionalReport.isPresent()) {
                            Report report = optionalReport.get();
                            if (report.isEdit()) {
                                report.setMenu(menu);
                                reportList.addElement(report);
                            }
                        } else {
                            Report report = new Report(day, month, year, kindergarten);
                            report.setMenu(menu);
                            reportList.addElement(report);
                        }
                    }
                });
                thread.start();
                a = a + number;
            }

            while (true) {
                if (threadGroup.activeCount() == 0) {
                    break;
                }
            }
        }
    }

    private void JDBC(Vector<Report> reportList) {

        String URL = "jdbc:postgresql://localhost:5432/fergana01";
        String USER = "postgres";
        String PASS = "1404";


        reportRepo.saveAll(reportList);
    }
}
