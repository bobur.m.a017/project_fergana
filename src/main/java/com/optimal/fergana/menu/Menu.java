package com.optimal.fergana.menu;

import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.multiMenu.MultiMenu;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Menu {
    @Id
    private UUID id = UUID.randomUUID();
    private String name;

    @CreationTimestamp
    private Timestamp createDate = new Timestamp( System.currentTimeMillis());
    @UpdateTimestamp
    private Timestamp updateDate;
    private Integer number;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menu",fetch = FetchType.LAZY)
    private List<MealTimeStandard> mealTimeStandardList;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private MultiMenu multiMenu;



    public Menu(String name, List<MealTimeStandard> mealTimeStandardList, MultiMenu multiMenu) {
        this.name = name;
        this.mealTimeStandardList = mealTimeStandardList;
        this.multiMenu = multiMenu;
    }
}
