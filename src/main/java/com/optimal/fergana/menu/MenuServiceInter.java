package com.optimal.fergana.menu;

import com.optimal.fergana.mealTime.MealTime;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.users.Users;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface MenuServiceInter {
    List<Menu> add(MultiMenu multiMenu, Integer day, List<MealTime> mealTimeList);
    List<MenuResDTO> getAll(MultiMenu multiMenu);

    StateMessage replace(UUID menuId, LocalDate date, Users users);
}
