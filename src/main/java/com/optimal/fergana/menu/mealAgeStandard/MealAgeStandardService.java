package com.optimal.fergana.menu.mealAgeStandard;

import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.meal.MealRepo;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardInterface;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.menu.ageStandard.AgeStandardService;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandardRepo;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandardServiceInter;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNormService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MealAgeStandardService implements MealAgeStandardInterface, AgeStandardInterface, MealAgeStandardServiceInter {


    private final AgeStandardService ageStandardService;
    private final MealTimeStandardRepo mealTimeStandardRepo;
    private final MealAgeStandardRepo mealAgeStandardRepo;
    private final MealRepo mealRepo;
    private final AgeStandardRepo ageStandardRepo;
    private final SanpinMenuNormService sanpinMenuNormService;


    public StateMessage add(MealAgeStandardDTO dto, UUID mealTimeStandardId, Users users) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<Meal> optionalMeal = mealRepo.findById(dto.getMealId());
        Optional<MealTimeStandard> optionalMealTimeStandard = mealTimeStandardRepo.findById(mealTimeStandardId);

        if (optionalMealTimeStandard.isPresent() && optionalMeal.isPresent()) {
            Meal meal = optionalMeal.get();
            MealTimeStandard mealTimeStandard = optionalMealTimeStandard.get();

            boolean res = mealAgeStandardRepo.existsAllByMealAndMealTimeStandard(meal, mealTimeStandard);

            if (mealTimeStandard.getMenu().getMultiMenu().getCreatedUsers().getId().equals(users.getId())) {
                if (!res) {
                    MealAgeStandard mealAgeStandard = new MealAgeStandard(meal, mealTimeStandard);
                    List<AgeStandard> ageStandardList = ageStandardService.add(dto.getAgeStandardList(), mealAgeStandard);
                    mealAgeStandard.setAgeStandardList(ageStandardList);
                    MealAgeStandard save = mealAgeStandardRepo.save(mealAgeStandard);


                    MultiMenu multiMenu = mealTimeStandard.getMenu().getMultiMenu();
                    sanpinMenuNormService.calculateSanPin(multiMenu);

                    message = Message.SUCCESS_UZ;
                } else {
                    message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
                }
            }
        }
        return new StateMessage().parse(message);
    }

    public StateMessage edit(MealAgeStandardDTO dto, UUID mealTimeStandardId, UUID id) {

        Optional<MealAgeStandard> optionalMealAgeStandard = mealAgeStandardRepo.findById(id);
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<Meal> optionalMeal = mealRepo.findById(dto.getMealId());
        Optional<MealTimeStandard> optionalMealTimeStandard = mealTimeStandardRepo.findById(mealTimeStandardId);

        if (optionalMealTimeStandard.isPresent() && optionalMeal.isPresent() && optionalMealAgeStandard.isPresent()) {

            Meal meal = optionalMeal.get();
            MealTimeStandard mealTimeStandard = optionalMealTimeStandard.get();
            MealAgeStandard mealAgeStandard = optionalMealAgeStandard.get();

            boolean res = false;
            if (!meal.getId().equals(mealAgeStandard.getMeal().getId())) {
                res = mealAgeStandardRepo.existsAllByMealAndMealTimeStandard(meal, mealTimeStandard);
            }

            if (!res) {
                List<AgeStandard> deleteAgeStandardList = new ArrayList<>(mealAgeStandard.getAgeStandardList());

                List<AgeStandard> ageStandardList = ageStandardService.add(dto.getAgeStandardList(), mealAgeStandard);
                mealAgeStandard.setAgeStandardList(ageStandardList);
                mealAgeStandardRepo.save(mealAgeStandard);

                ageStandardRepo.deleteAll(deleteAgeStandardList);

                message = Message.EDIT_UZ;
            } else {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            }
        }
        return new StateMessage().parse(message);
    }

    public StateMessage delete(UUID id, Users users) {
        Optional<MealAgeStandard> optionalMealAgeStandard = mealAgeStandardRepo.findById(id);
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optionalMealAgeStandard.isPresent()) {

            MealAgeStandard mealAgeStandard = optionalMealAgeStandard.get();

            if (mealAgeStandard.getMealTimeStandard().getMenu().getMultiMenu().getCreatedUsers().getId().equals(users.getId())) {
                mealAgeStandardRepo.delete(mealAgeStandard);
                message = Message.DELETE_UZ;
                sanpinMenuNormService.calculateSanPin(mealAgeStandard.getMealTimeStandard().getMenu().getMultiMenu());
            }

        }
        return new StateMessage().parse(message);
    }

    public List<MealAgeStandardResDTO> getAll(MealTimeStandard mealTimeStandard) {
        List<MealAgeStandardResDTO> list = new ArrayList<>();

        if (mealTimeStandard.getMealAgeStandardList() != null) {
            for (MealAgeStandard mealAgeStandard : mealTimeStandard.getMealAgeStandardList()) {

                MealAgeStandardResDTO dto = parser(mealAgeStandard);
                dto.setAgeStandardList(ageStandardService.parserList(mealAgeStandard.getAgeStandardList()));
                list.add(dto);
            }
        }

        return list;
    }
}
