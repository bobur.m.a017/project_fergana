package com.optimal.fergana.menu.mealAgeStandard;

import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface MealAgeStandardRepo extends JpaRepository<MealAgeStandard, UUID> {

    boolean existsAllByMealAndMealTimeStandard(Meal meal, MealTimeStandard mealTimeStandard);

}
