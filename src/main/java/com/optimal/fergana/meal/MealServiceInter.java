package com.optimal.fergana.meal;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MealServiceInter {
    StateMessage add(MealDTO dto, Users user, MultipartFile file);
    StateMessage edit(MealDTO dto, Integer id, MultipartFile file);
    StateMessage delete(Integer id);
    ResponseEntity<?> getOne(Integer id);
    List<MealResDTO> getAll(Users users);
}
