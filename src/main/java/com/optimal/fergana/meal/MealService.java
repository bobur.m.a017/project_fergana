package com.optimal.fergana.meal;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.attachment.Attachment;
import com.optimal.fergana.attachment.AttachmentService;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.mealCategory.MealCategory;
import com.optimal.fergana.mealCategory.MealCategoryRepo;
import com.optimal.fergana.menu.ageStandard.AgeStandardDTO;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.multiMenu.MultiMenuRepo;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.productMeal.*;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentRepo;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNorm;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNormResDTO;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;


@Service
@RequiredArgsConstructor
public class MealService implements MealInterface, ProductMealInterface, MealServiceInter {


    private final MealRepo mealRepo;
    private final ProductMealService productMealService;
    private final MealCategoryRepo mealCategoryRepo;
    private final AttachmentService attachmentService;
    private final RegionalDepartmentRepo regionalDepartmentRepo;
    private final ProductMealRepo productMealRepo;
    private final ProductRepo productRepo;
    private final CustomConverter converter;
    private final MultiMenuRepo multiMenuRepo;
    private final AgeGroupRepo ageGroupRepo;


    public StateMessage add(MealDTO dto, Users user, MultipartFile file) {

        RegionalDepartment regionalDepartment = user.getRegionalDepartment();
        boolean res = mealRepo.existsAllByNameAndRegionalDepartmentAndDeleteIsFalse(dto.getName(), regionalDepartment);
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        Optional<MealCategory> optionalMealCategory = mealCategoryRepo.findById(dto.getMealCategory());

        if (!res && optionalMealCategory.isPresent()) {
            StateMessage stateMessage = checkWeight(dto.getProductMealList());
            if (!stateMessage.isSuccess())
                return stateMessage;
            Meal meal = new Meal(
                    dto.getName(),
                    BigDecimal.valueOf(dto.getWeight()),
                    false,
                    dto.getComment(),
                    optionalMealCategory.get(),
                    regionalDepartment
            );
            List<ProductMeal> productMealList = productMealService.add(dto.getProductMealList(), meal);
            meal.setProductMealList(productMealList);
            meal.setProtein(getIngredient(productMealList, "P"));
            meal.setOil(getIngredient(productMealList, "O"));
            meal.setKcal(getIngredient(productMealList, "K"));
            meal.setCarbohydrates(getIngredient(productMealList, "C"));

            try {
                Attachment attachment = attachmentService.add(file);
                meal.setAttachment(attachment);
                Meal save = mealRepo.save(meal);
                List<Meal> mealList = regionalDepartment.getMealList();
                mealList.add(save);
                regionalDepartment.setMealList(mealList);
                regionalDepartmentRepo.save(regionalDepartment);
                message = Message.SUCCESS_UZ;
            } catch (Exception e) {
                message = Message.PROBLEM_SAVING_IMAGE;
                return new StateMessage().parse(message);
            }
        }
        return new StateMessage().parse(message);
    }

    private BigDecimal getIngredient(List<ProductMeal> list, String str) {
        BigDecimal p = BigDecimal.valueOf(0);
        BigDecimal o = BigDecimal.valueOf(0);
        BigDecimal c = BigDecimal.valueOf(0);
        BigDecimal k = BigDecimal.valueOf(0);

        for (ProductMeal productMeal : list) {
            p = p.add(productMeal.getProtein());
            o = o.add(productMeal.getOil());
            c = c.add(productMeal.getCarbohydrates());
            k = k.add(productMeal.getKcal());
        }

        return str.equals("P") ? p : (str.equals("O") ? o : (str.equals("C") ? c : k));
    }

    public StateMessage edit(MealDTO dto, Integer id, MultipartFile file) {

        Optional<Meal> optionalMeal = mealRepo.findById(id);
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        Optional<MealCategory> optionalMealCategory = mealCategoryRepo.findById(dto.getMealCategory());

        if (optionalMeal.isPresent()) {
            Meal meal = optionalMeal.get();
            StateMessage stateMessage = checkWeight(dto.getProductMealList());
            if (!stateMessage.isSuccess())
                return stateMessage;

            boolean res = false;
            if (!meal.getName().equals(dto.getName())) {
                res = mealRepo.existsAllByNameAndRegionalDepartmentAndDeleteIsFalse(dto.getName(), meal.getRegionalDepartment());
            }
            if (!res && optionalMealCategory.isPresent()) {

                List<ProductMeal> deleteProductMealList = new ArrayList<>(meal.getProductMealList());

                List<ProductMeal> productMealList = productMealService.add(dto.getProductMealList(), meal);
                meal.setProductMealList(productMealList);
                meal.setProtein(getIngredient(productMealList, "P"));
                meal.setOil(getIngredient(productMealList, "O"));
                meal.setKcal(getIngredient(productMealList, "K"));
                meal.setCarbohydrates(getIngredient(productMealList, "C"));
                meal.setComment(dto.getComment());
                meal.setName(dto.getName());
                meal.setWeight(BigDecimal.valueOf(dto.getWeight()));

                try {
                    if (file != null) {
                        if (!file.isEmpty()) {
                            Attachment attachment = attachmentService.add(file);
                            meal.setAttachment(attachment);
                        }
                    }
                    Meal save = mealRepo.save(meal);
                    productMealRepo.deleteAll(deleteProductMealList);
                    message = Message.EDIT_UZ;
                } catch (Exception e) {
                    Meal save = mealRepo.save(meal);
                    productMealRepo.deleteAll(deleteProductMealList);
                    message = Message.PROBLEM_SAVING_IMAGE;
                    return new StateMessage().parse(message);
                }
            }
        }
        return new StateMessage().parse(message);
    }

    public StateMessage delete(Integer id) {

        Optional<Meal> optionalMeal = mealRepo.findById(id);
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        if (optionalMeal.isPresent()) {
            Meal meal = optionalMeal.get();
            meal.setDelete(true);
            mealRepo.save(meal);
            message = Message.DELETE_UZ;
        }
        return new StateMessage().parse(message);
    }

    public ResponseEntity<?> getOne(Integer id) {
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<Meal> optionalMeal = mealRepo.findById(id);

        if (optionalMeal.isPresent()) {
            Meal meal = optionalMeal.get();
            MealResDTO dto = parse(meal);
            dto.setProductMealList(parseList(meal.getProductMealList()));
            return ResponseEntity.status(200).body(dto);
        }
        return ResponseEntity.status(message.getCode()).body(new StateMessage().parse(message));
    }

    public List<MealResDTO> getAll(Users users) {
        RegionalDepartment regionalDepartment = users.getRegionalDepartment();
        List<MealResDTO> list = new ArrayList<>();
        List<Meal> mealList = mealRepo.findAllByRegionalDepartmentAndDelete(regionalDepartment, false);
        for (Meal meal : mealList) {
            MealResDTO dto = parse(meal);
            dto.setProductMealList(parseList(meal.getProductMealList()));
            list.add(dto);
        }
        list.sort(Comparator.comparing(MealResDTO::getName));
        return list;
    }

    public StateMessage checkWeight(List<ProductMealDTO> productMealDTOList) {
        for (ProductMealDTO productMealDTO : productMealDTOList) {
            if (productMealDTO.getWaste() > productMealDTO.getWeight()) {
                Optional<Product> optional = productRepo.findById(productMealDTO.getProductId());
                Product product;
                if (optional.isPresent()) {
                    product = optional.get();
                    return new StateMessage(product.getName() + "da chiqitli miqdordan chiqitsiz miqdor katta kiritilgan", false, 400);
                }
                return new StateMessage("Maxsulotlarning nirida chiqitli miqdordan chiqitsiz miqdor katta kiritilgan", false, 400);
            }
        }
        return new StateMessage("", true);
    }

    public List<ProductMealDTOAddMenu> getProduct(Integer mealId) {


        List<ProductMeal> allByMeal_id = productMealRepo.findAllByMeal_Id(mealId);
        List<ProductMealDTOAddMenu> list = new ArrayList<>();

        for (ProductMeal productMeal : allByMeal_id) {
            list.add(new ProductMealDTOAddMenu(productMeal.getProduct().getName(), productMeal.getWeight(), productMeal.getMeal().getWeight(), productMeal.getMeal().getName()));
        }

        return list;

    }

    public List<SanpinMenuNormResDTO> getSanPin(UUID multiMenuId, Integer mealId, List<AgeStandardDTO> dtoList) {

        List<SanpinMenuNorm> sanPinList = new ArrayList<>();

        Optional<Meal> optionalMeal = mealRepo.findById(mealId);
        Optional<MultiMenu> optionalMultiMenu = multiMenuRepo.findById(multiMenuId);

        if (optionalMultiMenu.isPresent() && optionalMeal.isPresent()) {
            MultiMenu multiMenu = optionalMultiMenu.get();
            Meal meal = optionalMeal.get();

            for (ProductMeal productMeal : meal.getProductMealList()) {
                for (SanpinMenuNorm sanpinMenuNorm : multiMenu.getSanpinMenuNorm()) {
                    if (productMeal.getProduct().getSanpinCategory().getId().equals(sanpinMenuNorm.getSanpinCategory().getId())) {
                        sanPinList.add(sanpinMenuNorm);
                        break;
                    }
                }
            }

            sanPinList.sort(Comparator.comparing(SanpinMenuNorm::getId));

            for (AgeStandardDTO dto : dtoList) {
                Optional<AgeGroup> optionalAgeGroup = ageGroupRepo.findById(dto.getAgeGroupId());

                if (optionalAgeGroup.isPresent()) {
                    AgeGroup ageGroup = optionalAgeGroup.get();
                    BigDecimal mealWeight = meal.getWeight();
                    for (ProductMeal productMeal : meal.getProductMealList()) {
                        BigDecimal productWeight = productMeal.getWeight().divide(mealWeight, 6, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(dto.getWeight()));
                        for (SanpinMenuNorm sanpinMenuNorm : sanPinList) {
                            if (sanpinMenuNorm.getSanpinCategory().getId().equals(productMeal.getProduct().getSanpinCategory().getId())) {
                                for (AgeGroupSanpinNorm ageGroupSanpinNorm : sanpinMenuNorm.getAgeGroupSanpinNormList()) {
                                    if (ageGroup.getId().equals(ageGroupSanpinNorm.getAgeGroup().getId())) {
                                        ageGroupSanpinNorm.setDoneWeight(ageGroupSanpinNorm.getDoneWeight().add(productWeight));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return converter.sanpinMenuNormToDTO(sanPinList);
    }
}
