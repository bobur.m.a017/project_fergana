package com.optimal.fergana.meal;

import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentRepo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MealRepo extends JpaRepository<Meal, Integer> {
    boolean existsAllByNameAndRegionalDepartmentAndDeleteIsFalse(String name, RegionalDepartment regionalDepartment);

    List<Meal> findAllByRegionalDepartmentAndDelete(RegionalDepartment regionalDepartment, boolean res);
    List<Meal> findAllByComment(String comment);
}
