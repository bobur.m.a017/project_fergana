package com.optimal.fergana.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.optimal.fergana.accountant.Accountant;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.notification.Notification;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.role.Role;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;


@Getter
@Setter
@Entity
public class Users {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String fatherName;
    private String surname;
    private boolean state = true;
    private boolean delete = false;


    //MENU BIRIKTIRISH UCHUN RUXSAT BERISH
    private Boolean permissionState = false;
    private Timestamp permissionStateEditDate = new Timestamp(System.currentTimeMillis());


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @Column(unique = true)
    private String username;
    private String password;
    private String phoneNumber;
    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private List<Role> roles;

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private Department department;

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private RegionalDepartment regionalDepartment;

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private Kindergarten kindergarten;


    @OneToMany(mappedBy = "users")
    private List<Notification> notification;

    @OneToOne
    private Accountant accountant;


    public Users(boolean state, boolean delete, String username, String password, List<Role> roles, RegionalDepartment regionalDepartment) {
        this.state = state;
        this.delete = delete;
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.regionalDepartment = regionalDepartment;
    }

    public Users() {
    }

    public Users(Integer id, String name, String username, String password, List<Role> roles) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.roles = roles;
    }

    public Users(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public Users(String name, String fatherName, String surname, boolean state, boolean delete, String username, String password, String phoneNumber, List<Role> roles) {
        this.name = name;
        this.fatherName = fatherName;
        this.surname = surname;
        this.state = state;
        this.delete = delete;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.roles = roles;
    }


}
