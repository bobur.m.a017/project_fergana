package com.optimal.fergana.users;

import com.sun.istack.NotNull;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Size;

@Getter
public class UserCredentialDTO {

//    @Length(max = 25, min = 4)
    private String username;

    @Length(min = 6)
    @NotNull
    private String oldPassword;
    @Length(min = 6)
    private String newPassword1;
    @Length( min = 6)
    private String newPassword2;
}
