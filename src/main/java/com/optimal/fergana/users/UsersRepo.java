package com.optimal.fergana.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepo extends JpaRepository<Users, Integer> {

    Users findByUsername(String username);

    boolean existsByUsernameAndDeleteIsFalse(String username);
    boolean existsByUsername(String username);

    List<Users> findAllByDepartment_IdAndDeleteIsFalse(Integer department_id);


    List<Users> findAllByRegionalDepartment_IdAndDeleteIsFalse(Integer regionalDepartment_id);


    List<Users> findAllByKindergarten_IdAndDeleteIsFalse(Integer kindergarten_id);

}
