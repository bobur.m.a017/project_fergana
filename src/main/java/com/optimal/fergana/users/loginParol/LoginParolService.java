package com.optimal.fergana.users.loginParol;


import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoginParolService {

    private final LoginParolRepo loginParolRepo;
    private final UsersRepo usersRepo;

    public void add() {

        List<LoginParol> loginParols = new ArrayList<>();
        List<Users> list = usersRepo.findAllByDepartment_IdAndDeleteIsFalse(8);
        list.addAll(usersRepo.findAllByDepartment_IdAndDeleteIsFalse(9));
        list.addAll(usersRepo.findAllByDepartment_IdAndDeleteIsFalse(16));
        list.addAll(usersRepo.findAllByDepartment_IdAndDeleteIsFalse(21));
        list.addAll(usersRepo.findAllByDepartment_IdAndDeleteIsFalse(23));

        for (Users users : list) {

            if (users.getDepartment() != null)
                loginParols.add(new LoginParol(users.getName(), users.getFatherName(), users.getSurname(), users.getUsername(), users.getUsername(), users.getPhoneNumber(), users.getRoles().get(0).getPositionName(), users.getDepartment().getName(), users.getKindergarten() != null?users.getKindergarten().getNumber() + users.getKindergarten().getName():""));
        }

        loginParolRepo.saveAll(loginParols);
    }
}