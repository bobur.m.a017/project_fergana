package com.optimal.fergana.users;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.optimal.fergana.accountant.Accountant;
import com.optimal.fergana.accountant.AccountantRepo;
import com.optimal.fergana.accountant.AccountantService;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentRepo;
import com.optimal.fergana.role.Role;
import com.optimal.fergana.role.RoleRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.optimal.fergana.statics.StaticWords.JWT_SECRET_KEY;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;


@RequiredArgsConstructor
@Service
public class UserService implements UserDetailsService, UserServiceInter {

    private final UsersRepo userRepo;
    private final RoleRepo roleRepo;
    private final KindergartenRepo kindergartenRepo;
    private final PasswordEncoder passwordEncoder;
    private final DepartmentRepo departmentRepo;
    private final CustomConverter converter;
    private final AccountantRepo accountantRepo;


    @Override //security metodi
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = userRepo.findByUsername(username);
        if (user == null || !user.isState() || user.isDelete()) {
            throw new UsernameNotFoundException("Tizimdan foydalanishga taqiq o'rnatilgan");
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }


    public Users parseToken(HttpServletRequest httpServletRequest) {

        String authorizationHeader = httpServletRequest.getHeader(AUTHORIZATION);
        String token = authorizationHeader.replace("Bearer ", "");


        Algorithm algorithm = Algorithm.HMAC256(JWT_SECRET_KEY.getBytes());

        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(token);
        String username = decodedJWT.getSubject();
        return userRepo.findByUsername(username);
    }

    //ADMIN BOSHQARMAGA XODIMLAR QO`SHADI
    public StateMessage addUserRegion(UsersDTO usersDTO, HttpServletRequest request) {

        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        Users user = parseToken(request);

        boolean username = userRepo.existsByUsernameAndDeleteIsFalse(usersDTO.getUsername());

        Optional<Role> optionalRole = roleRepo.findById(usersDTO.getRoleId());

        if (optionalRole.isPresent() && (!username)) {
            Role role = optionalRole.get();
            createUser(usersDTO, user.getRegionalDepartment(), null, role);
            message = Message.SUCCESS_UZ;
        }
        return new StateMessage().parse(message);
    }

    //ADMIN BO`LIMGA XODIMLAR QO`SHADI
    public StateMessage addUserDepartment(Integer departmentId, UsersDTO dto, HttpServletRequest request) {

        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        Users user = parseToken(request);
        Optional<Department> optionalDepartment = departmentRepo.findById(departmentId);

        boolean username = userRepo.existsByUsernameAndDeleteIsFalse(dto.getUsername());

        Optional<Role> optionalRole = roleRepo.findById(dto.getRoleId());

        if (optionalRole.isPresent() && (!username) && optionalDepartment.isPresent()) {

            Role role = optionalRole.get();
            RegionalDepartment regionalDepartment = user.getRegionalDepartment();
            Department department = optionalDepartment.get();


            if (role.getId().equals(11)) {
                Users userAccountant = createUser(dto, regionalDepartment, department, role);

                Accountant accountant=new Accountant(
                        new HashSet<>(),userAccountant
                );
                accountantRepo.save(accountant);
                userAccountant.setAccountant(accountant);
                userRepo.save(userAccountant);
            }
            else {
                createUser(dto,regionalDepartment,department,role);
            }
            message = Message.SUCCESS_UZ;
        }

        if (optionalDepartment.isEmpty() || optionalRole.isEmpty()) {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage addUserKindergarten(Integer kindergartenId, UsersDTO dto, HttpServletRequest request) {

        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        Users user = parseToken(request);
        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(kindergartenId);

        boolean username = userRepo.existsByUsernameAndDeleteIsFalse(dto.getUsername());

        Optional<Role> optionalRole = roleRepo.findById(dto.getRoleId());

        if (optionalRole.isPresent() && (!username) && optionalKindergarten.isPresent()) {
            Users savedUser = createUser(dto, user.getRegionalDepartment(), optionalKindergarten.get().getDepartment(), optionalRole.get());
            Kindergarten kindergarten = optionalKindergarten.get();
            savedUser.setKindergarten(kindergarten);
            kindergarten.setUsersList(List.of(userRepo.save(savedUser)));
            kindergartenRepo.save(kindergarten);
            message = Message.SUCCESS_UZ;
        }

        if (optionalKindergarten.isEmpty() || optionalRole.isEmpty()) {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage().parse(message);
    }


    private Users createUser(UsersDTO usersDTO, RegionalDepartment regionalDepartment, Department department, Role role) {

        Users users = new Users(usersDTO.getName(), usersDTO.getFatherName(), usersDTO.getSurname(), true, false, usersDTO.getUsername(), passwordEncoder.encode(usersDTO.getPassword()), usersDTO.getPhoneNumber(), List.of(role));
        users.setDepartment(department);
        users.setRegionalDepartment(regionalDepartment);
        return userRepo.save(users);
    }


    public StateMessage edit(UsersDTO usersDTO, HttpServletRequest request, Integer id) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<Users> optionalUser = userRepo.findById(id);
        Optional<Role> optionalRole = roleRepo.findById(usersDTO.getRoleId());
        boolean res = true;


        if (optionalUser.isPresent() && optionalRole.isPresent()) {
            Users users = optionalUser.get();
            Role role = optionalRole.get();


            if (!usersDTO.getUsername().equals(users.getUsername())) {
                boolean b = userRepo.existsByUsernameAndDeleteIsFalse(usersDTO.getUsername());
                res = !b;
            }

            if (res) {
                editUser(usersDTO, users, role);
                message = Message.EDIT_UZ;
            } else {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            }
        }
        return new StateMessage().parse(message);
    }

    private void editUser(UsersDTO usersDTO, Users users, Role role) {
        users.setName(usersDTO.getName());
        users.setFatherName(usersDTO.getFatherName());
        users.setSurname(usersDTO.getSurname());
//        users.setRoles(List.of(role));
//        users.setPassword(passwordEncoder.encode(usersDTO.getPassword()));
        users.setState(usersDTO.isStatus());
        users.setPhoneNumber(usersDTO.getPhoneNumber());
        userRepo.save(users);
    }

    public StateMessage delete(Integer id) {
        Optional<Users> optionalUsers = userRepo.findById(id);
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        if (optionalUsers.isPresent()) {
            Users users = optionalUsers.get();
            users.setDelete(true);
            users.setState(false);
            userRepo.save(users);
            message = Message.DELETE_UZ;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage changeStatus(Integer userId, boolean status) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<Users> optionalUsers = userRepo.findById(userId);

        if (optionalUsers.isPresent()) {
            Users users = optionalUsers.get();
            users.setState(status);
            userRepo.save(users);
            message = Message.SUCCESS_UZ;
        }
        return new StateMessage().parse(message);
    }

    public List<UsersResDTO> getAll(String type, HttpServletRequest request, Integer id) {

        Users users = parseToken(request);
        List<Users> usersList = new ArrayList<>();

        if (id == null && type.equals("region")) {
            for (Users users1 : users.getRegionalDepartment().getUsersList()) {
                if (users1.getDepartment() == null && users1.getKindergarten() == null && (!users1.isDelete()) && !Objects.equals(users1.getId(), users.getId())) {
                    usersList.add(users1);
                }
            }
        } else if (type.equals("kindergarten") && id != null) {
            Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(id);
            if (optionalKindergarten.isPresent()) {
                for (Users users1 : optionalKindergarten.get().getUsersList()) {
                    if ((!users1.isDelete()) && !Objects.equals(users1.getId(), users.getId())) {
                        usersList.add(users1);
                    }
                }
            }
        } else if (type.equals("department") && id != null) {
            Optional<Department> optionalDepartment = departmentRepo.findById(id);
            if (optionalDepartment.isPresent()) {
                for (Users users1 : optionalDepartment.get().getUsersList()) {
                    if (users1.getKindergarten() == null && (!users1.isDelete()) && !Objects.equals(users1.getId(), users.getId())) {
                        usersList.add(users1);
                    }
                }
            }
        }
        return converter.userToDTO(usersList);
    }

    public UsersResDTO getOne(Integer id) {
        Optional<Users> optionalUsers = userRepo.findById(id);
        if (optionalUsers.isPresent()) {
            return converter.userToDTO(optionalUsers.get());
        } else {
            throw new UsernameNotFoundException("User not found in db");
        }
    }

    public UsersResDTO getMyData(HttpServletRequest request) {
        Users users = parseToken(request);
        return converter.userToDTO(users);
    }

    public StateMessage changeCredentialsLogin(HttpServletRequest request, UserCredentialDTO userCredentialDTO) {
        Users user = parseToken(request);
        String username1 = user.getUsername();

        boolean matches = passwordEncoder.matches(userCredentialDTO.getOldPassword(), user.getPassword());

        StateMessage stateMessage = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);

        if (matches) {
            if (!username1.equals(userCredentialDTO.getUsername())) {
                boolean exists = userRepo.existsByUsername(userCredentialDTO.getUsername());
                if (exists) {
                    stateMessage = new StateMessage("Bunday username mavjud, boshqa username tanlang", false, 401);
                } else {
                    user.setUsername(userCredentialDTO.getUsername());
                    stateMessage = new StateMessage("Muvaffaqiyatli o'zgartirildi", true, 200);
                }
            }
        }
        userRepo.save(user);
        return stateMessage;
    }

    public StateMessage changeCredentialsPassword(HttpServletRequest request, UserCredentialDTO userCredentialDTO) {
        Users user = parseToken(request);
        boolean matches = passwordEncoder.matches(userCredentialDTO.getOldPassword(), user.getPassword());
        StateMessage stateMessage;

        if (userCredentialDTO.getNewPassword1().equals(userCredentialDTO.getNewPassword2()) && (userCredentialDTO.getNewPassword1() != null && userCredentialDTO.getNewPassword2() != null)) {
            if (matches) {
                user.setPassword(passwordEncoder.encode(userCredentialDTO.getNewPassword1()));
                stateMessage = new StateMessage("Muvaffaqiyatli o'zgartirildi", true, 200);
            } else {
                stateMessage = new StateMessage("Joriy parol xato kiritildi", false, 401);
            }
        } else {
            stateMessage = new StateMessage("Kiritgan yangi ikki parolingiz bir biriga to'g'ri kelmadi", false, 401);
        }

        userRepo.save(user);
        return stateMessage;
    }

    public StateMessage resetUserCredentials(Integer userId) {
//        Users currentUser = parseToken(request);

//        String roleName = currentUser.getRoles().get(0).getName();

        Optional<Users> optionalUsers = userRepo.findById(userId);

        if (optionalUsers.isPresent()) {
            Users user = optionalUsers.get();

            String passsword = user.getUsername() + user.getUsername();

            user.setPassword(passwordEncoder.encode(passsword));
            userRepo.save(user);
            return new StateMessage("Foydalanuvchi ma'lumotlari muvaffaqiyatli tiklandi\n Yangi parol: " + passsword, true, 200);
        } else {
            return new StateMessage("Tizimda bunday foydalanuvchi mavjud emas", false, 401);
        }

//        return new StateMessage("Sizda foydalanuvchi ma'lumotlarini o'zgartirish huquqi mavjud emas",false,403);
    }
}