package com.optimal.fergana.selectMenu;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface SelectMenuRepo extends JpaRepository<SelectMenu, UUID> {

    Optional<SelectMenu> findByRegionalDepartment_Id(Integer regionalDepartment_id);

}
