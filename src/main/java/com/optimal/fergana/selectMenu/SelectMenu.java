package com.optimal.fergana.selectMenu;

import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class SelectMenu {

    @Id
    private UUID id = UUID.randomUUID();
    private Boolean state = false;

    @ManyToOne
    private MultiMenu menu;

    @OneToOne
    private RegionalDepartment regionalDepartment;
}
