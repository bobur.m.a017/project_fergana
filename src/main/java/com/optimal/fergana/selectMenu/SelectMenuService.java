package com.optimal.fergana.selectMenu;

import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.MenuRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.multiMenu.MultiMenuRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@RequiredArgsConstructor
@Service
public class SelectMenuService implements SelectMenuServiceInterface {

    private final SelectMenuRepo selectMenuRepo;
    private final MultiMenuRepo multiMenuRepo;
    private final ReportRepo reportRepo;


    @Override
    public StateMessage select(UUID menuId, Users users) {

        RegionalDepartment regionalDepartment = users.getRegionalDepartment();
        Optional<MultiMenu> byId = multiMenuRepo.findById(menuId);

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (byId.isPresent()) {
            MultiMenu multiMenu = byId.get();


            Optional<SelectMenu> optionalSelectMenu = selectMenuRepo.findByRegionalDepartment_Id(regionalDepartment.getId());

            if (optionalSelectMenu.isPresent()) {
                SelectMenu selectMenu = optionalSelectMenu.get();
                selectMenu.setMenu(multiMenu);

                selectMenuRepo.save(selectMenu);

                message = Message.SUCCESS_UZ;
            } else {
                SelectMenu selectMenu = new SelectMenu();
                selectMenu.setRegionalDepartment(regionalDepartment);
                selectMenu.setMenu(multiMenu);

                selectMenuRepo.save(selectMenu);
                message = Message.SUCCESS_UZ;
            }
        }
        return new StateMessage().parse(message);
    }

    public Menu getMenu(Kindergarten kindergarten) {
        RegionalDepartment regionalDepartment = kindergarten.getRegionalDepartment();

        Optional<SelectMenu> optionalSelectMenu = selectMenuRepo.findByRegionalDepartment_Id(regionalDepartment.getId());

        if (optionalSelectMenu.isPresent()) {
            SelectMenu selectMenu = optionalSelectMenu.get();

            Optional<Report> optionalReport = reportRepo.findFirstByKindergarten_IdOrderByDateDesc(kindergarten.getId());

            if (optionalReport.isPresent()) {
                Report report = optionalReport.get();
                Menu menu = report.getMenu();

                if (menu.getMultiMenu().getId().equals(selectMenu.getMenu().getId())) {
                    Integer number = menu.getNumber();
                    MultiMenu multiMenu = selectMenu.getMenu();

                    List<Menu> menuList = multiMenu.getMenuList();
                    menuList.sort(Comparator.comparing(Menu::getNumber));

                    for (int i = 1; i < menuList.size() * 2; i++) {
                        if (Objects.equals(menuList.get(i - 1).getNumber(), number)) {
                            if(i == menuList.size()){
                                return menuList.get(0);
                            }else {
                                return menuList.get(i);
                            }
                        }
                    }
                } else {
                    MultiMenu multiMenu = selectMenu.getMenu();
                    List<Menu> menuList = multiMenu.getMenuList();
                    menuList.sort(Comparator.comparing(Menu::getNumber));
                    return menuList.get(0);
                }
            } else {
                MultiMenu multiMenu = selectMenu.getMenu();
                List<Menu> menuList = multiMenu.getMenuList();
                menuList.sort(Comparator.comparing(Menu::getNumber));
                return menuList.get(0);
            }
        }
        return null;
    }
}
