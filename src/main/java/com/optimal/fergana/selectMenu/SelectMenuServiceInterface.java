package com.optimal.fergana.selectMenu;

import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;

import java.util.UUID;

public interface SelectMenuServiceInterface {
    StateMessage select(UUID menuId, Users users);
    Menu getMenu(Kindergarten kindergarten);
}
