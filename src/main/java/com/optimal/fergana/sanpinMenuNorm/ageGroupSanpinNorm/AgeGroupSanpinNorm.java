package com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNorm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class AgeGroupSanpinNorm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer daily;

    @Column(precision = 19, scale = 6)
    private BigDecimal doneWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal planWeight;


    @ManyToOne
    private AgeGroup ageGroup;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private SanpinMenuNorm sanpinMenuNorm;


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    public AgeGroupSanpinNorm(Integer daily, BigDecimal doneWeight, BigDecimal planWeight, SanpinMenuNorm sanpinMenuNorm,AgeGroup ageGroup) {
        this.daily = daily;
        this.doneWeight = doneWeight;
        this.planWeight = planWeight;
        this.sanpinMenuNorm = sanpinMenuNorm;
        this.ageGroup = ageGroup;
    }
}
