package com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AgeGroupSanpinNormRepo extends JpaRepository<AgeGroupSanpinNorm, Integer> {

    Optional<AgeGroupSanpinNorm> findBySanpinMenuNorm_SanpinCategory_IdAndSanpinMenuNorm_MultiMenu_IdAndAgeGroup_Id(Integer sanpinMenuNorm_sanpinCategory_id, UUID sanpinMenuNorm_multiMenu_id, Integer ageGroup_id);
    List<AgeGroupSanpinNorm> findBySanpinMenuNorm_MultiMenu_Id(UUID sanpinMenuNorm_multiMenu_id);
}
