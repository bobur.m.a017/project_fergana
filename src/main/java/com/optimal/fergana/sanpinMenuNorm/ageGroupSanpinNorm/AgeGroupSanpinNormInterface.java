package com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public interface AgeGroupSanpinNormInterface {
    default AgeGroupSanpinNormResDTO parser(AgeGroupSanpinNorm ageGroupSanpinNorm) {
        return new AgeGroupSanpinNormResDTO(
                ageGroupSanpinNorm.getId(),
                ageGroupSanpinNorm.getDaily(),
                ageGroupSanpinNorm.getDoneWeight().setScale(0,RoundingMode.HALF_UP),
                ageGroupSanpinNorm.getPlanWeight().setScale(0,RoundingMode.HALF_UP),
                ageGroupSanpinNorm.getAgeGroup().getName()
        );
    }

    default List<AgeGroupSanpinNormResDTO> parserList(List<AgeGroupSanpinNorm> list) {

        List<AgeGroupSanpinNormResDTO> dtoList = new ArrayList<>();
        for (AgeGroupSanpinNorm ageGroupSanpinNorm : list) {
            dtoList.add(parser(ageGroupSanpinNorm));
        }
        return dtoList;
    }
}
