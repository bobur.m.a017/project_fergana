package com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AgeGroupSanpinNormResDTO {

    private Integer id;
    private Integer daily;
    private BigDecimal doneWeight;
    private BigDecimal planWeight;
    private String ageGroupName;
}
