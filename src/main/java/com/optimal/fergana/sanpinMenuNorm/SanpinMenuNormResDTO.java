package com.optimal.fergana.sanpinMenuNorm;

import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategory;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SanpinMenuNormResDTO {

    private Integer id;
    private String sanpinCategoryName;
    private Integer sanpinCategoryId;
    private List<AgeGroupSanpinNormResDTO> ageGroupSanpinNormList;
    private BigDecimal doneProtein;
    private BigDecimal planProtein;
    private BigDecimal doneKcal;
    private BigDecimal planKcal;
    private BigDecimal doneOil;
    private BigDecimal planOil;
    private BigDecimal doneCarbohydrates;
    private BigDecimal planCarbohydrates;
    private String multiMenuName;
    private UUID multiMenuId;


    public SanpinMenuNormResDTO(Integer id, String sanpinCategoryName, Integer sanpinCategoryId, BigDecimal doneProtein, BigDecimal planProtein, BigDecimal doneKcal, BigDecimal planKcal, BigDecimal doneOil, BigDecimal planOil, BigDecimal doneCarbohydrates, BigDecimal planCarbohydrates, String multiMenuName, UUID multiMenuId) {
        this.id = id;
        this.sanpinCategoryName = sanpinCategoryName;
        this.sanpinCategoryId = sanpinCategoryId;
        this.doneProtein = doneProtein;
        this.planProtein = planProtein;
        this.doneKcal = doneKcal;
        this.planKcal = planKcal;
        this.doneOil = doneOil;
        this.planOil = planOil;
        this.doneCarbohydrates = doneCarbohydrates;
        this.planCarbohydrates = planCarbohydrates;
        this.multiMenuName = multiMenuName;
        this.multiMenuId = multiMenuId;
    }
}
