package com.optimal.fergana.sanpinMenuNorm;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface SanpinMenuNormRepo extends JpaRepository<SanpinMenuNorm,Integer> {

    Optional<SanpinMenuNorm> findBySanpinCategory_IdAndMultiMenu_Id(Integer sanpinCategory_id, UUID multiMenu_id);
}
