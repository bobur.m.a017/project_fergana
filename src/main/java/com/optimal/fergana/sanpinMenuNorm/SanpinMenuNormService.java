package com.optimal.fergana.sanpinMenuNorm;

import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategory;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategoryRepo;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNorm;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormInterface;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SanpinMenuNormService implements SanpinMenuNormInterface, AgeGroupSanpinNormInterface, SanpinMenuNormInter {


    private final SanpinCategoryRepo sanpinCategoryRepo;
    private final AgeGroupSanpinNormRepo ageGroupSanpinNormRepo;
    private final AgeStandardRepo ageStandardRepo;


    public List<SanpinMenuNorm> add(MultiMenu multiMenu, Integer day) {
        BigDecimal zero = BigDecimal.valueOf(0);

        List<SanpinMenuNorm> list = new ArrayList<>();

        for (SanpinCategory sanpinCategory : sanpinCategoryRepo.findAll()) {
            SanpinMenuNorm sanpinMenuNorm = new SanpinMenuNorm(sanpinCategory, zero, zero, zero, zero, zero, zero, zero, zero, multiMenu);

            List<AgeGroupSanpinNorm> ageGroupSanpinNormList = new ArrayList<>();

            for (DailyNorm dailyNorm : sanpinCategory.getDailyNormList()) {

                if (dailyNorm.getStayTime().getId().equals(multiMenu.getStayTime().getId())) {
                    ageGroupSanpinNormList.add(new AgeGroupSanpinNorm(day, zero, dailyNorm.getWeight().multiply(BigDecimal.valueOf(day)), sanpinMenuNorm, dailyNorm.getAgeGroup()));
                }
            }
            sanpinMenuNorm.setAgeGroupSanpinNormList(ageGroupSanpinNormList);
            list.add(sanpinMenuNorm);
        }
        return list;
    }

    public List<SanpinMenuNormResDTO> get(MultiMenu multiMenu) {
        List<SanpinMenuNormResDTO> list = new ArrayList<>();

        for (SanpinMenuNorm sanpinMenuNorm : multiMenu.getSanpinMenuNorm()) {
            SanpinMenuNormResDTO dto = parser(sanpinMenuNorm);
            dto.setAgeGroupSanpinNormList(parserList(sanpinMenuNorm.getAgeGroupSanpinNormList()));
            list.add(dto);
        }
        return list;
    }

    public void calculateSanPin(MultiMenu multiMenu) {
        List<AgeStandard> ageStandardList = ageStandardRepo.findAllByMealAgeStandard_MealTimeStandard_Menu_MultiMenu_Id(multiMenu.getId());

        List<AgeGroupSanpinNorm> normList = ageGroupSanpinNormRepo.findBySanpinMenuNorm_MultiMenu_Id(multiMenu.getId());
        for (AgeGroupSanpinNorm ageGroupSanpinNorm : normList) {
            ageGroupSanpinNorm.setDoneWeight(BigDecimal.valueOf(0));
        }
        ageGroupSanpinNormRepo.saveAll(normList);


        List<AgeGroupSanpinNorm> list = new ArrayList<>();

        for (AgeStandard ageStandard : ageStandardList) {

            Meal meal = ageStandard.getMealAgeStandard().getMeal();
            BigDecimal mealWeight = meal.getWeight();

            BigDecimal weight = ageStandard.getWeight();
            for (ProductMeal productMeal : meal.getProductMealList()) {

                Product product = productMeal.getProduct();

                if (product.getId() != 211) {
                    BigDecimal divide = productMeal.getWeight().divide(mealWeight, 6, RoundingMode.HALF_UP);

                    BigDecimal productWeight = weight.multiply(divide);


                    boolean res = true;
                    for (AgeGroupSanpinNorm ageGroupSanpinNorm : list) {
                        if (ageGroupSanpinNorm.getSanpinMenuNorm().getSanpinCategory().getId().equals(product.getSanpinCategory().getId()) && ageGroupSanpinNorm.getAgeGroup().getId().equals(ageStandard.getAgeGroup().getId())) {
                            ageGroupSanpinNorm.setDoneWeight((ageGroupSanpinNorm.getDoneWeight().add(productWeight)));
                            res = false;
                            break;
                        }
                    }
                    if (res) {
                        Optional<AgeGroupSanpinNorm> optional = ageGroupSanpinNormRepo.findBySanpinMenuNorm_SanpinCategory_IdAndSanpinMenuNorm_MultiMenu_IdAndAgeGroup_Id(product.getSanpinCategory().getId(), multiMenu.getId(), ageStandard.getAgeGroup().getId());
                        if (optional.isPresent()) {
                            AgeGroupSanpinNorm ageGroupSanpinNorm = optional.get();
                            ageGroupSanpinNorm.setDoneWeight((ageGroupSanpinNorm.getDoneWeight().add(productWeight)));
                            list.add(ageGroupSanpinNorm);
                        }
                    }
                }

            }
        }
        ageGroupSanpinNormRepo.saveAll(list);
    }
}
