package com.optimal.fergana.productMeal;

import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductMealService implements ProductMealServiceInter{

    private final ProductRepo productRepo;

    public ProductMealService(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public List<ProductMeal> add(List<ProductMealDTO> list, Meal meal) {

        List<ProductMeal> productMealList = new ArrayList<>();

        for (ProductMealDTO productMealDTO : list) {
            Optional<Product> optionalProduct = productRepo.findById(productMealDTO.getProductId());
            if (optionalProduct.isPresent()) {
                Product product = optionalProduct.get();
                productMealList.add(new ProductMeal(
                        BigDecimal.valueOf(productMealDTO.getWeight()),
                        BigDecimal.valueOf(productMealDTO.getWaste()),
                        getProtein(product, productMealDTO.getWeight()),
                        getKcal(product, productMealDTO.getWeight()),
                        getOil(product, productMealDTO.getWeight()),
                        getCarbohydrates(product, productMealDTO.getWeight()),
                        product,
                        meal
                ));
            }
        }
        return productMealList;
    }

    private BigDecimal getProtein(Product product, Double weight) {

        BigDecimal protein = product.getProtein();
        BigDecimal protein100 = protein.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP);
        return protein100.multiply(BigDecimal.valueOf(weight));
    }

    private BigDecimal getOil(Product product, Double weight) {
        BigDecimal oil = product.getOil();
        BigDecimal oil100 = oil.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP);
        return oil100.multiply(BigDecimal.valueOf(weight));
    }

    private BigDecimal getCarbohydrates(Product product, Double weight) {
        BigDecimal carbohydrates = product.getCarbohydrates();
        BigDecimal carbohydrates100 = carbohydrates.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP);
        return carbohydrates100.multiply(BigDecimal.valueOf(weight));
    }

    private BigDecimal getKcal(Product product, Double weight) {
        BigDecimal kcal = product.getKcal();
        BigDecimal kcal100 = kcal.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP);
        return kcal100.multiply(BigDecimal.valueOf(weight));
    }
}
