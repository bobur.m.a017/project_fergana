package com.optimal.fergana.productMeal;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductMealRepo extends JpaRepository<ProductMeal, Integer> {

    List<ProductMeal> findAllByMeal_Id(Integer id);
}
