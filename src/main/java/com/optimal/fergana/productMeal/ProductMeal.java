package com.optimal.fergana.productMeal;

import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class    ProductMeal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;
    @Column(precision = 19, scale = 6)
    private BigDecimal waste;

    @Column(precision = 19, scale = 6)
    private BigDecimal protein;
    @Column(precision = 19, scale = 6)
    private BigDecimal kcal;
    @Column(precision = 19, scale = 6)
    private BigDecimal oil;
    @Column(precision = 19, scale = 6)
    private BigDecimal carbohydrates;

    @ManyToOne
    private Product product;

    @ManyToOne
    private Meal meal;


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;


    public ProductMeal(BigDecimal weight,BigDecimal waste, BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates, Product product, Meal meal) {
        this.weight = weight;
        this.waste = waste;
        this.protein = protein;
        this.kcal = kcal;
        this.oil = oil;
        this.carbohydrates = carbohydrates;
        this.product = product;
        this.meal = meal;
    }

    public String getProductSort() {
        return product.getName();
    }
}
