package com.optimal.fergana.productMeal;

import java.util.ArrayList;
import java.util.List;

public interface ProductMealInterface {
    default ProductMealResDTO parse(ProductMeal productMeal) {
        return new ProductMealResDTO(
                productMeal.getId(),
                productMeal.getWeight(),
                productMeal.getWaste(),
                productMeal.getProtein(),
                productMeal.getKcal(),
                productMeal.getOil(),
                productMeal.getCarbohydrates(),
                productMeal.getProduct().getName(),
                productMeal.getProduct().getId(),
                productMeal.getMeal().getName(),
                productMeal.getMeal().getId()
        );
    }

    default List<ProductMealResDTO> parseList(List<ProductMeal> productMealList) {
        List<ProductMealResDTO> list = new ArrayList<>();
        for (ProductMeal productMeal : productMealList) {
            list.add(parse(productMeal));
        }
        return list;
    }
}
