package com.optimal.fergana.notification.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NotificationResAllDTO {
    private  Integer id;
    private String header;
    private boolean read;
    private Timestamp date;
    private String sender;
}
