package com.optimal.fergana.supplier.STIRParser;

import com.optimal.fergana.supplier.SupplierDTO;
import com.optimal.fergana.supplier.SupplierRepo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class STIRParser {

    public SupplierDTO parserLiveSTIR(String STIR) throws IOException {

        try {
            String baseURL = "https://orginfo.uz";

            String linkSearchBySTIR = baseURL + "/search/organizations/?q=" + STIR;

            Document document = Jsoup.connect(linkSearchBySTIR).get();

            Elements value = document.getElementsByAttributeValue("class", "text-decoration-none og-card");
            String companyLink = value.attr("href");
            String companyInfoLink = baseURL + "/uz" + companyLink;

            Document doc = Jsoup.connect(companyInfoLink).get();

            Elements elementsAddress = doc.getElementsByAttributeValue("class", "card-body py-3");
            Elements addressess = elementsAddress.get(0).getElementsByClass("col-12");
            Elements directorsElement = null;
            if (elementsAddress.size() > 1)
                directorsElement = elementsAddress.get(1).getElementsByClass("col-12");

            Elements elementsByTagName = doc.getElementsByAttributeValue("class", "py-3");
            String nameComp = elementsByTagName.get(0).getElementsByTag("h1").text();
            String address = elementsAddress.get(0).getElementsByTag("h5").text();
            Elements elementsByAttributeValue = elementsByTagName.get(0).getElementsByAttributeValue("class", "col-12");
            Element element = elementsByAttributeValue.get(0);
            Elements elementsByClass1 = element.getElementsByClass("col-12");
            elementsByClass1.addAll(addressess);
            if (elementsAddress.size() > 1)
                elementsByClass1.addAll(directorsElement);
            SupplierDTO supplier = new SupplierDTO();
            for (Element elementsByClass : elementsByClass1) {
                System.out.println();
                supplier.setName(nameComp);
                int index = 0;
                for (Element name : elementsByClass.getElementsByTag("span")) {
                    index++;
                    if (name.getElementsByTag("span").text().equals("Faollik holati")) {
                        supplier.setStatus(elementsByClass.getElementsByTag("span").get(index).text());
                    } else if (name.getElementsByTag("span").text().equals("Ro'yxatdan o'tgan sana")) {
                        supplier.setDate(elementsByClass.getElementsByTag("span").get(index).text());
                    } else if (name.getElementsByTag("span").text().equals("STIR")) {
                        supplier.setSTIR(elementsByClass.getElementsByTag("span").get(index).text());
                    } else if (name.getElementsByTag("span").text().equals("THSHT")) {
                        supplier.setTHSHT(elementsByClass.getElementsByTag("span").get(index).text());
                    } else if (name.getElementsByTag("span").text().equals("DBIBT")) {
                        supplier.setDBIBT(elementsByClass.getElementsByTag("span").get(index).text());
                    } else if (name.getElementsByTag("span").text().equals("IFUT")) {
                        supplier.setIFUT(elementsByClass.getElementsByTag("span").get(index).text());
                    } else if (name.getElementsByTag("span").text().equals("Ustav fondi")) {
                        supplier.setFond(elementsByClass.getElementsByTag("span").get(index).text());
                    } else if (name.getElementsByTag("span").text().equals("Manzili")) {
                        supplier.setAddress(elementsByClass.getElementsByTag("span").get(index).text());
                    } else if (name.getElementsByTag("span").text().equals("Telefon raqami")) {
                        supplier.setPhoneNumber(elementsByClass.getElementsByTag("span").get(index).text());
                    } else if (name.getElementsByTag("span").text().equals("Rahbar")) {
                        supplier.setDirector(elementsByClass.getElementsByTag("span").get(index).text());
                    }
//
//                    if (!(name.text().equals("Kontakt ma'lumotlari") || name.text().equals("Boshqaruv ma'lumotlari") || name.text().equals("Ta'sischilar"))) {
//
//                        index++;
//                    } else {
//                        throw new UsernameNotFoundException("bunday STIR mavjud emas!!!!");
//                    }


                }
            }
//            System.out.println(supplier);
            return supplier;
        } catch (
                IOException e) {
            throw new IOException("ulanishda muammo");
        }
    }
}


