package com.optimal.fergana.supplier;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RequiredArgsConstructor
@Service
public class SupplierService implements SupplierServiceInter {
    private final SupplierRepo supplierRepo;
    private final UserService userService;
    private final CustomConverter customConverter;


    public StateMessage add(SupplierDTO supplierDTO, HttpServletRequest request) {
        Users user = userService.parseToken(request);
        RegionalDepartment regionalDepartment = user.getRegionalDepartment();

        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        Optional<Supplier> optionalSupplier = supplierRepo.findBySTIR(supplierDTO.getSTIR());
        Supplier supplier;
        if (optionalSupplier.isEmpty()) {
            supplier = new Supplier(
                    supplierDTO.getName(),
                    supplierDTO.getDate(),
                    supplierDTO.getSTIR(),
                    supplierDTO.getStatus(),
                    supplierDTO.getTHSHT(),
                    supplierDTO.getDBIBT(),
                    supplierDTO.getIFUT(),
                    supplierDTO.getFond(),
                    supplierDTO.getAddress(),
                    supplierDTO.getPhoneNumber(),
                    supplierDTO.getDirector(),
                    Set.of(user.getDepartment()),
                    List.of(regionalDepartment)
            );
        } else {
            supplier = optionalSupplier.get();
            Set<Department> departments = supplier.getDepartments();

            departments.add(user.getDepartment());
            supplier.setDepartments(departments);
            List<RegionalDepartment> regionalDepartments = supplier.getRegionalDepartments();
            regionalDepartments.add(regionalDepartment);
            supplier.setRegionalDepartments(regionalDepartments);
        }
        message = Message.SUCCESS_UZ;
        supplierRepo.save(supplier);

        return new StateMessage().parse(message);
    }

    public StateMessage edit(SupplierDTO supplierDTO, Integer id) {
        Optional<Supplier> optionalSupplier = supplierRepo.findById(id);

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optionalSupplier.isPresent()) {

            Supplier supplier = optionalSupplier.get();

            supplier.setName(supplierDTO.getName());
            supplier.setSTIR(supplierDTO.getSTIR());
            supplierRepo.save(supplier);
            message = Message.EDIT_UZ;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage delete(Integer id) {

        Message message = Message.DELETE_UZ;

        supplierRepo.deleteById(id);

        return new StateMessage().parse(message);

    }

    public CustomPageable getAll(HttpServletRequest request, Integer departmentId, Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);
        Users users = userService.parseToken(request);
        Page<Supplier> page;
        long count;

        if (departmentId != null) {
            page = (supplierRepo.findAllByDepartments_Id(departmentId, pageable));
            count = supplierRepo.count();
        } else if (users.getDepartment().getId() != null) {
            page = supplierRepo.findAllByDepartments_Id(users.getDepartment().getId(), pageable);
            count = supplierRepo.countAllByDepartments_Id(users.getDepartment().getId());
        } else {
            page = (supplierRepo.findAllByRegionalDepartments_Id(users.getRegionalDepartment().getId(), pageable));
            count = supplierRepo.countAllByRegionalDepartments_Id(users.getRegionalDepartment().getId());
        }
        return new CustomPageable(page.getSize(), page.getNumber(), customConverter.supplierToDTO(page.stream().toList()), count);
    }

    public Supplier getOne(Integer id) throws UserPrincipalNotFoundException {
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Optional<Supplier> optionalSupplier = supplierRepo.findById(id);
        if (optionalSupplier.isPresent()) {
            return optionalSupplier.get();
        } else {
            throw new UserPrincipalNotFoundException(message.getName());
        }
    }

}
