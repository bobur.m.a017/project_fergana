package com.optimal.fergana.supplier;

import com.optimal.fergana.department.Department;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.supplier.contract.Contract;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Supplier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String date;

    private String STIR;

    private String status;

    private String THSHT;

    private String DBIBT;

    private String IFUT;

    private String fond;

    private String address;

    private String phoneNumber;

    private String director;

    @ManyToMany
    private Set<Department> departments;

    @ManyToMany
    private List<RegionalDepartment> regionalDepartments;

    @OneToMany(mappedBy = "supplier")
    private List<Contract> contract;


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    public Supplier(String name, String date, String STIR, String status, String THSHT, String DBIBT, String IFUT, String fond, String address, String phoneNumber, String director, Set<Department> departments, List<RegionalDepartment> regionalDepartments) {
        this.name = name;
        this.date = date;
        this.STIR = STIR;
        this.status = status;
        this.THSHT = THSHT;
        this.DBIBT = DBIBT;
        this.IFUT = IFUT;
        this.fond = fond;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.director = director;
        this.departments = departments;
        this.regionalDepartments = regionalDepartments;

    }
}
