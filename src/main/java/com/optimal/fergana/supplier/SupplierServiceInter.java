package com.optimal.fergana.supplier;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;
import org.springframework.data.domain.Page;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;

public interface SupplierServiceInter {
    StateMessage add(SupplierDTO supplierDTO, HttpServletRequest request);
    StateMessage edit(SupplierDTO supplierDTO, Integer id);
    StateMessage delete(Integer id);
    Supplier getOne(Integer id) throws UserPrincipalNotFoundException;
//    public Page<Supplier> getAll(Users users, Integer departmentId, Integer number, Integer pageNumber, Integer pageSize);
}
