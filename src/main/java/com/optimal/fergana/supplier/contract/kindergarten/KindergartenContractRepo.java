package com.optimal.fergana.supplier.contract.kindergarten;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface KindergartenContractRepo extends JpaRepository<KindergartenContract, UUID> {

    Optional<KindergartenContract> findByKindergarten_Id(Integer kindergarten_id);
}
