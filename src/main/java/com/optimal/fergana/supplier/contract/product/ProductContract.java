package com.optimal.fergana.supplier.contract.product;


import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.acceptedProducts.AcceptedProduct;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContract;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductContract {

    @Id
    private UUID id = UUID.randomUUID();

    @Column(precision = 19, scale = 6)
    private BigDecimal weight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal pack = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal packWeight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal successWeight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal successPackWeight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal price;

    @Column(precision = 19, scale = 6)
    private BigDecimal totalSum = BigDecimal.valueOf(0);

    private Boolean successState = true;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;


    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private KindergartenContract kindergartenContract;


    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productContract")
    private List<AcceptedProduct> acceptedProductList;

    public ProductContract(BigDecimal weight, BigDecimal price, BigDecimal totalSum, Product product, KindergartenContract kindergartenContract) {
        this.weight = weight;
        this.price = price;
        this.totalSum = totalSum;
        this.product = product;
        this.kindergartenContract = kindergartenContract;
    }


    public ProductContract(BigDecimal weight, BigDecimal pack, BigDecimal packWeight, BigDecimal price, BigDecimal totalSum, Product product, KindergartenContract kindergartenContract) {
        this.weight = weight;
        this.pack = pack;
        this.packWeight = packWeight;
        this.price = price;
        this.totalSum = totalSum;
        this.product = product;
        this.kindergartenContract = kindergartenContract;
    }
}
