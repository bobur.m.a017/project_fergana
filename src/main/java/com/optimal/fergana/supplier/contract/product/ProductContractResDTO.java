package com.optimal.fergana.supplier.contract.product;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductContractResDTO {
    private UUID id;
    private BigDecimal weight;
    private BigDecimal pack;
    private BigDecimal packWeight;

    private BigDecimal successWeight;
    private BigDecimal successPackWeight;

    private BigDecimal residualWeight;
    private BigDecimal residualPackWeight;

    private BigDecimal price;
    private Integer productId;
    private String productName;
    private BigDecimal totalSum;
    private BigDecimal successTotalSum;
    private BigDecimal residualTotalSum;
    private String yetkazibBeruvchi;
    private String shartnomaRaqami;
}
