package com.optimal.fergana.supplier.contract.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ProductContractRepo extends JpaRepository<ProductContract, UUID> {

    Page<ProductContract> findAllByKindergartenContract_Contract_StatusAndKindergartenContract_Kindergarten_IdAndSuccessStateIsTrueOrderByProduct_Name(String kindergartenContract_contract_status, Integer kindergartenContract_kindergarten_id, Pageable pageable);
}
