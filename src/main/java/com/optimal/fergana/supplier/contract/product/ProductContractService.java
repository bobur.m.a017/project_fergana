package com.optimal.fergana.supplier.contract.product;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.productPack.ProductPackRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.supplier.contract.ContractRepo;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContract;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ProductContractService {


    private final ProductRepo productRepo;
    private final ProductContractRepo productContractRepo;
    private final ContractRepo contractRepo;
    private final CustomConverter converter;
    private final ProductPackRepo productPackRepo;
    private final ProductPackService productPackService;


    public ProductContract add(ProductContractDTO dto, KindergartenContract kindergartenContract) {
        Optional<Product> optionalProduct = productRepo.findById(dto.getProductId());
        if (optionalProduct.isPresent()) {


            Product product = optionalProduct.get();

            BigDecimal weight = BigDecimal.valueOf(dto.getPackWeight());
            BigDecimal packWeight = BigDecimal.valueOf(dto.getPackWeight());
            BigDecimal pack = BigDecimal.ZERO;
            BigDecimal price = BigDecimal.valueOf(dto.getPrice());


            BigDecimal checkPack = productPackService.checkPack(product.getId(), kindergartenContract.getContract().getDepartment().getId());

            if (checkPack != null) {
                BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.ONE);
                if (bigDecimals[1].compareTo(BigDecimal.ZERO) > 0) {
                    return null;
                }
                pack = checkPack;
                weight = pack.multiply(packWeight);
            }
            return new ProductContract(
                    weight, pack, packWeight, price, packWeight.multiply(price), product, kindergartenContract
            );
        }
        return null;
    }

    public Set<ProductContract> add(List<ProductContractDTO> dtoList, KindergartenContract kindergartenContract) {

        Set<ProductContract> list = new HashSet<>();

        for (ProductContractDTO productContractDTO : dtoList) {
            ProductContract productContract = add(productContractDTO, kindergartenContract);
            if (productContract == null) {
                return null;
            }
            list.add(productContract);
        }
        return list;
    }

    public Set<ProductContract> edit(KindergartenContract kindergartenContract, List<ProductContractDTO> productContracts) {

        Set<ProductContract> productContractSet = kindergartenContract.getProductContracts();
        Set<ProductContract> deleteSet = new HashSet<>();
        Set<ProductContract> returnSet = new HashSet<>();


        for (ProductContract productContract : productContractSet) {
            boolean res = true;
            for (ProductContractDTO contract : productContracts) {
                if (contract.getId().equals(productContract.getId())) {
                    res = false;
                    break;
                }
            }
            if (res) {
                deleteSet.add(productContract);
            }
        }

        for (ProductContractDTO productContract : productContracts) {
            if (productContract.getId() != null) {

                Optional<ProductContract> optionalProductContract = productContractRepo.findById(productContract.getId());
                if (optionalProductContract.isPresent()) {

                    BigDecimal weight = BigDecimal.valueOf(productContract.getPackWeight());
                    Double pack = productContract.getPack();
                    BigDecimal packWeight = BigDecimal.valueOf(productContract.getPackWeight());
                    BigDecimal price = BigDecimal.valueOf(productContract.getPrice());


                    if (pack > 0) {
                        weight = packWeight.multiply(BigDecimal.valueOf(pack));
                    }

                    ProductContract contract = optionalProductContract.get();
                    contract.setPrice(price);
                    contract.setWeight(weight);
                    contract.setTotalSum(packWeight.multiply(price));
                    contract.setPack(BigDecimal.valueOf(pack));
                    contract.setPackWeight(packWeight);

                    returnSet.add(contract);
                }
            } else {
                returnSet.add(add(productContract, kindergartenContract));
            }
        }
        productContractRepo.deleteAll(deleteSet);

        return new HashSet<>(productContractRepo.saveAll(returnSet));
    }

    public CustomPageable getAll(Users users, Integer page, Integer pageSize) {

        List<ProductContract> list = new ArrayList<>();

        if (page == null)
            page = 0;
        if (pageSize == null)
            pageSize = 10;


//        Sort sort = new Sort();

        Pageable pageable = PageRequest.of(page, pageSize);

        if (users.getKindergarten() != null) {
            Kindergarten kindergarten = users.getKindergarten();
            Page<ProductContract> productContractPage = productContractRepo.findAllByKindergartenContract_Contract_StatusAndKindergartenContract_Kindergarten_IdAndSuccessStateIsTrueOrderByProduct_Name(Status.TASDIQLANDI.getName(), kindergarten.getId(), pageable);

            List<ProductContractResDTO> dtoList = converter.productContractToDTO(new HashSet<>(productContractPage.stream().toList()));
            List<ProductContractResDTO> dtoList2 = new ArrayList<>();

            for (ProductContractResDTO productContractResDTO : dtoList) {
                if (productContractResDTO.getResidualWeight().compareTo(BigDecimal.valueOf(0)) > 0) {
                    dtoList2.add(productContractResDTO);
                }
            }

            return new CustomPageable(pageSize, page, dtoList2, productContractPage.getTotalElements());
//            for (Contract contract : contractList) {
//                if (contract.isVerified() && (!contract.isFullyCompleted()) && contract.getStatus().equals(Status.TASDIQLANDI.getName())) {
//                    for (KindergartenContract kindergartenContract : contract.getKindergartenContractList()) {
//                        if (kindergartenContract.getKindergarten().getId().equals(kindergarten.getId())) {
//                            for (ProductContract productContract : kindergartenContract.getProductContracts()) {
//                                if (productContract.getWeight().compareTo(productContract.getSuccessWeight()) > 0) {
//                                    list.add(productContract);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
        }

        return new CustomPageable();
    }
}