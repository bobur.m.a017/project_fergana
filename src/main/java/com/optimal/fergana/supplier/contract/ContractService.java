package com.optimal.fergana.supplier.contract;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.productPack.ProductPack;
import com.optimal.fergana.product.productPack.ProductPackRepo;
import com.optimal.fergana.product.product_price.ProductPrice;
import com.optimal.fergana.product.product_price.ProductPriceRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.supplier.SupplierRepo;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContract;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContractDTO;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContractService;
import com.optimal.fergana.supplier.contract.product.ProductContractDTO;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Service
public class ContractService implements ContractServiceInterface {

    private final ContractRepo contractRepo;
    private final SupplierRepo supplierRepo;
    private final DepartmentRepo departmentRepo;
    private final KindergartenContractService kindergartenContractService;
    private final ProductPriceRepo productPriceRepo;
    private final CustomConverter converter;
    private final ProductPackRepo productPackRepo;


    public StateMessage add(ContractDTO dto, Users user) {

        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        if (dto.getSupplierId() == null || dto.getStartDay() == null || dto.getEndDay() == null){

            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
            return new StateMessage().parse(message);
        }




        Optional<Supplier> optionalSupplier = supplierRepo.findById(dto.getSupplierId());
        Department department = user.getDepartment();

        if (!(contractRepo.existsAllByNumber(dto.getNumber())) && optionalSupplier.isPresent()) {
            if (!(checkPrice(dto, department.getRegionalDepartment(), dto.getStartDay().toLocalDateTime().toLocalDate()))) {
                message = Message.THE_CONTRACT_DOES_NOT_THE_SPECIFIED_CRITERIA;
            } else {

                StateMessage stateMessage = checkPack(dto, department);
                if (stateMessage.isSuccess()) {
                    Contract contract = new Contract(
                            false,
                            dto.getNumber(),
                            BigDecimal.valueOf(0),
                            dto.getLotNumber(),
                            dto.getStartDay(),
                            dto.getEndDay(),
                            optionalSupplier.get(),
                            department,
                            user
                    );
                    List<KindergartenContract> contractList = kindergartenContractService.add(dto.getKindergartenContractList(), contract);

                    if (contractList != null) {
                        contract.setKindergartenContractList(contractList);
                        contract.setTotalSum(getTotalSum(contractList));
                        contract.setCreatedBy(user);
                        contract.setStatus(Status.NEW.getName());
                        contract.setUpdatedBy(user);
                        contractRepo.save(contract);
                        message = Message.SUCCESS_UZ;
                    }
                } else {
                    return stateMessage;
                }
            }
        }
        return new StateMessage().parse(message);
    }

    public boolean checkPrice(ContractDTO contractDTO, RegionalDepartment regionalDepartment, LocalDate date) {
        for (KindergartenContractDTO kindergartenContractDTO : contractDTO.getKindergartenContractList()) {
            for (ProductContractDTO productContractDTO : kindergartenContractDTO.getProductContracts()) {

                if (productContractDTO.getProductId() < 1) {
                    return false;
                }
                List<ProductPrice> productPriceList = productPriceRepo.getProductPriceCheck(productContractDTO.getProductId(), regionalDepartment.getId(), date, false);
                if (productPriceList.size() > 0) {
                    for (ProductPrice productPrice : productPriceList) {
                        if (BigDecimal.valueOf(productContractDTO.getPrice()).compareTo(productPrice.getMaxPrice()) > 0) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public StateMessage checkPack(ContractDTO contractDTO, Department department) {

        StateMessage message = new StateMessage("", true, 200);

        for (KindergartenContractDTO kindergartenContractDTO : contractDTO.getKindergartenContractList()) {
            for (ProductContractDTO productContract : kindergartenContractDTO.getProductContracts()) {

                Optional<ProductPack> optionalProductPack = productPackRepo.findByProduct_IdAndDepartment_Id(productContract.getProductId(), department.getId());

                if (optionalProductPack.isPresent()) {
                    ProductPack productPack = optionalProductPack.get();
                    double pack = productPack.getPack().doubleValue();
                    double qoldiq = productContract.getPackWeight() % 1;

                    if (qoldiq > 0) {
                        message.setSuccess(false);
                        message.setText(message.getText() + ", " + productPack.getProduct().getName());
                    } else {
                        productContract.setPack(pack);
                    }
                }
            }
        }
        if (!message.isSuccess()) {
            message.setText(message.getText() + " ushbu maxsulotlarni yarimtalab qabul qilib bo`lmaydi.");
        }

        return message;
    }

    public StateMessage edit(ContractDTO contractDTO, Integer id, Users users) {

        Optional<Contract> optionalContract = contractRepo.findById(id);

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optionalContract.isPresent()) {

            Contract contract = optionalContract.get();

            if (contract.getStatus().equals(Status.NEW.getName())) {

                if (!(checkPrice(contractDTO, contract.getDepartment().getRegionalDepartment(), contractDTO.getStartDay().toLocalDateTime().toLocalDate()))) {
                    message = Message.THE_CONTRACT_DOES_NOT_THE_SPECIFIED_CRITERIA;
                } else {
                    contract.setLotNumber(contractDTO.getLotNumber());
                    contract.setNumber(contractDTO.getNumber());
                    contract.setStartDay(contractDTO.getStartDay());
                    contract.setEndDay(contractDTO.getEndDay());


                    List<KindergartenContract> edit = kindergartenContractService.edit(contractDTO.getKindergartenContractList(), contract);
                    contract.setTotalSum(getTotalSum((edit)));

                    contract.setUpdatedBy(users);
                    contractRepo.save(contract);
                    message = Message.EDIT_UZ;
                }
            }
        }
        return new StateMessage().parse(message);
    }

    public BigDecimal getTotalSum(List<KindergartenContract> kindergartenContractList) {
        BigDecimal totalSum = BigDecimal.valueOf(0);

        for (KindergartenContract kindergartenContract : kindergartenContractList) {
            totalSum = totalSum.add(kindergartenContract.getTotalSum());
        }
        return totalSum;
    }

    public StateMessage delete(Integer id) {
        Optional<Contract> optionalContract = contractRepo.findById(id);
        Message message = Message.THE_CONTRACT_CANNOT_BE_CANCELED;

        if (optionalContract.isPresent()) {
            Contract contract = optionalContract.get();
            if (contract.getStatus().equals(Status.NEW.getName()) && (!contract.isFullyCompleted())) {
                contract.setDelete(true);
                contractRepo.save(contract);
                message = Message.DELETE_UZ;
            }
        }
        return new StateMessage().parse(message);
    }

    public ContractResDTO getOne(Integer id) {
        Optional<Contract> optionalContract = contractRepo.findById(id);
        if (optionalContract.isPresent()) {
            Contract contract = optionalContract.get();

            ContractResDTO contractResDTO = converter.contractToDTO(contract);
            contractResDTO.setKindergartenContractList(converter.kindergartenContractToDTO(contract.getKindergartenContractList()));
            return contractResDTO;
        } else {
            throw new UsernameNotFoundException("this object not found in the database");
        }
    }

    public CustomPageable getAll(Users users, Integer pageSize, Integer page, Integer supplierId) {

        if (page == null)
            page = 0;
        if (pageSize == null)
            pageSize = 20;


        Pageable pageable = PageRequest.of(page, pageSize);
        Page<Contract> contractPage = null;

        if (supplierId == null && users.getDepartment() != null) {
            contractPage = contractRepo.findAllByDeleteIsFalseAndDepartment_Id(users.getDepartment().getId(), pageable);
        } else if (supplierId == null && users.getDepartment() == null) {
            contractPage = contractRepo.findAllByDeleteIsFalseAndDepartment_RegionalDepartment_Id(users.getRegionalDepartment().getId(), pageable);
        } else if (supplierId != null && users.getDepartment() != null) {
            contractPage = contractRepo.findAllByDeleteIsFalseAndDepartment_IdAndSupplier_Id(users.getDepartment().getId(), supplierId, pageable);
        } else if (supplierId != null && users.getDepartment() == null) {
            contractPage = contractRepo.findAllByDeleteIsFalseAndDepartment_RegionalDepartment_IdAndSupplier_Id(users.getRegionalDepartment().getId(), supplierId, pageable);
        }

        if (contractPage != null) {

            return new CustomPageable(contractPage.getSize(), contractPage.getNumber(), converter.contractToDTO(contractPage.stream().toList()), 0);
        } else {
            throw new UsernameNotFoundException("Siz kiritgan paramertlar bo`yicha ma‘lumot mavjud emas");
        }
    }

    public StateMessage confirmation(Users users, Integer id) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Optional<Contract> optionalContract = contractRepo.findById(id);
        if (optionalContract.isPresent()) {
            Contract contract = optionalContract.get();
            if (contract.getStatus().equals(Status.NEW.getName()) && (!contract.isVerified())) {
                contract.setStatus(Status.TASDIQLANDI.getName());
                contract.setVerifiedBy(users);
                contract.setVerified(true);
                contractRepo.save(contract);
                message = Message.SUCCESS_VERIFIED;
            }
        }
        return new StateMessage().parse(message);
    }

    @Scheduled(fixedDelay = 1000 * 60 * 60)
    public void checkContractPeriod() {
        List<Contract> contractList = contractRepo.checkContractPeriod(LocalDate.now());
        for (Contract contract : contractList) {
            contract.setStatus(Status.SHARTNOMA_MUDDATI_TUGADI.getName());
            contractRepo.save(contract);
        }
    }
}
