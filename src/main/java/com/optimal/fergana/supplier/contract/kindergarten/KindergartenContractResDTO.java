package com.optimal.fergana.supplier.contract.kindergarten;

import com.optimal.fergana.supplier.contract.product.ProductContractResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KindergartenContractResDTO {

    private UUID id;
    private Integer kindergartenId;
    private Integer number;
    private String kindergartenName;
    private List<ProductContractResDTO> productContracts;
    private BigDecimal totalSum;
}