package com.optimal.fergana.attachment;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface AttachmentServiceInter {
    Attachment add(MultipartFile file) throws IOException;
    Attachment edit(MultipartFile file, Integer id);
    byte[] attachmentToBytes(Integer id);
}
