package com.optimal.fergana.attachment;

import com.optimal.fergana.statics.StaticWords;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

@Service
public record AttachmentService(
        AttachmentRepository attachmentRepository) implements AttachmentServiceInter{

    public Attachment add(MultipartFile file) throws IOException {
        try {
            return attachmentRepository.save(uploadSystem(file));
        } catch (Exception e) {
            return null;
        }
    }

    public Attachment edit(MultipartFile file, Integer id) {

        Attachment attachment = attachmentRepository.findById(id).get();

        try {
            return uploadSystemEdit(file, attachment);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return attachmentRepository.save(attachment);
    }

        public static final String uploadDirectories = StaticWords.DEST;
//    public static final String uploadDirectories = "D:\\Git hub\\project_fergana\\files";

    private Attachment uploadSystem(MultipartFile file) throws IOException {


        if (file != null) {
            String originalFilename = file.getOriginalFilename();
            Attachment attachment = new Attachment();


            if (originalFilename == null) {

                originalFilename = "123456";
            }

            attachment.setFileOriginalName(originalFilename);
            attachment.setSize(file.getSize());
            attachment.setContentType(file.getContentType());


            //FILENING CONTENTINI OLISH UCHUN KERAK
            String[] split = originalFilename.split("\\.");

            //rasm nameni unique qilish uchun kerak
            String name = UUID.randomUUID().toString() + "." + split[split.length - 1];
            attachment.setName(name);

            //papka saqlanadigan yo'l
            Path path = Paths.get(uploadDirectories + "/" + name);

            attachment.setPath(path.toString());

            Attachment saveAttachment = attachmentRepository.save(attachment);

            Files.copy(file.getInputStream(), path);

            return saveAttachment;
        }
        return null;
    }

    private Attachment uploadSystemEdit(MultipartFile file, Attachment attachment) throws IOException {


        if (file != null) {
            String originalFilename = file.getOriginalFilename();


            if (originalFilename == null) {

                originalFilename = "123456";
            }

            attachment.setFileOriginalName(originalFilename);
            attachment.setSize(file.getSize());
            attachment.setContentType(file.getContentType());


            //FILENING CONTENTINI OLISH UCHUN KERAK
//            String[] split = originalFilename.split("\\.");

            //rasm nameni unique qilish uchun kerak
            String name = UUID.randomUUID().toString();
            attachment.setName(name);

            //papka saqlanadigan yo'l
            Path path = Paths.get(uploadDirectories + "/" + name);

            attachment.setPath(path.toString());

            Attachment saveAttachment = attachmentRepository.save(attachment);

            Files.copy(file.getInputStream(), path);

            return saveAttachment;
        }
        return null;
    }

    public byte[] attachmentToBytes(Integer id) {
        try {
            Optional<Attachment> optionalAttachment = attachmentRepository.findById(id);
            if (optionalAttachment.isPresent()) {
                Attachment attachment = optionalAttachment.get();
                File file = new File(attachment.getPath());
                Path path = Paths.get(file.getPath());
                return Files.readAllBytes(path);
            }
            return null;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
