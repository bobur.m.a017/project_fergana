package com.optimal.fergana.regionalDepartment;

import com.optimal.fergana.address.region.Region;
import com.optimal.fergana.address.region.RegionRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.role.Role;
import com.optimal.fergana.role.RoleRepo;
import com.optimal.fergana.role.RoleType;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersRepo;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RegionalDepartmentService implements RegionalDepartmentInterface, RegionalDepartmentServiseInter {

    private final RegionalDepartmentRepo regionalDepartmentRepo;
    private final RegionRepo regionRepo;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepo roleRepo;
    private final UsersRepo usersRepo;


    public RegionalDepartmentService(RegionalDepartmentRepo regionalDepartmentRepo, RegionRepo regionRepo, PasswordEncoder passwordEncoder, RoleRepo roleRepo, UsersRepo usersRepo) {
        this.regionalDepartmentRepo = regionalDepartmentRepo;
        this.regionRepo = regionRepo;
        this.passwordEncoder = passwordEncoder;
        this.roleRepo = roleRepo;
        this.usersRepo = usersRepo;
    }

    public StateMessage add(RegionalDepartmentDTO dto) {

        Message message;
        Optional<Region> optional = regionRepo.findById(dto.getRegionId());
        if (optional.isPresent()) {
            RegionalDepartment regionalDepartment = new RegionalDepartment(dto.getName(), optional.get());


            message = Message.SUCCESS_UZ;

        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage(message.getName(), message.isState(), message.getCode());
    }


    public StateMessage edit(RegionalDepartmentDTO dto, Integer id) {

        Message message;
        Optional<Region> optional = regionRepo.findById(dto.getRegionId());
        Optional<RegionalDepartment> optionalRegionalDepartment = regionalDepartmentRepo.findById(id);

        if (optional.isPresent() && optionalRegionalDepartment.isPresent()) {

            RegionalDepartment regionalDepartment = optionalRegionalDepartment.get();
            regionalDepartment.setName(dto.getName());
            regionalDepartment.setRegion(optional.get());
            regionalDepartmentRepo.save(regionalDepartment);

            message = Message.EDIT_UZ;
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage(message.getName(), message.isState(), message.getCode());
    }

    public ResponseEntity<?> getOne(Integer id) {

        Optional<RegionalDepartment> optional = regionalDepartmentRepo.findById(id);
        if (optional.isPresent()) {
            return ResponseEntity.status(200).body(parse(optional.get()));
        }
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        return ResponseEntity.status(message.getCode()).body(new StateMessage(message.getName(), message.isState(), message.getCode()));
    }

    public ResponseEntity<?> getAll() {
        List<RegionalDepartmentResDTO> list = new ArrayList<>();

        for (RegionalDepartment regionalDepartment : regionalDepartmentRepo.findAll()) {
            list.add(parse(regionalDepartment));
        }
        return ResponseEntity.status(200).body(list);
    }

    public StateMessage delete(Integer id) {

        Optional<RegionalDepartment> optionalRegionalDepartment = regionalDepartmentRepo.findById(id);

        Message message;
        if (optionalRegionalDepartment.isPresent()) {
            regionalDepartmentRepo.delete(optionalRegionalDepartment.get());
            message = Message.DELETE_UZ;
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }

        return new StateMessage().parse(message);
    }
}
