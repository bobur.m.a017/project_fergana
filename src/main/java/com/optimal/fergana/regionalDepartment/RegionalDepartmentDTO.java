package com.optimal.fergana.regionalDepartment;


import com.sun.istack.NotNull;

public class RegionalDepartmentDTO {
    @NotNull
    private String name;
    @NotNull
    private Integer regionId;

    public RegionalDepartmentDTO(String name, Integer regionId) {
        this.name = name;
        this.regionId = regionId;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public RegionalDepartmentDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
