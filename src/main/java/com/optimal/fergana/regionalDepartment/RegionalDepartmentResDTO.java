package com.optimal.fergana.regionalDepartment;


public class RegionalDepartmentResDTO {

    private Integer id;
    private String name;
    private String regionName;

    public RegionalDepartmentResDTO() {
    }

    public RegionalDepartmentResDTO(Integer id, String name, String regionName) {
        this.id = id;
        this.name = name;
        this.regionName = regionName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}
