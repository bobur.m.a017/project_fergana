package com.optimal.fergana.regionalDepartment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionalDepartmentRepo extends JpaRepository<RegionalDepartment,Integer> {

}
