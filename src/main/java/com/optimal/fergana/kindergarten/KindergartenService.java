package com.optimal.fergana.kindergarten;


import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.kindergarten.dto.KindergartenByAddressDTO;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class KindergartenService implements KindergartenServiceInter {

    private final KindergartenRepo kindergartenRepo;
    private final DepartmentRepo departmentRepo;
    private final CustomConverter converter;


    public StateMessage add(KindergartenDTO dto, Users users) {

        Optional<Department> optionalDepartment = departmentRepo.findById(dto.getDepartmentId());
        Message message;
        RegionalDepartment regionalDepartment = users.getRegionalDepartment();

        if (optionalDepartment.isPresent()) {
            Department department = optionalDepartment.get();
            boolean res = kindergartenRepo.existsAllByDepartmentAndNameAndNumberAndDeleteIsFalse(department, dto.getName(), dto.getNumber());
            if (res && regionalDepartment.getId().equals(department.getRegionalDepartment().getId())) {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            } else {
                Kindergarten kindergarten = (new Kindergarten(
                        dto.getName(),
                        dto.getNumber(),
                        Status.ACTIVE.getName(),
                        department,
                        regionalDepartment,
                        dto.getStreet()

                ));
                List<Kindergarten> kindergartenList = department.getKindergartenList() != null ? department.getKindergartenList() : new ArrayList<>();
                kindergartenList.add(kindergarten);
                department.setKindergartenList(kindergartenList);
                departmentRepo.save(department);
                message = Message.SUCCESS_UZ;
            }
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage edit(KindergartenDTO dto, Users users, Integer id) {

        Optional<Department> optionalDepartment = departmentRepo.findById(dto.getDepartmentId());
        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(id);
        Message message;

        if (optionalDepartment.isPresent() && optionalKindergarten.isPresent()) {
            Department department = optionalDepartment.get();
            Kindergarten kindergarten = optionalKindergarten.get();

            boolean res = false;
            if ((!(dto.getName().equals(kindergarten.getName())) || !(dto.getNumber().equals(kindergarten.getNumber())))) {
                res = kindergartenRepo.existsAllByDepartmentAndNameAndNumberAndDeleteIsFalse(department, dto.getName(), dto.getNumber());
            }

            if (res && users.getRegionalDepartment().getId().equals(department.getRegionalDepartment().getId())) {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            } else {

                if (!(dto.getName().equals(kindergarten.getName()))) {
                    kindergarten.setName(dto.getName());
                }


                if (!(dto.getStreet().equals(kindergarten.getStreet()))) {
                    kindergarten.setStreet(dto.getStreet());
                }


                if (!(dto.getNumber().equals(kindergarten.getNumber()))) {
                    kindergarten.setNumber(dto.getNumber());
                }
                if (kindergarten.getDepartment().equals(department)) {
                    Department oldDepartment = kindergarten.getDepartment();
                    List<Kindergarten> list = oldDepartment.getKindergartenList();
                    list.remove(kindergarten);
                    oldDepartment.setKindergartenList(list);

                    kindergarten.setDepartment(department);
                    List<Kindergarten> kindergartenList = department.getKindergartenList() != null ? department.getKindergartenList() : new ArrayList<>();
                    kindergartenList.add(kindergarten);
                    department.setKindergartenList(kindergartenList);
                    departmentRepo.save(department);
                    departmentRepo.save(oldDepartment);
                }
                kindergartenRepo.save(kindergarten);
                message = Message.EDIT_UZ;
            }
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage().parse(message);
    }

    @Override
    public ResponseEntity<?> getOne(Integer id) {

        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(id);

        if (optionalKindergarten.isPresent()) {
            Kindergarten kindergarten = optionalKindergarten.get();
            KindergartenResDTO dto = converter.kindergartenToDTO(kindergarten);
            dto.setUsersList(converter.userToDTO(kindergarten.getUsersList()));
            return ResponseEntity.status(200).body(dto);
        }
        StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
        return ResponseEntity.status(message.getCode()).body(message);
    }

    public CustomPageable getAll(Users users, Integer departmentId, Integer number, Integer pageNumber, Integer pageSize) {

        Page<Kindergarten> page;
        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20, Sort.by("number").ascending());

        long count;

        if (departmentId != null && number != null) {
            page = kindergartenRepo.findAllByRegionalDepartment_IdAndDepartment_IdAndNumberAndDeleteIsFalse(users.getRegionalDepartment().getId(), departmentId, number, pageable);
            count = kindergartenRepo.countAllByRegionalDepartment_IdAndDepartment_IdAndNumberAndDeleteIsFalse(users.getRegionalDepartment().getId(), departmentId, number);
        } else if (departmentId != null) {
            page = (kindergartenRepo.findAllByRegionalDepartment_IdAndDepartment_IdAndDeleteIsFalse(users.getRegionalDepartment().getId(), departmentId, pageable));
            count = kindergartenRepo.countAllByRegionalDepartment_IdAndDepartment_IdAndDeleteIsFalse(users.getRegionalDepartment().getId(), departmentId);
        } else if (number != null) {
            page = (kindergartenRepo.findAllByRegionalDepartment_IdAndNumberAndDeleteIsFalse(users.getRegionalDepartment().getId(), number, pageable));
            count = kindergartenRepo.countAllByRegionalDepartment_IdAndNumberAndDeleteIsFalse(users.getRegionalDepartment().getId(), number);
        } else {
            page = (kindergartenRepo.findAllByRegionalDepartment_IdAndDeleteIsFalse(users.getRegionalDepartment().getId(), pageable));
            count = kindergartenRepo.count();
        }
        List<Kindergarten> list = new ArrayList<>(page.stream().toList());

        list.sort(Comparator.comparing(Kindergarten::getNumber));

        List<KindergartenResDTO> dtoList = converter.kindergartenToDTO(list);
        dtoList.sort(Comparator.comparing(KindergartenResDTO::getNumber));

        return new CustomPageable(page.getSize(), page.getNumber(), dtoList, count);
    }


    public List<KindergartenByAddressDTO> get(Users users) {

        List<Kindergarten> list = kindergartenRepo.findAll();
        List<KindergartenByAddressDTO> dtoList = new ArrayList<>();

        for (Kindergarten kindergarten : list) {
            dtoList.add(new KindergartenByAddressDTO(kindergarten.getId(), kindergarten.getNumber(), kindergarten.getName(), false, kindergarten.getDepartment().getId()));
        }
        return dtoList;
    }

    @Override
    public List<KindergartenResDTO> getByDepartmentId(Users users) {

        List<Kindergarten> kindergartenList = users.getDepartment().getKindergartenList();
        return converter.kindergartenToDTO(kindergartenList);
    }

    public List<KindergartenResDTO> getByDepartmentId(Integer id, Users users) {

        Optional<Department> optionalDepartment = departmentRepo.findById(id);

        if (optionalDepartment.isPresent()) {

            List<Kindergarten> kindergartenList = new ArrayList<>();

            if (users.getKindergarten() != null) {
                kindergartenList.add(users.getKindergarten());
            } else {
                kindergartenList = optionalDepartment.get().getKindergartenList();
            }


            kindergartenList.sort(Comparator.comparing(Kindergarten::getNumber));
            return converter.kindergartenToDTO(kindergartenList);
        }
        return new ArrayList<>();
    }


    public StateMessage delete(Users users, Integer id) {

        RegionalDepartment regionalDepartment = users.getRegionalDepartment();
        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(id);

        Message message;

        if (optionalKindergarten.isPresent()) {
            Kindergarten kindergarten = optionalKindergarten.get();
            if (kindergarten.getRegionalDepartment().getId().equals(regionalDepartment.getId())) {
                kindergarten.setDelete(true);
                kindergartenRepo.save(kindergarten);
                message = Message.DELETE_UZ;
            } else {
                message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
            }
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage().parse(message);
    }
}
