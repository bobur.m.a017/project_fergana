package com.optimal.fergana.kindergarten;

import com.optimal.fergana.department.Department;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;

public interface KindergartenRepo extends JpaRepository<Kindergarten, Integer> {

    Page<Kindergarten> findAllByIdAndDeleteIsFalse(Integer id, Pageable pageable);

    List<Kindergarten> findAllByIdAndDeleteIsFalse(Integer id);


    Page<Kindergarten> findAllByDepartment_IdAndDeleteIsFalse(Integer department_id, Pageable pageable);

    List<Kindergarten> findAllByDepartment_IdAndDeleteIsFalse(Integer id);

    boolean existsAllByDepartmentAndNameAndNumberAndDeleteIsFalse(Department department, String name, Integer number);

    List<Kindergarten> findAllByRegionalDepartmentAndDeleteIsFalse(RegionalDepartment regionalDepartment);

    Page<Kindergarten> findAllByRegionalDepartment_IdAndDepartment_IdAndNumberAndDeleteIsFalse(Integer regionalDepartment_id, Integer department_id, Integer number, Pageable pageable);

    Page<Kindergarten> findAllByRegionalDepartment_IdAndDepartment_IdAndDeleteIsFalse(Integer regionalDepartment_id, Integer department_id, Pageable pageable);

    Page<Kindergarten> findAllByRegionalDepartment_IdAndNumberAndDeleteIsFalse(Integer regionalDepartment_id, Integer number, Pageable pageable);

    Page<Kindergarten> findAllByRegionalDepartment_IdAndDeleteIsFalse(Integer regionalDepartment_id, Pageable pageable);

    int countAllByRegionalDepartment_IdAndDeleteIsFalse(Integer regionalDepartment_Id);

    int countAllByDepartment_IdAndDeleteIsFalseAndDeleteIsFalse(Integer regionalDepartment_Id);

    int countAllByDepartment_IdAndDeleteIsFalse(Integer department_Id);

    @Override
    long count();

    Long countAllByRegionalDepartment_IdAndDepartment_IdAndNumberAndDeleteIsFalse(Integer regionalDepartment_id, Integer department_id, Integer number);

    Long countAllByRegionalDepartment_IdAndDepartment_IdAndDeleteIsFalse(Integer regionalDepartment_id, Integer department_id);

    Long countAllByRegionalDepartment_IdAndNumberAndDeleteIsFalse(Integer regionalDepartment_id, Integer number);




    Optional<Kindergarten> findBySTIR(@Size(min = 9, max = 9) String STIR);
}
