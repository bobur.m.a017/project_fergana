package com.optimal.fergana.kindergarten;


import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.dto.KidsNumberResDTO;
import com.optimal.fergana.users.UsersResDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.OneToOne;
import java.util.List;


@Getter
@Setter
public class KindergartenResDTO {

    private Integer id;
    private String name;
    private Integer number;
    private String status;
    private String departmentName;
    private Integer departmentId;
    private String regionalDepartmentName;
    private Integer regionalDepartmentId;
    private String street;
    private List<UsersResDTO> usersList;
    private KidsNumberResDTO averageKidsNumber;
    private Integer stayTimeNumber;

    public KindergartenResDTO() {
    }

    public KindergartenResDTO(Integer id, String name, Integer number, String status, String departmentName, Integer departmentId, String regionalDepartmentName, Integer regionalDepartmentId, String street, Integer stayTimeNumber) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.status = status;
        this.departmentName = departmentName;
        this.departmentId = departmentId;
        this.regionalDepartmentName = regionalDepartmentName;
        this.regionalDepartmentId = regionalDepartmentId;
        this.street = street;
        this.stayTimeNumber = stayTimeNumber;
    }
}
