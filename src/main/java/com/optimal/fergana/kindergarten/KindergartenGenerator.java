package com.optimal.fergana.kindergarten;
import com.optimal.fergana.address.district.District;
import com.optimal.fergana.address.region.Region;
import com.optimal.fergana.address.region.RegionRepo;
import com.optimal.fergana.department.Department;

import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentService;
import com.optimal.fergana.role.Role;
import com.optimal.fergana.role.RoleRepo;
import com.optimal.fergana.role.RoleType;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@RequiredArgsConstructor
@Service
public class KindergartenGenerator {
    private final RegionRepo regionRepo;
    private final RoleRepo roleRepo;
    private final PasswordEncoder passwordEncoder;
    private final RegionalDepartmentService regionalDepartmentService;
    private final DepartmentRepo departmentRepo;
    private final RegionalDepartmentRepo regionalDepartmentRepo;


    public void generator() {

        Region region = regionRepo.findById(12).get();

        RegionalDepartment regionalDepartment = regionalDepartmentRepo.findById(1).get();

        List<Department> departmentList = new ArrayList<>();

        for (District district : region.getDistrictList()) {

            Department department = new Department(district.getName(), district, regionalDepartment);
            department.setUsersList(addUser(regionalDepartment, department));

            List<Kindergarten> kindergartenList = new ArrayList<>();

            for (int i = 1; i < 71; i++) {

                Kindergarten kindergarten = (new Kindergarten(
                        "-DMTT",
                        i,
                        Status.ACTIVE.getName(),
                        department,
                        regionalDepartment,
                        "Alisher navoiy ko`chasi " + i + "-uy")

                );
                kindergartenList.add(kindergarten);
            }
            department.setKindergartenList(kindergartenList);
            departmentList.add(department);
        }
        departmentRepo.saveAll(departmentList);
    }

    public List<Users> addUser(RegionalDepartment regionalDepartment, Department department) {

        District district = department.getDistrict();
        Role role = roleRepo.findByName(RoleType.HUMAN_RESOURCES.getName()).get();
        Role role1 = roleRepo.findByName(RoleType.ACCOUNTANT.getName()).get();

        Users users = new Users(true, false, "xodim" + district.getName(), passwordEncoder.encode("xodim" + district.getName()), List.of(role), regionalDepartment);
        Users users1 = new Users(true, false, "buxgalter" + district.getName(), passwordEncoder.encode("buxgalter" + district.getName()), List.of(role1), regionalDepartment);
        users.setDepartment(department);
        users1.setDepartment(department);
        List<Users> usersList = new ArrayList<>();
        usersList.add(users);
        usersList.add(users1);
        return usersList;
        }

}

