package com.optimal.fergana.kindergarten;

import com.optimal.fergana.kindergarten.dto.KindergartenByAddressDTO;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface KindergartenServiceInter {
    StateMessage add(KindergartenDTO dto, Users users);
    StateMessage edit(KindergartenDTO dto, Users users, Integer id);
    ResponseEntity<?> getOne(Integer id);
    CustomPageable getAll(Users users, Integer departmentId, Integer number, Integer pageNumber, Integer pageSize);
    List<KindergartenByAddressDTO> get(Users users);
    List<KindergartenResDTO> getByDepartmentId(Users users);
}
