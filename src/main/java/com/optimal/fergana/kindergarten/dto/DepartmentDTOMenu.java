package com.optimal.fergana.kindergarten.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentDTOMenu {

    private Integer departmentId;
    private String departmentName;
    private int attachedNumber;
    private int notAttachedNumber;
    private boolean attached;
    private List<KindergartenByAddressDTO> kindergartenList;
    private Date date;

    public DepartmentDTOMenu(Integer departmentId, String departmentName) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
    }
}
