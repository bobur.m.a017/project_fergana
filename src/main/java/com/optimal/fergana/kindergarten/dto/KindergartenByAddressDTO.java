package com.optimal.fergana.kindergarten.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KindergartenByAddressDTO {

    @NotNull
    private Integer id;
    private Integer number;
    private String name;
    private boolean attached;
    private UUID reportId;
    private String menuName;
    private Integer departmentId;
    private Timestamp date;
    private Integer stayTimeNumber;


    public KindergartenByAddressDTO(Integer id, Integer number, String name, boolean attached, Integer departmentId) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.attached = attached;
        this.departmentId = departmentId;
    }
}
