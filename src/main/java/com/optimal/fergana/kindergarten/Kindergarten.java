package com.optimal.fergana.kindergarten;


import com.optimal.fergana.accountant.Accountant;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.stayTime.StayTime;
import com.optimal.fergana.supplier.contract.Contract;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.Warehouse;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Kindergarten {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private Integer number;
    private String status;
    private String street;
    private Boolean delete = false;


    private String fullName;

    @Size(min = 9, max = 9)
    @NotNull
    @Column(unique = true)
    private String STIR;

    private String date;

    private String statusORG;

    private String THSHT;

    private String DBIBT;

    private String IFUT;

    private String fond;

    private String address;

    private String phoneNumber;

    private String director;


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private Department department;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private RegionalDepartment regionalDepartment;

    @OneToMany(mappedBy = "kindergarten", fetch = FetchType.LAZY)
    private List<Users> usersList;

    @OneToMany(mappedBy = "kindergarten", fetch = FetchType.LAZY)
    private List<Warehouse> warehouseList;

    @ManyToOne
    private Accountant accountant;

    @OneToOne
    private KidsNumber averageKidsNumber;


    @ManyToMany
    private List<StayTime> stayTimeList;


    public Kindergarten(String name, Integer number, String status, Department department, RegionalDepartment regionalDepartment, String street) {
        this.name = name;
        this.number = number;
        this.status = status;
        this.department = department;
        this.regionalDepartment = regionalDepartment;
        this.street = street;
    }
}
