package com.optimal.fergana.ageGroup;

import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AgeGroupRepo extends JpaRepository<AgeGroup, Integer> {


    boolean existsAllByName( String name);

    Optional<AgeGroup> findByNameAndDelete(String name, Boolean delete);

    List<AgeGroup> findAllByDeleteIsFalse();

    List<AgeGroup> findAllByDeleteIsFalseOrderBySortNumber();
}
