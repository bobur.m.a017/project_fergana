package com.optimal.fergana.ageGroup;

import com.sun.istack.NotNull;

public class AgeGroupDTO {
    @NotNull
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
