package com.optimal.fergana.ageGroup;

import java.util.ArrayList;
import java.util.List;

public interface AgeGroupInterface {

    default AgeGroupResDTO parse(AgeGroup ageGroup) {
        return new AgeGroupResDTO(
                ageGroup.getId(),
                ageGroup.getName(),
                ageGroup.getDelete(),
                ageGroup.getSortNumber());
    }

    default List<AgeGroupResDTO> parseList(List<AgeGroup> list) {
        List<AgeGroupResDTO> ageGroupResDTOList = new ArrayList<>();
        for (AgeGroup ageGroup : list) {
            ageGroupResDTOList.add(parse(ageGroup));
        }
        return ageGroupResDTOList;
    }
}
