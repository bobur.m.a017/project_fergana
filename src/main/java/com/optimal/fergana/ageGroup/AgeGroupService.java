package com.optimal.fergana.ageGroup;


import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.stayTime.StayTime;
import com.optimal.fergana.users.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class AgeGroupService implements AgeGroupInterface, AgeGroupServiceInter {

    @Autowired
    private AgeGroupRepo ageGroupRepo;


    public StateMessage add(AgeGroupDTO dto, Users users) {
        boolean res = ageGroupRepo.existsAllByName(dto.getName());
        Message message;
        if (!res) {
            ageGroupRepo.save(new AgeGroup(dto.getName(), false));
            message = Message.SUCCESS_UZ;
        } else {
            Optional<AgeGroup> optionalAgeGroup = ageGroupRepo.findByNameAndDelete(dto.getName(), true);
            if (optionalAgeGroup.isPresent()) {
                AgeGroup ageGroup = optionalAgeGroup.get();
                ageGroup.setDelete(false);
                ageGroupRepo.save(ageGroup);
                message = Message.SUCCESS_UZ;
            } else {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            }
        }
        return new StateMessage(message.getName(), message.isState(), message.getCode());
    }

    public ResponseEntity<?> getOne(Integer id) {

        Optional<AgeGroup> optional = ageGroupRepo.findById(id);
        if (optional.isPresent()) {
            AgeGroup ageGroup = optional.get();
            return ResponseEntity.status(200).body(parse(ageGroup));
        }
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        return ResponseEntity.status(message.getCode()).body(new StateMessage(message.getName(), message.isState(), message.getCode()));
    }

    public ResponseEntity<?> getAll(Users users) {

        List<AgeGroup> ageGroupList = new ArrayList<>();

        if (users.getKindergarten() != null) {
            Kindergarten kindergarten = users.getKindergarten();
            for (StayTime stayTime : kindergarten.getStayTimeList()) {
                ageGroupList.addAll(stayTime.getAgeGroupList());
            }
        } else {
            ageGroupList = ageGroupRepo.findAllByDeleteIsFalse();
        }


        List<AgeGroupResDTO> list = new ArrayList<>();

        for (AgeGroup ageGroup : ageGroupList) {
            list.add(parse(ageGroup));
        }
        list.sort(Comparator.comparing(AgeGroupResDTO::getSortNumber));

        return ResponseEntity.status(200).body(list);
    }

    public StateMessage edit(Integer id, AgeGroupDTO dto, Users users) {

        Optional<AgeGroup> optional = ageGroupRepo.findById(id);
        Message message;

        if (optional.isPresent()) {
            boolean res = ageGroupRepo.existsAllByName(dto.getName());

            if (!res) {
                AgeGroup ageGroup = optional.get();
                ageGroup.setName(dto.getName());
                ageGroupRepo.save(ageGroup);
                message = Message.EDIT_UZ;
            } else {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            }
        } else {
            message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        }
        return new StateMessage(message.getName(), message.isState(), message.getCode());
    }

    public StateMessage delete(Integer id, Users user) throws ChangeSetPersister.NotFoundException {
        Message message;
        Optional<AgeGroup> optionalAgeGroup = ageGroupRepo.findById(id);
        if (optionalAgeGroup.isEmpty()) {
            throw new ChangeSetPersister.NotFoundException();
        } else {
            AgeGroup ageGroup = optionalAgeGroup.get();

            ageGroup.setDelete(true);
            ageGroupRepo.save(ageGroup);
            message = Message.DELETE_UZ;
            return new StateMessage().parse(message);
        }
    }
}
