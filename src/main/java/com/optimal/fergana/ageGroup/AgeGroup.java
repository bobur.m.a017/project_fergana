package com.optimal.fergana.ageGroup;


import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.stayTime.StayTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AgeGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Boolean delete = false;
    @CreationTimestamp
    private Timestamp createDate;

    private Integer sortNumber = 99;

    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToOne
    private StayTime stayTime;

    private Boolean sanPin = false;

    public AgeGroup(String name, Boolean delete) {
        this.name = name;
        this.delete = delete;
    }
}
