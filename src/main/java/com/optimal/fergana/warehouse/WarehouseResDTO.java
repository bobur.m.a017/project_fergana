package com.optimal.fergana.warehouse;

import com.optimal.fergana.warehouse.inOut.InOutPrice;
import com.optimal.fergana.warehouse.inOut.InOutPriceResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WarehouseResDTO {
    private UUID id;
    private BigDecimal weight;
    private BigDecimal packWeight;
    private String kindergartenName;
    private Integer kindergartenId;
    private boolean delete;
    private Integer productId;
    private String productName;
    private List<InOutPriceResDTO> inOutList;
}
