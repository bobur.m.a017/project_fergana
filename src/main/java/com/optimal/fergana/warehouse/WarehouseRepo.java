package com.optimal.fergana.warehouse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface WarehouseRepo extends JpaRepository<Warehouse, UUID> {

    Optional<Warehouse> findByKindergarten_IdAndProduct_IdAndDeleteIsFalse(Integer kindergarten_id, Integer product_id);

    Page<Warehouse> findAllByKindergarten_IdAndDeleteIsFalseOrderByProduct_Name(Integer kindergarten_id, Pageable pageable);
    Warehouse findByKindergarten_IdAndDeleteIsFalseOrderByTotalWeight(Integer kindergarten_id);
}
