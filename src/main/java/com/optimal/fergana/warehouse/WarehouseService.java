package com.optimal.fergana.warehouse;


import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.inputOutput.InputOutputService;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.acceptedProducts.AcceptedProduct;
import com.optimal.fergana.product.acceptedProducts.AcceptedProductRepo;
import com.optimal.fergana.product.productPack.ProductPack;
import com.optimal.fergana.product.productPack.ProductPackRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.reporter.pdf.ProductStoreReportByMTT;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.inOut.InOutPriceDTO;
import com.optimal.fergana.warehouse.inOut.InOutPriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;

@RequiredArgsConstructor
@Service
public class WarehouseService {

    private final WarehouseRepo warehouseRepo;
    private final CustomConverter converter;
    private final InOutPriceService inOutPriceService;
    private final ProductStoreReportByMTT productStoreReportByMTT;
    private final ProductRepo productRepo;
    private final KindergartenRepo kindergartenRepo;
    private final ProductPackRepo productPackRepo;
    private final InputOutputService inputOutputService;
    private final ProductPackService productPackService;
    private final AcceptedProductRepo acceptedProductRepo;

    public void add(Product product, BigDecimal weight, Kindergarten kindergarten, BigDecimal price, BigDecimal pack, BigDecimal packWeight) {

        Optional<Warehouse> optionalWarehouse = warehouseRepo.findByKindergarten_IdAndProduct_IdAndDeleteIsFalse(kindergarten.getId(), product.getId());

        if (optionalWarehouse.isPresent()) {
            Warehouse warehouse = optionalWarehouse.get();

            warehouse.setTotalPackWeight(warehouse.getTotalPackWeight().add(packWeight));
            warehouse.setTotalWeight(warehouse.getTotalWeight().add(weight));

            inputOutputService.addInput(product, kindergarten, LocalDate.now(), weight, packWeight, pack);
            inOutPriceService.add(price, weight, warehouse, pack, packWeight);
            warehouseRepo.save(warehouse);

        } else {
            Warehouse warehouse = warehouseRepo.save(new Warehouse(kindergarten, weight, packWeight, product));
            inputOutputService.addInput(product, kindergarten, LocalDate.now(), weight, packWeight, pack);
            inOutPriceService.add(price, weight, warehouse, pack, packWeight);
        }
    }

    public ResponseEntity<?> getAll(Users users, Integer kindergarten_id, Pageable pageable) {

        if (users.getKindergarten() != null) {
            kindergarten_id = users.getKindergarten().getId();
        } else if (kindergarten_id == null) {
            return ResponseEntity.status(417).body("MTT idsi kelmayapti");
        }
        Page<Warehouse> page = warehouseRepo.findAllByKindergarten_IdAndDeleteIsFalseOrderByProduct_Name(kindergarten_id, pageable);

        List<Warehouse> warehouses = page.stream().toList();

        List<WarehouseResDTO> dtoList = converter.warehouseToDTO(warehouses);

        dtoList.sort(Comparator.comparing(WarehouseResDTO::getProductName));


        CustomPageable customPageable = new CustomPageable(page.getSize(), page.getNumber(), dtoList, page.getTotalElements());
        return ResponseEntity.status(200).body(customPageable);
    }

    public String getFile(Integer kindergartenId, Users users, HttpServletResponse response) {

        if (kindergartenId == null && users.getKindergarten() != null) {
            kindergartenId = users.getKindergarten().getId();
        }

        if (kindergartenId != null) {

            return productStoreReportByMTT.createPDFMenu(kindergartenId);
        }
        return null;
    }

    public StateMessage addProductFront(Double doubleWeight, Double price, Integer productId, Integer kindergartenId, Users users) {
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (users.getKindergarten() != null) {
            kindergartenId = users.getKindergarten().getId();
        }
        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(kindergartenId);
        Optional<Product> optionalProduct = productRepo.findById(productId);

        if (doubleWeight == 0 || doubleWeight < 0 || price == 0 || price < 0) {
            return new StateMessage().parse(message);
        }

        if (optionalKindergarten.isPresent() && optionalProduct.isPresent()) {
            Kindergarten kindergarten = optionalKindergarten.get();
            Product product = optionalProduct.get();
            BigDecimal checkPack = productPackService.checkPack(product.getId(), users.getDepartment().getId());

            BigDecimal weight = BigDecimal.valueOf(doubleWeight).setScale(3, RoundingMode.HALF_UP);
            BigDecimal weightPack = BigDecimal.valueOf(doubleWeight).setScale(3, RoundingMode.HALF_UP);
            BigDecimal pack = BigDecimal.ZERO;

            if (checkPack != null) {
                BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.ONE);
                if (bigDecimals[1].compareTo(BigDecimal.ZERO) > 0) {
                    return new StateMessage("Maxsulot miqdorini donada kiriting", false, 417);
                }
                pack = checkPack;
                weight = weightPack.multiply(pack).divide(BigDecimal.valueOf(1000),3,RoundingMode.HALF_UP);
            }
            AcceptedProduct save = acceptedProductRepo.save(new AcceptedProduct(weight, weightPack, pack, null, users, new Date(System.currentTimeMillis()),product,BigDecimal.valueOf(price),kindergarten));
            add(product, weight, kindergarten, BigDecimal.valueOf(price), pack, weightPack);

            message = Message.SUCCESS_UZ;
        }
        return new StateMessage().parse(message);
    }
}
