package com.optimal.fergana.warehouse.inOut;


import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.productPack.ProductPack;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.WarehouseRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@RequiredArgsConstructor
@Service
public class InOutPriceService {

    private final InOutPriceRepo inOutPriceRepo;
    private final WarehouseRepo warehouseRepo;
    private final ProductPackService productPackService;

    public InOutPrice add(BigDecimal price, BigDecimal weight, Warehouse warehouse, BigDecimal pack, BigDecimal packWeight) {
        return inOutPriceRepo.save(new InOutPrice(price, weight, pack, packWeight, warehouse));
    }

    public List<InOutPriceDTO> subtract(Product product, BigDecimal weight, Kindergarten kindergarten) {

        Optional<Warehouse> optionalWarehouse = warehouseRepo.findByKindergarten_IdAndProduct_IdAndDeleteIsFalse(kindergarten.getId(), product.getId());

        List<InOutPriceDTO> returnList = new ArrayList<>();
        if (optionalWarehouse.isPresent()) {
            Warehouse warehouse = optionalWarehouse.get();

            List<InOutPrice> inOutPriceList = warehouse.getInOutPriceList();
            List<InOutPrice> delete = new ArrayList<>();
            inOutPriceList.sort(Comparator.comparing(InOutPrice::getCreateDate));

            for (InOutPrice inOutPrice : inOutPriceList) {
                if (inOutPrice.getWeight().compareTo(weight) > 0) {
                    BigDecimal weightPack = weight;
                    if (inOutPrice.getPack().compareTo(BigDecimal.ZERO) > 0) {
                        weightPack = weight.multiply(BigDecimal.valueOf(1000)).divide(inOutPrice.getPack(), 6, RoundingMode.HALF_UP);
                    }

                    inOutPrice.setWeight(inOutPrice.getWeight().subtract(weight));
                    inOutPrice.setPackWeight(inOutPrice.getPackWeight().subtract(weightPack));
                    inOutPriceRepo.save(inOutPrice);
                    returnList.add(new InOutPriceDTO(inOutPrice.getPrice(), weight, weightPack));
                    return returnList;

                } else if (inOutPrice.getWeight().compareTo(weight) < 0) {
                    BigDecimal weightPack = weight;
                    if (inOutPrice.getPack().compareTo(BigDecimal.ZERO) > 0) {
                        weightPack = weight.multiply(BigDecimal.valueOf(1000)).divide(inOutPrice.getPack(), 6, RoundingMode.HALF_UP);
                    }
                    delete.add(inOutPrice);
                    weight = weight.subtract(inOutPrice.getWeight());
                    returnList.add(new InOutPriceDTO(inOutPrice.getPrice(), inOutPrice.getWeight(), weightPack));

                } else {
                    BigDecimal weightPack = weight;
                    if (inOutPrice.getPack().compareTo(BigDecimal.ZERO) > 0) {
                        weightPack = weight.multiply(BigDecimal.valueOf(1000)).divide(inOutPrice.getPack(), 6, RoundingMode.HALF_UP);
                    }
                    returnList.add(new InOutPriceDTO(inOutPrice.getPrice(), weight, weightPack));
                    delete.add(inOutPrice);
                }
            }
            inOutPriceRepo.deleteAll(delete);
        }
        return returnList;
    }

    public StateMessage edit(UUID id, BigDecimal price, BigDecimal weightDto) {


        Optional<InOutPrice> optional = inOutPriceRepo.findById(id);

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optional.isPresent() && (price.compareTo(BigDecimal.ZERO) >= 0) && (weightDto.compareTo(BigDecimal.ZERO) >= 0)) {
            InOutPrice inOutPrice = optional.get();

            BigDecimal weight = weightDto;

            BigDecimal checkPack = productPackService.checkPack(inOutPrice.getWarehouse().getProduct().getId(), inOutPrice.getWarehouse().getKindergarten().getDepartment().getId());

            if (checkPack != null) {
                BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.ONE);
                if (bigDecimals[1].compareTo(BigDecimal.ZERO) > 0) {
                    return new StateMessage("Maxsulot miqdorini donada kiriting", false, 417);
                }
                weight = weightDto.multiply(checkPack).divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP);
            }

            inOutPrice.setWeight(weight);
            inOutPrice.setPrice(price);
            inOutPrice.setPackWeight(weightDto);
            InOutPrice save = inOutPriceRepo.save(inOutPrice);

            Warehouse warehouse = save.getWarehouse();

            BigDecimal totalWeight = BigDecimal.ZERO;
            BigDecimal totalWeightPack = BigDecimal.ZERO;

            for (InOutPrice outPrice : warehouse.getInOutPriceList()) {
                totalWeight = totalWeightPack.add(outPrice.getWeight());
                totalWeightPack = totalWeightPack.add(outPrice.getPackWeight());
            }

            warehouse.setTotalWeight(totalWeight);
            warehouse.setTotalPackWeight(totalWeightPack);
            warehouseRepo.save(warehouse);
            message = Message.EDIT_UZ;

        }

        return new StateMessage().parse(message);
    }
}
