package com.optimal.fergana.warehouse.inOut;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InOutPriceDTO {

    private BigDecimal price = new BigDecimal(0);
    private BigDecimal weight = new BigDecimal(0);
    private BigDecimal weightPack = new BigDecimal(0);
}
