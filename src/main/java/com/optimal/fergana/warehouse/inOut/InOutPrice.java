package com.optimal.fergana.warehouse.inOut;


import com.optimal.fergana.warehouse.Warehouse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InOutPrice {


    @Id
    private UUID id = UUID.randomUUID();

    @Column(precision = 19, scale = 6)
    private BigDecimal price = new BigDecimal(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal weight = new BigDecimal(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal pack = new BigDecimal(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal packWeight = new BigDecimal(0);


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private Warehouse warehouse;

    public InOutPrice(BigDecimal price, BigDecimal weight, Warehouse warehouse) {
        this.price = price;
        this.weight = weight;
        this.warehouse = warehouse;
    }

    public InOutPrice(BigDecimal price, BigDecimal weight, BigDecimal pack, BigDecimal packWeight, Warehouse warehouse) {
        this.price = price;
        this.weight = weight;
        this.pack = pack;
        this.packWeight = packWeight;
        this.warehouse = warehouse;
    }
}
