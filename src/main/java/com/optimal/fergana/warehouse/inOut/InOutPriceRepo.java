package com.optimal.fergana.warehouse.inOut;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface InOutPriceRepo extends JpaRepository<InOutPrice, UUID> {
}
