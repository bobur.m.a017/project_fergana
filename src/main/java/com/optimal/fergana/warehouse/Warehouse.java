package com.optimal.fergana.warehouse;

import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.warehouse.inOut.InOutPrice;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Warehouse {

    @Id
    private UUID id = UUID.randomUUID();

    @ManyToOne(fetch = FetchType.LAZY)
    private Kindergarten kindergarten;

    private boolean delete = false;

    @Column(precision = 19, scale = 3)
    private BigDecimal totalWeight;

    @Column(precision = 19, scale = 3)
    private BigDecimal totalPackWeight;


    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @OneToMany(mappedBy = "warehouse", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutPrice> inOutPriceList;


    public Warehouse(Kindergarten kindergarten, Product product) {
        this.kindergarten = kindergarten;
        this.product = product;
    }

    public Warehouse(Kindergarten kindergarten, BigDecimal totalWeight, BigDecimal totalPackWeight, Product product) {
        this.kindergarten = kindergarten;
        this.totalWeight = totalWeight;
        this.totalPackWeight = totalPackWeight;
        this.product = product;
    }

    public BigDecimal getTotalWeight() {
        BigDecimal weight = BigDecimal.valueOf(0);

        for (InOutPrice inOutPrice : this.inOutPriceList) {
            weight = weight.add(inOutPrice.getWeight());
        }
        return weight;
    }

    public String getProductNameSort() {
        return this.product.getName();
    }


//    public void setTotalWeight() {
//        BigDecimal weight = BigDecimal.valueOf(0);
//
//        for (InOutPrice inOutPrice : this.inOutPriceList) {
//            weight = weight.add(inOutPrice.getWeight());
//        }
//        this.totalWeight = weight;
//    }
}
