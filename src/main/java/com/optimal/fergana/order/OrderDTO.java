package com.optimal.fergana.order;

import com.optimal.fergana.order.kindergarten.KindergartenOrderDTO;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderDTO {

    @NotNull
    private String name;
    @NotNull
    private Integer month;
    @NotNull
    private Integer year;

    private List<KindergartenOrderDTO> kindergartenOrderDTOList;
}
