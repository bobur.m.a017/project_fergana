package com.optimal.fergana.order;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


@Getter
@Setter
public class ContractOrderDTO {


    @NotNull
    private String number;
    @NotNull
    private String lotNumber;
    @NotNull
    private Timestamp startDay;
    @NotNull
    private Timestamp endDay;
    @NotNull
    private Integer supplierId;

    @NotNull
    private List<Integer> productIdList;
}
