package com.optimal.fergana.order;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.order.kindergarten.KindergartenOrder;
import com.optimal.fergana.order.kindergarten.KindergartenOrderRepo;
import com.optimal.fergana.order.kindergarten.KindergartenOrderService;
import com.optimal.fergana.order.product.ProductOrder;
import com.optimal.fergana.order.product.ProductOrderRepo;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductResDTO;
import com.optimal.fergana.product.productPack.ProductPack;
import com.optimal.fergana.product.productPack.ProductPackRepo;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.supplier.SupplierRepo;
import com.optimal.fergana.supplier.contract.Contract;
import com.optimal.fergana.supplier.contract.ContractRepo;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContract;
import com.optimal.fergana.supplier.contract.product.ProductContract;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;


@RequiredArgsConstructor
@Service
public class MyOrderService implements OrderServiceInterface {

    private final OrderRepo orderRepo;
    private final KindergartenOrderService kindergartenOrderService;
    private final CustomConverter converter;
    private final ReportRepo reportRepo;
    private final AgeStandardRepo ageStandardRepo;
    private final KindergartenOrderRepo kindergartenOrderRepo;
    private final ProductPackRepo productPackRepo;
    private final ProductOrderRepo productOrderRepo;
    private final ContractRepo contractRepo;
    private final SupplierRepo supplierRepo;

    public StateMessage add(OrderDTO dto, Users user) {
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        Department department = user.getDepartment();

        boolean byName = orderRepo.existsAllByName(dto.getName());

        if (!byName) {
            MyOrder myOrder = new MyOrder(dto.getName(), dto.getYear(), dto.getMonth(), department, user);

            myOrder = orderRepo.save(myOrder);
            for (Kindergarten kindergarten : department.getKindergartenList()) {
                KidsNumber averageKidsNumber = kindergarten.getAverageKidsNumber();
                if (averageKidsNumber != null) {

                    KindergartenOrder kindergartenOrder = new KindergartenOrder(kindergarten, myOrder, new HashSet<>());
                    Set<ProductOrder> productOrderSet = new HashSet<>();

                    List<Report> reportList = reportRepo.findAllByKindergarten_IdAndYearAndMonth(kindergarten.getId(), myOrder.getYear(), myOrder.getMonth());

                    for (Report report : reportList) {
                        if (report.getMenu() != null) {
                            calculateProduct(averageKidsNumber, productOrderSet, report.getMenu(), kindergartenOrder);
                        }
                    }
                    if (productOrderSet.size() > 0) {
                        for (ProductOrder productOrder : productOrderSet) {
                            Optional<ProductPack> optional = productPackRepo.findByProduct_IdAndDepartment_Id(productOrder.getProduct().getId(), kindergartenOrder.getKindergarten().getDepartment().getId());

                            productOrder.setWeight(productOrder.getWeight().divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP));
                            productOrder.setPackWeight(productOrder.getPackWeight().divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP));

                            if (optional.isPresent()) {
                                ProductPack productPack = optional.get();
                                BigDecimal divide = productOrder.getWeight().multiply(BigDecimal.valueOf(1000)).divide(productPack.getPack(), 0, RoundingMode.UP);
                                productOrder.setPackWeight(divide);
                                productOrder.setPack(productPack.getPack());
                                productOrder.setWeight((productPack.getPack().multiply(divide)).divide(BigDecimal.valueOf(1000),6,RoundingMode.HALF_UP));
                            }
                        }
                        kindergartenOrder.setProductOrders(productOrderSet);
                        kindergartenOrderRepo.save(kindergartenOrder);
                    }
                }
            }
            message = Message.SUCCESS_UZ;
        }

        return new StateMessage().parse(message);
    }

    public void calculateProduct(KidsNumber kidsNumber, Set<ProductOrder> productOrderList, Menu menu, KindergartenOrder kindergartenOrder) {

        for (AgeStandard ageStandard : ageStandardRepo.findAllByMealAgeStandard_MealTimeStandard_Menu_Id(menu.getId())) {
            Meal meal = ageStandard.getMealAgeStandard().getMeal();
            BigDecimal mealWeight = meal.getWeight();
            for (ProductMeal productMeal : meal.getProductMealList()) {
                BigDecimal divide = productMeal.getWeight().divide(mealWeight, 6, RoundingMode.HALF_UP);
                BigDecimal multiply = divide.multiply(ageStandard.getWeight());
                for (KidsNumberSub kidsNumberSub : kidsNumber.getKidsNumberSubList()) {
                    if (kidsNumberSub.getAgeGroup().getId().equals(ageStandard.getAgeGroup().getId())) {
                        BigDecimal weightMultiply = multiply.multiply(BigDecimal.valueOf(kidsNumberSub.getNumber()));
                        addProductList(productOrderList, productMeal.getProduct(), weightMultiply, kindergartenOrder);
                    }
                }
            }
        }
    }

    public void addProductList(Set<ProductOrder> productOrderList, Product product, BigDecimal weight, KindergartenOrder kindergartenOrder) {
        boolean res = true;

        for (ProductOrder productOrder : productOrderList) {
            if (productOrder.getProduct().getId().equals(product.getId())) {
                productOrder.setWeight(productOrder.getWeight().add(weight));
                productOrder.setPackWeight(productOrder.getPackWeight().add(weight));
                res = false;
                break;
            }
        }
        if (res) {
            productOrderList.add(new ProductOrder(weight, weight, product, kindergartenOrder));
        }
    }

    public StateMessage edit(OrderDTO contractDTO, Integer id, Users users) {

        Optional<MyOrder> optionalOrder = orderRepo.findById(id);

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optionalOrder.isPresent()) {
            MyOrder myOrder = optionalOrder.get();
            List<KindergartenOrder> edit = kindergartenOrderService.edit(contractDTO.getKindergartenOrderDTOList(), myOrder);
            myOrder.setUpdatedBy(users);
            orderRepo.save(myOrder);
            message = Message.EDIT_UZ;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage delete(Integer id) {
        Optional<MyOrder> optionalContract = orderRepo.findById(id);
        Message message = Message.THE_CONTRACT_CANNOT_BE_CANCELED;

        if (optionalContract.isPresent()) {
            MyOrder myOrder = optionalContract.get();
            myOrder.setDelete(true);
            orderRepo.save(myOrder);
            message = Message.DELETE_UZ;
        }
        return new StateMessage().parse(message);
    }

    public OrderResDTO getOne(Integer id) {
        Optional<MyOrder> optionalContract = orderRepo.findById(id);
        if (optionalContract.isPresent()) {
            MyOrder myOrder = optionalContract.get();

            OrderResDTO orderResDTO = converter.orderToDTO(myOrder);
            orderResDTO.setKindergartenContractList(converter.kindergartenOrderToDTO(myOrder.getKindergartenOrderList()));
            return orderResDTO;
        } else {
            throw new UsernameNotFoundException("this object not found in the database");
        }
    }

    public CustomPageable getAll(Users users, Integer pageSize, Integer page) {

        if (page == null) page = 0;
        if (pageSize == null) pageSize = 10;


        Pageable pageable = PageRequest.of(page, pageSize);
        Page<MyOrder> contractPage = null;

        contractPage = orderRepo.findAllByDeleteIsFalseAndDepartment_Id(users.getDepartment().getId(), pageable);

        if (contractPage != null) {

            return new CustomPageable(contractPage.getSize(), contractPage.getNumber(), converter.orderToDTO(contractPage.stream().toList()), 0);
        } else {
            throw new UsernameNotFoundException("Siz kiritgan paramertlar bo`yicha ma‘lumot mavjud emas");
        }
    }

    public StateMessage toRoundOff(Integer id, BigDecimal weight) {

        Optional<MyOrder> optional = orderRepo.findById(id);

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        List<ProductOrder> list = new ArrayList<>();

        if (optional.isPresent()) {
            MyOrder myOrder = optional.get();

            for (KindergartenOrder kindergartenOrder : myOrder.getKindergartenOrderList()) {
                for (ProductOrder productOrder : kindergartenOrder.getProductOrders()) {
                    BigDecimal pack = productOrder.getPack();
                    if (pack.compareTo(BigDecimal.valueOf(0)) == 0) {

                        BigDecimal orderWeight = productOrder.getWeight();

                        BigDecimal divide = orderWeight.divide(weight, 0, RoundingMode.UP);
                        BigDecimal multiply = divide.multiply(weight);

                        productOrder.setWeight(multiply);
                        productOrder.setPackWeight(multiply);

                        list.add(productOrder);
                    }
                }
            }
            productOrderRepo.saveAll(list);
            message = Message.EDIT_UZ;
        }

        return new StateMessage().parse(message);

    }

    public List<ProductResDTO> getProduct(Integer orderId) {
        Optional<MyOrder> optionalMyOrder = orderRepo.findById(orderId);
        List<ProductResDTO> list = new ArrayList<>();

        if (optionalMyOrder.isPresent()) {
            MyOrder myOrder = optionalMyOrder.get();

            for (KindergartenOrder kindergartenOrder : myOrder.getKindergartenOrderList()) {
                for (ProductOrder productOrder : kindergartenOrder.getProductOrders()) {
                    boolean res = true;
                    for (ProductResDTO productResDTO : list) {
                        if (Objects.equals(productOrder.getProduct().getId(), productResDTO.getId())) {
                            res = false;
                            break;
                        }
                    }
                    if (res)
                        list.add(converter.productToDTO(productOrder.getProduct()));
                }
            }
        }
        list.sort(Comparator.comparing(ProductResDTO::getName));

        return list;
    }

    public StateMessage addContract(Integer id, ContractOrderDTO dto, Users users) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Optional<MyOrder> optionalMyOrder = orderRepo.findById(id);

        if (optionalMyOrder.isPresent()) {
            MyOrder myOrder = optionalMyOrder.get();

            List<ProductOrder> list = new ArrayList<>();
            for (Integer integer : dto.getProductIdList()) {
                list.addAll(productOrderRepo.findAllByKindergartenOrder_MyOrder_IdAndProduct_Id(myOrder.getId(), integer));
            }

            message = add(dto, users, list);

        }
        return new StateMessage().parse(message);
    }

    public Message add(ContractOrderDTO dto, Users user, List<ProductOrder> list) {

        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        Optional<Supplier> optionalSupplier = supplierRepo.findById(dto.getSupplierId());
        Department department = user.getDepartment();

        if (!(contractRepo.existsAllByNumber(dto.getNumber())) && optionalSupplier.isPresent()) {

            Contract contract = new Contract(
                    false,
                    dto.getNumber(),
                    BigDecimal.valueOf(0),
                    dto.getLotNumber(),
                    dto.getStartDay(),
                    dto.getEndDay(),
                    optionalSupplier.get(),
                    department,
                    user
            );
            List<KindergartenContract> contractList = add(list, contract);

            if (contractList != null) {
                contract.setKindergartenContractList(contractList);
                contract.setTotalSum(getTotalSum(contractList));
                contract.setCreatedBy(user);
                contract.setStatus(Status.NEW.getName());
                contract.setUpdatedBy(user);
                contractRepo.save(contract);
                message = Message.SUCCESS_UZ;
            }


        }
        return (message);
    }

    public BigDecimal getTotalSum(List<KindergartenContract> kindergartenContractList) {
        BigDecimal totalSum = BigDecimal.valueOf(0);

        for (KindergartenContract kindergartenContract : kindergartenContractList) {
            totalSum = totalSum.add(kindergartenContract.getTotalSum());
        }
        return totalSum;
    }

    public List<KindergartenContract> add(List<ProductOrder> list, Contract contract) {
        List<KindergartenContract> listKin = new ArrayList<>();

        for (ProductOrder productOrder : list) {

            boolean res = true;
            for (KindergartenContract kindergartenContract : listKin) {
                if (Objects.equals(productOrder.getKindergartenOrder().getKindergarten().getId(), kindergartenContract.getKindergarten().getId())) {
                    Set<ProductContract> productContracts = kindergartenContract.getProductContracts();
                    productContracts.add(new ProductContract(
                            productOrder.getWeight(), productOrder.getPack(), productOrder.getPackWeight(), BigDecimal.valueOf(0), BigDecimal.valueOf(0), productOrder.getProduct(), kindergartenContract
                    ));

                    kindergartenContract.setTotalSum(getTotalSum(productContracts));
                    kindergartenContract.setProductContracts(productContracts);

                    res = false;
                }
            }
            if (res) {
                KindergartenContract kindergartenContract = new KindergartenContract(
                        productOrder.getKindergartenOrder().getKindergarten(),
                        BigDecimal.valueOf(0),
                        contract,
                        null
                );
                Set<ProductContract> productContracts = new HashSet<>();
                productContracts.add(new ProductContract(
                        productOrder.getWeight(), productOrder.getPack(), productOrder.getPackWeight(), BigDecimal.valueOf(0), BigDecimal.valueOf(0), productOrder.getProduct(), kindergartenContract
                ));
                kindergartenContract.setTotalSum(getTotalSum(productContracts));
                kindergartenContract.setProductContracts(productContracts);
                listKin.add(kindergartenContract);
            }
        }

        return listKin;
    }

    public BigDecimal getTotalSum(Set<ProductContract> productContractList) {

        BigDecimal totalSum = BigDecimal.valueOf(0);

        for (ProductContract productContract : productContractList) {
            totalSum = totalSum.add(productContract.getTotalSum());
        }
        return totalSum;

    }
}
