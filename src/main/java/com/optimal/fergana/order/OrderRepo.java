package com.optimal.fergana.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface OrderRepo extends JpaRepository<MyOrder, Integer> {

    boolean existsAllByName(String number);

    Page<MyOrder> findAllByDeleteIsFalseAndDepartment_Id(Integer departmentId, Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT e FROM contract e WHERE e.end_day < :date AND e.delete == :false AND e.verified == :true")
    List<MyOrder> checkContractPeriod(LocalDate date);

    @Query(nativeQuery = true,
            value = "SELECT e FROM contract e WHERE e.end_day < :date AND e.delete == :false AND e.verified == :true")
    List<MyOrder> checkContractPeriodByKindergarten(LocalDate date);


}
