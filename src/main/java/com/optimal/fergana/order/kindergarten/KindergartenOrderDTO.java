package com.optimal.fergana.order.kindergarten;

import com.optimal.fergana.order.product.ProductOrderDTO;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
public class KindergartenOrderDTO {

    private UUID id;

    @NotNull
    private Integer kindergartenId;
    @NotNull
    private List<ProductOrderDTO> productContracts;
}