package com.optimal.fergana.order.product;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductOrderResDTO {
    private UUID id;
    private BigDecimal weight;
    private BigDecimal pack;
    private BigDecimal packWeight;
    private Integer productId;
    private String productName;
    private String price="";

    public ProductOrderResDTO(UUID id, BigDecimal weight, BigDecimal pack, BigDecimal packWeight, Integer productId, String productName) {
        this.id = id;
        this.weight = weight;
        this.pack = pack;
        this.packWeight = packWeight;
        this.productId = productId;
        this.productName = productName;
    }
}
