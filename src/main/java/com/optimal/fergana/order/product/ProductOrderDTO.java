package com.optimal.fergana.order.product;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductOrderDTO {

    private UUID id;
    @NotNull
    private Double packWeight;
    @NotNull
    private Integer productId;
    @NotNull
    private Double pack;
}
