package com.optimal.fergana.order.product;

import com.optimal.fergana.order.OrderRepo;
import com.optimal.fergana.order.kindergarten.KindergartenOrder;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ProductOrderService {


    private final ProductRepo productRepo;
    private final ProductOrderRepo productOrderRepo;
    private final CustomConverter converter;


    public ProductOrder add(ProductOrderDTO dto, KindergartenOrder kindergartenOrder) {
        Optional<Product> optionalProduct = productRepo.findById(dto.getProductId());
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();

            BigDecimal weight = BigDecimal.valueOf(dto.getPackWeight());
            Double pack = dto.getPack();
            BigDecimal packWeight = BigDecimal.valueOf(dto.getPackWeight());


            if (pack > 0){
                weight = packWeight.multiply(BigDecimal.valueOf(pack));
            }
            return new ProductOrder(
                    weight,BigDecimal.valueOf(pack),packWeight,product, kindergartenOrder
            );
        }
        return null;
    }

    public Set<ProductOrder> add(List<ProductOrderDTO> dtoList, KindergartenOrder kindergartenOrder) {

        Set<ProductOrder> list = new HashSet<>();

        for (ProductOrderDTO productOrderDTO : dtoList) {
            ProductOrder productOrder = add(productOrderDTO, kindergartenOrder);
            if (productOrder == null) {
                return null;
            }
            list.add(productOrder);
        }
        return list;
    }

    public Set<ProductOrder> edit(KindergartenOrder kindergartenOrder, List<ProductOrderDTO> productContracts) {

        Set<ProductOrder> productOrderSet = kindergartenOrder.getProductOrders();
        Set<ProductOrder> deleteSet = new HashSet<>();
        Set<ProductOrder> returnSet = new HashSet<>();


        for (ProductOrder productOrder : productOrderSet) {
            boolean res = true;
            for (ProductOrderDTO contract : productContracts) {
                if (contract.getId().equals(productOrder.getId())) {
                    res = false;
                    break;
                }
            }
            if (res) {
                deleteSet.add(productOrder);
            }
        }

        for (ProductOrderDTO productOrder : productContracts) {
            if (productOrder.getId() != null) {

                Optional<ProductOrder> optionalProductContract = productOrderRepo.findById(productOrder.getId());
                if (optionalProductContract.isPresent()) {

                    BigDecimal weight = BigDecimal.valueOf(productOrder.getPackWeight());
                    Double pack = productOrder.getPack();
                    BigDecimal packWeight = BigDecimal.valueOf(productOrder.getPackWeight());


                    if (pack > 0){
                        weight = packWeight.multiply(BigDecimal.valueOf(pack));
                    }

                    ProductOrder contract = optionalProductContract.get();
                    contract.setWeight(weight);
                    contract.setPack(BigDecimal.valueOf(pack));
                    contract.setPackWeight(packWeight);

                    returnSet.add(contract);
                }
            } else {
                returnSet.add(add(productOrder, kindergartenOrder));
            }
        }
        productOrderRepo.deleteAll(deleteSet);

        return new HashSet<>(productOrderRepo.saveAll(returnSet));
    }

}