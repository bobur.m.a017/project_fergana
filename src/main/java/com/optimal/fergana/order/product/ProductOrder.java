package com.optimal.fergana.order.product;


import com.optimal.fergana.order.kindergarten.KindergartenOrder;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.acceptedProducts.AcceptedProduct;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductOrder {

    @Id
    private UUID id = UUID.randomUUID();

    @Column(precision = 19, scale = 6)
    private BigDecimal weight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal pack = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal packWeight = BigDecimal.valueOf(0);


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;


    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private KindergartenOrder kindergartenOrder;



    public ProductOrder(BigDecimal weight,BigDecimal packWeight, Product product, KindergartenOrder kindergartenOrder) {
        this.weight = weight;
        this.packWeight = packWeight;
        this.product = product;
        this.kindergartenOrder = kindergartenOrder;
    }


    public ProductOrder(BigDecimal weight, BigDecimal pack, BigDecimal packWeight, Product product, KindergartenOrder kindergartenOrder) {
        this.weight = weight;
        this.pack = pack;
        this.packWeight = packWeight;
        this.product = product;
        this.kindergartenOrder = kindergartenOrder;
    }
}
