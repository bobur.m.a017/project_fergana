package com.optimal.fergana.order.product;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ProductOrderRepo extends JpaRepository<ProductOrder, UUID> {


    List<ProductOrder> findAllByKindergartenOrder_MyOrder_IdAndProduct_Id(Integer kindergartenOrder_myOrder_id, Integer product_id);

}
