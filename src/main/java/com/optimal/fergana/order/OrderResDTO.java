package com.optimal.fergana.order;

import com.optimal.fergana.order.kindergarten.KindergartenOrderResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderResDTO {
    private Integer id;
    private String name;
    private Integer departmentId;
    private String departmentName;
    private List<KindergartenOrderResDTO> kindergartenContractList;
    private boolean delete;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String createdBy;
    private String updatedBy;

}
