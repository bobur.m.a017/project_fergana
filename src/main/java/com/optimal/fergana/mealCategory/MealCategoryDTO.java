package com.optimal.fergana.mealCategory;


import com.optimal.fergana.meal.Meal;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;

public class MealCategoryDTO {
    @NotNull
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
