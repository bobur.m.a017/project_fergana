package com.optimal.fergana;


import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.meal.MealRepo;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.multiMenu.MultiMenuRepo;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.productMeal.ProductMealRepo;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.report.menuReport.MealProductReport;
import com.optimal.fergana.report.menuReport.MealProductReportRepo;
import com.optimal.fergana.report.menuReport.MenuReport;
import com.optimal.fergana.report.menuReport.MenuReportRepo;
import com.optimal.fergana.role.Role;
import com.optimal.fergana.role.RoleRepo;
import com.optimal.fergana.selectMenu.SelectMenuServiceInterface;
import com.optimal.fergana.supplier.STIRParser.STIRParser;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersRepo;
import com.optimal.fergana.users.UsersSave;
import com.optimal.fergana.users.UsersSaveRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;


@RequiredArgsConstructor
@SpringBootApplication
public class
Project_FerganaApplication implements CommandLineRunner {


    private final KindergartenRepo kindergartenRepo;
    private final STIRParser stirParser;
    private final UsersSaveRepo usersSaveRepo;
    private final UsersRepo usersRepo;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepo roleRepo;
    private final SelectMenuServiceInterface selectMenuServiceInterface;
    private final ReportRepo reportRepo;
    private final MultiMenuRepo multiMenuRepo;
    private final MealRepo mealRepo;
    private final ProductMealRepo productMealRepo;

    private final MealProductReportRepo mealProductReportRepo;

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+5:00"));
        SpringApplication.run(Project_FerganaApplication.class, args);
    }

    @Override
    public void run(String... args) throws IOException {


    }
}
