package com.optimal.fergana.kids.kidsSub;

import com.optimal.fergana.product.product_price.ProductPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.Vector;

public interface KidsNumberSubRepo extends JpaRepository<KidsNumberSub, Integer> {

    List<KidsNumberSub> findAllByKidsNumber_Report_Kindergarten_IdAndKidsNumber_DeleteIsFalseAndAgeGroup_IdAndKidsNumber_DateBetween(Integer kidsNumber_report_kindergarten_id, Integer ageGroup_id, LocalDate date1, LocalDate date2);

    List<KidsNumberSub> findAllByKidsNumber_DateBetweenAndKidsNumber_DeleteIsFalseAndKidsNumber_Report_Kindergarten_Department_IdAndAgeGroup_Id(LocalDate kidsNumber_date, LocalDate kidsNumber_date2, Integer department_id,Integer ageGroupId);


    @Query(nativeQuery = true,
            value = "SELECT e.* FROM product_price e WHERE (e.product_id = :productId AND e.regional_department_id = :regionalDepartmentId) AND  ((e.start_day <= :date1 AND e.end_day >= :date1) OR (e.start_day <= :date2 AND e.end_day >= :date2) OR (e.start_day >= :date1 AND e.start_day <= :date2)) AND e.delete = :res")
    List<ProductPrice> getProductPrice(Integer productId, Integer regionalDepartmentId, LocalDate date1, LocalDate date2, boolean res);


    Optional<KidsNumberSub> findByAgeGroup_IdAndKidsNumber_Report_IdAndKidsNumberDeleteIsFalse(Integer ageGroup_id, UUID kidsNumber_report_id);
    Optional<KidsNumberSub> findByAgeGroup_IdAndKidsNumber_Report_Id(Integer ageGroup_id, UUID kidsNumber_report_id);

//
//    @Query(value = "SELECT SUM(total_days) FROM MyEntity", nativeQuery = true)
//    List<KidsNumber> findAllByReport_Kindergarten_IdAndDateBetweenAndDeleteIsFalse(Integer report_kindergarten_id, LocalDate date, LocalDate date2);

}
