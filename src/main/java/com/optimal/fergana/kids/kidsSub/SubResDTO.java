package com.optimal.fergana.kids.kidsSub;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubResDTO {

    private Integer id;
    private Integer ageGroupId;
    private String ageGroupName;
    private Integer number;
    private Integer sortNumber;
}
