package com.optimal.fergana.kids.kidsSub;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kindergarten.Kindergarten;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KidsNumberSub {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private AgeGroup ageGroup;

    private Integer number = 0;

    private Integer sortNumber = 10;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private KidsNumber kidsNumber;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    public KidsNumberSub(AgeGroup ageGroup, Integer number, KidsNumber kidsNumber, Integer sortNumber) {
        this.ageGroup = ageGroup;
        this.number = number;
        this.kidsNumber = kidsNumber;
        this.sortNumber = sortNumber;
    }
}
