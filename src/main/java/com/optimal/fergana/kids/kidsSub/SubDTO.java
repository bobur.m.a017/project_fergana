package com.optimal.fergana.kids.kidsSub;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SubDTO {

    private Integer id;

    @NotNull
    private Integer ageGroupId;

    @NotNull
    private Integer number;
}
