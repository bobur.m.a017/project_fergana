package com.optimal.fergana.kids;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.exception.AgeGroupNotFoundException;
import com.optimal.fergana.exception.KindergartenNotFoundException;
import com.optimal.fergana.exception.MenuNotFoundException;
import com.optimal.fergana.kids.dto.KidsNumberDTO;
import com.optimal.fergana.kids.dto.KidsNumberResDTO;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.KidsNumberSubRepo;
import com.optimal.fergana.kids.kidsSub.SubDTO;
import com.optimal.fergana.kids.kidsSub.SubResDTO;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.report.ReportService;
import com.optimal.fergana.report.menuReport.*;
import com.optimal.fergana.reporter.pdf.KidsNumberPDFReporterByDate;
import com.optimal.fergana.selectMenu.SelectMenuServiceInterface;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.stayTime.StayTime;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import lombok.Synchronized;
import org.hibernate.annotations.Synchronize;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class KidsNumberService {

    private final KidsNumberRepo kidsNumberRepo;
    private final KidsNumberSubRepo kidsNumberSubRepo;
    private final ReportRepo reportRepo;
    private final UserService userService;
    private final ReportService reportService;
    private final CustomConverter converter;
    private final KidsNumberPDFReporterByDate kidsNumberPDFReporterByDate;
    private final DepartmentRepo departmentRepo;
    private final KindergartenRepo kindergartenRepo;
    private final SelectMenuServiceInterface selectMenuServiceInterface;
    private final MenuReportService menuReportService;
    private final MenuReportRepo menuReportRepo;
    private final AgeGroupReportRepo ageGroupReportRepo;


    @Synchronized
    public StateMessage add(KidsNumberDTO dto, HttpServletRequest request) throws AgeGroupNotFoundException, KindergartenNotFoundException, MenuNotFoundException {


        Users user = userService.parseToken(request);
        StateMessage stateMessage = checkNumberOfKids(dto.getSubDTO());
        if (!stateMessage.isSuccess()) {
            return stateMessage;
        }

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Kindergarten kindergarten = user.getKindergarten();
        LocalDate localDate = dto.getDate().toLocalDateTime().toLocalDate();
        Optional<KidsNumber> optionalKidsNumber = kidsNumberRepo.findByDateAndReport_KidsNumber_IdAndDeleteIsFalse(localDate, kindergarten.getId());
        Optional<Report> optionalReport = reportRepo.findByYearAndMonthAndDayAndKindergarten_Id(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth(), kindergarten.getId());


        if (optionalKidsNumber.isPresent()) {
            KidsNumber kidsNumber = optionalKidsNumber.get();
            return edit(dto, kidsNumber.getId(), request);
        }


        Department department = user.getDepartment();

        boolean resTime = false;
        LocalDateTime date = dto.getDate().toLocalDateTime();
        LocalDateTime time = new Timestamp(System.currentTimeMillis()).toLocalDateTime();

//        if (department.getLocalTime() != null) {
//            LocalTime localTime = department.getLocalTime();
//            LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(), localTime);
//            LocalDate date = report.getDate();
//            LocalDateTime localDateTime1 = LocalDateTime.of(date, LocalTime.now());
//
//            if ((date.getYear() == localDateTime.getYear()) && (date.getMonthValue() == localDateTime.getMonthValue()) && (date.getDayOfMonth() == localDateTime.getDayOfMonth()) && (localDate.getDayOfMonth() == date.getDayOfMonth())) {
//                resTime = (localDateTime.isBefore(localDateTime1));
//            }
//
//        } else


        int dateYear = date.getYear();
        int timeYear = time.getYear();

        int dateMonthValue = date.getMonthValue();
        int timeMonthValue = time.getMonthValue();

        int dateDayOfMonth = date.getDayOfMonth();
        int timeDayOfMonth = time.getDayOfMonth();

        if (dateYear == timeYear && dateMonthValue >= timeMonthValue && dateDayOfMonth >= timeDayOfMonth || dateYear == timeYear && dateMonthValue > timeMonthValue) {
            resTime = true;
        }

        if (resTime) {

            Report report;

            if (optionalReport.isPresent()) {
                report = optionalReport.get();
            } else {
                Menu menu = selectMenuServiceInterface.getMenu(kindergarten);
                if (menu == null) {
                    return new StateMessage("Sizda xali menyu tanlanmagan adminga murojaat qiling", false, 417);
                }
                report = new Report(localDate.getDayOfMonth(), localDate.getMonthValue(), localDate.getYear(), kindergarten);
                report.setMenu(menu);

                reportRepo.save(report);
            }

            if (report.getMenu() != null) {
                if (report.getKidsNumber() == null) {
                    KidsNumber kidsNumber = new KidsNumber();
                    kidsNumber.setDate(dto.getDate().toLocalDateTime().toLocalDate());

                    Set<KidsNumberSub> list = new HashSet<>();


                    List<AgeGroup> ageGroupList = new ArrayList<>();

                    for (StayTime stayTime : kindergarten.getStayTimeList()) {
                        ageGroupList.addAll(stayTime.getAgeGroupList());
                    }

                    for (AgeGroup ageGroup : ageGroupList) {

                        boolean res = true;

                        for (SubDTO subDTO : dto.getSubDTO()) {
                            if (subDTO.getAgeGroupId().equals(ageGroup.getId())) {
                                KidsNumberSub kidsNumberSub = new KidsNumberSub();
                                kidsNumberSub.setNumber(subDTO.getNumber());
                                kidsNumberSub.setSortNumber(ageGroup.getSortNumber());
                                kidsNumberSub.setKidsNumber(kidsNumber);
                                kidsNumberSub.setAgeGroup(ageGroup);
                                list.add(kidsNumberSub);
                                res = false;
                            }
                        }
                        if (res) {
                            KidsNumberSub kidsNumberSub = new KidsNumberSub();
                            kidsNumberSub.setNumber(0);
                            kidsNumberSub.setSortNumber(ageGroup.getSortNumber());
                            kidsNumberSub.setKidsNumber(kidsNumber);
                            kidsNumberSub.setAgeGroup(ageGroup);
                            list.add(kidsNumberSub);
                        }
                    }


                    kidsNumber.setKidsNumberSubList(list);
                    kidsNumber.setReport(report);
                    kidsNumber.setStatus(Status.NEW.getName());

                    message = Message.SUCCESS_UZ;
                    report.setKidsNumber(kidsNumberRepo.save(kidsNumber));

                    Report save2 = reportRepo.save(report);

                    if (save2.getMenuReport() == null){
                        Thread thread = new Thread(() -> {
                            save2.setMenuReport(menuReportService.parseMenu(report.getMenu(), report));

                            Report save12 = reportRepo.save(report);
                            menuReportService.addAgeGroupReport(save12);
                        });
                        thread.start();
                    }
                }
            }
        } else {
            return new StateMessage("Bola sonini kiritish vaqti belgilangan vaqtdan o'tib ketdi.", false, 417);
        }
        return new StateMessage().parse(message);
    }

    @Synchronized
    public StateMessage edit(KidsNumberDTO dto, Integer kidsNumberId, HttpServletRequest request) throws AgeGroupNotFoundException, KindergartenNotFoundException {
        Users user = userService.parseToken(request);
        Department department = user.getDepartment();
        boolean resTime = false;

//        if (department.getLocalTime() != null) {
//            LocalTime localTime = department.getLocalTime();
//            LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(), localTime);
//            LocalDate date = report.getDate();
//            LocalDateTime localDateTime1 = LocalDateTime.of(date, LocalTime.now());
//
//            if ((date.getYear() == localDateTime.getYear()) && (date.getMonthValue() == localDateTime.getMonthValue()) && (date.getDayOfMonth() == localDateTime.getDayOfMonth()) && (localDate.getDayOfMonth() == date.getDayOfMonth())) {
//                resTime = (localDateTime.isBefore(localDateTime1));
//            }
//
//        } else {
        LocalDateTime date = dto.getDate().toLocalDateTime();
        LocalDateTime time = new Timestamp(System.currentTimeMillis()).toLocalDateTime();
        int dateYear = date.getYear();
        int timeYear = time.getYear();

        int dateMonthValue = date.getMonthValue();
        int timeMonthValue = time.getMonthValue();

        int dateDayOfMonth = date.getDayOfMonth();
        int timeDayOfMonth = time.getDayOfMonth();

        if (dateYear == timeYear && dateMonthValue >= timeMonthValue && dateDayOfMonth >= timeDayOfMonth || dateYear == timeYear && dateMonthValue > timeMonthValue) {
            resTime = true;
        }

        if (resTime) {
            StateMessage stateMessage = checkNumberOfKids(dto.getSubDTO());
            if (!stateMessage.isSuccess()) {
                return stateMessage;
            }
            Optional<KidsNumber> optionalKidsNumber = kidsNumberRepo.findById(kidsNumberId);
            if (optionalKidsNumber.isPresent()) {
                KidsNumber kidsNumber = optionalKidsNumber.get();
                kidsNumber.setDate(dto.getDate().toLocalDateTime().toLocalDate());
                Set<KidsNumberSub> kidsNumberSubList = kidsNumber.getKidsNumberSubList();

                for (SubDTO subDTO : dto.getSubDTO()) {
                    for (KidsNumberSub kidsNumberSub : kidsNumberSubList) {
                        if (subDTO.getId().equals(kidsNumberSub.getId())) {
                            kidsNumberSub.setNumber(subDTO.getNumber());
                        }
                    }
                }
                KidsNumber save = kidsNumberRepo.save(kidsNumber);

                Report report = save.getReport();
                report.setFilePathPdf(null);
                report.setFilePathExcel(null);

                Report save2 = reportRepo.save(report);
                Thread thread = new Thread(() -> {
//                    save2.setMenuReport(menuReportService.parseMenu(save2.getMenu(), save2));
//                    Report save12 = reportRepo.save(report);
                    menuReportService.addAgeGroupReport(save2);
                });
                thread.start();
                return new StateMessage().parse(Message.EDIT_UZ);
            } else {
                throw new KindergartenNotFoundException("Notogri sana kiritdingiz");
            }
        }
        return new StateMessage("Bola sonini kiritish vaqti belgilangan vaqtdan o'tib ketdi.", false, 417);
    }

    public StateMessage delete(Integer id) {
        Optional<KidsNumber> optionalKidsNumber = kidsNumberRepo.findById(id);
        if (optionalKidsNumber.isPresent()) {
            KidsNumber kidsNumber = optionalKidsNumber.get();
            kidsNumber.setDelete(true);
            kidsNumberRepo.save(kidsNumber);
            return new StateMessage(Message.DELETE_UZ.getName(), true, 200);
        } else {
            return new StateMessage(Message.THE_DATA_WAS_ENTERED_INCORRECTLY.getName(), true, 401);
        }
    }

    public StateMessage verified(Integer id, Users users) {

        return new StateMessage("Xozirda bola sonini tasdiqlash jarayoni qayta ko`rib chiqilmoqda. Shuning uchun xozir ushbu amalni bajarish imkoni mavjud emas", false, 417);
//
//        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
//
//        Optional<KidsNumber> optional = kidsNumberRepo.findById(id);
//        if (optional.isPresent()) {
//            KidsNumber kidsNumber = optional.get();
//            if (!kidsNumber.isVerified() && users.getKindergarten() != null) {
//                if (kidsNumber.getReport().getKindergarten().getId().equals(users.getKindergarten().getId())) {
//
//                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//
//                    int hours = timestamp.getHours();
//                    if (hours < 12) {
//                        return new StateMessage("Bola sonini xar kuni soat 13:00 dan keyin tasdiqlash mumkin", false, 417);
//                    }
//
//                    kidsNumber.setVerified(true);
//                    kidsNumber.setStatus(Status.TASDIQLANDI.getName());
//                    kidsNumberRepo.save(kidsNumber);
//
//                    message = Message.SUCCESS_VERIFIED;
//
//                    Thread thread = new Thread(() -> {
//                        reportService.calculate(kidsNumber.getReport());
//                    });
//                    thread.start();
//                }
//            }
//        }
//
//        return new StateMessage().parse(message);
    }

    public KidsNumberResDTO getOne(HttpServletRequest request, LocalDate date) {

        Users users = userService.parseToken(request);
        if (users.getKindergarten() != null) {
            Optional<Report> optionalReport = reportRepo.findByKindergarten_IdAndYearAndMonthAndDay(users.getKindergarten().getId(), date.getYear(), date.getMonthValue(), date.getDayOfMonth());
            if (optionalReport.isPresent()) {
                Report report = optionalReport.get();
                if (report.getKidsNumber() != null) {

                    List<SubResDTO> list = new ArrayList<>();
                    KidsNumberResDTO kidsNumberResDTO = converter.kidsNumberToDTO(report.getKidsNumber());

                    for (StayTime stayTime : report.getKindergarten().getStayTimeList()) {

                        for (AgeGroup ageGroup : stayTime.getAgeGroupList()) {
                            for (SubResDTO subResDTO : kidsNumberResDTO.getSubDTO()) {
                                if (ageGroup.getId().equals(subResDTO.getAgeGroupId())) {
                                    list.add(subResDTO);
                                }
                            }
                        }
                    }

                    list.sort(Comparator.comparing(SubResDTO::getSortNumber));
                    kidsNumberResDTO.setSubDTO(list);
                    return kidsNumberResDTO;
                }
            }
        }
        return new KidsNumberResDTO();
    }

    public List<KidsNumberResDTO> getAllByDepartmentId(Users users, Integer id, LocalDate date) {


        if (users.getDepartment() != null)
            id = users.getDepartment().getId();

        List<KidsNumber> list = kidsNumberRepo.findAllByReport_Kindergarten_Department_IdAndDateAndDeleteIsFalse(id, date);

        List<KidsNumberResDTO> dtoList = converter.kidsNumberToDTO(list);
        dtoList.sort(Comparator.comparing(KidsNumberResDTO::getKindergartenName));

        return dtoList;
    }

    public String getAllByDepartmentIdPDF(Users users, Integer id, LocalDate date) {
        if (users.getDepartment() != null)
            id = users.getDepartment().getId();


        List<KidsNumber> list = kidsNumberRepo.findAllByReport_Kindergarten_Department_IdAndDateAndDeleteIsFalse(id, date);

        String path = kidsNumberPDFReporterByDate.getAllByDepartmentIdPDF(list, date, departmentRepo.findById(id).get());

        String str = StaticWords.URI + path.substring(StaticWords.DEST.length());

        return str;
    }

    public List<KidsNumberResDTO> getAllByDate(Users users, Integer id, LocalDate startDate, LocalDate endDate) {

        if (users.getKindergarten() != null) {
            id = users.getKindergarten().getId();
        }

        List<KidsNumber> list = kidsNumberRepo.findAllByReport_Kindergarten_IdAndDateBetweenAndDeleteIsFalse(id, startDate, endDate);
        List<KidsNumberResDTO> dtoList = converter.kidsNumberToDTO(list);

        dtoList.sort(Comparator.comparing(KidsNumberResDTO::getDate));


        return dtoList;
    }

    public String getAllByDatePDF(Users users, Integer id, LocalDate startDate, LocalDate endDate) {
        if (users.getKindergarten() != null) {
            id = users.getKindergarten().getId();
        }

        List<KidsNumber> list = kidsNumberRepo.findAllByReport_Kindergarten_IdAndDateBetweenAndDeleteIsFalse(id, startDate, endDate);

        Optional<Kindergarten> optional = kindergartenRepo.findById(id);
        if (optional.isPresent()) {
            String str = kidsNumberPDFReporterByDate.createPDFKindergarten(list, startDate, endDate, optional.get());
            return StaticWords.URI + str.substring(StaticWords.DEST.length());
        }
        return null;
    }

    public StateMessage checkNumberOfKids(List<SubDTO> list) {

        for (SubDTO subDTO : list) {
            if (subDTO.getNumber() > 700 || subDTO.getNumber() < 0) {
                return new StateMessage("Kiritilgan bola soni xaqiqatdan yiroq. Iltimos tekshirib qaytadan kiriting", false, 417);
            }
        }

        return new StateMessage("", true, 200);
    }

//    @Scheduled(cron = "0 1 1 * * ?")
//    public void autoVerifier() {
//        List<KidsNumber> kidsNumberList = kidsNumberRepo.findAllByReport_Kindergarten_RegionalDepartment_IdAndVerifiedIsFalseAndDeleteIsFalseAndDate(1, LocalDate.now());
//
//        for (KidsNumber kidsNumber : kidsNumberList) {
//            for (KidsNumberSub kidsNumberSub : kidsNumber.getKidsNumberSubList()) {
//                if (!(kidsNumberSub.getNumber() > 0)) {
//                    Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(kidsNumber.getReport().getKindergarten().getId());
//                    if (optionalKindergarten.isPresent()) {
//                        Kindergarten kindergarten = optionalKindergarten.get();
//                        for (KidsNumberSub numberSub : kindergarten.getAverageKidsNumber().getKidsNumberSubList()) {
//                            if (kidsNumberSub.getAgeGroup().getId().equals(numberSub.getAgeGroup().getId())) {
//                                kidsNumberSub.setKidsNumber(numberSub.getKidsNumber());
//                                kidsNumberSubRepo.save(kidsNumberSub);
//                            }
//                        }
//                    }
//                }
//            }
//            kidsNumber.setVerified(true);
//            kidsNumber.setStatus(Status.TASDIQLANDI.getName());
//            kidsNumberRepo.save(kidsNumber);
//            reportService.calculate(kidsNumber.getReport());
//        }
//    }

}
