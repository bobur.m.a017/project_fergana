package com.optimal.fergana.kids.dto;

import com.optimal.fergana.kids.kidsSub.SubDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class KidsNumberDTO {

    private Timestamp date;

//    @NotNull
    private List<SubDTO> subDTO;
}


