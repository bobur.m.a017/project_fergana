package com.optimal.fergana.kids.dto;

import com.optimal.fergana.kids.kidsSub.SubResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class KidsNumberResDTO {

    private Integer id;
    private LocalDate date;
    private List<SubResDTO> subDTO;
    private boolean verified;
    private String status;
    private String kindergartenName;


}
