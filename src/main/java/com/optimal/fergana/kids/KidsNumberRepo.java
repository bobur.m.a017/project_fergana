package com.optimal.fergana.kids;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface KidsNumberRepo extends JpaRepository<KidsNumber,Integer> {

    Optional<KidsNumber> findByDateAndReport_KidsNumber_IdAndDeleteIsFalse(LocalDate date, Integer kindergartenId);

    List<KidsNumber> findAllByReport_Kindergarten_IdAndDateBetweenAndDeleteIsFalse(Integer report_kindergarten_id, LocalDate date, LocalDate date2);
    List<KidsNumber> findAllByReport_Kindergarten_Department_IdAndDateAndDeleteIsFalse(Integer report_department_id, LocalDate date);
    List<KidsNumber> findAllByReport_Kindergarten_RegionalDepartment_IdAndVerifiedIsFalseAndDeleteIsFalseAndDate(Integer report_kindergarten_department_id, LocalDate date);

    List<KidsNumber> findAllByStatus(String status);
}
