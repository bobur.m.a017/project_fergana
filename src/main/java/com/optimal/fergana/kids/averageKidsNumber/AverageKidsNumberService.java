package com.optimal.fergana.kids.averageKidsNumber;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.exception.AgeGroupNotFoundException;
import com.optimal.fergana.exception.KindergartenNotFoundException;
import com.optimal.fergana.exception.MenuNotFoundException;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.KidsNumberRepo;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.KidsNumberSubRepo;
import com.optimal.fergana.kids.kidsSub.SubDTO;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.kindergarten.KindergartenResDTO;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
@RequiredArgsConstructor
public class AverageKidsNumberService {
    private final KidsNumberRepo kidsNumberRepo;
    private final KidsNumberSubRepo kidsNumberSubRepo;
    private final AgeGroupRepo ageGroupRepo;
    private final KindergartenRepo kindergartenRepo;
    private final UserService userService;

    private final CustomConverter converter;

    public StateMessage add(List<SubDTO> dto, Integer kindergartenId) throws AgeGroupNotFoundException, KindergartenNotFoundException, MenuNotFoundException {
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;


        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(kindergartenId);

        if (optionalKindergarten.isPresent()) {
            Kindergarten kindergarten = optionalKindergarten.get();


            KidsNumber averageKidsNumber = kindergarten.getAverageKidsNumber();


            if (averageKidsNumber == null) {
                KidsNumber kidsNumber = new KidsNumber();

                Set<KidsNumberSub> list = new HashSet<>();

                for (SubDTO subDTO : dto) {

                    Optional<AgeGroup> optionalAgeGroup = ageGroupRepo.findById(subDTO.getAgeGroupId());

                    if (optionalAgeGroup.isPresent()) {
                        KidsNumberSub kidsNumberSub = new KidsNumberSub();
                        kidsNumberSub.setNumber(subDTO.getNumber());
                        kidsNumberSub.setKidsNumber(kidsNumber);
                        AgeGroup ageGroup = optionalAgeGroup.get();
                        kidsNumberSub.setAgeGroup(ageGroup);

                        list.add(kidsNumberSub);
                    } else {
                        return new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
                    }
                }
                kidsNumber.setKidsNumberSubList(list);
                kidsNumber.setStatus(Status.NEW.getName());
                KidsNumber save = kidsNumberRepo.save(kidsNumber);
                kindergarten.setAverageKidsNumber(save);
                kindergartenRepo.save(kindergarten);
                message = Message.SUCCESS_UZ;
            }
        }

        return new StateMessage().parse(message);
    }

    public StateMessage edit(List<SubDTO> dto, Integer kindergartenId) throws AgeGroupNotFoundException, KindergartenNotFoundException {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(kindergartenId);
        if (optionalKindergarten.isPresent()) {
            Kindergarten kindergarten = optionalKindergarten.get();
            KidsNumber averageKidsNumber = kindergarten.getAverageKidsNumber();

            if (averageKidsNumber != null) {
                Set<KidsNumberSub> set = new HashSet<>();
                for (SubDTO subDTO : dto) {
                    boolean res = true;
                    for (KidsNumberSub kidsNumberSub : averageKidsNumber.getKidsNumberSubList()) {
                        if (kidsNumberSub.getAgeGroup().getId().equals(subDTO.getAgeGroupId())) {
                            kidsNumberSub.setNumber(subDTO.getNumber());
                            set.add(kidsNumberSub);
                            res = false;
                        }
                    }
                    if (res) {
                        Optional<AgeGroup> optionalAgeGroup = ageGroupRepo.findById(subDTO.getAgeGroupId());
                        if (optionalAgeGroup.isPresent()) {
                            AgeGroup ageGroup = optionalAgeGroup.get();
                            set.add((new KidsNumberSub(ageGroup, subDTO.getNumber(), averageKidsNumber,ageGroup.getSortNumber())));
                        }
                    }
                }
                averageKidsNumber.setKidsNumberSubList(new HashSet<>(kidsNumberSubRepo.saveAll(set)));
                KidsNumber save = kidsNumberRepo.save(averageKidsNumber);
                message = Message.EDIT_UZ;
            }
        }
        return new StateMessage().parse(message);
    }

    public List<KindergartenResDTO> getAll(HttpServletRequest request, Integer departmentId) {

        Users user = userService.parseToken(request);

        List<Kindergarten> kindergartenList;
        if (user.getKindergarten() != null) {
             kindergartenList = kindergartenRepo.findAllByIdAndDeleteIsFalse(user.getKindergarten().getId());
        } else if (user.getDepartment() != null) {
            kindergartenList = kindergartenRepo.findAllByDepartment_IdAndDeleteIsFalse(user.getDepartment().getId());
        } else {
            kindergartenList = kindergartenRepo.findAllByDepartment_IdAndDeleteIsFalse(departmentId);
        }

        List<KindergartenResDTO> dtoList = converter.kindergartenToDTOAverageKidsNumber(kindergartenList);

        dtoList.sort(Comparator.comparing(KindergartenResDTO::getNumber));
        return dtoList;
    }
}
