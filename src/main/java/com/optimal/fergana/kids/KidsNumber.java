package com.optimal.fergana.kids;

import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.report.Report;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class KidsNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private LocalDate date;

    private boolean verified=false;

    private String status;

    private boolean delete=false;

    @OneToMany(mappedBy = "kidsNumber", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<KidsNumberSub> kidsNumberSubList;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne
    private Report report;
}
