package com.optimal.fergana.controller;


import com.optimal.fergana.department.DepartmentDTO;
import com.optimal.fergana.department.DepartmentService;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/department")
public class DepartmentController {


    private final DepartmentService departmentService;
    private final UserService userService;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody DepartmentDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = departmentService.add(dto, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody DepartmentDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = departmentService.edit(dto, users, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = departmentService.delete(users, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            return departmentService.getOne(users, id);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            return departmentService.getAll(users);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }





    @PreAuthorize("hasAnyRole('XODIMLAR_BO`LIMI')")
    @GetMapping("/time")
    public ResponseEntity<?> getTime(HttpServletRequest request) {
        try {
            return ResponseEntity.status(200).body(departmentService.getTime(request));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('XODIMLAR_BO`LIMI')")
    @PutMapping("/time")
    public ResponseEntity<?> setTime (@RequestParam ("hours") Integer hours, @RequestParam("minutes") Integer minutes, HttpServletRequest request) {
        try {
            StateMessage stateMessage = departmentService.setTime(hours, minutes, request);

            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
