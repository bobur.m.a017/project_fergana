package com.optimal.fergana.controller;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategoryDTO;
import com.optimal.fergana.product.sanpin_catecory.SanpinService;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/out/api/sanpinCategory")
public class SanPinCategoryController {

    private final SanpinService sanpinService;
    private final UserService userService;

    public SanPinCategoryController(SanpinService sanpinService, UserService userService) {
        this.sanpinService = sanpinService;
        this.userService = userService;
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody SanpinCategoryDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = sanpinService.add(dto, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody SanpinCategoryDTO dto) {
        try {
            StateMessage message = sanpinService.edit(dto, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = sanpinService.delete(id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping()
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        Users user = userService.parseToken(request);
        return sanpinService.getAll(user);
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        return sanpinService.getOne(id);
    }
}


