package com.optimal.fergana.controller;


import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.productPack.ProductPackDTO;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;


@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/pack")
public class ProductPackController {


    private final UserService userService;
    private final ProductPackService productPackService;


    @PreAuthorize("hasAnyRole('XODIMLAR_BO`LIMI')")
    @PostMapping("/{productId}")
    public ResponseEntity<?> add(@PathVariable Integer productId, @RequestBody ProductPackDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);

            StateMessage message = productPackService.add(users, dto, productId);

            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('XODIMLAR_BO`LIMI')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable UUID id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);

            StateMessage message = productPackService.delete(id);

            return ResponseEntity.status(200).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('XODIMLAR_BO`LIMI')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request, @Param("page") Integer page, @Param("pageSize") Integer pageSize) {
        try {
            Users users = userService.parseToken(request);

            CustomPageable pageable = productPackService.getAll(users, page, pageSize);

            return ResponseEntity.status(200).body(pageable);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


}
