package com.optimal.fergana.controller;


import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.permission.PermissionDTO;
import com.optimal.fergana.users.permission.PermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/permission")
public class PermissionController {


    private final UserService userService;
    private final PermissionService permissionService;


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @PostMapping("/{id}")
    public ResponseEntity<?> changeStatus(@PathVariable Integer id, HttpServletRequest request) {

        try {
            Users users = userService.parseToken(request);
            StateMessage message = permissionService.changeStatus(users, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(e.hashCode()).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @GetMapping
    public ResponseEntity<?> get(HttpServletRequest request) {

        try {
            Users users = userService.parseToken(request);
            List<PermissionDTO> dtoList = permissionService.get(users);
            return ResponseEntity.status(200).body(dtoList);
        } catch (Exception e) {
            return ResponseEntity.status(e.hashCode()).body(e.getMessage());
        }
    }
}
