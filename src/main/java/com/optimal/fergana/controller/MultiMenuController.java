package com.optimal.fergana.controller;


import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandardDTO;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandardService;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.multiMenu.MultiMenuDTO;
import com.optimal.fergana.multiMenu.MultiMenuService;
import com.optimal.fergana.multiMenu.product.SanPinCategoryWeight;
import com.optimal.fergana.reporter.pdf.MenuPDFReporter;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.stylesheets.LinkStyle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/multiMenu")
public class MultiMenuController {


    private final MultiMenuService multiMenuService;
    private final UserService userService;
    private final MealAgeStandardService mealAgeStandardService;
    private final MenuPDFReporter menuPDFReporter;


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody @Valid MultiMenuDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = multiMenuService.add(users, dto);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable UUID id, @Param("name") String name, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = multiMenuService.edit(id, name, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG','BO`LIM_BUXGALTER','HAMSHIRA','OMBORCHI')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable UUID id, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);

            return multiMenuService.getOne(id, users);
//            return ResponseEntity.status(200).body(new Timestamp(System.currentTimeMillis()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG','BO`LIM_BUXGALTER','HAMSHIRA','BUXGALTER','OMBORCHI')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            return multiMenuService.getAll(users);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable UUID id, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);
            StateMessage message = multiMenuService.delete(id, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @PostMapping("/addMeal/{id}")
    public ResponseEntity<?> addMeal(@PathVariable UUID id, @RequestBody MealAgeStandardDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = mealAgeStandardService.add(dto, id, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @DeleteMapping("/deleteMeal/{id}")
    public ResponseEntity<?> deleteMeal(@PathVariable UUID id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = mealAgeStandardService.delete(id, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping("/getFile/{id}")
    public String getFile(@PathVariable UUID id, HttpServletRequest request) {

        try {
            Users users = userService.parseToken(request);
            String path = menuPDFReporter.createPDFMenu(id);

            return StaticWords.URI + path.substring(StaticWords.DEST.length());

//            Resource file = load(path);
//
//            return ResponseEntity.ok()
//                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + path.substring(StaticWords.DEST_PDF.length()) + "\"").body(file);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping("/getFileDefault")
    public String getFileDefault() {

        try {
            String path = menuPDFReporter.createPDFMenu(UUID.fromString("7ffc531d-1c24-4320-b62e-c7d4907c7fce"));

            return StaticWords.URI + path.substring(StaticWords.DEST.length());

//            Resource file = load(path);
//
//            return ResponseEntity.ok()
//                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + path.substring(StaticWords.DEST_PDF.length()) + "\"").body(file);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping("/getProduct/{id}")
    public ResponseEntity<?> getProduct(@PathVariable UUID id, HttpServletRequest request) {

        try {
            List<SanPinCategoryWeight> list = multiMenuService.getProduct(id);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping("/getProductFile/{id}")
    public String getProductFile(@PathVariable UUID id) {

        try {
            return multiMenuService.getProductFile(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
