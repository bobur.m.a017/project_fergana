package com.optimal.fergana.controller;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.supplier.STIRParser.STIRParser;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.supplier.SupplierDTO;
import com.optimal.fergana.supplier.SupplierService;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/out/api/supplier")
public class SupplierController {
    private final SupplierService supplierService;
    private final STIRParser stirParser;


    public SupplierController(SupplierService supplierService, STIRParser stirParser) {
        this.supplierService = supplierService;
        this.stirParser = stirParser;
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER')")
    @PostMapping
    public ResponseEntity<?> add(@Valid @RequestBody SupplierDTO dto, HttpServletRequest request) {
        try {
            StateMessage stateMessage = supplierService.add(dto, request);
            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@RequestBody SupplierDTO dto, @RequestBody Integer id) {
        try {
            StateMessage stateMessage = supplierService.edit(dto, id);
            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@RequestBody Integer id) {
        try {
            StateMessage stateMessage = supplierService.delete(id);
            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@RequestBody Integer id) {
        try {
            Supplier supplier = supplierService.getOne(id);
            return ResponseEntity.status(HttpStatus.OK).body(supplier);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER')")
    @GetMapping("/info")
    public ResponseEntity<?> getSupplier(@Param("STIR") String STIR) {
        try {
            SupplierDTO supplier = stirParser.parserLiveSTIR(STIR);
            return ResponseEntity.ok().body(supplier);
        } catch (UsernameNotFoundException usernameNotFoundException) {
            return ResponseEntity.status(401).body(usernameNotFoundException.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request, Integer departmentId, Integer pageNumber, Integer pageSize) {
        try {
            CustomPageable customPageable = supplierService.getAll(request, departmentId, pageNumber, pageSize);
            return ResponseEntity.status(HttpStatus.OK).body(customPageable);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
