package com.optimal.fergana.controller;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.product.product_category.ProductCategoryDTO;
import com.optimal.fergana.product.product_category.ProductCategoryService;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/out/api/productCategory")
public class ProductCategoryController {

    private final ProductCategoryService productCategoryService;
    private final UserService userService;

    public ProductCategoryController(ProductCategoryService productCategoryService, UserService userService) {
        this.productCategoryService = productCategoryService;
        this.userService = userService;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody ProductCategoryDTO dto, HttpServletRequest request) {
//        try {
        Users users = userService.parseToken(request);
        StateMessage message = productCategoryService.add(dto, users);
        return ResponseEntity.status(message.getCode()).body(message);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody ProductCategoryDTO dto) {
        try {
            StateMessage message = productCategoryService.edit(dto,id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<ProductCategoryDTO> list = productCategoryService.getAll(users);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = productCategoryService.delete(id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
