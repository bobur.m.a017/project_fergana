package com.optimal.fergana.controller;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.order.ContractOrderDTO;
import com.optimal.fergana.order.OrderDTO;
import com.optimal.fergana.order.OrderResDTO;
import com.optimal.fergana.order.MyOrderService;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.ProductResDTO;
import com.optimal.fergana.reporter.pdf.ContractPDFReportBYMTT;
import com.optimal.fergana.reporter.pdf.OrderPDFReporter;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/order")
public class OrderController {

    private final MyOrderService myOrderService;
    private final ContractPDFReportBYMTT contractPDFReportBYMTT;
    private final UserService userService;
    private final OrderPDFReporter orderPDFReporter;


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody @Valid OrderDTO dto, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);
            StateMessage message;

            if (dto.getMonth() == null || dto.getYear() == null || dto.getMonth() > 12 || dto.getMonth() < 1 || dto.getYear() < 2023 || dto.getYear() > 2025) {
                message = new StateMessage("Null qiymat qabul qilinmaydi", false, 417);
            } else {
                message = myOrderService.add(dto, users);
            }
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@RequestBody @Valid OrderDTO orderDTO, @PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = myOrderService.edit(orderDTO, id, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = myOrderService.delete(id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','BUXGALTER')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            OrderResDTO orderResDTO = myOrderService.getOne(id);
            return ResponseEntity.status(HttpStatus.OK).body(orderResDTO);
        } catch (Exception e) {
            e.printStackTrace();

            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("page") Integer page, @Param("pageSize") Integer pageSize, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            CustomPageable pageable = myOrderService.getAll(users, pageSize, page);
            return ResponseEntity.status(HttpStatus.OK).body(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @GetMapping("/getProduct/{id}")
    public ResponseEntity<?> getProduct(@PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<ProductResDTO> list = myOrderService.getProduct(id);
            return ResponseEntity.status(HttpStatus.OK).body(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @PostMapping("/{id}")
    public ResponseEntity<?> addContract(@PathVariable Integer id, @RequestBody ContractOrderDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = myOrderService.addContract(id, dto, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @PutMapping("/toRoundOff/{id}")
    public ResponseEntity<?> toRoundOff(@Param("weight") Double weight, @PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = myOrderService.toRoundOff(id, BigDecimal.valueOf(weight).divide(BigDecimal.valueOf(1000), 2, RoundingMode.HALF_UP));
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('BOSHQARMA_BUXGALTER','BO`LIM_BUXGALTER','BUXGALTER')")
    @GetMapping("/reportPDFByOrder/{orderId}")
    public ResponseEntity<?> getReportByOrder(@PathVariable Integer orderId, HttpServletRequest request) {
        try {
            String paragraph = orderPDFReporter.createPDFMenu("Buyurtma Jadvali", orderId);
            return ResponseEntity.ok(paragraph);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


}
