package com.optimal.fergana.controller;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.reporter.pdf.ContractPDFReportBYMTT;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.supplier.contract.ContractDTO;
import com.optimal.fergana.supplier.contract.ContractResDTO;
import com.optimal.fergana.supplier.contract.ContractService;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/contract")
public class ContractController {

    private final ContractService contractService;
    private final ContractPDFReportBYMTT contractPDFReportBYMTT;
    private final UserService userService;


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody @Valid ContractDTO dto, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);
            StateMessage message = contractService.add(dto, users);

            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@RequestBody @Valid ContractDTO contractDTO, @PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = contractService.edit(contractDTO, id, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = contractService.delete(id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','BUXGALTER')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            ContractResDTO contract = contractService.getOne(id);
            return ResponseEntity.status(HttpStatus.OK).body(contract);
        } catch (Exception e) {
            e.printStackTrace();

            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("page") Integer page, @Param("pageSize") Integer pageSize, @Param("supplierId") Integer supplierId, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            CustomPageable pageable = contractService.getAll(users, pageSize, page, supplierId);
            return ResponseEntity.status(HttpStatus.OK).body(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER')")
    @PostMapping("/verified/{id}")
    public ResponseEntity<?> verified(@PathVariable Integer id, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);
            StateMessage message = contractService.confirmation(users, id);

            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER','BOSHQARMA_BUXGALTER')")
    @GetMapping("/getFile/{id}")
    public String getReportById(@PathVariable Integer id, HttpServletResponse response) {
        try {
            String path = contractPDFReportBYMTT.createPDFKindergarten(id, response);


            return StaticWords.URI + path.substring(StaticWords.DEST.length());
//            File file = new File(pdfKindergarten);
//            FileInputStream fileInputStream = new FileInputStream(file);
//            FileCopyUtils.copy(fileInputStream, response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
