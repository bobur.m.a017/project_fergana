package com.optimal.fergana.controller;

import com.optimal.fergana.accountant.Accountant;
import com.optimal.fergana.accountant.AccountantDTO;
import com.optimal.fergana.accountant.AccountantResDTO;
import com.optimal.fergana.accountant.AccountantService;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.message.StateMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/accountant")
public class AccountantController {
    private final AccountantService accountantService;

    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER','OMBORCHI')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody AccountantDTO dto) {
        try {
            StateMessage message = accountantService.add(dto.getUserId(), dto.getKindergartenIds());
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER','OMBORCHI')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        List<AccountantResDTO> accountants = accountantService.getAll(request);
        return ResponseEntity.status(200).body(accountants);
    }

    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER','OMBORCHI')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            Set<Kindergarten> one = accountantService.getOne(id);
            return ResponseEntity.status(200).body(one);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
