package com.optimal.fergana.controller;


import com.optimal.fergana.kindergarten.dto.DepartmentDTOMenu;
import com.optimal.fergana.kindergarten.dto.KindergartenByAddressDTO;
import com.optimal.fergana.menu.AddMenuDTO;
import com.optimal.fergana.menu.MenuResDTO;
import com.optimal.fergana.menu.MenuService;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.selectMenu.SelectMenuServiceInterface;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;


@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/menu")
public class MenuController {


    private final MenuService menuService;
    private final UserService userService;
    private final SelectMenuServiceInterface selectMenuServiceInterface;


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG','HAMSHIRA','BUXGALTER','BO`LIM_BUXGALTER')")
    @GetMapping("/getKindergartenByDepartmentIdAddMenu/{id}")
    public ResponseEntity<?> getKindergartenByDepartmentIdAddMenu(@PathVariable Integer id, @RequestParam("date") Long date, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<KindergartenByAddressDTO> list = menuService.getKindergartenByDepartmentIdAddMenu(id, new Timestamp(date), users, false);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG','HAMSHIRA','BUXGALTER','BO`LIM_BUXGALTER')")
    @GetMapping("/getDepartmentAddMenu")
    public ResponseEntity<?> getDepartmentIdAddMenu(@RequestParam("date") Long date, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<DepartmentDTOMenu> list = menuService.getDepartmentIdAddMenu(new Timestamp(date), users, false);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG','HAMSHIRA')")
    @GetMapping("/getKindergartenByDepartmentIdAddMenuAdd/{id}")
    public ResponseEntity<?> getKindergartenByDepartmentIdAddMenuAdd(@PathVariable Integer id, @RequestParam("date") Long date, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<KindergartenByAddressDTO> list = menuService.getKindergartenByDepartmentIdAddMenu(id, new Timestamp(date), users, true);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG','HAMSHIRA')")
    @GetMapping("/getDepartmentAddMenuAdd")
    public ResponseEntity<?> getKindergartenByDepartmentIdAddMenuAdd(@RequestParam("date") Long date, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<DepartmentDTOMenu> list = menuService.getDepartmentIdAddMenu(new Timestamp(date), users, true);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','XODIMLAR_BO`LIMI','HAMSHIRA','TEXNOLOG')")
    @PostMapping("/{menuId}")
    public ResponseEntity<?> add(@PathVariable UUID menuId, @RequestBody List<AddMenuDTO> dtoList) {
        try {

            StateMessage message = menuService.addMenuKindergarten(menuId, dtoList);
            return ResponseEntity.status(message.getCode()).body(message);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','TEXNOLOG')")
    @PostMapping("/selectMenu/{multiMenuId}")
    public ResponseEntity<?> selectMenu(@PathVariable UUID multiMenuId, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);

            StateMessage message = selectMenuServiceInterface.select(multiMenuId, users);
            return ResponseEntity.status(message.getCode()).body(message);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','HAMSHIRA')")
    @PostMapping("/replace")
    public ResponseEntity<?> replace(@RequestParam UUID menuId, @RequestParam Long time, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);

            LocalDate date = (new Timestamp(time).toLocalDateTime().toLocalDate());

            StateMessage message = menuService.replace(menuId, date, users);
            return ResponseEntity.status(message.getCode()).body(message);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

//
//    @PreAuthorize("hasAnyRole('SUPER_ADMIN','OMBORCHI','TEXNOLOG','HAMSHIRA','RAXBAR','BUXGALTER','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','BUXGALTER','XODIMLAR_BO`LIMI')")
//    @GetMapping("/{menuId}")
//    public ResponseEntity<?> getOne(@PathVariable String menuId) {
//        try {
//
//            if (menuId.equals("undefined"))
//                return ResponseEntity.status(500).body("Ma‘lumot mavjud emas");
//
//            MenuResDTO dto = menuService.getOne(UUID.fromString(menuId));
//            return ResponseEntity.status(200).body(dto);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
//    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','OMBORCHI','TEXNOLOG','HAMSHIRA','RAXBAR','BUXGALTER','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','BUXGALTER','XODIMLAR_BO`LIMI')")
    @GetMapping("/getMenuNew")
    public ResponseEntity<?> getOne(@RequestParam String menuId) {
        try {

            if (menuId.equals("undefined"))
                return ResponseEntity.status(500).body("Ma‘lumot mavjud emas");

            MenuResDTO dto = menuService.getOne(UUID.fromString(menuId));
            return ResponseEntity.status(200).body(dto);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','OMBORCHI','TEXNOLOG','HAMSHIRA','RAXBAR','BUXGALTER','BO`LIM_BUXGALTE','BOSHQARMA_BUXGALTER','XODIMLAR_BO`LIMI')")
    @GetMapping
    public ResponseEntity<?> getOne1(@RequestParam("menuId") String menuId) {
        try {

            MenuResDTO dto = menuService.getOne(UUID.fromString(menuId));
            return ResponseEntity.status(200).body(dto);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
