package com.optimal.fergana.controller;


import com.google.gson.Gson;
import com.optimal.fergana.meal.MealDTO;
import com.optimal.fergana.meal.MealResDTO;
import com.optimal.fergana.meal.MealService;
import com.optimal.fergana.menu.ageStandard.AgeStandardDTO;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/meal")
public class MealController {


    private final MealService mealService;
    private final UserService userService;


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @PostMapping
    public ResponseEntity<?> add(@Valid @RequestParam String jsonString, MultipartHttpServletRequest request) throws IOException {
//        try {
        Gson gson = new Gson();
        MealDTO mealDTO = gson.fromJson(jsonString, MealDTO.class);
        Users users = userService.parseToken(request);
        MultipartFile files = request.getFile("file");
        StateMessage stateMessage = mealService.add(mealDTO, users, files);
        return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @Valid @RequestParam String jsonString, MultipartHttpServletRequest request) throws IOException {
        try {
            Gson gson = new Gson();
            MealDTO mealDTO = gson.fromJson(jsonString, MealDTO.class);
            Users users = userService.parseToken(request);
            MultipartFile files = request.getFile("file");
            StateMessage stateMessage = mealService.edit(mealDTO, id, files);
            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
//            StateMessage stateMessage = mealService.delete(id);
//            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
            return ResponseEntity.status(417).body(new StateMessage("Ovqatni o‘chirib bo‘lmaydi.",false));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            return mealService.getOne(id);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<MealResDTO> list = mealService.getAll(users);

            list.sort(Comparator.comparing(MealResDTO::getName));

            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @GetMapping("/getProduct/{id}")
    public ResponseEntity<?> getProduct(@PathVariable Integer id) {
        try {
            return ResponseEntity.status(200).body(mealService.getProduct(id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @PostMapping("/getSanPin")
    public ResponseEntity<?> getSanPin(@RequestParam("mealId") Integer mealId, @RequestParam("multiMenuId") UUID multiMenuId, @RequestBody List<AgeStandardDTO> dtoList) {
        try {

            for (AgeStandardDTO dto : dtoList) {
                if (dto.getAgeGroupId() == null) {
                    return ResponseEntity.status(417).body(new StateMessage("Null qiymat qabul qilinmaydi", false, 417));
                }
                if (dto.getWeight() == null) {
                    dto.setWeight(0.0);
                }
            }
            return ResponseEntity.status(200).body(mealService.getSanPin(multiMenuId, mealId, dtoList));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
