package com.optimal.fergana.controller;

import com.optimal.fergana.exception.AgeGroupNotFoundException;
import com.optimal.fergana.exception.KindergartenNotFoundException;
import com.optimal.fergana.exception.MenuNotFoundException;
import com.optimal.fergana.kids.KidsNumberService;
import com.optimal.fergana.kids.averageKidsNumber.AverageKidsNumberService;
import com.optimal.fergana.kids.dto.KidsNumberDTO;
import com.optimal.fergana.kids.dto.KidsNumberResDTO;
import com.optimal.fergana.kids.kidsSub.SubDTO;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenResDTO;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/out/api/averageKidsNumber")
public class AverageKidsNumberController {


    private final AverageKidsNumberService averageKidsNumberService;


    @PreAuthorize("hasAnyRole('XODIMLAR_BO`LIMI')")
    @PostMapping("/{kindergartenId}")
    public ResponseEntity<?> add(@RequestBody @Valid List<SubDTO> dto, @PathVariable Integer kindergartenId) {
        try {
            for (SubDTO subDTO : dto) {
                if (subDTO.getNumber() == null || subDTO.getAgeGroupId() == null) {
                    StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
                    message.setText("NULL QIYMAT QABUL QILINMAYDI");
                    return ResponseEntity.status(message.getCode()).body(message);
                }
            }

            StateMessage message = averageKidsNumberService.add(dto, kindergartenId);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (AgeGroupNotFoundException | KindergartenNotFoundException | MenuNotFoundException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(exception.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('XODIMLAR_BO`LIMI')")
    @PutMapping("/{kindergartenId}")
    public ResponseEntity<?> edit(@PathVariable Integer kindergartenId, @RequestBody List<SubDTO> dto) {
        try {
            for (SubDTO subDTO : dto) {
                if (subDTO.getNumber() == null || subDTO.getAgeGroupId() == null || subDTO.getId() == null) {
                    StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
                    message.setText("NULL QIYMAT QABUL QILINMAYDI");
                    return ResponseEntity.status(message.getCode()).body(message);
                }
            }
            StateMessage message = averageKidsNumberService.edit(dto, kindergartenId);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (AgeGroupNotFoundException | KindergartenNotFoundException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(exception.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('HAMSHIRA','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','TEXNOLOG','RAXBAR','XODIMLAR_BO`LIMI')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request, @Param("departmentId") Integer departmentId) {
        try {
            List<KindergartenResDTO> kindergartenList = averageKidsNumberService.getAll(request, departmentId);
            return ResponseEntity.status(200).body(kindergartenList);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
