package com.optimal.fergana.controller;


import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.*;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/out/api/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }



    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("/region")
    public ResponseEntity<?> addUserRegion(@Valid @RequestBody UsersDTO dto, HttpServletRequest request) {
        try {
            StateMessage message = userService.addUserRegion(dto, request);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("/department")
    public ResponseEntity<?> addUserDepartment(@RequestParam("departmentId") Integer departmentId,@Valid @RequestBody UsersDTO dto, HttpServletRequest request) {
        try {
            StateMessage message = userService.addUserDepartment(departmentId, dto, request);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('XODIMLAR_BO`LIMI')")
    @PostMapping("/kindergarten")
    public ResponseEntity<?> addUserKindergarten(@RequestParam("kindergartenId") Integer kindergartenId,@Valid  @RequestBody UsersDTO dto, HttpServletRequest request) {
        try {
            StateMessage message = userService.addUserKindergarten(kindergartenId, dto, request);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@RequestBody UsersDTO dto, HttpServletRequest request, @PathVariable Integer id) {
        try {
            StateMessage message = userService.edit(dto, request, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @PutMapping("changeStatus/{id}")
    public ResponseEntity<?> changeStatus(@RequestParam("status") boolean status, @PathVariable Integer id) {
        try {
            StateMessage message = userService.changeStatus(id, status);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = userService.delete(id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            UsersResDTO users = userService.getOne(id);
            return ResponseEntity.status(HttpStatus.OK).body(users);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('SUPPER_ADMIN','ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getMyData")
    public ResponseEntity<?> getMyData(HttpServletRequest request) {
        try {
            UsersResDTO users = userService.getMyData(request);
            return ResponseEntity.status(HttpStatus.OK).body(users);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("id") Integer id, @Param("type") String type, HttpServletRequest request) {
        try {
            List<UsersResDTO> usersList = userService.getAll(type, request, id);
            return ResponseEntity.status(HttpStatus.OK).body(usersList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','SUPER_ADMIN','ADMIN_DEPARTMENT','BOSHQARMA_BUXGALTER','TEXNOLOG','BO`LIM_BUXGALTER','HAMSHIRA','OMBORCHI','RAXBAR','BUXGALTER')")
    @PutMapping("credentials/login")
    public ResponseEntity<?> editLogin(@RequestBody @Valid UserCredentialDTO dto, HttpServletRequest request) {
        try {
            StateMessage message = userService.changeCredentialsLogin(request,dto);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','SUPER_ADMIN','ADMIN_DEPARTMENT','BOSHQARMA_BUXGALTER','TEXNOLOG','BO`LIM_BUXGALTER','HAMSHIRA','OMBORCHI','RAXBAR','BUXGALTER')")
    @PutMapping("credentials/password")
    public ResponseEntity<?> editPassword(@RequestBody @Valid UserCredentialDTO dto, HttpServletRequest request, Errors errors) {
        try {
            if (errors.hasErrors())
                return ResponseEntity.status(400).body(errors.toString());
            StateMessage message = userService.changeCredentialsPassword(request,dto);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @PutMapping("reset/password/{userId}")
    public ResponseEntity<?> resetUserCredentials(@PathVariable Integer userId, HttpServletRequest request) {
        try {
            StateMessage message = userService.resetUserCredentials(userId);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
