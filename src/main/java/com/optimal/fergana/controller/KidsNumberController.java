package com.optimal.fergana.controller;

import com.optimal.fergana.exception.AgeGroupNotFoundException;
import com.optimal.fergana.exception.KindergartenNotFoundException;
import com.optimal.fergana.exception.MenuNotFoundException;
import com.optimal.fergana.kids.dto.KidsNumberDTO;
import com.optimal.fergana.kids.dto.KidsNumberResDTO;
import com.optimal.fergana.kids.KidsNumberService;
import com.optimal.fergana.kids.kidsSub.SubDTO;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/out/api/kidsNumber")
public class KidsNumberController {


    private final KidsNumberService kidsNumberService;
    private final UserService userService;


    @PreAuthorize("hasAnyRole('HAMSHIRA')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody KidsNumberDTO dto, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);
            if (dto.getSubDTO() == null) {
                StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
                message.setText("NULL QIYMAT QABUL QILINMAYDI SUB dto null");
                return ResponseEntity.status(message.getCode()).body(message);
            }


            for (SubDTO subDTO : dto.getSubDTO()) {
                if (subDTO.getNumber() == null || subDTO.getAgeGroupId() == null) {
                    StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
                    message.setText("NULL QIYMAT QABUL QILINMAYDI");
                    return ResponseEntity.status(message.getCode()).body(message);
                }
            }

            StateMessage message = kidsNumberService.add(dto, request);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(exception.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('RAXBAR')")
    @PostMapping("/verified/{id}")
    public ResponseEntity<?> verified(@PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);

            StateMessage message = kidsNumberService.verified(id, users);
            return ResponseEntity.status(message.getCode()).body(message);

        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(exception.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('HAMSHIRA')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody KidsNumberDTO dto, HttpServletRequest request) {
        try {
            for (SubDTO subDTO : dto.getSubDTO()) {
                if (subDTO.getNumber() == null || subDTO.getAgeGroupId() == null || subDTO.getId() == null) {
                    StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
                    message.setText("NULL QIYMAT QABUL QILINMAYDI");
                    return ResponseEntity.status(message.getCode()).body(message);
                }
            }
            StateMessage message = kidsNumberService.edit(dto, id, request);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (AgeGroupNotFoundException | KindergartenNotFoundException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(exception.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','TEXNOLOG','BUXGALTER')")
    @GetMapping("/getAllByDepartmentId/{departmentId}")
    public ResponseEntity<?> getAllByDepartmentId(HttpServletRequest request, @PathVariable Integer departmentId, @Param("date") Long date) {
        try {

            Users users = userService.parseToken(request);
            List<KidsNumberResDTO> list = kidsNumberService.getAllByDepartmentId(users, departmentId, new Timestamp(date != null ? date : System.currentTimeMillis()).toLocalDateTime().toLocalDate());
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','TEXNOLOG','BUXGALTER','HAMSHIRA','RAXBAR')")
    @GetMapping("/getAllByDate/{id}")
    public ResponseEntity<?> getAllByDate(HttpServletRequest request, @PathVariable Integer id, @Param("startDate") Long startDate, @Param("endDate") Long endDate) {
        try {
            LocalDate start = new Timestamp(startDate != null ? startDate : System.currentTimeMillis()).toLocalDateTime().toLocalDate();
            LocalDate end = new Timestamp(endDate != null ? endDate : System.currentTimeMillis()).toLocalDateTime().toLocalDate();

            Users users = userService.parseToken(request);
            List<KidsNumberResDTO> list = kidsNumberService.getAllByDate(users, id, start, end);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('HAMSHIRA','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','TEXNOLOG','RAXBAR')")
    @GetMapping("/getOne")
    public ResponseEntity<?> getOne(HttpServletRequest request, @Param("date") Long date) {
        try {
            if (date == null) {
                date = System.currentTimeMillis();
            }
            KidsNumberResDTO one = kidsNumberService.getOne(request, new Timestamp(date).toLocalDateTime().toLocalDate());

            return ResponseEntity.status(200).body(one);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }

    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        StateMessage message = kidsNumberService.delete(id);
        return ResponseEntity.status(message.getCode()).body(message);
    }


    @PreAuthorize("hasAnyRole('HAMSHIRA','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','TEXNOLOG','RAXBAR')")
    @GetMapping("/getStat")
    public ResponseEntity<?> getStat(HttpServletRequest request, @Param("date") Long date) {
        try {
            if (date == null) {

                date = System.currentTimeMillis();
            }
            KidsNumberResDTO one = kidsNumberService.getOne(request, new Timestamp(date).toLocalDateTime().toLocalDate());


            return ResponseEntity.status(200).body(one);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }

    }


}
