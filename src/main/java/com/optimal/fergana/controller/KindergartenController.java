package com.optimal.fergana.controller;


import com.optimal.fergana.kindergarten.KindergartenDTO;
import com.optimal.fergana.kindergarten.KindergartenResDTO;
import com.optimal.fergana.kindergarten.KindergartenService;
import com.optimal.fergana.kindergarten.dto.KindergartenByAddressDTO;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;


@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/kindergarten")
public class KindergartenController {


    private final KindergartenService kindergartenService;
    private final UserService userService;


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody KindergartenDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = kindergartenService.add(dto, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping("{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody KindergartenDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = kindergartenService.edit(dto, users, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = kindergartenService.delete(users, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            return kindergartenService.getOne(id);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("pageNumber") Integer pageNumber, @Param("pageSize") Integer pageSize, @Param("departmentId") Integer departmentId, @Param("number") Integer number, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            CustomPageable pageable = kindergartenService.getAll(users, departmentId, number, pageNumber, pageSize);
            return ResponseEntity.status(200).body(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','XODIMLAR_BO`LIMI')")
    @GetMapping("/get")
    public ResponseEntity<?> get(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<KindergartenByAddressDTO> list = kindergartenService.get(users);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','SUPER_ADMIN','XODIMLAR_BO`LIMI')")
    @GetMapping("/getDate")
    public ResponseEntity<?> getDate() {
        try {
            return ResponseEntity.status(200).body(new Timestamp(System.currentTimeMillis()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','TEXNOLOG','HAMSHIRA','BOSHQARMA_BUXGALTER')")
    @GetMapping("/getByDepartmentId/{departmentId}")
    public ResponseEntity<?> getByDepartmentId(@PathVariable Integer departmentId, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);

            List<KindergartenResDTO> list = kindergartenService.getByDepartmentId(departmentId, users);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI')")
    @GetMapping("/getByDepartment")
    public ResponseEntity<?> getByDepartment(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<KindergartenResDTO> list = kindergartenService.getByDepartmentId(users);


            list.sort(Comparator.comparing(KindergartenResDTO::getNumber));


            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
