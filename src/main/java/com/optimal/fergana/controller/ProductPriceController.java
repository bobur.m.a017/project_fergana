package com.optimal.fergana.controller;


import com.optimal.fergana.exception.ProductNotFoundException;
import com.optimal.fergana.exception.ProductPriceNotFoundException;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.product_price.ProductPriceDTO;
import com.optimal.fergana.product.product_price.ProductPriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/out/api/productPrice")
@RequiredArgsConstructor
public class ProductPriceController {

    private final ProductPriceService productPriceService;


    @PreAuthorize("hasAnyRole('BOSHQARMA_BUXGALTER','ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(HttpServletRequest request, @RequestBody ProductPriceDTO dto) {
        try {
            StateMessage message = productPriceService.add(request, dto);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (ProductNotFoundException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(401).body(exception.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('BOSHQARMA_BUXGALTER','ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@RequestBody ProductPriceDTO dto, @PathVariable Integer id) {
        try {
            StateMessage message = productPriceService.edit(dto, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (ProductNotFoundException | ProductPriceNotFoundException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(401).body(exception.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('BOSHQARMA_BUXGALTER','ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = productPriceService.delete(id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity.status(401).body(exception.getMessage());
        }

    }

//    @PreAuthorize("hasAnyRole('BOSHQARMA_BUXGALTER','ADMIN')")
//    @GetMapping("/{id}")
//    public ResponseEntity<?> getOne(@PathVariable Integer id) {
//        return null;
//
//    }

    @PreAuthorize("hasAnyRole('BOSHQARMA_BUXGALTER','ADMIN')")
    @GetMapping("/{productId}")
    public ResponseEntity<?> getAllByProduct(HttpServletRequest request,@PathVariable Integer productId,@Param("pageNumber") Integer pageNumber,@Param("pageSize") Integer pageSize) {
       try {
           CustomPageable customPageable = productPriceService.getAllPriceByProduct(request, productId, pageNumber, pageSize);
           return ResponseEntity.status(200).body(customPageable);
       }catch (Exception exception){
           exception.printStackTrace();
           return ResponseEntity.status(exception.hashCode()).body(exception.getMessage());
       }
    }

    @PreAuthorize("hasAnyRole('BOSHQARMA_BUXGALTER','ADMIN')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request,@Param("pageNumber") Integer pageNumber, @Param("search") String search,@Param("pageSize") Integer pageSize) {
       try {
           CustomPageable customPageable = productPriceService.getAll(request, pageNumber, pageSize,search );
           return ResponseEntity.status(200).body(customPageable);
       }catch (Exception exception){
           exception.printStackTrace();
           return ResponseEntity.status(exception.hashCode()).body(exception.getMessage());
       }
    }

}
