package com.optimal.fergana.controller;


import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.acceptedProducts.AcceptedProductDTO;
import com.optimal.fergana.product.acceptedProducts.AcceptedProductService;
import com.optimal.fergana.supplier.contract.product.ProductContractService;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/acceptedProduct")
public class AcceptedProductController {


    private final AcceptedProductService acceptedProductService;
    private final UserService userService;
    private final ProductContractService productContractService;



    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @PostMapping("/{productContractId}")
    public ResponseEntity<?> add(@PathVariable UUID productContractId, @RequestBody AcceptedProductDTO dto, HttpServletRequest request) {
        try {

            Users user = userService.parseToken(request);
            StateMessage message = acceptedProductService.add(productContractId, dto, user);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

//
//    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
//    @PutMapping("/{id}")
//    public ResponseEntity<?> edit(@PathVariable UUID id, @RequestBody AcceptedProductDTO dto, HttpServletRequest request) {
//        try {
//            Users user = userService.parseToken(request);
//            StateMessage message = acceptedProductService.edit(id, dto, user);
//            return ResponseEntity.status(200).body(message);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
//    }


//    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
//    @DeleteMapping("/{id}")
//    public ResponseEntity<?> delete(@PathVariable UUID id, HttpServletRequest request) {
//        try {
//            Users user = userService.parseToken(request);
//            StateMessage message = acceptedProductService.delete(id, user);
//            return ResponseEntity.status(200).body(message);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
//    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','RAXBAR','HAMSHIRA','BOSHQARMA_BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("page") Integer page, @Param("pageSize") Integer pageSize, HttpServletRequest request) {
        try {
            Users user = userService.parseToken(request);
            CustomPageable pageable = acceptedProductService.getAll(page, pageSize, user);
            return ResponseEntity.status(200).body(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','RAXBAR','HAMSHIRA','BOSHQARMA_BUXGALTER')")
    @GetMapping("/contract")
    public ResponseEntity<?> getShartnomalar(@Param("page") Integer page, @Param("pageSize") Integer pageSize, HttpServletRequest request) {
        try {
            Users user = userService.parseToken(request);
            CustomPageable pageable = productContractService.getAll(user,page,pageSize);
            return ResponseEntity.status(200).body(pageable);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
