package com.optimal.fergana.controller;


import com.optimal.fergana.kids.KidsNumberService;
import com.optimal.fergana.report.ReportResDTO;
import com.optimal.fergana.report.ReportService;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/report")
public class ReportController {


    private final ReportService reportService;
    private final UserService userService;
    private final KidsNumberService kidsNumberService;


    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping("/getReportById/{id}")
    public ResponseEntity<?> getReportById(@PathVariable UUID id) {
        try {
            ReportResDTO dto = reportService.getReportById(id);
            return ResponseEntity.status(200).body(dto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('HAMSHIRA','RAXBAR','OMBORCHI')")
    @GetMapping("/getReportByDate")
    public ResponseEntity<?> getReportByDate(HttpServletRequest request, @Param("date") Long date) {
        try {
            Users users = userService.parseToken(request);

            if (date == null)
                date = System.currentTimeMillis();
            ReportResDTO dto = reportService.getReportByDate(users, new Timestamp(date).toLocalDateTime().toLocalDate());
            return ResponseEntity.status(200).body(dto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping("/getKidsNumberReport")
    public String getKidsNumberReport(@RequestParam("startDate") Long startDate, @RequestParam("endDate") Long endDate, @Param("kindergartenId") Integer kindergartenId, HttpServletRequest request, HttpServletResponse response) {
        try {

            Users users = userService.parseToken(request);
            String path = reportService.getKidsNumberPDF(new Timestamp(startDate).toLocalDateTime().toLocalDate(), new Timestamp(endDate).toLocalDateTime().toLocalDate(), kindergartenId, response, users);

//            Resource file = load(path);
            String str = StaticWords.URI + path.substring(StaticWords.DEST.length());

            return str;

//           return ResponseEntity.ok()
//                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + path.substring(StaticWords.DEST_PDF.length()) + "\"").body(file);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping("/getMenuReport")
    public String getMenu(@RequestParam("reportId") UUID reportId, @RequestParam("type") String type, HttpServletRequest request, HttpServletResponse response) {

        try {
            Users users = userService.parseToken(request);
            String path = reportService.getMenuReport(reportId, type, response);
//            Resource file = load(path);
            String str = StaticWords.URI + path.substring(StaticWords.DEST.length());

            return str;
//
//            return ResponseEntity.ok()
//                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + path.substring(StaticWords.DEST_PDF.length()) + "\"").body(file);

        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping("/getReportInputOutPut")
    public String getReportInputOutPut(@RequestParam("start") Long start, @RequestParam("end") Long end, @Param("kindergartenId") Integer kindergartenId, HttpServletResponse response, HttpServletRequest request) {

        try {
            Users users = userService.parseToken(request);
            String path = reportService.getReportInputOutPut(new Timestamp(start).toLocalDateTime().toLocalDate(), new Timestamp(end).toLocalDateTime().toLocalDate(), kindergartenId, users);

            if (path == null) {
                throw new RuntimeException("MA`LUMOTLAR NOTO`G`RI KIRITILGAN");
            }
            return StaticWords.URI + path.substring(StaticWords.DEST.length());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Resource load(String filename) {
        try {
            Path file = Paths.get("uploads").resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping("/getAllByDepartmentId/{departmentId}")
    public String getAllByDepartmentId(HttpServletRequest request, @PathVariable Integer departmentId, @Param("date") Long date) {
        try {

            Users users = userService.parseToken(request);
            return kidsNumberService.getAllByDepartmentIdPDF(users, departmentId, new Timestamp(date != null ? date : System.currentTimeMillis()).toLocalDateTime().toLocalDate());

        } catch (Exception e) {

            e.printStackTrace();

            return e.getMessage();
        }
    }


    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','TEXNOLOG','BUXGALTER','HAMSHIRA','RAXBAR')")
    @GetMapping("/getAllByDate/{id}")
    public String getAllByDate(HttpServletRequest request, @PathVariable Integer id, @Param("startDate") Long startDate, @Param("endDate") Long endDate) {
        try {
            LocalDate start = new Timestamp(startDate != null ? startDate : System.currentTimeMillis()).toLocalDateTime().toLocalDate();
            LocalDate end = new Timestamp(endDate != null ? endDate : System.currentTimeMillis()).toLocalDateTime().toLocalDate();

            Users users = userService.parseToken(request);
            String allByDatePDF = kidsNumberService.getAllByDatePDF(users, id, start, end);
            return allByDatePDF;
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
