package com.optimal.fergana.controller;


import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.WarehouseService;
import com.optimal.fergana.warehouse.inOut.InOutPriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/warehouse")
public class WarehouseController {

    private final UserService userService;
    private final WarehouseService warehouseService;
    private final InOutPriceService inOutPriceService;


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','RAXBAR','BOSHQARMA_BUXGALTER','BO`LIM_BUXGALTER','TEXNOLOG','HAMSHIRA')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("kindergartenId") Integer kindergartenId, HttpServletRequest request, @Param("pageSize") Integer pageSize, @Param("pageNumber") Integer pageNumber) {
        try {

            Users users = userService.parseToken(request);
            Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);

            return warehouseService.getAll(users, kindergartenId, pageable);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','RAXBAR','TEXNOLOG','BO`LIM_BUXGALTER','BUXGALTER','BOSHQARMA_BUXGALTER')")
    @GetMapping("/getFile")
    public String getMenu(@Param("kindergartenId") Integer kindergartenId, HttpServletRequest request, HttpServletResponse response) {

        try {
            Users users = userService.parseToken(request);
            String path = warehouseService.getFile(kindergartenId, users, response);

            String str = StaticWords.URI + path.substring(StaticWords.DEST.length());

            return str;

//            Resource file = load(path);
//
//            return ResponseEntity.ok()
//                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + path.substring(StaticWords.DEST_PDF.length()) + "\"").body(file);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @PostMapping
    public ResponseEntity<?> add(@Param("kindergartenId") Integer kindergartenId, @RequestParam("productId") Integer productId, @RequestParam("weight") Double weight, @RequestParam("price") Double price, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);

            if (weight > 10000 || price > 1000000) {
                return ResponseEntity.status(417).body(new StateMessage("Kiritilgan son meyyordan ortiq", false, 417));
            }
            if (weight < 0.01 || price < 1) {
                return ResponseEntity.status(417).body(new StateMessage("Kiritilgan son meyyordan ortiq", false, 417));
            }

            StateMessage message = warehouseService.addProductFront(weight, price, productId, kindergartenId, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable UUID id, @RequestParam("packWeight") Double packWeight, @RequestParam("price") Double price, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);

            StateMessage message = inOutPriceService.edit(id, BigDecimal.valueOf(price), BigDecimal.valueOf(packWeight));
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    public Resource load(String filename) {
        try {
            Path file = Paths.get("uploads").resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
}
