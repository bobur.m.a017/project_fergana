package com.optimal.fergana.controller;


import com.optimal.fergana.ageGroup.AgeGroupDTO;
import com.optimal.fergana.ageGroup.AgeGroupService;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Controller
@ResponseBody
@RequestMapping("/out/api/ageGroup")
public class AgeGroupController {

    private final AgeGroupService ageGroupService;
    private final UserService userService;

    public AgeGroupController(AgeGroupService ageGroupService, UserService userService) {
        this.ageGroupService = ageGroupService;
        this.userService = userService;
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody AgeGroupDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = ageGroupService.add(dto, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody AgeGroupDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = ageGroupService.edit(id,dto, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();

            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete (@PathVariable Integer id, HttpServletRequest request) throws ChangeSetPersister.NotFoundException {
        try {
            Users user = userService.parseToken(request);
            StateMessage message = ageGroupService.delete(id,user);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();

            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id, HttpServletRequest request) {
        try {
            return ageGroupService.getOne(id);
        } catch (Exception e) {
            e.printStackTrace();

            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            return ageGroupService.getAll(users);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

}
