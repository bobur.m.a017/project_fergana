package com.optimal.fergana.controller;

import com.optimal.fergana.exception.NotificationNotFoundException;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.notification.NotificationService;
import com.optimal.fergana.notification.dto.NotificationDTO;
import com.optimal.fergana.notification.dto.NotificationOneResDTO;
import com.optimal.fergana.page.CustomPageable;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/notification")
public class NotificationController {


    private final NotificationService notificationService;


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','ADMIN_DEPARTMENT','BOSHQARMA_BUXGALTER','TEXNOLOG','BO`LIM_BUXGALTER','HAMSHIRA','OMBORCHI','XODIMLAR_BO`LIMI','RAXBAR','BUXGALTER')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            NotificationOneResDTO one = notificationService.getOne(id);
            return ResponseEntity.status(200).body(one);
        } catch (NotificationNotFoundException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }



    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','ADMIN_DEPARTMENT','BOSHQARMA_BUXGALTER','TEXNOLOG','BO`LIM_BUXGALTER','HAMSHIRA','OMBORCHI','XODIMLAR_BO`LIMI','RAXBAR','BUXGALTER')")
    @GetMapping()
    public ResponseEntity<?> getAll(@Param("page") Integer page,@Param("pageSize") Integer pageSize, HttpServletRequest request) {

      try {
          CustomPageable all = notificationService.getAll(request, page, pageSize);
          return ResponseEntity.status(200).body(all);
      }catch (Exception e){
          e.printStackTrace();
          return ResponseEntity.status(e.hashCode()).body(e.getMessage());
      }
    }



    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER')")
    @PostMapping()
    public ResponseEntity<?> add(HttpServletRequest request, @RequestBody NotificationDTO dto) {
        try {
            StateMessage message = notificationService.add(request, dto);
            return ResponseEntity.status(message.getCode()).body(message);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


}
