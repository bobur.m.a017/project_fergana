package com.optimal.fergana.address.region;

import com.optimal.fergana.address.district.DistrictDTO;

import java.util.List;

public class RegionDTO {

    private Integer id;
    private String name;
    private List<DistrictDTO> districtList;

    public RegionDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public RegionDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DistrictDTO> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<DistrictDTO> districtList) {
        this.districtList = districtList;
    }
}
