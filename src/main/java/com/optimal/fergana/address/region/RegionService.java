package com.optimal.fergana.address.region;

import com.optimal.fergana.address.district.District;
import com.optimal.fergana.address.district.DistrictDTO;
import com.optimal.fergana.address.district.DistrictService;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.report.ReportResDTO;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RequiredArgsConstructor
@Service
public class RegionService {
    private final RegionRepo regionRepo;
    private final DistrictService districtService;


    public List<RegionDTO> getAll(Users users) {

        List<RegionDTO> list = new ArrayList<>();
        List<Region> regionList = new ArrayList<>();
        if (users.getRegionalDepartment() == null) {
            regionList.addAll(regionRepo.findAll());
        } else {
            regionList.add(users.getRegionalDepartment().getRegion());
        }

        for (Region region : regionList) {
            RegionDTO dto = new RegionDTO(region.getId(), region.getName());

            List<DistrictDTO> districtList = new ArrayList<>();
            for (District district : region.getDistrictList()) {
                districtList.add(districtService.parse(district));
            }
            dto.setDistrictList(districtList);
            list.add(dto);
        }
        return list;
    }
}
