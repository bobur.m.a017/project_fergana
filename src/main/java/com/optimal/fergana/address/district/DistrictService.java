package com.optimal.fergana.address.district;

import org.springframework.stereotype.Service;

@Service
public record DistrictService(
        DistrictRepo districtRepo
) {

    public DistrictDTO parse(District district) {
        return new DistrictDTO(district.getId(), district.getName());
    }
}
