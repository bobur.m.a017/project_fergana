package com.optimal.fergana.address.district;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DistrictRepo extends JpaRepository<District,Integer> {
}
