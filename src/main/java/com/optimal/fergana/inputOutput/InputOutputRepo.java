package com.optimal.fergana.inputOutput;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface InputOutputRepo extends JpaRepository<InputOutput, UUID> {

    Optional<InputOutput> findByKindergarten_IdAndLocalDate(Integer kindergarten_id, LocalDate localDate);
    List<InputOutput> findAllByKindergarten_IdAndLocalDateBetween(Integer kindergarten_id, LocalDate localDate, LocalDate localDate2);

}
