package com.optimal.fergana.inputOutput.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;



@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InputOutputDTO {
    private Integer productId;
    private String productName;
    private List<DateInputOutPutDTO> dateList;
}
