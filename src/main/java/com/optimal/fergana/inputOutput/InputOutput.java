package com.optimal.fergana.inputOutput;


import com.optimal.fergana.inputOutput.inOut.InOut;
import com.optimal.fergana.kindergarten.Kindergarten;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InputOutput {

    @Id
    private UUID id = UUID.randomUUID();
    private LocalDate localDate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "inputOutput", cascade = CascadeType.ALL)
    private Set<InOut> inOuts;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private Kindergarten kindergarten;


    public InputOutput(LocalDate localDate, Kindergarten kindergarten) {
        this.localDate = localDate;
        this.kindergarten = kindergarten;
    }
}
