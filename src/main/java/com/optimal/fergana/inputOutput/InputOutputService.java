package com.optimal.fergana.inputOutput;


import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.inputOutput.inOut.InOut;
import com.optimal.fergana.inputOutput.inOut.InOutRepo;
import com.optimal.fergana.inputOutput.inOutAgeGroup.InOutAgeGroup;
import com.optimal.fergana.inputOutput.inOutAgeGroup.InOutAgeGroupRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.menu.ProductMenuDTO;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.WarehouseRepo;
import com.optimal.fergana.warehouse.inOut.InOutPriceDTO;
import com.optimal.fergana.warehouse.inOut.InOutPriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

@Service
@RequiredArgsConstructor
public class InputOutputService {

    private final InputOutputRepo inputOutputRepo;
    private final InOutRepo inOutRepo;
    private final InOutAgeGroupRepo inOutAgeGroupRepo;
    private final InOutPriceService inOutPriceService;
    private final KindergartenRepo kindergartenRepo;
    private final WarehouseRepo warehouseRepo;

    public void addInput(Product product, Kindergarten kindergarten, LocalDate localDate, BigDecimal weight, BigDecimal packWeight, BigDecimal pack) {

        Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByKindergarten_IdAndLocalDate(kindergarten.getId(), localDate);

        if (inputOutputOptional.isPresent()) {
            InputOutput inputOutput = inputOutputOptional.get();
            Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_IdAndProduct_Id(inputOutput.getId(), product.getId());

            if (optionalInOut.isPresent()) {
                InOut inOut = optionalInOut.get();
                inOut.setInputWeight(inOut.getInputWeight().add(weight));
                inOut.setInputPackWeight(inOut.getInputPackWeight().add(packWeight));
                inOutRepo.save(inOut);
            } else {
                InOut inOut = new InOut(product, inputOutput);
                inOut.setInputWeight(weight);
                inOut.setInputPackWeight(packWeight);
                inOutRepo.save(inOut);
            }
        } else {

            InputOutput inputOutput = new InputOutput(localDate, kindergarten);
            InOut inOut = new InOut(product, inputOutput);
            inOut.setInputWeight(weight);
            inOut.setInputPackWeight(packWeight);
            inputOutputRepo.save(inputOutput);
            inOutRepo.save(inOut);
        }
    }

    public InOut addOutput(Product product, Kindergarten kindergarten, LocalDate localDate, BigDecimal weight) {

        Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByKindergarten_IdAndLocalDate(kindergarten.getId(), localDate);

        if (inputOutputOptional.isPresent()) {
            InputOutput inputOutput = inputOutputOptional.get();
            Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_IdAndProduct_Id(inputOutput.getId(), product.getId());

            if (optionalInOut.isPresent()) {
                InOut inOut = optionalInOut.get();

                List<InOutPriceDTO> subtractList = inOutPriceService.subtract(product, weight, kindergarten);

                BigDecimal total = BigDecimal.ZERO;
                for (InOutPriceDTO inOutPriceDTO : subtractList) {
                    inOut.setOutputWeight(inOut.getOutputWeight().add(inOutPriceDTO.getWeight()));
                    inOut.setOutputPackWeight(inOut.getOutputPackWeight().add(inOutPriceDTO.getWeightPack()));
                    total = total.add(inOutPriceDTO.getPrice().multiply(inOutPriceDTO.getWeightPack()));
                }
                inOut.setOutputPrice(total.divide(inOut.getOutputPackWeight(), 6, RoundingMode.HALF_UP));

                return inOutRepo.save(inOut);

            } else {
                InOut inOut = new InOut(product, inputOutput);

                List<InOutPriceDTO> subtractList = inOutPriceService.subtract(product, weight, kindergarten);

                BigDecimal total = BigDecimal.ZERO;
                for (InOutPriceDTO inOutPriceDTO : subtractList) {
                    inOut.setOutputWeight(inOut.getOutputWeight().add(inOutPriceDTO.getWeight()));
                    inOut.setOutputPackWeight(inOut.getOutputPackWeight().add(inOutPriceDTO.getWeightPack()));
                    total = total.add(inOutPriceDTO.getPrice().multiply(inOutPriceDTO.getWeightPack()));
                }
                if (inOut.getOutputPackWeight().compareTo(BigDecimal.ZERO) != 0) {
                    inOut.setOutputPrice(total.divide(inOut.getOutputPackWeight(), 6, RoundingMode.HALF_UP));
                } else {
                    inOut.setOutputPrice(total);
                }

                return inOutRepo.save(inOut);
            }
        } else {
            InputOutput inputOutput = new InputOutput(localDate, kindergarten);
            InOut inOut = new InOut(product, inputOutput);

            List<InOutPriceDTO> subtractList = inOutPriceService.subtract(product, weight, kindergarten);
            BigDecimal total = BigDecimal.ZERO;
            for (InOutPriceDTO inOutPriceDTO : subtractList) {
                inOut.setOutputWeight(inOut.getOutputWeight().add(inOutPriceDTO.getWeight()));
                inOut.setOutputPackWeight(inOut.getOutputPackWeight().add(inOutPriceDTO.getWeightPack()));
                total = total.add(inOutPriceDTO.getPrice().multiply(inOutPriceDTO.getWeightPack()));
            }
            inOut.setOutputPrice(total.divide(inOut.getOutputPackWeight(), 6, RoundingMode.HALF_UP));

            inputOutputRepo.save(inputOutput);
            return inOut;
        }
    }

//    private List<InOutAgeGroup> addInOutAgeGroup(List<InOutAgeGroup> list, Product product, BigDecimal weight, InOut inOut, LocalDate date, AgeGroup ageGroup, BigDecimal price) {
//
//        boolean res = true;
//        for (InOutAgeGroup inOutAgeGroup : list) {
//            if (inOutAgeGroup.getAgeGroup().getId().equals(ageGroup.getId()) && (inOutAgeGroup.getPrice().compareTo(price) == 0)) {
//                BigDecimal addWeight = inOutAgeGroup.getOutputWeight().add(weight);
//                inOutAgeGroup.setOutputWeight(addWeight);
//                inOutAgeGroup.setTotalPrice(inOutAgeGroup.getPrice().multiply(addWeight));
//                res = false;
//            }
//        }
//        if (res) {
//            InOutAgeGroup inOutAgeGroup = new InOutAgeGroup(product, weight, date, inOut, ageGroup, price, weight.multiply(price));
//            list.add(inOutAgeGroup);
//        }
//        return
//                list;
//    }

    public Vector<ProductMenuDTO> getPrice(Vector<ProductMenuDTO> productList, LocalDate date, Kindergarten kindergarten) {

        for (ProductMenuDTO productMenuDTO : productList) {
//            List<InOutAgeGroup> list = inOutAgeGroupRepo.findAllByInOut_InputOutput_LocalDateAndProduct_Id(date, productMenuDTO.getId());
//            BigDecimal price = BigDecimal.valueOf(0);
//            BigDecimal totalWeight = BigDecimal.valueOf(0);
//            BigDecimal totalSum = BigDecimal.valueOf(0);
//
//            int count = 0;
//            for (InOutAgeGroup inOutAgeGroup : list) {
//                BigDecimal price1 = inOutAgeGroup.getPrice();
//                BigDecimal outputWeight = inOutAgeGroup.getOutputWeight();
//
//                totalWeight = totalWeight.add(outputWeight);
//                totalSum = totalSum.add(price1.multiply(outputWeight));
//            }
//
//            if (totalSum.compareTo(BigDecimal.ZERO) == 0 || totalWeight.compareTo(BigDecimal.ZERO) == 0) {
//                productMenuDTO.setPrice(BigDecimal.ZERO);
//            } else {
//                productMenuDTO.setPrice(totalSum.divide(totalWeight, 6, RoundingMode.HALF_UP));
//            }
            Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_LocalDateAndInputOutput_Kindergarten_IdAndProduct_Id(date, kindergarten.getId(), productMenuDTO.getId());
            if (optionalInOut.isPresent()) {
                InOut inOut = optionalInOut.get();
                productMenuDTO.setPrice(inOut.getOutputPrice() != null ? inOut.getOutputPrice() : BigDecimal.ZERO);
            } else {
                productMenuDTO.setPrice(BigDecimal.ZERO);
            }
        }
        return productList;
    }


    @Scheduled(cron = "0 50 23 * * ?")
    public void qoldiqXisoblash() {

        List<Kindergarten> all = kindergartenRepo.findAll();
        LocalDate localDate = LocalDate.now();

        for (Kindergarten kindergarten : all) {
            List<Warehouse> warehouses = kindergarten.getWarehouseList();

            for (Warehouse warehouse : warehouses) {

                if (warehouse.getTotalWeight().compareTo(BigDecimal.ZERO) > 0) {

                    Product product = warehouse.getProduct();
                    BigDecimal weight = warehouse.getTotalWeight();
                    BigDecimal packWeight = warehouse.getTotalPackWeight();

                    Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByKindergarten_IdAndLocalDate(kindergarten.getId(), localDate);

                    if (inputOutputOptional.isPresent()) {
                        InputOutput inputOutput = inputOutputOptional.get();
                        Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_IdAndProduct_Id(inputOutput.getId(), product.getId());

                        if (optionalInOut.isPresent()) {
                            InOut inOut = optionalInOut.get();
                            inOut.setResidual(inOut.getResidual().add(weight));
                            inOut.setResidualPack(inOut.getResidualPack().add(packWeight));
                            inOutRepo.save(inOut);
                        } else {
                            InOut inOut = new InOut(product, inputOutput);
                            inOut.setResidual(weight);
                            inOut.setResidualPack(packWeight);
                            inOutRepo.save(inOut);
                        }
                    } else {

                        InputOutput inputOutput = new InputOutput(localDate, kindergarten);
                        InOut inOut = new InOut(product, inputOutput);
                        inOut.setResidual(weight);
                        inOut.setResidualPack(packWeight);
                        inputOutputRepo.save(inputOutput);
                        inOutRepo.save(inOut);
                    }
                }
            }
        }
    }

}
