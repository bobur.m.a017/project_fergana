package com.optimal.fergana.inputOutput.inOutAgeGroup;


import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.inputOutput.inOut.InOut;
import com.optimal.fergana.product.Product;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class InOutAgeGroup {

    @Id
    private UUID id = UUID.randomUUID();

    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    private Product product;


    @Column(precision = 19, scale = 6)
    private BigDecimal outputWeight = BigDecimal.valueOf(0);

    private LocalDate date;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    private InOut inOut;

    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    private AgeGroup ageGroup;

    @Column(precision = 19, scale = 6)
    private BigDecimal price = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal totalPrice;


    public InOutAgeGroup(Product product, BigDecimal outputWeight, LocalDate date, InOut inOut, AgeGroup ageGroup,BigDecimal price, BigDecimal totalPrice) {
        this.product = product;
        this.outputWeight = outputWeight;
        this.date = date;
        this.inOut = inOut;
        this.ageGroup = ageGroup;
        this.price = price;
        this.totalPrice = totalPrice;

    }


    public BigDecimal getTotalPrice() {
        return this.price.multiply(this.outputWeight);
    }
}
