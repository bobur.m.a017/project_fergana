package com.optimal.fergana.inputOutput.inOutAgeGroup;


import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface InOutAgeGroupRepo extends JpaRepository<InOutAgeGroup, UUID> {

Optional<InOutAgeGroup> findByAgeGroup_IdAndDateAndInOut_IdAndPrice(Integer ageGroup_id, LocalDate date, UUID inOut_id, BigDecimal price);

List<InOutAgeGroup> findAllByInOut_InputOutput_Kindergarten_IdAndProduct_IdAndAgeGroup_IdAndInOut_InputOutput_LocalDateBetween(Integer kindergarten_id, Integer product_id, Integer ageGroup_id, LocalDate date, LocalDate date2);


List<InOutAgeGroup> findAllByInOut_InputOutput_LocalDateAndProduct_Id(LocalDate inOut_inputOutput_localDate, Integer product_id);

}
