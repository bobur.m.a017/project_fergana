package com.optimal.fergana.inputOutput.inOut;


import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

public interface InOutRepo extends JpaRepository<InOut, UUID> {

    Optional<InOut> findByInputOutput_IdAndProduct_Id(UUID inputOutput_id, Integer product_id);
    Optional<InOut> findByInputOutput_LocalDateAndInputOutput_Kindergarten_IdAndProduct_Id(LocalDate inputOutput_localDate, Integer inputOutput_kindergarten_id, Integer product_id);

}
