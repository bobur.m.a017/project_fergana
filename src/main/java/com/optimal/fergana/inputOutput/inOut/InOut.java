package com.optimal.fergana.inputOutput.inOut;


import com.optimal.fergana.inputOutput.InputOutput;
import com.optimal.fergana.inputOutput.inOutAgeGroup.InOutAgeGroup;
import com.optimal.fergana.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InOut {

    @Id
    private UUID id = UUID.randomUUID();

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(precision = 19, scale = 6)
    private BigDecimal inputWeight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal inputPackWeight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal outputWeight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal outputPackWeight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal outputPrice = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal percentageOutput = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal residual = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal residualPack = BigDecimal.valueOf(0);

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private InputOutput inputOutput;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "inOut", cascade = CascadeType.ALL)
    private List<InOutAgeGroup> inOutAgeGroupList;

    public InOut(Product product, InputOutput inputOutput) {
        this.product = product;
        this.inputOutput = inputOutput;
    }
}
