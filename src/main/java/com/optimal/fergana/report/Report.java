package com.optimal.fergana.report;

import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.report.menuReport.AgeGroupReport;
import com.optimal.fergana.report.menuReport.MenuReport;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Report {

    @Id
    private UUID id = UUID.randomUUID();
    private Integer day;
    private Integer month;
    private Integer year;
    private LocalDate date;
    private boolean edit = true;
    private String filePathExcel;
    private String filePathPdf;
    private Boolean fullSuccess = false;


    @CreationTimestamp
    private Timestamp createDate = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;


    @ManyToOne(fetch = FetchType.LAZY)
    private Kindergarten kindergarten;


    @ManyToOne(fetch = FetchType.LAZY)
    private Menu menu;


    @OneToOne(fetch = FetchType.LAZY, mappedBy = "report")
    private MenuReport menuReport;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "report", fetch = FetchType.LAZY)
    private List<AgeGroupReport> ageGroupReportList;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(mappedBy = "report", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private KidsNumber kidsNumber;


    public Report(Integer day, Integer month, Integer year, Kindergarten kindergarten) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.kindergarten = kindergarten;
        setDate(day, month, year);
    }


    public void setDate(Integer day, Integer month, Integer year) {
        this.date = LocalDate.of(year, month, day);
    }
}
