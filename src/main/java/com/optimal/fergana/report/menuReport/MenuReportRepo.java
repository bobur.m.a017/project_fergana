package com.optimal.fergana.report.menuReport;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MenuReportRepo extends JpaRepository<MenuReport, UUID> {
}
