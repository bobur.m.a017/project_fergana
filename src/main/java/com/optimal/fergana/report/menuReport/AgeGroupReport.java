package com.optimal.fergana.report.menuReport;

import com.optimal.fergana.report.Report;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class AgeGroupReport {

    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;


    private Integer ageGroupId;
    private String ageGroupName;

    private BigDecimal number;

    private Integer sortNumber;


    private BigDecimal oneKidsTotalSum;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private Report report;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ageGroupReport", fetch = FetchType.LAZY)
    private List<AgeGroupTotal> ageGroupTotalList;


    public AgeGroupReport(Integer ageGroupId, String ageGroupName, BigDecimal number, Integer sortNumber, Report report) {
        this.ageGroupId = ageGroupId;
        this.ageGroupName = ageGroupName;
        this.number = number;
        this.sortNumber = sortNumber;
        this.report = report;
    }
}
