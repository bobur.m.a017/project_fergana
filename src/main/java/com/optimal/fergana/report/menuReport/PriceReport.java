package com.optimal.fergana.report.menuReport;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class PriceReport {

    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;

    private String productName;
    private Integer productId;

    @Column(precision = 19, scale = 6)
    private BigDecimal price;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private MenuReport menuReport;
}
