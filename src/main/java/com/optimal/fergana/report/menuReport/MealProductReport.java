package com.optimal.fergana.report.menuReport;


import com.optimal.fergana.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealProductReport {

    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;

    private String productName;
    private Integer idProduct;
    @Column(precision = 19, scale = 6)
    private BigDecimal weight;
    @Column(precision = 19, scale = 6)
    private BigDecimal kcal;
    @Column(precision = 19, scale = 6)
    private BigDecimal oil;
    @Column(precision = 19, scale = 6)
    private BigDecimal carbohydrates;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private MealAgeStandardReport mealAgeStandardReport;

    @OneToOne
    private Product product;


    @Column(precision = 19, scale = 6)
    private BigDecimal protein;

    public MealProductReport(String productName, Integer idProduct, MealAgeStandardReport mealAgeStandardReport, Product product) {
        this.productName = productName;
        this.idProduct = idProduct;
        this.mealAgeStandardReport = mealAgeStandardReport;
        this.product = product;
    }

    public MealProductReport(String productName, Integer idProduct, BigDecimal weight, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates, BigDecimal protein) {
        this.productName = productName;
        this.idProduct = idProduct;
        this.weight = weight;
        this.kcal = kcal;
        this.oil = oil;
        this.carbohydrates = carbohydrates;
        this.protein = protein;
    }
}
