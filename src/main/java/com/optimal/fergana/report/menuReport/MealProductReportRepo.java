package com.optimal.fergana.report.menuReport;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface MealProductReportRepo extends JpaRepository<MealProductReport, UUID> {


    List<MealProductReport> findAllByMealAgeStandardReport_MealReport_MealTimeReport_MenuReport_Report_IdAndMealAgeStandardReport_AgeGroupId(UUID report_id, Integer ageGroupId);
    List<MealProductReport> findAllByProductIsNull();
}
