package com.optimal.fergana.report.menuReport;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;


@Entity
@Getter
@Setter
@NoArgsConstructor
public class MealTimeReport {


    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate  = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;

    private String mealTimeName;

    private Integer sortNumber;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mealTimeReport",fetch = FetchType.LAZY)
    private List<MealReport> mealReportList;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private MenuReport menuReport;

    public MealTimeReport(String mealTimeName, Integer sortNumber, MenuReport menuReport) {
        this.mealTimeName = mealTimeName;
        this.sortNumber = sortNumber;
        this.menuReport = menuReport;
    }
}
