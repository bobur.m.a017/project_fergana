package com.optimal.fergana.report.menuReport;


import com.optimal.fergana.menu.Menu;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class MealReport {

    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate  = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;


    private String mealName;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private MealTimeReport mealTimeReport;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mealReport", fetch = FetchType.LAZY)
    private List<MealAgeStandardReport> mealAgeStandardReportList;

    public MealReport(String mealName, MealTimeReport mealTimeReport) {
        this.mealName = mealName;
        this.mealTimeReport = mealTimeReport;
    }
}
