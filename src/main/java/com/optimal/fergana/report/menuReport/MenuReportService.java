package com.optimal.fergana.report.menuReport;

import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNorm;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNormRepo;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class MenuReportService {

    private final MealProductReportRepo mealProductReportRepo;
    private final ReportRepo reportRepo;
    private final MenuReportRepo menuReportRepo;
    private final DailyNormRepo dailyNormRepo;
    private final AgeGroupReportRepo ageGroupReportRepo;

    public MenuReport parseMenu(Menu menu, Report report) {

//        if (report.getMenuReport() != null) {
//            MenuReport menuReport = report.getMenuReport();
//            menuReportRepo.delete(menuReport);
//        }
        MenuReport menuReport = new MenuReport(menu.getName(), menu.getMultiMenu().getName(), menu.getId());

        List<MealTimeReport> mealTimeReportVector = new Vector<>();

        List<MealTimeStandard> mealTimeStandardVector = new Vector<>(menu.getMealTimeStandardList());

        ThreadGroup group = new ThreadGroup("active");

        for (int i = 0; i < mealTimeStandardVector.size(); i++) {
            int finalI = i;
            Thread thread = new Thread(group, () -> {
                MealTimeStandard mealTimeStandard = mealTimeStandardVector.get(finalI);
                MealTimeReport mealTimeReport = new MealTimeReport(mealTimeStandard.getMealTime().getName(), mealTimeStandard.sortNumber, menuReport);
                List<MealReport> mealReportList = new Vector<>();
                List<MealAgeStandard> mealAgeStandardVector = new Vector<>(mealTimeStandard.getMealAgeStandardList());
                for (MealAgeStandard mealAgeStandard : mealAgeStandardVector) {
                    MealReport mealReport = new MealReport(mealAgeStandard.getMeal().getName(), mealTimeReport);

                    List<MealAgeStandardReport> mealAgeStandardReportVector = new Vector<>();

                    List<AgeStandard> ageStandardVector = new Vector<>(mealAgeStandard.getAgeStandardList());

                    for (AgeStandard ageStandard : ageStandardVector) {

                        MealAgeStandardReport mealAgeStandardReport = new MealAgeStandardReport(ageStandard.getAgeGroup().getSortNumber(), ageStandard.getAgeGroup().getName(), ageStandard.getAgeGroup().getId(), ageStandard.getWeight(), mealReport);

                        List<MealProductReport> mealProductReportVector = new Vector<>();

                        Meal meal = mealAgeStandard.getMeal();

                        List<ProductMeal> productMealVector = new Vector<>(meal.getProductMealList());

                        for (ProductMeal productMeal : productMealVector) {

                            MealProductReport mealProductReport = new MealProductReport(productMeal.getProduct().getName(), productMeal.getProduct().getId(), mealAgeStandardReport, productMeal.getProduct());

                            BigDecimal divide = productMeal.getWeight().divide(meal.getWeight(), 10, RoundingMode.HALF_UP);
                            BigDecimal weight = divide.multiply(ageStandard.getWeight());

                            mealProductReport.setWeight(weight);

                            mealProductReport.setProtein(productMeal.getProtein().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP).multiply(weight));
                            mealProductReport.setKcal(productMeal.getKcal().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP).multiply(weight));
                            mealProductReport.setCarbohydrates(productMeal.getCarbohydrates().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP).multiply(weight));
                            mealProductReport.setOil(productMeal.getOil().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP).multiply(weight));
                            mealProductReportVector.add(mealProductReport);
                        }
                        mealAgeStandardReport.setMealProductReportList(mealProductReportVector);
                        mealAgeStandardReportVector.add(mealAgeStandardReport);
                    }
                    mealReport.setMealAgeStandardReportList(mealAgeStandardReportVector);
                    mealReportList.add(mealReport);
                }
                mealTimeReport.setMealReportList(mealReportList);
                mealTimeReportVector.add(mealTimeReport);
            });
            thread.start();
        }

        while (true) {
            int countActiveThread = group.activeCount();
            if (countActiveThread == 0) {
                break;
            }
        }
        menuReport.setMealTimeReportList(mealTimeReportVector);
        if(report.getMenuReport() == null){
            menuReport.setReport(report);
        }
        return menuReportRepo.save(menuReport);
    }

    public void addAgeGroupReport(Report report) {

        KidsNumber kidsNumber = report.getKidsNumber();

        if (report.getAgeGroupReportList() != null) {
            List<AgeGroupReport> deleteList = report.getAgeGroupReportList();
            ageGroupReportRepo.deleteAll(deleteList);
        }
        List<MealProductReport> vector = new Vector<>();
        List<AgeGroupReport> ageGroupReportVector = new Vector<>();
        List<KidsNumberSub> kidsNumberSubVector = new Vector<>(kidsNumber.getKidsNumberSubList().stream().toList());

        ThreadGroup group = new ThreadGroup("active thread");
        for (int i = 0; i < kidsNumberSubVector.size(); i++) {
            int finalI = i;
            Thread thread = new Thread(group, () -> {

                KidsNumberSub kidsNumberSub = kidsNumberSubVector.get(finalI);

                AgeGroupReport ageGroupReport = new AgeGroupReport(kidsNumberSub.getAgeGroup().getId(), kidsNumberSub.getAgeGroup().getName(), BigDecimal.valueOf(kidsNumberSub.getNumber()), kidsNumberSub.getAgeGroup().getSortNumber(), report);
                List<AgeGroupTotal> ageGroupTotalList = new Vector<>();

                List<MealProductReport> allVector = new Vector<>(mealProductReportRepo.findAllByMealAgeStandardReport_MealReport_MealTimeReport_MenuReport_Report_IdAndMealAgeStandardReport_AgeGroupId(report.getId(), kidsNumberSub.getAgeGroup().getId()));

                for (MealProductReport mealProductReport : allVector) {
                    mealProductReport.setWeight(mealProductReport.getWeight().multiply(BigDecimal.valueOf(kidsNumberSub.getNumber())));
                    boolean result = true;
                    for (AgeGroupTotal ageGroupTotal : ageGroupTotalList) {
                        if (ageGroupTotal.getProductId().equals(mealProductReport.getIdProduct())) {
                            ageGroupTotal.setTotalWeight(ageGroupTotal.getTotalWeight().add(mealProductReport.getWeight()));
                            result = false;
                        }
                    }
                    if (result) {
                        Optional<DailyNorm> optionalDailyNorm = dailyNormRepo.findByAgeGroup_IdAndSanpinCategory_Id(kidsNumberSub.getAgeGroup().getId(), mealProductReport.getProduct().getSanpinCategory().getId());
                        AgeGroupTotal ageGroupTotal = new AgeGroupTotal(mealProductReport.getProduct().getId(), mealProductReport.getProductName(), mealProductReport.getWeight(), ageGroupReport);
                        ageGroupTotal.setSanpinNorm(optionalDailyNorm.isEmpty() ? BigDecimal.ZERO : optionalDailyNorm.get().getWeight());
                        ageGroupTotalList.add(ageGroupTotal);
                    }
                }
                vector.addAll(allVector);
                ageGroupReport.setAgeGroupTotalList(ageGroupTotalList);
                ageGroupReportVector.add(ageGroupReport);
            });
            thread.start();
        }

        while (true) {
            int activeThreadCount = group.activeCount();
            if (activeThreadCount == 0)
                break;
        }
        report.setAgeGroupReportList(ageGroupReportRepo.saveAll(ageGroupReportVector));
        reportRepo.save(report);
        mealProductReportRepo.saveAll(vector);
    }
}