package com.optimal.fergana.report.menuReport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AgeGroupTotal {

    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;

    private Integer productId;
    private String productName;

    @Column(precision = 19, scale = 6)
    private BigDecimal sanpinNorm= BigDecimal.ZERO;

    @Column(precision = 19, scale = 6)
    private BigDecimal totalSum= BigDecimal.ZERO;

    @Column(precision = 19, scale = 6)
    private BigDecimal totalWeight= BigDecimal.ZERO;

    @Column(precision = 19, scale = 6)
    private BigDecimal productPrice = BigDecimal.ZERO;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private AgeGroupReport ageGroupReport;


    public AgeGroupTotal(Integer productId, String productName, BigDecimal totalWeight, AgeGroupReport ageGroupReport) {
        this.productId = productId;
        this.productName = productName;
        this.totalWeight = totalWeight;
        this.ageGroupReport = ageGroupReport;
    }

    public AgeGroupTotal(Integer productId, String productName, BigDecimal sanpinNorm, BigDecimal totalSum, BigDecimal totalWeight, BigDecimal productPrice) {
        this.productId = productId;
        this.productName = productName;
        this.sanpinNorm = sanpinNorm;
        this.totalSum = totalSum;
        this.totalWeight = totalWeight;
        this.productPrice = productPrice;
    }

    public BigDecimal getTotalSum() {
        return totalWeight.multiply(productPrice);
    }
}
