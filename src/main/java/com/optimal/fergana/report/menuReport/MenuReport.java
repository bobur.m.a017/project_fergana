package com.optimal.fergana.report.menuReport;

import com.optimal.fergana.report.Report;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;


@Entity
@Getter
@Setter
@NoArgsConstructor
public class MenuReport {


    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;

    private String name;
    private String multiMenuName;
    private UUID menuId;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(fetch = FetchType.LAZY)
    private Report report;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menuReport", fetch = FetchType.LAZY)
    private List<MealTimeReport> mealTimeReportList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menuReport", fetch = FetchType.LAZY)
    private List<PriceReport> priceReportList;

    public MenuReport(String name, String multiMenuName, UUID menuId) {
        this.name = name;
        this.multiMenuName = multiMenuName;
        this.menuId = menuId;
    }
}
