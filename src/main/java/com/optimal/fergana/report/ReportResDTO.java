package com.optimal.fergana.report;

import com.optimal.fergana.kids.dto.KidsNumberResDTO;
import com.optimal.fergana.menu.MenuResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReportResDTO {
    private UUID id;
    private Integer day;
    private Integer month;
    private Integer year;
    private boolean edit;
    private Timestamp createDate;
    private Timestamp updateDate;
    private Integer kindergartenId;
    private String kindergartenName;
    private MenuResDTO menu;
    private KidsNumberResDTO kidsNumber;

}
