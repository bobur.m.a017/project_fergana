package com.optimal.fergana.report;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.exception.NotificationNotFoundException;
import com.optimal.fergana.inputOutput.InputOutput;
import com.optimal.fergana.inputOutput.InputOutputRepo;
import com.optimal.fergana.inputOutput.InputOutputService;
import com.optimal.fergana.inputOutput.inOut.InOut;
import com.optimal.fergana.inputOutput.inOut.InOutRepo;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.KidsNumberRepo;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.KidsNumberSubRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.ProductMenuDTO;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.productMeal.ProductMealDTO;
import com.optimal.fergana.report.menuReport.MenuReportService;
import com.optimal.fergana.reporter.excel.InputOutputReporterExcel;
import com.optimal.fergana.reporter.excel.MenuExcelReporter;
import com.optimal.fergana.reporter.pdf.InputOutputReporterPDF;
import com.optimal.fergana.reporter.pdf.KidsNumberPDFReporterByDate;
import com.optimal.fergana.reporter.pdf.MenuPDFReporter;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.WarehouseRepo;
import com.optimal.fergana.warehouse.inOut.InOutPrice;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;

@RequiredArgsConstructor
@Service
public class ReportService implements ReportServiceInter {


    private final ReportRepo reportRepo;
    private final CustomConverter converter;
    private final InputOutputService inputOutputService;
    private final AgeStandardRepo ageStandardRepo;
    private final KidsNumberSubRepo kidsNumberSubRepo;
    private final MenuExcelReporter menuExcelReporter;
    private final MenuPDFReporter menuPDFReporter;
    private final InputOutputReporterPDF inputOutputReporterPDF;
    private final KindergartenRepo kindergartenRepo;
    private final KidsNumberRepo kidsNumberRepo;
    private final KidsNumberPDFReporterByDate kidsNumberPDFReporterByDate;
    private final InputOutputRepo inputOutputRepo;
    private final InOutRepo inOutRepo;
    private final ProductRepo productRepo;
    private final WarehouseRepo warehouseRepo;


    public ReportResDTO getReportById(UUID id) {

        Optional<Report> optionalReport = reportRepo.findById(id);

        ReportResDTO dto = new ReportResDTO();

        if (optionalReport.isPresent()) {
            Report report = optionalReport.get();
            dto = converter.reportToDto(report);
        }
        return dto;
    }

    synchronized
    public void calculate(Report report) {

        if (report.getMenu() != null) {

            Menu menu = report.getMenu();
            Kindergarten kindergarten = report.getKindergarten();
            LocalDate date = report.getDate();

            Vector<AgeStandard> vector = ageStandardRepo.findAllByMealAgeStandard_MealTimeStandard_Menu_Id(menu.getId());
            Vector<ProductMenuDTO> productList = new Vector<>();

            for (AgeStandard ageStandard : vector) {
                Optional<KidsNumberSub> numberSubOptional = kidsNumberSubRepo.findByAgeGroup_IdAndKidsNumber_Report_IdAndKidsNumberDeleteIsFalse(ageStandard.getAgeGroup().getId(), report.getId());
                if (numberSubOptional.isPresent()) {
                    KidsNumberSub kidsNumberSub = numberSubOptional.get();

                    Integer number = kidsNumberSub.getNumber();
                    Meal meal = ageStandard.getMealAgeStandard().getMeal();
                    BigDecimal mealWeight = meal.getWeight();
                    for (ProductMeal productMeal : meal.getProductMealList()) {
                        BigDecimal productMealWeight = productMeal.getWeight();
                        BigDecimal divide = productMealWeight.divide(mealWeight, 6, RoundingMode.HALF_UP);
                        BigDecimal weight = divide.multiply(ageStandard.getWeight()).multiply(BigDecimal.valueOf(number));
                        weight = weight.divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
                        addProductList(productList, productMeal.getProduct(), weight);
                    }
                }
            }

            for (ProductMenuDTO productMenuDTO : productList) {
                Optional<Product> optionalProduct = productRepo.findById(productMenuDTO.getId());
                if (optionalProduct.isPresent()) {
                    Product product = optionalProduct.get();
                    InOut inOut = inputOutputService.addOutput(product, kindergarten, date, productMenuDTO.getWeight());
                    BigDecimal percentage = inOut.getOutputWeight().divide(productMenuDTO.getWeight(), 6, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
                    inOut.setPercentageOutput(percentage);
                    inOutRepo.save(inOut);
                } else {
                    System.out.println(productMenuDTO.getId());
                }
            }

            List<Warehouse> list = kindergarten.getWarehouseList();

            Vector<InOutPrice> inOutPriceVector = new Vector<>();

            for (Warehouse warehouse : list) {
                BigDecimal weight = BigDecimal.ZERO;
                BigDecimal weightPack = BigDecimal.ZERO;

                for (InOutPrice inOutPrice : warehouse.getInOutPriceList()) {
                    if (inOutPrice.getWeight().compareTo(BigDecimal.valueOf(0.001)) < 0) {

                    } else {
                        weight = weight.add(inOutPrice.getWeight());
                        weightPack = weightPack.add(inOutPrice.getPackWeight());
                    }
                }
                warehouse.setTotalWeight(weight);
                warehouse.setTotalPackWeight(weightPack);
            }
            warehouseRepo.saveAll(list);

            if (report.getMenuReport() != null) {
                String path = menuPDFReporter.createPDFReport1(report);
            } else {
                String path = menuPDFReporter.createPDFReport(report, true);
            }

        }
    }

    public void addProductList(Vector<ProductMenuDTO> list, Product product, BigDecimal weight) {

        boolean res = true;
        for (ProductMenuDTO productMenuDTO : list) {
            if (productMenuDTO.getId().equals(product.getId())) {
                productMenuDTO.setWeight(productMenuDTO.getWeight().add(weight));
                res = false;
                break;
            }
        }

        if (res) {
            list.add(new ProductMenuDTO(product.getId(), weight));
        }
    }

    public void setPercentage(Report report, List<ProductMenuDTO> productList) {

        Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByKindergarten_IdAndLocalDate(report.getKindergarten().getId(), report.getDate());

        if (inputOutputOptional.isPresent()) {
            InputOutput inputOutput = inputOutputOptional.get();

            Set<InOut> inOuts = inputOutput.getInOuts();

            for (ProductMenuDTO product : productList) {
                for (InOut inOut : inOuts) {
                    if (product.getId().equals(inOut.getProduct().getId())) {
                        BigDecimal percentage = inOut.getOutputWeight().divide(product.getWeight(), 6, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));

                        inOut.setPercentageOutput(percentage);
                    }
                }
            }
            inOutRepo.saveAll(inOuts);
        }
    }

    public String getMenuReport(UUID reportId, String type, HttpServletResponse response) {

        type = "pdf";

        Optional<Report> optionalReport = reportRepo.findById(reportId);
        if (optionalReport.isPresent()) {
            Report report = optionalReport.get();
            if (report.getMenu() != null && report.getKidsNumber() != null) {
//                if (report.getKidsNumber().isVerified()) {

                String path = "";

                if (type.equals("excel")) {
                    if (report.getFilePathExcel() != null) {
                        path = report.getFilePathExcel();
                    } else {
                        path = menuExcelReporter.createExcelReport(report);
                    }
                } else if (type.equals("pdf")) {
                    if (report.getFilePathPdf() != null) {
                        path = report.getFilePathPdf();
                    } else {
                        if (report.getMenuReport() != null) {
                            path = menuPDFReporter.createPDFReport1(report);
                        } else {
                            path = menuPDFReporter.createPDFReport(report, report.getKidsNumber().isVerified());
                        }
                    }
                }

                if (path != null) {
                    return path;
                }
//                } else {
//                    throw new NotificationNotFoundException("Bu kunga kiritilgan bola soni mtt tomonidan tasdiqlanmagan.");
//                }
            } else {
                throw new NotificationNotFoundException("Bu kunga bola soni mtt tomonidan kiritilmagan.");
            }
        }
        return null;
    }

    public ReportResDTO getReportByDate(Users users, LocalDate date) {
        ReportResDTO dto = new ReportResDTO();
        if (users.getKindergarten() != null) {

            Optional<Report> optionalReport = reportRepo.findByKindergarten_IdAndYearAndMonthAndDay(users.getKindergarten().getId(), date.getYear(), date.getMonthValue(), date.getDayOfMonth());
            if (optionalReport.isPresent()) {
                Report report = optionalReport.get();
                dto = converter.reportToDto(report);
            }
        }

        return dto;
    }

    public String getReportInputOutPut(LocalDate start, LocalDate end, Integer kindergartenId, Users users) {
        if (users.getKindergarten() != null) {
            kindergartenId = users.getKindergarten().getId();
        }
        if (kindergartenId != null) {
            Optional<Kindergarten> optional = kindergartenRepo.findById(kindergartenId);
            if (optional.isPresent()) {
                try {

                    return inputOutputReporterPDF.createInputOutputPDF(start, end, optional.get());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public String getKidsNumberPDF(LocalDate start, LocalDate end, Integer kindergartenId, HttpServletResponse response, Users users) {

        if (users.getKindergarten() != null) {
            kindergartenId = users.getKindergarten().getId();
        }

        if (kindergartenId != null) {

            try {
                Optional<Kindergarten> optional = kindergartenRepo.findById(kindergartenId);
                if (optional.isPresent()) {

                    List<KidsNumber> kidsNumberList = kidsNumberRepo.findAllByReport_Kindergarten_IdAndDateBetweenAndDeleteIsFalse(kindergartenId, start, end);
                    String path = kidsNumberPDFReporterByDate.createPDFKindergarten(kidsNumberList, start, end, optional.get());

                    return path;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
