package com.optimal.fergana.multiMenu;

import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.mealTime.MealTime;
import com.optimal.fergana.mealTime.MealTimeRepo;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.MenuService;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.multiMenu.product.DateWeight;
import com.optimal.fergana.multiMenu.product.ProductWeight;
import com.optimal.fergana.multiMenu.product.SanPinCategoryWeight;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategory;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategoryRepo;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNorm;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.reporter.pdf.MultiMenuProductPDF;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNorm;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNormService;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import com.optimal.fergana.selectMenu.SelectMenu;
import com.optimal.fergana.selectMenu.SelectMenuRepo;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.stayTime.StayTime;
import com.optimal.fergana.stayTime.StayTimeRepo;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
@RequiredArgsConstructor
public class MultiMenuService implements MultiMenuInterface, MultiMenuServiceInter {

    private final MultiMenuRepo multiMenuRepo;
    private final MealTimeRepo mealTimeRepo;
    private final MenuService menuService;
    private final SanpinMenuNormService sanpinMenuNormService;
    private final AgeStandardRepo ageStandardRepo;
    private final SanpinCategoryRepo sanpinCategoryRepo;
    private final MultiMenuProductPDF multiMenuProductPDF;
    private final StayTimeRepo stayTimeRepo;
    private final SelectMenuRepo selectMenuRepo;


    public StateMessage add(Users users, MultiMenuDTO multiMenuDTO) {

        RegionalDepartment regionalDepartment = users.getRegionalDepartment();
        boolean res = multiMenuRepo.existsAllByNameAndRegionalDepartmentAndDeleteIsFalse(multiMenuDTO.getName(), regionalDepartment);
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        if (!res) {
            List<MealTime> mealTimeList = mealTimeRepo.findAllById(multiMenuDTO.getMealTimeIdList());
            if (mealTimeList.size() > 2) {

                MultiMenu multiMenu = new MultiMenu(multiMenuDTO.getName(), multiMenuDTO.getDaily(), regionalDepartment, mealTimeList.size());
                multiMenu.setDepartment(users.getDepartment());

                List<Menu> menuList = menuService.add(multiMenu, multiMenu.getDaily(), mealTimeList);
                multiMenu.setMenuList(menuList);

                if (mealTimeList.size() > 4) {
                    StayTime stayTime = stayTimeRepo.findById(2).get();
                    multiMenu.setStayTime(stayTime);
                } else {
                    StayTime stayTime = stayTimeRepo.findById(1).get();
                    multiMenu.setStayTime(stayTime);
                }

                multiMenu.setSanpinMenuNorm(sanpinMenuNormService.add(multiMenu, multiMenu.getDaily()));

                multiMenu.setCreatedUsers(users);

                multiMenuRepo.save(multiMenu);
                message = Message.SUCCESS_UZ;
            }

        }
        return new StateMessage().parse(message);
    }

    public StateMessage edit(UUID id, String name, Users users) {
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        Optional<MultiMenu> optionalMultiMenu = multiMenuRepo.findById(id);

        if (optionalMultiMenu.isPresent()) {

            MultiMenu multiMenu = optionalMultiMenu.get();

            if (multiMenu.getCreatedUsers().getId().equals(users.getId())) {

                boolean res = false;
                if (!name.equals(multiMenu.getName())) {
                    res = multiMenuRepo.existsAllByNameAndRegionalDepartmentAndDeleteIsFalse(name, multiMenu.getRegionalDepartment());
                }

                if (!res) {
                    multiMenu.setName(name);
                    multiMenuRepo.save(multiMenu);
                    message = Message.EDIT_UZ;
                }
            }
        }
        return new StateMessage().parse(message);
    }

    public ResponseEntity<?> getOne(UUID id, Users users) {

        Optional<MultiMenu> optionalMultiMenu = multiMenuRepo.findById(id);

        if (optionalMultiMenu.isPresent()) {
            MultiMenu multiMenu = optionalMultiMenu.get();
            MultiMenuResDTO dto = parser(multiMenu, users);
            dto.setMenuList(menuService.getAll(multiMenu));
            dto.setSanpinMenuNorm(sanpinMenuNormService.get(multiMenu));
            return ResponseEntity.status(200).body(dto);
        }
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        return ResponseEntity.status(message.getCode()).body(new StateMessage().parse(message));
    }

    public ResponseEntity<?> getAll(Users users) {

        List<MultiMenu> menuList = multiMenuRepo.findAllByRegionalDepartmentAndDeleteIsFalse(users.getRegionalDepartment());

        List<MultiMenuResDTO> dtoList = new ArrayList<>();

        for (MultiMenu multiMenu : menuList) {

            RegionalDepartment regionalDepartment = users.getRegionalDepartment();

            MultiMenuResDTO dto = parser(multiMenu, users);

            BigDecimal max = BigDecimal.valueOf(0);
            BigDecimal min = BigDecimal.valueOf(0);
            BigDecimal cred = BigDecimal.valueOf(0);

            int num = 0;
            for (SanpinMenuNorm sanpinMenuNorm : multiMenu.getSanpinMenuNorm()) {
                for (AgeGroupSanpinNorm ageGroupSanpinNorm : sanpinMenuNorm.getAgeGroupSanpinNormList()) {
                    BigDecimal doneWeight = ageGroupSanpinNorm.getDoneWeight();

                    BigDecimal divide = BigDecimal.valueOf(0);
                    if (doneWeight.compareTo(BigDecimal.valueOf(0)) != 0) {
                        divide = (doneWeight.divide(ageGroupSanpinNorm.getPlanWeight(), 4, RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(100));
                    }
                    if (max.compareTo(divide) < 0) {
                        max = divide;
                    }
                    if ((min.compareTo(divide) > 0) && (num != 0)) {
                        min = divide;
                    } else if (num == 0) {
                        min = divide;
                    }
                    cred = cred.add(divide);
                    num++;
                }
            }
            dto.setSanPinPercentage((cred.compareTo(BigDecimal.valueOf(0)) != 0 ? (cred.divide(BigDecimal.valueOf(num), 0, RoundingMode.HALF_UP)) : cred).setScale(0, RoundingMode.UP));
            dto.setSanPinPercentageMax(max.setScale(0, RoundingMode.HALF_UP));
            dto.setSanPinPercentageMin(min.setScale(0, RoundingMode.HALF_UP));
            dto.setStayTimeNumber(multiMenu.getStayTime().getMealTimeList().size());

            Optional<SelectMenu> optional = selectMenuRepo.findByRegionalDepartment_Id(regionalDepartment.getId());

            if (optional.isPresent()){
                SelectMenu selectMenu = optional.get();
                MultiMenu menuMenu = selectMenu.getMenu();
                if (menuMenu.getId().equals(dto.getId())){
                    dto.setSelect(true);
                }
            }

            dtoList.add(dto);
        }
        return ResponseEntity.status(200).body(dtoList);
    }

    public StateMessage delete(UUID id, Users users) {

        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        Optional<MultiMenu> optionalMultiMenu = multiMenuRepo.findById(id);

        if (optionalMultiMenu.isPresent()) {
            MultiMenu multiMenu = optionalMultiMenu.get();

            if (multiMenu.getCreatedUsers().getId().equals(users.getId())) {
                multiMenu.setDelete(true);
                multiMenuRepo.save(multiMenu);
//            multiMenuRepo.delete(multiMenu);
                message = Message.DELETE_UZ;
            }
        }
        return new StateMessage().parse(message);
    }

    public List<SanPinCategoryWeight> getProduct(UUID id) {

        List<SanPinCategoryWeight> list = new ArrayList<>();

        Optional<MultiMenu> optionalMultiMenu = multiMenuRepo.findById(id);
        if (optionalMultiMenu.isPresent()) {

            MultiMenu multiMenu = optionalMultiMenu.get();

            list = getSCWeight(multiMenu);

            for (AgeStandard ageStandard : ageStandardRepo.findAllByMealAgeStandard_MealTimeStandard_Menu_MultiMenu_Id(id)) {

                if (ageStandard.getAgeGroup().getSanPin()) {
                    Meal meal = ageStandard.getMealAgeStandard().getMeal();

                    Integer day = ageStandard.getMealAgeStandard().getMealTimeStandard().getMenu().getNumber();
                    for (ProductMeal productMeal : meal.getProductMealList()) {
                        BigDecimal divide = productMeal.getWeight().divide(meal.getWeight(), 6, RoundingMode.HALF_UP);
                        BigDecimal weight = divide.multiply(ageStandard.getWeight());
                        addList(list, productMeal.getProduct(), weight, day, multiMenu);
                    }
                }
            }
        }
        return list;
    }

    public void addList(List<SanPinCategoryWeight> list, Product product, BigDecimal weight, int day, MultiMenu multiMenu) {

        for (SanPinCategoryWeight sanPinCategory : list) {
            if (sanPinCategory.getSanPinCategoryId().equals(product.getSanpinCategory().getId())) {
                boolean res = false;
                List<ProductWeight> productWeightList = sanPinCategory.getProductWeightList();
                for (ProductWeight productWeight : productWeightList) {
                    if (product.getId().equals(productWeight.getProductId())) {
                        for (DateWeight dateWeight : productWeight.getDateWeightList()) {
                            if (dateWeight.getDay() == day) {
                                dateWeight.setWeight(dateWeight.getWeight().add(weight));
                                sanPinCategory.setDoneWeight(sanPinCategory.getDoneWeight().add(weight));
                            }
                        }
                        res = true;
                    }
                }
                if (!res) {
                    ProductWeight productWeight = new ProductWeight(product.getId(), product.getName(), getDateList(multiMenu));
                    for (DateWeight dateWeight : productWeight.getDateWeightList()) {
                        if (dateWeight.getDay() == day) {
                            dateWeight.setWeight(dateWeight.getWeight().add(weight));
                            sanPinCategory.setDoneWeight(sanPinCategory.getDoneWeight().add(weight));
                        }
                    }
                    productWeightList.add(productWeight);
                }
            }
        }
    }

    public List<DateWeight> getDateList(MultiMenu multiMenu) {
        List<DateWeight> list = new ArrayList<>();

        for (int i = 1; i <= multiMenu.getDaily(); i++) {
            list.add(new DateWeight(i, BigDecimal.ZERO));
        }
        list.sort(Comparator.comparing(DateWeight::getDay));
        return list;
    }

    public List<SanPinCategoryWeight> getSCWeight(MultiMenu multiMenu) {
        List<SanPinCategoryWeight> list = new ArrayList<>();


        for (SanpinCategory sanpinCategory : sanpinCategoryRepo.findAll()) {
            BigDecimal weight = BigDecimal.valueOf(0);
            for (DailyNorm dailyNorm : sanpinCategory.getDailyNormList()) {

                if (dailyNorm.getStayTime().getId().equals(multiMenu.getStayTime().getId()))
                    weight = weight.add(dailyNorm.getWeight().multiply(BigDecimal.valueOf(multiMenu.getDaily())));
            }
            list.add(new SanPinCategoryWeight(sanpinCategory.getId(), sanpinCategory.getName(), new ArrayList<>(), getDateList(multiMenu), weight, BigDecimal.valueOf(0)));
        }
        return list;
    }

    public String getProductFile(UUID id) {
        List<SanPinCategoryWeight> product = getProduct(id);

        Optional<MultiMenu> optionalMultiMenu = multiMenuRepo.findById(id);
        if (optionalMultiMenu.isPresent()) {
            MultiMenu multiMenu = optionalMultiMenu.get();
            try {
                String fileProduct = multiMenuProductPDF.getFileProduct(product, multiMenu);
                return StaticWords.URI + fileProduct.substring(StaticWords.DEST.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
