package com.optimal.fergana.multiMenu;


import com.optimal.fergana.menu.MenuResDTO;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNormResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MultiMenuResDTO {

    private UUID id;
    private String name;
    private Integer daily;
    private Timestamp createDate;
    private Timestamp updateDate;
    private List<MenuResDTO> menuList;
    private List<SanpinMenuNormResDTO> sanpinMenuNorm;
    private String regionalDepartmentName;
    private Integer regionalDepartmentId;
    private String createdBy;
    private BigDecimal sanPinPercentage;
    private BigDecimal sanPinPercentageMin;
    private BigDecimal sanPinPercentageMax;
    private boolean edit;
    private String numberOfFeedingsName;
    private Integer numberOfFeedings;
    private Integer stayTimeNumber;
    private String stayTimeName;
    private boolean select = false;

}