package com.optimal.fergana.multiMenu.product;

import com.optimal.fergana.multiMenu.product.ProductWeight;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SanPinCategoryWeight {

    private Integer sanPinCategoryId;
    private String sanPinCategoryName;
    private List<ProductWeight> productWeightList;
    private List<DateWeight> totalDateWeight;
    private BigDecimal planWeight = BigDecimal.ZERO;
    private BigDecimal doneWeight = BigDecimal.ZERO;


    public SanPinCategoryWeight(Integer sanPinCategoryId, String sanPinCategoryName, List<DateWeight> totalDateWeight, BigDecimal planWeight, BigDecimal doneWeight) {
        this.sanPinCategoryId = sanPinCategoryId;
        this.sanPinCategoryName = sanPinCategoryName;
        this.totalDateWeight = totalDateWeight;
        this.planWeight = planWeight;
        this.doneWeight = doneWeight;
    }
}
