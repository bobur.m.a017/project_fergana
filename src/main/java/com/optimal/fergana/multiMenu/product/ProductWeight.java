package com.optimal.fergana.multiMenu.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductWeight {

    private Integer productId;
    private String productName;
    private List<DateWeight> dateWeightList;
}
