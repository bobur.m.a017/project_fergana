package com.optimal.fergana.multiMenu.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DateWeight {

    private int day;
    private BigDecimal weight =BigDecimal.ZERO;
}
