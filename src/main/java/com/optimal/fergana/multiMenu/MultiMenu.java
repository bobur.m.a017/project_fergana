package com.optimal.fergana.multiMenu;


import com.optimal.fergana.department.Department;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNorm;
import com.optimal.fergana.stayTime.StayTime;
import com.optimal.fergana.users.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MultiMenu {

    @Id
    private UUID id = UUID.randomUUID();
    private String name;
    private Integer daily;
    private Boolean delete = false;
    private Integer numberOfFeedings;


    @OneToOne
    private Users createdUsers;


    @CreationTimestamp
    private Timestamp createDate = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "multiMenu")
    private List<Menu> menuList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "multiMenu")
    private List<SanpinMenuNorm> sanpinMenuNorm;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private RegionalDepartment regionalDepartment;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private Department department;


    @ManyToOne
    private StayTime stayTime;


    public MultiMenu(String name, Integer daily, RegionalDepartment regionalDepartment, Integer numberOfFeedings) {
        this.name = name;
        this.daily = daily;
        this.regionalDepartment = regionalDepartment;
        this.numberOfFeedings = numberOfFeedings;

    }
}
