package com.optimal.fergana.multiMenu;

import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface MultiMenuRepo extends JpaRepository<MultiMenu, UUID> {

    boolean existsAllByNameAndRegionalDepartmentAndDeleteIsFalse(String name, RegionalDepartment regionalDepartment);

    List<MultiMenu> findAllByRegionalDepartmentAndDeleteIsFalse(RegionalDepartment regionalDepartment);
}
