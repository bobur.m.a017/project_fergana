package com.optimal.fergana.multiMenu;

import com.optimal.fergana.users.Users;

import java.math.BigDecimal;

public interface MultiMenuInterface {

    default MultiMenuResDTO parser(MultiMenu multiMenu, Users users) {
        return new MultiMenuResDTO(
                multiMenu.getId(),
                multiMenu.getName(),
                multiMenu.getDaily(),
                multiMenu.getCreateDate(),
                multiMenu.getUpdateDate(),
                null, null,
                multiMenu.getRegionalDepartment().getName(),
                multiMenu.getRegionalDepartment().getId(),
                multiMenu.getDepartment() != null ? multiMenu.getDepartment().getName() : multiMenu.getRegionalDepartment().getName(),
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0),
                BigDecimal.valueOf(0),
                multiMenu.getCreatedUsers().getId().equals(users.getId()),
                multiMenu.getNumberOfFeedings() + " mahal ovqatlanish",
                multiMenu.getNumberOfFeedings(),
                multiMenu.getStayTime().getMealTimeList().size(),
                multiMenu.getStayTime().getName(),
                false
        );
    }
}
