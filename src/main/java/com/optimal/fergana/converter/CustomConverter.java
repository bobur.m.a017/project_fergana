package com.optimal.fergana.converter;


import com.optimal.fergana.department.Department;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.dto.AverageKidsNumberResDTO;
import com.optimal.fergana.kids.dto.KidsNumberResDTO;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.SubResDTO;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenResDTO;
import com.optimal.fergana.kindergarten.dto.DepartmentDTOMenu;
import com.optimal.fergana.kindergarten.dto.KindergartenByAddressDTO;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.MenuResDTO;
import com.optimal.fergana.notification.Notification;
import com.optimal.fergana.notification.NotificationRepo;
import com.optimal.fergana.notification.dto.NotificationResAllDTO;
import com.optimal.fergana.order.MyOrder;
import com.optimal.fergana.order.OrderResDTO;
import com.optimal.fergana.order.kindergarten.KindergartenOrder;
import com.optimal.fergana.order.kindergarten.KindergartenOrderResDTO;
import com.optimal.fergana.order.product.ProductOrder;
import com.optimal.fergana.order.product.ProductOrderResDTO;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductResDTO;
import com.optimal.fergana.product.acceptedProducts.AcceptedProduct;
import com.optimal.fergana.product.acceptedProducts.AcceptedProductResDTO;
import com.optimal.fergana.product.productPack.ProductPack;
import com.optimal.fergana.product.productPack.ProductPackRepo;
import com.optimal.fergana.product.product_price.ProductPrice;
import com.optimal.fergana.product.product_price.ProductPriceResDTO;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportResDTO;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNorm;
import com.optimal.fergana.sanpinMenuNorm.SanpinMenuNormResDTO;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNormResDTO;
import com.optimal.fergana.stayTime.StayTime;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.supplier.SupplierResDTO;
import com.optimal.fergana.supplier.contract.Contract;
import com.optimal.fergana.supplier.contract.ContractResDTO;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContract;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContractResDTO;
import com.optimal.fergana.supplier.contract.product.ProductContract;
import com.optimal.fergana.supplier.contract.product.ProductContractResDTO;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersResDTO;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.WarehouseResDTO;
import com.optimal.fergana.warehouse.inOut.InOutPrice;
import com.optimal.fergana.warehouse.inOut.InOutPriceResDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.util.*;

@RequiredArgsConstructor
@Service
public class CustomConverter {

    private final NotificationRepo notificationRepo;
    private final ProductPackRepo productPackRepo;


    public KindergartenResDTO kindergartenToDTO(Kindergarten kindergarten) {
        return new KindergartenResDTO(
                kindergarten.getId(),
                kindergarten.getName(),
                kindergarten.getNumber(),
                kindergarten.getStatus(),
                kindergarten.getDepartment().getName(),
                kindergarten.getDepartment().getId(),
                kindergarten.getRegionalDepartment().getName(),
                kindergarten.getRegionalDepartment().getId(),
                kindergarten.getStreet(),
                getStatTime(kindergarten)
        );
    }

    public int getStatTime(Kindergarten kindergarten) {

        int res = 0;
        int num = 0;
        for (StayTime stayTime : kindergarten.getStayTimeList()) {

            int size = stayTime.getMealTimeList().size();

            if (num == 0) {
                res = size;
            } else {
                if (res < size) {
                    res = size;
                }
            }
            num++;
        }

        return res;
    }

    public List<KindergartenResDTO> kindergartenToDTO(List<Kindergarten> kindergartenList) {

        List<KindergartenResDTO> dtoList = new ArrayList<>();

        for (Kindergarten kindergarten : kindergartenList) {
            dtoList.add(kindergartenToDTO(kindergarten));
        }
        return dtoList;
    }

    public List<KindergartenResDTO> kindergartenToDTOAverageKidsNumber(List<Kindergarten> kindergartenList) {

        List<KindergartenResDTO> dtoList = new ArrayList<>();

        for (Kindergarten kindergarten : kindergartenList) {
            KindergartenResDTO dto = kindergartenToDTO(kindergarten);
            dto.setAverageKidsNumber(kidsNumberToDTO(kindergarten.getAverageKidsNumber(), kindergarten));
            dtoList.add(dto);
        }
        return dtoList;
    }


    public List<KindergartenByAddressDTO> kindergartenToByAddressDTO(List<Kindergarten> kindergartenList) {
        List<KindergartenByAddressDTO> dtoList = new ArrayList<>();

        for (Kindergarten kindergarten : kindergartenList) {
            dtoList.add(new KindergartenByAddressDTO(kindergarten.getId(), kindergarten.getNumber(), kindergarten.getName(), false, kindergarten.getDepartment().getId()));
        }
        return dtoList;
    }

    public KindergartenByAddressDTO kindergartenToByAddressDTO(Kindergarten kindergarten) {
        return (new KindergartenByAddressDTO(kindergarten.getId(), kindergarten.getNumber(), kindergarten.getName(), false, kindergarten.getDepartment().getId()));
    }


    public MenuResDTO menuToDTO(Menu menu) {
        return new MenuResDTO(
                menu.getId(),
                menu.getName(),
                menu.getCreateDate(),
                menu.getUpdateDate(),
                menu.getMultiMenu().getName(),
                menu.getMultiMenu().getId()
        );
    }


    public UsersResDTO userToDTO(Users users) {

        return new UsersResDTO(
                users.getId(),
                users.getName(),
                users.getFatherName(),
                users.getSurname(),
                users.isState(),
                users.isDelete(),
                users.getUsername(),
                users.getPhoneNumber(),
                users.getRoles().stream().findFirst().get().getName(),
                users.getRoles().stream().findFirst().get().getPositionName(),
                users.getDepartment() != null ? users.getDepartment().getName() : "",
                users.getDepartment() != null ? users.getDepartment().getId() : null,
                users.getRegionalDepartment() != null ? users.getRegionalDepartment().getName() : "",
                users.getRegionalDepartment() != null ? users.getRegionalDepartment().getId() : null,
                users.getKindergarten() != null ? users.getKindergarten().getName() : "",
                users.getKindergarten() != null ? users.getKindergarten().getId() : null,
                notificationRepo.countAllByReadIsFalseAndUsers_Id(users.getId()),
                users.getPermissionState()
        );
    }

    public List<UsersResDTO> userToDTO(List<Users> userList) {
        List<UsersResDTO> list = new ArrayList<>();

        for (Users users : userList) {
            list.add(userToDTO(users));
        }
        return list;
    }


    public List<SupplierResDTO> supplierToDTO(List<Supplier> supplierList) {
        List<SupplierResDTO> supplierResDTOList = new ArrayList<>();
        for (Supplier supplier : supplierList) {
            SupplierResDTO supplierResDTO = new SupplierResDTO(
                    supplier.getId(),
                    supplier.getName(),
                    supplier.getSTIR(),
                    supplier.getDate(),
                    supplier.getStatus(),
                    supplier.getTHSHT(),
                    supplier.getDBIBT(),
                    supplier.getIFUT(),
                    supplier.getFond(),
                    supplier.getAddress(),
                    supplier.getPhoneNumber(),
                    supplier.getDirector()
            );
            supplierResDTOList.add(supplierResDTO);
        }
        return supplierResDTOList;
    }


    public List<ProductPriceResDTO> productPriceToDTO(List<ProductPrice> productPriceList) {
        List<ProductPriceResDTO> priceResDTOList = new ArrayList<>();
        for (ProductPrice productPrice : productPriceList) {
            ProductPriceResDTO productPriceResDTO = new ProductPriceResDTO(
                    productPrice.getId(),
                    productPrice.getProduct().getId(),
                    productToDTO(productPrice.getProduct()),
                    productPrice.getMinPrice(),
                    productPrice.getMaxPrice(),
                    Timestamp.from(productPrice.getStartDay().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                    Timestamp.from(productPrice.getEndDay().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                    productPrice.isEdit()
            );
            priceResDTOList.add(productPriceResDTO);
        }
        return priceResDTOList;
    }


    public ProductPriceResDTO productPriceToDTO(ProductPrice productPrice) {

        ProductPriceResDTO productPriceResDTO = new ProductPriceResDTO(
                productPrice.getId(),
                productPrice.getProduct().getId(),
                productToDTO(productPrice.getProduct()),
                productPrice.getMinPrice(),
                productPrice.getMaxPrice(),
                Timestamp.from(productPrice.getStartDay().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                Timestamp.from(productPrice.getEndDay().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                productPrice.isEdit()
        );


        return productPriceResDTO;
    }


    public DepartmentDTOMenu departmentToDTO(Department department) {
        return new DepartmentDTOMenu(
                department.getId(),
                department.getName()
        );
    }

    public List<DepartmentDTOMenu> departmentToDTO(List<Department> departmentList) {

        List<DepartmentDTOMenu> dtoList = new ArrayList<>();
        for (Department department : departmentList) {
            dtoList.add(departmentToDTO(department));
        }
        return dtoList;
    }

    public ReportResDTO reportToDto(Report report) {
        return new ReportResDTO(
                report.getId(),
                report.getDay(),
                report.getMonth(),
                report.getYear(),
                report.isEdit(),
                report.getCreateDate(),
                report.getUpdateDate(),
                report.getKindergarten().getId(),
                report.getKindergarten().getName(),
                menuToDTO(report.getMenu()),
                kidsNumberToDTO(report.getKidsNumber())
        );
    }


    public KidsNumberResDTO kidsNumberToDTO(KidsNumber kidsNumber) {
        if (kidsNumber == null)
            return null;

        List<SubResDTO> subResDTOS = new ArrayList<>();
        kidsNumber.getKidsNumberSubList().forEach(a -> subResDTOS.add(new SubResDTO(a.getId(), a.getAgeGroup().getId(), a.getAgeGroup().getName(), a.getNumber(), a.getAgeGroup().getSortNumber())));


        subResDTOS.sort(Comparator.comparing(SubResDTO::getSortNumber));


        return new KidsNumberResDTO(kidsNumber.getId(), kidsNumber.getDate(), subResDTOS, kidsNumber.isVerified(), kidsNumber.getStatus(), kidsNumber.getReport().getKindergarten().getNumber() + kidsNumber.getReport().getKindergarten().getName());
    }

    public KidsNumberResDTO kidsNumberToDTO(KidsNumber kidsNumber, Kindergarten kindergarten) {
        if (kidsNumber == null)
            return null;

        List<SubResDTO> subResDTOS = new ArrayList<>();
        kidsNumber.getKidsNumberSubList().forEach(a -> subResDTOS.add(new SubResDTO(a.getId(), a.getAgeGroup().getId(), a.getAgeGroup().getName(), a.getNumber(), a.getAgeGroup().getSortNumber())));

        subResDTOS.sort(Comparator.comparing(SubResDTO::getSortNumber));

        return new KidsNumberResDTO(kidsNumber.getId(), kidsNumber.getDate(), subResDTOS, kidsNumber.isVerified(), kidsNumber.getStatus(), kindergarten.getNumber() + kindergarten.getName());
    }

    public List<KidsNumberResDTO> kidsNumberToDTO(List<KidsNumber> kidsNumberList) {

        List<KidsNumberResDTO> dtoList = new ArrayList<>();
        for (KidsNumber kidsNumber : kidsNumberList) {
            dtoList.add(kidsNumberToDTO(kidsNumber));
        }
        return dtoList;
    }


    public ContractResDTO contractToDTO(Contract contract) {
        return new ContractResDTO(
                contract.getId(),
                contract.getNumber(),
                contract.getLotNumber(),
                contract.getStartDay(),
                contract.getEndDay(),
                contract.getSupplier().getId(),
                contract.getSupplier().getName(),
                contract.getDepartment().getId(),
                contract.getDepartment().getName(),
                null,
                contract.isFullyCompleted(),
                contract.getTotalSum(),
                contract.getStatus(),
                contract.isDelete(),
                contract.getCreateDate(),
                contract.getUpdateDate(),
                contract.getCreatedBy().getName() + " " + contract.getCreatedBy().getFatherName() + " " + contract.getCreatedBy().getSurname(),
                contract.getUpdatedBy().getName() + " " + contract.getUpdatedBy().getFatherName() + " " + contract.getUpdatedBy().getSurname()
        );
    }

    public List<ContractResDTO> contractToDTO(List<Contract> contractList) {
        List<ContractResDTO> dtoList = new ArrayList<>();

        for (Contract contract : contractList) {
            dtoList.add(contractToDTO(contract));
        }
        return dtoList;
    }


    public OrderResDTO orderToDTO(MyOrder myOrder) {
        return new OrderResDTO(
                myOrder.getId(),
                myOrder.getName(),
                myOrder.getDepartment().getId(),
                myOrder.getDepartment().getName(),
                null,
                myOrder.isDelete(),
                myOrder.getCreateDate(),
                myOrder.getUpdateDate(),
                myOrder.getCreatedBy().getName() + " " + myOrder.getCreatedBy().getFatherName() + " " + myOrder.getCreatedBy().getSurname(),
                myOrder.getUpdatedBy() != null ? (myOrder.getUpdatedBy().getName() + " " + myOrder.getUpdatedBy().getFatherName() + " " + myOrder.getUpdatedBy().getSurname()) : ""
        );
    }

    public List<OrderResDTO> orderToDTO(List<MyOrder> contractList) {
        List<OrderResDTO> dtoList = new ArrayList<>();

        for (MyOrder myOrder : contractList) {
            dtoList.add(orderToDTO(myOrder));
        }
        return dtoList;
    }


    public KindergartenContractResDTO kindergartenContractToDTO(KindergartenContract kindergartenContract) {
        return new KindergartenContractResDTO(
                kindergartenContract.getId(),
                kindergartenContract.getKindergarten().getId(),
                kindergartenContract.getKindergarten().getNumber(),
                kindergartenContract.getKindergarten().getName(),
                productContractToDTO(kindergartenContract.getProductContracts()),
                kindergartenContract.getTotalSum()
        );
    }


    public List<KindergartenContractResDTO> kindergartenContractToDTO(List<KindergartenContract> kindergartenContractList) {
        List<KindergartenContractResDTO> dtoList = new ArrayList<>();

        for (KindergartenContract kindergartenContract : kindergartenContractList) {
            dtoList.add(kindergartenContractToDTO(kindergartenContract));
        }
        return dtoList;
    }


    public KindergartenOrderResDTO kindergartenOrderToDTO(KindergartenOrder kindergartenOrder) {
        return new KindergartenOrderResDTO(
                kindergartenOrder.getId(),
                kindergartenOrder.getKindergarten().getId(),
                kindergartenOrder.getKindergarten().getNumber(),
                kindergartenOrder.getKindergarten().getName(),
                productOrderToDTO(kindergartenOrder.getProductOrders())
        );
    }


    public List<KindergartenOrderResDTO> kindergartenOrderToDTO(List<KindergartenOrder> kindergartenOrderList) {
        List<KindergartenOrderResDTO> dtoList = new ArrayList<>();

        for (KindergartenOrder kindergartenOrder : kindergartenOrderList) {
            dtoList.add(kindergartenOrderToDTO(kindergartenOrder));
        }
        return dtoList;
    }


    public ProductContractResDTO productContractToDTO(ProductContract productContract) {
        return new ProductContractResDTO(
                productContract.getId(),
                productContract.getWeight(),
                productContract.getPack(),
                productContract.getPackWeight(),
                productContract.getSuccessWeight(),
                productContract.getSuccessPackWeight(),
                productContract.getWeight().subtract(productContract.getSuccessWeight()),
                productContract.getPackWeight().subtract(productContract.getSuccessPackWeight()),
                productContract.getPrice(),
                productContract.getProduct().getId(),
                productContract.getProduct().getName(),
                productContract.getTotalSum(),
                productContract.getSuccessWeight().multiply(productContract.getPrice()),
                (productContract.getWeight().subtract(productContract.getSuccessWeight())).multiply(productContract.getPrice()),
                productContract.getKindergartenContract().getContract().getSupplier().getName(),
                productContract.getKindergartenContract().getContract().getNumber()
        );
    }

    public List<ProductContractResDTO> productContractToDTO(Set<ProductContract> productContractList) {
        List<ProductContractResDTO> dtoList = new ArrayList<>();

        for (ProductContract productContract : productContractList) {
            dtoList.add(productContractToDTO(productContract));
        }

        dtoList.sort(Comparator.comparing(ProductContractResDTO::getProductName));

        return dtoList;
    }

    public ProductOrderResDTO productOrderToDTO(ProductOrder productOrder) {
        return new ProductOrderResDTO(
                productOrder.getId(),
                productOrder.getWeight(),
                productOrder.getPack(),
                productOrder.getPackWeight(),
                productOrder.getProduct().getId(),
                productOrder.getProduct().getName()
        );
    }

    public List<ProductOrderResDTO> productOrderToDTO(Set<ProductOrder> productOrderSet) {
        List<ProductOrderResDTO> dtoList = new ArrayList<>();

        for (ProductOrder productOrder : productOrderSet) {
            dtoList.add(productOrderToDTO(productOrder));
        }

        dtoList.sort(Comparator.comparing(ProductOrderResDTO::getProductName));

        return dtoList;
    }


    public ProductResDTO productToDTO(Product product) {
        return new ProductResDTO(product.getId(), product.getName(), product.getMeasurementType(), product.getProtein(), product.getKcal(), product.getOil(), product.getCarbohydrates(), product.getProductCategory().getName(), product.getProductCategory().getId(), product.getSanpinCategory().getName(), product.getSanpinCategory().getId(), product.getRegionalDepartment().getName(), product.getRegionalDepartment().getId());
    }

    public List<ProductResDTO> productToDTO(List<Product> productList) {

        List<ProductResDTO> dtoList = new ArrayList<>();

        for (Product product : productList) {

            dtoList.add(productToDTO(product));
        }
        return dtoList;
    }


    public WarehouseResDTO warehouseToDTO(Warehouse warehouse) {

        Optional<ProductPack> optionalProductPack = productPackRepo.findByProduct_IdAndDepartment_Id(warehouse.getProduct().getId(), warehouse.getKindergarten().getDepartment().getId());

        BigDecimal pack = BigDecimal.ONE;
        BigDecimal weight = BigDecimal.ZERO;
        BigDecimal packWeight = BigDecimal.ZERO;

        if (optionalProductPack.isPresent()) {
            ProductPack productPack = optionalProductPack.get();
            pack = productPack.getPack();
        }

        weight = warehouse.getTotalWeight();

        packWeight = weight.divide(pack, 0, RoundingMode.HALF_UP);

        return new WarehouseResDTO(warehouse.getId(), warehouse.getTotalWeight(), warehouse.getTotalPackWeight(), warehouse.getKindergarten().getName(), warehouse.getKindergarten().getId(), warehouse.isDelete(), warehouse.getProduct().getId(), warehouse.getProduct().getName(), inOutPriceToDTO(warehouse.getInOutPriceList()));
    }

    public List<InOutPriceResDTO> inOutPriceToDTO(List<InOutPrice> priceList) {
        List<InOutPriceResDTO> dtoList = new ArrayList<>();

        for (InOutPrice inOutPrice : priceList) {
            dtoList.add(new InOutPriceResDTO(inOutPrice.getId(), inOutPrice.getPrice(), inOutPrice.getWeight(), inOutPrice.getPack(), inOutPrice.getPackWeight(), inOutPrice.getCreateDate(), inOutPrice.getUpdateDate()));
        }
        return dtoList;
    }

    public List<WarehouseResDTO> warehouseToDTO(List<Warehouse> warehouseList) {

        List<WarehouseResDTO> dtoList = new ArrayList<>();

        for (Warehouse warehouse : warehouseList) {
            dtoList.add(warehouseToDTO(warehouse));
        }

        return dtoList;

    }

    public List<NotificationResAllDTO> notificationToDTO(List<Notification> notificationList) {
        List<NotificationResAllDTO> notificationResAllDTOS = new ArrayList<>();
        for (Notification notification : notificationList) {
            NotificationResAllDTO notificationResAllDTO = new NotificationResAllDTO(notification.getId(), notification.getHeader(), notification.isRead(), notification.getCreateDate(), notification.getSender());
            notificationResAllDTOS.add(notificationResAllDTO);
        }
        return notificationResAllDTOS;
    }

//    public List<KidsNumberResDTO> kidsNumberToResDTo(List<Report> reportList) {
//
//        List<KidsNumberResDTO> kidsNumberResDTOS = new ArrayList<>();
//
//        for (Report report : reportList) {
//            List<SubResDTO> subResDTOS = new ArrayList<>();
//            report.getKidsNumber().getKidsNumberSubList().forEach(a -> subResDTOS.add(new SubResDTO(a.getId(), a.getAgeGroup().getId(), a.getAgeGroup().getName(), a.getNumber())));
//
//            KidsNumberResDTO kidsNumberResDTO = new KidsNumberResDTO(
//
//                    report.getDate(),
//                    subResDTOS,
//                    report.getKidsNumber().isVerified(),
//                    report.getKidsNumber().getStatus());
//            kidsNumberResDTOS.add(kidsNumberResDTO);
//        }
//
//        return kidsNumberResDTOS;
//    }

    public List<AcceptedProductResDTO> acceptedProductToDTO(List<AcceptedProduct> toList) {
        List<AcceptedProductResDTO> list = new ArrayList<>();

        for (AcceptedProduct acceptedProduct : toList) {
            list.add(acceptedProductToDTO(acceptedProduct));
        }
        return list;
    }

    public AcceptedProductResDTO acceptedProductToDTO(AcceptedProduct acceptedProduct) {

        return new AcceptedProductResDTO(acceptedProduct.getId(),
                acceptedProduct.getWeight(),
                acceptedProduct.isDelete(),
                acceptedProduct.getCreateDate(),
                acceptedProduct.getUpdateDate(),
                acceptedProduct.getRecipient().getName() + " " + acceptedProduct.getRecipient().getSurname(),
                acceptedProduct.getDate(),
                acceptedProduct.getPackWeight(),
                acceptedProduct.getPack(),
                acceptedProduct.getPrice(),
                acceptedProduct.getProductContract() != null ? acceptedProduct.getProductContract().getKindergartenContract().getContract().getSupplier().getName() : "Ta`minotchi ko`rsatilmagan",
                acceptedProduct.getProductContract() != null ? acceptedProduct.getProductContract().getKindergartenContract().getContract().getNumber() : "-",
                acceptedProduct.getProduct().getName());
    }


    public List<AverageKidsNumberResDTO> averageKidsNumberToResDTo(List<Kindergarten> kindergartenList) {

        List<AverageKidsNumberResDTO> averageKidsNumberResDTOList = new ArrayList<>();

        for (Kindergarten kindergarten : kindergartenList) {

            if (kindergarten.getAverageKidsNumber() != null) {
                KidsNumber averageKidsNumber = kindergarten.getAverageKidsNumber();
                Set<KidsNumberSub> kidsNumberSubList = averageKidsNumber.getKidsNumberSubList();
                List<SubResDTO> subResDTOList = new ArrayList<>();
                for (KidsNumberSub kidsNumberSub : kidsNumberSubList) {
                    SubResDTO subResDTO = new SubResDTO(
                            kidsNumberSub.getId(),
                            kidsNumberSub.getAgeGroup().getId(),
                            kidsNumberSub.getAgeGroup().getName(),
                            kidsNumberSub.getNumber(),
                            kidsNumberSub.getAgeGroup().getSortNumber()
                    );

                    subResDTOList.add(subResDTO);
                }
                AverageKidsNumberResDTO averageKidsNumberResDTO = new AverageKidsNumberResDTO(kindergarten.getNumber() + kindergarten.getName(), subResDTOList);
                averageKidsNumberResDTOList.add(averageKidsNumberResDTO);
            }
        }
        return averageKidsNumberResDTOList;
    }

    public SanpinMenuNormResDTO sanpinMenuNormToDTO(SanpinMenuNorm sanpinMenuNorm) {
        return new SanpinMenuNormResDTO(
                sanpinMenuNorm.getId(),
                sanpinMenuNorm.getSanpinCategory().getName(),
                sanpinMenuNorm.getSanpinCategory().getId(),
                ageGroupSanpinNormToDTO(sanpinMenuNorm.getAgeGroupSanpinNormList()),
                sanpinMenuNorm.getDoneProtein(),
                sanpinMenuNorm.getPlanProtein(),
                sanpinMenuNorm.getDoneKcal(),
                sanpinMenuNorm.getPlanKcal(),
                sanpinMenuNorm.getDoneOil(),
                sanpinMenuNorm.getPlanOil(),
                sanpinMenuNorm.getDoneCarbohydrates(),
                sanpinMenuNorm.getPlanCarbohydrates(),
                sanpinMenuNorm.getMultiMenu().getName(),
                sanpinMenuNorm.getMultiMenu().getId()
        );
    }

    public List<SanpinMenuNormResDTO> sanpinMenuNormToDTO(List<SanpinMenuNorm> list) {

        List<SanpinMenuNormResDTO> dtoList = new ArrayList<>();

        for (SanpinMenuNorm sanpinMenuNorm : list) {
            dtoList.add(sanpinMenuNormToDTO(sanpinMenuNorm));
        }
        return dtoList;
    }

    public AgeGroupSanpinNormResDTO ageGroupSanpinNormToDTO(AgeGroupSanpinNorm ageGroupSanpinNorm) {
        return new AgeGroupSanpinNormResDTO(
                ageGroupSanpinNorm.getId(),
                ageGroupSanpinNorm.getDaily(),
                ageGroupSanpinNorm.getDoneWeight().setScale(0, RoundingMode.HALF_UP),
                ageGroupSanpinNorm.getPlanWeight().setScale(0, RoundingMode.HALF_UP),
                ageGroupSanpinNorm.getAgeGroup().getName()
        );
    }

    public List<AgeGroupSanpinNormResDTO> ageGroupSanpinNormToDTO(List<AgeGroupSanpinNorm> list) {

        List<AgeGroupSanpinNormResDTO> dtoList = new ArrayList<>();

        for (AgeGroupSanpinNorm ageGroupSanpinNorm : list) {
            dtoList.add(ageGroupSanpinNormToDTO(ageGroupSanpinNorm));
        }
        return dtoList;
    }
}
